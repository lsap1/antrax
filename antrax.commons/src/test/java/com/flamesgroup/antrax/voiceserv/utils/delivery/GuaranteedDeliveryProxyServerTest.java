/*
 * Copyright (C) 2017 Flames Group SIA
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.flamesgroup.antrax.voiceserv.utils.delivery;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import mockit.Expectations;
import mockit.Mocked;
import mockit.integration.junit4.JMockit;
import org.junit.Test;
import org.junit.runner.RunWith;

import java.io.IOException;
import java.lang.reflect.Method;
import java.util.HashSet;
import java.util.Set;

@RunWith(JMockit.class)
public class GuaranteedDeliveryProxyServerTest {

  @Mocked
  private TestInterface mock;

  private interface TestInterface {
    String methodWithRetval(int i) throws IOException;

    void methodWithArgs(String first, String second) throws IOException;

    void thirdMethod() throws IOException;

    void exceptionless() throws IOException;

    void syncExceptionless() throws Exception;
  }

  private GuaranteedDeliveryProxyServer mkDeliveryServer() {
    Set<Class<? extends Throwable>> exc = new HashSet<>();
    exc.add(IOException.class);
    return new GuaranteedDeliveryProxyServer("TestServ", exc);
  }

  private DeliveryConfig getDeliveryConfig() {
    return new DeliveryConfig() {

      @Override
      public Object defaultValue(final Method method) {
        return null;
      }

      @Override
      public boolean isAsync(final Method method) {
        if (method.getName().equals("exceptionless")) {
          return true;
        }
        return false;
      }

      @Override
      public boolean isExceptionless(final Method method) {
        if (method.getName().equals("exceptionless") || method.getName().equals("syncExceptionless")) {
          return true;
        }
        return false;
      }

      @Override
      public boolean isGuaranteed(final Method method) {
        return true;
      }

    };
  }

  @Test
  public void testSimpleExecutionDelivered() throws InterruptedException, IOException {
    GuaranteedDeliveryProxyServer server = mkDeliveryServer();
    server.start();

    new Expectations() {{
      mock.methodWithArgs("first", "second");
    }};

    TestInterface proxy = server.proxy(mock, TestInterface.class, getDeliveryConfig());

    proxy.methodWithArgs("first", "second");
  }

  @Test
  public void testReturnValues() throws IOException {
    GuaranteedDeliveryProxyServer server = mkDeliveryServer();
    server.start();

    new Expectations() {{
      mock.methodWithRetval(5);
      result = "Hello";
    }};

    TestInterface proxy = server.proxy(mock, TestInterface.class, getDeliveryConfig());

    String data = proxy.methodWithRetval(5);
    assertEquals("Hello", data);
  }

  @Test
  public void testSeriaOfCalls() throws IOException {
    GuaranteedDeliveryProxyServer server = mkDeliveryServer();
    server.start();

    new Expectations() {{
      mock.methodWithArgs("a", "b");
      mock.methodWithRetval(1);
      mock.thirdMethod();
    }};

    TestInterface proxy = server.proxy(mock, TestInterface.class, getDeliveryConfig());

    proxy.methodWithArgs("a", "b");
    proxy.methodWithRetval(1);
    proxy.thirdMethod();
  }

  @Test(expected = RuntimeException.class)
  public void testExceptionIsRethrown() throws IOException {
    GuaranteedDeliveryProxyServer server = mkDeliveryServer();
    server.start();

    new Expectations() {{
      mock.thirdMethod();
      result = new RuntimeException();
    }};

    TestInterface proxy = server.proxy(mock, TestInterface.class, getDeliveryConfig());

    proxy.thirdMethod();
  }

  @Test
  public void testExceptionless() throws IOException, InterruptedException {
    GuaranteedDeliveryProxyServer server = mkDeliveryServer();
    server.start();

    new Expectations() {{
      mock.exceptionless();
      result = new IOException();
    }};

    TestInterface proxy = server.proxy(mock, TestInterface.class, getDeliveryConfig());

    proxy.exceptionless();
    Thread.sleep(100);
  }

  @Test
  public void testExceptionlessWithRuntimeException() throws IOException, InterruptedException {
    GuaranteedDeliveryProxyServer server = mkDeliveryServer();
    server.start();

    new Expectations() {{
      mock.exceptionless();
      result = new RuntimeException();
    }};

    TestInterface proxy = server.proxy(mock, TestInterface.class, getDeliveryConfig());

    proxy.exceptionless();
    Thread.sleep(100);
  }

  @Test
  public void testDelayedExecutionIsNotFreezeSystemOnExceptionless() throws Exception {
    GuaranteedDeliveryProxyServer serv = mkDeliveryServer();
    serv.start();
    SleepyTestInterface sleepy = new SleepyTestInterface();
    TestInterface proxy = serv.proxy(sleepy, TestInterface.class, getDeliveryConfig());
    proxy.exceptionless();
    Thread.sleep(10);
    assertTrue(sleepy.executing);
    Thread.sleep(10);
    assertTrue(sleepy.executing);
    Thread.sleep(100);
    assertFalse(sleepy.executing);
  }

  @Test
  public void testSyncDelayedExecutionOfExceptionless() throws Exception {
    GuaranteedDeliveryProxyServer serv = mkDeliveryServer();
    serv.start();
    SleepyTestInterface sleepy = new SleepyTestInterface();
    TestInterface proxy = serv.proxy(sleepy, TestInterface.class, getDeliveryConfig());
    proxy.syncExceptionless();
    assertTrue(sleepy.executed);
  }

  class SleepyTestInterface implements TestInterface {

    private volatile boolean executing;
    private volatile boolean executed;

    @Override
    public void exceptionless() throws IOException {
      executing = true;
      try {
        Thread.sleep(100);
      } catch (InterruptedException ignored) {
      }
      executing = false;
    }

    @Override
    public void syncExceptionless() throws Exception {
      try {
        Thread.sleep(100);
      } catch (InterruptedException ignored) {
      }
      executed = true;
    }

    @Override
    public void methodWithArgs(final String first, final String second) throws IOException {
      throw new UnsupportedOperationException("Not implemented yet");
    }

    @Override
    public String methodWithRetval(final int i) throws IOException {
      throw new UnsupportedOperationException("Not implemented yet");
    }

    @Override
    public void thirdMethod() throws IOException {
      throw new UnsupportedOperationException("Not implemented yet");
    }

  }

}
