/*
 * Copyright (C) 2017 Flames Group SIA
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.flamesgroup.commons;

import org.junit.Test;

public class SNRPatternTest {

  @Test
  public void validPattern() {
    SNRPattern snrPattern = new SNRPattern("223XXX");
  }

  @Test
  public void validRandomWithPhonePatternOnce() {
    SNRPattern snrPattern = new SNRPattern("223X(N2-3)X");
  }

  @Test
  public void validRandomWithPhonePatternRepeatedly() {
    SNRPattern snrPattern = new SNRPattern("223X(N2-3)(N3+3)");
  }

  @Test(expected = IllegalArgumentException.class)
  public void checkBadPattern() {
    SNRPattern snrPattern = new SNRPattern("223XaX");
  }

  @Test(expected = IllegalArgumentException.class)
  public void badBracesAtPhonePattern() {
    SNRPattern snrPattern = new SNRPattern("223X(N2-4(N3+3)");
  }

  @Test(expected = IllegalArgumentException.class)
  public void badPhonePattern() {
    SNRPattern snrPattern = new SNRPattern("223X(N2-)X");
  }

}
