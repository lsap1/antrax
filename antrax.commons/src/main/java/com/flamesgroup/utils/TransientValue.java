/*
 * Copyright (C) 2017 Flames Group SIA
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.flamesgroup.utils;

/**
 * Value with lifetime. After lifetime ends value will be refreshed.
 *
 * @param <T> type of value
 */
public abstract class TransientValue<T> {
  /**
   * Reads value of object. Will be called maximum every <code>lifetime</code>
   * period
   *
   * @return fresh value
   */
  abstract protected T readValue();

  private final long lifetime;
  private long updateTimestamp;
  private T value;

  /**
   * Constructs TransientValue which will be refreshed after
   * <code>lifetime</code> if requested
   *
   * @param lifetime of the value
   */
  public TransientValue(final long lifetime) {
    this.lifetime = lifetime;
    this.updateTimestamp = 0;
  }

  /**
   * @return value at least read <code>lifetime</code> long ago
   */
  public T getValue() {
    if (System.currentTimeMillis() - updateTimestamp >= lifetime) {
      value = readValue();
      updateTimestamp = System.currentTimeMillis();
    }
    return value;
  }

  /**
   * Forces read of value on next get
   */
  public void reset() {
    updateTimestamp = 0;
  }

}
