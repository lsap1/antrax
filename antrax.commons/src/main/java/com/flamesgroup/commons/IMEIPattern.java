/*
 * Copyright (C) 2017 Flames Group SIA
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.flamesgroup.commons;

import java.io.Serializable;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class IMEIPattern implements Serializable {

  private static final long serialVersionUID = 5263622401501283886L;

  private static final String REGEX_TEMPLATE = "((\\d)|(X)|(\\(N\\d+[+-]\\d\\))){14,98}";
  private static final Pattern regexPattern = Pattern.compile(REGEX_TEMPLATE);

  private final String pattern;

  public IMEIPattern(final String pattern) {
    validate(pattern);
    this.pattern = pattern;
  }

  public String getPattern() {
    return pattern;
  }

  private void validate(final String pattern) {
    Matcher matcher = regexPattern.matcher(pattern);
    if (!matcher.matches()) {
      throw new IllegalArgumentException("Wrong pattern [" + pattern + "]. Pattern must conform regex:" + matcher.pattern());
    }
  }

  @Override
  public String toString() {
    return getPattern();
  }

}
