/*
 * Copyright (C) 2017 Flames Group SIA
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.flamesgroup.commons;

import java.io.Serializable;
import java.util.Objects;

public class VoiceServerCallStatistic implements Serializable {

  private static final long serialVersionUID = 3982587692548609949L;

  private String server;
  private String prefix;

  private int total;
  private int successful;
  private long duration;

  public String getServer() {
    return server;
  }

  public void setServer(final String server) {
    this.server = server;
  }

  public String getPrefix() {
    return prefix;
  }

  public void setPrefix(final String prefix) {
    this.prefix = prefix;
  }

  public int getTotal() {
    return total;
  }

  public void setTotal(final int total) {
    this.total = total;
  }

  public int getSuccessful() {
    return successful;
  }

  public void setSuccessful(final int successful) {
    this.successful = successful;
  }

  public long getDuration() {
    return duration;
  }

  public void setDuration(final long duration) {
    this.duration = duration;
  }

  @Override
  public boolean equals(final Object object) {
    if (this == object) {
      return true;
    }
    if (!(object instanceof VoiceServerCallStatistic)) {
      return false;
    }
    VoiceServerCallStatistic that = (VoiceServerCallStatistic) object;

    return total == that.total
        && successful == that.successful
        && duration == that.duration
        && Objects.equals(server, that.server)
        && Objects.equals(prefix, that.prefix);
  }

  @Override
  public int hashCode() {
    final int prime = 31;
    int result = Objects.hashCode(server);
    result = prime * result + Objects.hashCode(prefix);
    result = prime * result + total;
    result = prime * result + successful;
    result = prime * result + Long.hashCode(duration);
    return result;
  }

}
