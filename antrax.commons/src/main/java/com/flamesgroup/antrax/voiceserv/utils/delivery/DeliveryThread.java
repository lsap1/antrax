/*
 * Copyright (C) 2017 Flames Group SIA
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.flamesgroup.antrax.voiceserv.utils.delivery;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.lang.reflect.InvocationTargetException;
import java.util.LinkedList;
import java.util.List;

class DeliveryThread extends Thread {

  private final Logger logger = LoggerFactory.getLogger(getClass());

  private final LinkedList<ExecutionCommand> queue;
  private final List<Class<? extends Throwable>> ioExceptions;
  private final int networkProblemAttemptCount = 3;

  private int networkProblemAttempt = 0;

  public DeliveryThread(final String name, final LinkedList<ExecutionCommand> queue, final List<Class<? extends Throwable>> ioExceptions) {
    super(name);
    setDaemon(false); // This thread should finish it's work
    this.queue = queue;
    this.ioExceptions = ioExceptions;
  }

  @Override
  public void run() {
    logger.trace("[{}] - started {}", this, getName());
    while (!interrupted()) {
      executeCommands();
      try {
        synchronized (queue) {
          queue.wait(1000);
        }
      } catch (InterruptedException ignored) {
        break;
      }
    }
    executeCommands();

    interruptLeftCommands();

    logger.trace("[{}] - stopped {}", this, getName());
  }

  private void interruptLeftCommands() {
    synchronized (queue) {
      for (ExecutionCommand c : queue) {
        c.getHandler().interrupt();
      }
    }
  }

  private void executeCommands() {
    while (executeCommand()) {
    }
  }

  private boolean executeCommand() {
    ExecutionCommand cmd = null;
    synchronized (queue) {
      if (!queue.isEmpty()) {
        cmd = queue.getFirst();
      }
    }
    if (cmd == null) {
      return false;
    }
    try {
      logger.trace("[{}] - invoking {}", this, cmd);
      Object res = cmd.getMethod().invoke(cmd.getDelegate(), cmd.getArgs());
      cmd.getHandler().handleExecuted(res);
      logger.trace("[{}] - successfully invoked {}", this, cmd);
    } catch (InvocationTargetException e) {
      if (isNetworkProblems(e.getCause())) {
        if (hasNetworkProblemAttempts()) {
          return false;
        }

        failCommands(e.getCause());
      } else {
        handleException(cmd, e.getCause());
      }
      logger.warn("[{}] - failed to execute {}", this, cmd, e);
    } catch (Throwable e) {
      if (isNetworkProblems(e)) {
        if (hasNetworkProblemAttempts()) {
          return false;
        }

        failCommands(e);
      } else {
        handleException(cmd, e);
      }
      logger.warn("[{}] - Failed to execute {}", this, cmd, e);
    }
    synchronized (queue) {
      queue.removeFirst();
    }
    networkProblemAttempt = 0;
    return true;
  }

  private void failCommands(final Throwable exception) {
    synchronized (queue) {
      for (ExecutionCommand c : queue) {
        handleException(c, exception);
      }
    }
  }

  private void handleException(final ExecutionCommand cmd, final Throwable e) {
    if (cmd.getDeliveryConfig().isExceptionless(cmd.getMethod())) {
      cmd.getHandler().handleExecuted(cmd.getMethod().getDefaultValue());
    } else {
      cmd.getHandler().handleException(e);
    }
  }

  private boolean isNetworkProblems(final Throwable cause) {
    for (Class<? extends Throwable> io : ioExceptions) {
      if (io.isInstance(cause)) {
        return true;
      }
    }
    return false;
  }

  private boolean hasNetworkProblemAttempts() {
    networkProblemAttempt++;
    return networkProblemAttempt < networkProblemAttemptCount;
  }
}
