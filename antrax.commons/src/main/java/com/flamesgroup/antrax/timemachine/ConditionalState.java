/*
 * Copyright (C) 2017 Flames Group SIA
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.flamesgroup.antrax.timemachine;

/**
 * State for {@link TimeMachine} which can determine whether it is finished by
 * meeting some condition.
 *
 * @version 1.0
 * @see TimeMachine
 * @see State
 * @see TransientState
 */
public interface ConditionalState extends State {
  /**
   * Determines whether this state is finished
   *
   * @return true when this state is finished
   */
  boolean isFinished();
}
