/*
 * Copyright (C) 2017 Flames Group SIA
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.flamesgroup.antrax.voiceserver;

import com.flamesgroup.antrax.configuration.IPingServerManager;
import com.flamesgroup.antrax.control.communication.ControlServerPingException;
import com.flamesgroup.antrax.control.voiceserver.IActivityLogger;
import com.flamesgroup.antrax.distributor.Server;
import com.flamesgroup.antrax.distributor.ServerType;
import com.flamesgroup.antrax.voiceserver.properties.VoiceServerPropUtils;
import com.flamesgroup.utils.AntraxProperties;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.rmi.RemoteException;

public class ControlServerPingThread extends Thread {

  private static final Logger logger = LoggerFactory.getLogger(ControlServerPingThread.class);

  private final Server server = new Server(AntraxProperties.SERVER_NAME, ServerType.VOICE_SERVER);

  private final IPingServerManager pingServerManager;
  private final IActivityLogger activityLogger;
  private final long timeout;

  private volatile Thread currentThread;

  public ControlServerPingThread(final IPingServerManager pingServerManager, final IActivityLogger activityLogger, final long timeout) {
    super("ControlServerPingThread (" + pingServerManager + ")");
    this.pingServerManager = pingServerManager;
    this.activityLogger = activityLogger;
    this.timeout = timeout;
  }

  @Override
  public void run() {
    try {
      pingServerManager.setExpireTimeout(server, timeout);
    } catch (RemoteException e) {
      logger.warn("[{}] - Control server not responding, terminate thread", this);
      throw new ControlServerPingException(e);
    }

    Thread thisThread = Thread.currentThread();
    currentThread = thisThread;
    while (currentThread == thisThread) {
      int rmiRegistryPort = VoiceServerPropUtils.getInstance().getVoiceServerProperties().getRmiRegistryPort();
      try {
        pingServerManager.ping(server, activityLogger.getShortServerInfo().getStatus(), rmiRegistryPort);
      } catch (RemoteException e) {
        logger.warn("[{}] - Control server not responding, terminate thread", this);
        throw new ControlServerPingException(e);
      }

      try {
        Thread.sleep(timeout);
      } catch (InterruptedException e) {
        logger.error("[{}] - Unexpected interruption of controlServerPingThread ", this, e);
        break;
      }
    }
  }

  public void terminate() {
    currentThread = null;
  }

}
