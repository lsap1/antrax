/*
 * Copyright (C) 2017 Flames Group SIA
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.flamesgroup.antrax.voiceserver.channels;

import com.flamesgroup.antrax.storage.commons.impl.CellEqualizerAlgorithm;
import com.flamesgroup.antrax.voiceserver.CellEqualizerManager;
import com.flamesgroup.antrax.voiceserver.channels.error.GsmHardwareError;
import com.flamesgroup.unit.CellInfo;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Arrays;

public abstract class CellEqualizerExecutor {

  protected final Logger logger = LoggerFactory.getLogger(getClass());

  protected final GSMUnit gsmUnit;
  protected final CellEqualizerManager cellEqualizerManager;

  protected CellInfo[] lastCellInfos;

  public abstract void execute(final CellEqualizerAlgorithm cellEqualizerAlgorithm) throws GsmHardwareError;

  public abstract void terminate();

  public CellEqualizerExecutor(final GSMUnit gsmUnit, final CellEqualizerManager cellEqualizerManager) {
    this.gsmUnit = gsmUnit;
    this.cellEqualizerManager = cellEqualizerManager;
  }

  public CellEqualizerExecutor setLastCellInfos(final CellInfo[] lastCellInfos) {
    logger.debug("[{}] - cellInfos: [{}]", this, Arrays.toString(lastCellInfos));
    this.lastCellInfos = lastCellInfos;
    return this;
  }

}
