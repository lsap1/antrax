/*
 * Copyright (C) 2017 Flames Group SIA
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.flamesgroup.antrax.voiceserver.channels;

import com.flamesgroup.antrax.storage.commons.impl.CellEqualizerAlgorithm;
import com.flamesgroup.antrax.voiceserver.CellEqualizerManager;
import com.flamesgroup.antrax.voiceserver.channels.error.GsmHardwareError;

public class CellEqualizerBasicAlgorithmExecutor extends CellEqualizerExecutor {

  private long cellEqualizerLastTimeExecutedLastUpdateCellAdjustments;
  private long lastTimeUpdateCellAdjustments;

  public CellEqualizerBasicAlgorithmExecutor(final GSMUnit gsmUnit, final CellEqualizerManager cellEqualizerManager) {
    super(gsmUnit, cellEqualizerManager);
  }

  public CellEqualizerBasicAlgorithmExecutor setLastTimeUpdateCellAdjustments(final long lastTimeUpdateCellAdjustments) {
    this.lastTimeUpdateCellAdjustments = lastTimeUpdateCellAdjustments;
    return this;
  }

  @Override
  public void execute(final CellEqualizerAlgorithm cellEqualizerAlgorithm) throws GsmHardwareError {
    if (lastTimeUpdateCellAdjustments > cellEqualizerLastTimeExecutedLastUpdateCellAdjustments) {
      gsmUnit.executeCellEqualizer(cellEqualizerManager.getServerCellAdjustments());
      cellEqualizerLastTimeExecutedLastUpdateCellAdjustments = System.currentTimeMillis();
    }
  }

  @Override
  public void terminate() {
  }

}
