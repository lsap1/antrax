/*
 * Copyright (C) 2017 Flames Group SIA
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.flamesgroup.antrax.voiceserver;

import com.flamesgroup.antrax.storage.enums.CallType;
import com.flamesgroup.jiax2.IMediaStream;
import com.flamesgroup.jiax2.IMediaStreamHandler;
import com.flamesgroup.unit.PhoneNumber;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.BufferedOutputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.nio.file.Path;
import java.time.LocalDateTime;

public class SpyMediaStream implements IMediaStream, IMediaStreamHandler {

  private static final Logger logger = LoggerFactory.getLogger(SpyMediaStream.class);

  private final IMediaStream mediaStream;

  private String audioOutputStreamName;
  private String mediaOutputStreamName;

  private OutputStream audioOutputStream;
  private OutputStream mediaOutputStream;

  private IMediaStreamHandler mediaStreamHandler;

  public SpyMediaStream(final IMediaStream mediaStream, final Path path, final CallType callType, final PhoneNumber phoneNumber, final String description) {
    this.mediaStream = mediaStream;

    createOutputStreams(path, callType, phoneNumber, description);
  }

  @Override
  public void open(final IMediaStreamHandler mediaStreamHandler) {
    this.mediaStreamHandler = mediaStreamHandler;
    mediaStream.open(this);
  }

  @Override
  public void close() {
    mediaStream.close();

    if (audioOutputStream != null) {
      try {
        audioOutputStream.close();
      } catch (IOException e) {
        logger.info("[{}] - while closing file {}", this, audioOutputStreamName, e);
      }
    }

    if (mediaOutputStream != null) {
      try {
        mediaOutputStream.close();
      } catch (IOException e) {
        logger.info("[{}] - while closing file {}", this, mediaOutputStreamName, e);
      }
    }
  }

  @Override
  public boolean isOpen() {
    return mediaStream.isOpen();
  }

  @Override
  public void write(final int timestamp, final byte[] data, final int offset, final int length) {
    mediaStream.write(timestamp, data, offset, length);

    if (audioOutputStream == null) {
      return;
    }

    try {
      audioOutputStream.write(data, offset, length);
    } catch (IOException e) {
      logger.debug("[{}] - while writing to file {}", this, audioOutputStreamName, e);
    }
  }

  @Override
  public void handleMedia(final long timestamp, final byte[] data, final int offset, final int length) {
    mediaStreamHandler.handleMedia(timestamp, data, offset, length);

    if (mediaOutputStream == null) {
      return;
    }

    try {
      mediaOutputStream.write(data, offset, length);
    } catch (IOException e) {
      logger.debug("[{}] - while writing to file {}", this, mediaOutputStreamName, e);
    }
  }

  private void createOutputStreams(final Path path, final CallType callType, final PhoneNumber phoneNumber, final String description) {
    LocalDateTime time = LocalDateTime.now();
    String number = phoneNumber == null ? "" : phoneNumber.getValue();

    switch (callType) {
      case VS_TO_GSM:
        audioOutputStreamName = time + "_O_" + description + "_" + number + "_g2s.sln";
        mediaOutputStreamName = time + "_O_" + description + "_" + number + "_s2g.sln";
        break;
      case VOIP_TO_GSM:
        audioOutputStreamName = time + "_O_" + description + "_" + number + "_g2v.sln";
        mediaOutputStreamName = time + "_O_" + description + "_" + number + "_v2g.sln";
        break;
      case GSM_TO_VS:
        audioOutputStreamName = time + "_I_" + description + "_" + number + "_g2s.sln";
        mediaOutputStreamName = time + "_I_" + description + "_" + number + "_s2g.sln";
        break;
      case GSM_TO_VOIP:
        audioOutputStreamName = time + "_I_" + description + "_" + number + "_g2v.sln";
        mediaOutputStreamName = time + "_I_" + description + "_" + number + "_v2g.sln";
        break;
      default:
        throw new IllegalArgumentException("call type " + callType + " not supported yet");
    }

    try {
      audioOutputStream = new BufferedOutputStream(new FileOutputStream(path.resolve(audioOutputStreamName).toFile()), 8000);
    } catch (FileNotFoundException e) {
      logger.info("[{}] - while creating file {}", this, audioOutputStreamName, e);
    }

    try {
      mediaOutputStream = new BufferedOutputStream(new FileOutputStream(path.resolve(mediaOutputStreamName).toFile()), 8000);
    } catch (FileNotFoundException e) {
      logger.info("[{}] - while creating file {}", this, mediaOutputStreamName, e);
    }
  }

}
