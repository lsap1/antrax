/*
 * Copyright (C) 2017 Flames Group SIA
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.flamesgroup.antrax.voiceserver;

import com.flamesgroup.antrax.automation.actionscripts.Action;
import com.flamesgroup.antrax.storage.state.CallState;
import com.flamesgroup.unit.ICCID;
import com.flamesgroup.unit.PhoneNumber;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Collections;
import java.util.Comparator;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.function.BiPredicate;
import java.util.stream.Collectors;

public class CallChannelPool {

  private static final Logger logger = LoggerFactory.getLogger(CallChannelPool.class);

  private final List<CallChannel> channels = new LinkedList<>();
  private final Comparator<? super CallChannel> comparator = new CallChannelComparator();

  private final long resortTimeout;

  private long lastSortTime;

  public CallChannelPool(final long resortTimeout) {
    this.resortTimeout = resortTimeout;
  }

  public synchronized void add(final CallChannel callChannel) {
    channels.add(callChannel);
    logger.debug("[{}] - {} added to the pool", this, callChannel);
  }

  public synchronized void remove(final CallChannel callChannel) {
    channels.remove(callChannel);
    logger.debug("[{}] - {} removed from the pool", this, callChannel);
  }

  private boolean isFreeToCall(final CallChannel c, final StringBuilder busyReasons) {
    if (!c.isEnabled()) {
      busyReasons.append(c).append(":").append("disabled");
      return false;
    } else if (c.isInBusinessActivity()) {
      busyReasons.append(c).append(":").append("in business activity [").append(c.getSIMUnit().getBusinessActivityDescription()).append("]");
      return false;
    } else if (c.getSIMUnit().shouldStartBusinessActivity()) {
      busyReasons.append(c).append(":").append("required business activity");
      return false;
    } else if (c.isTaken()) {
      busyReasons.append(c).append(":").append("took");
      return false;
    } else if (c.owned()) {
      busyReasons.append(c).append(":").append("owned");
      return false;
    } else if (c.getIncomingCall() != null) {
      busyReasons.append(c).append(":").append("in incoming call");
      return false;
    } else if (c.isUSSDSessionOpen()) {
      busyReasons.append(c).append(":").append("opened USSD session");
      return false;
    } else if (c.isSendingSms()) {
      busyReasons.append(c).append(":").append("sending sms now");
      return false;
    } else {
      return true;
    }
  }

  private boolean isFreeToSms(final CallChannel c, final StringBuilder busyReasons) {
    if (c.isSendingSms()) {
      busyReasons.append(c).append(":").append("sending sms now");
      return false;
    } else if (c.isInBusinessActivity()) {
      busyReasons.append(c).append(":").append("in business activity [").append(c.getSIMUnit().getBusinessActivityDescription()).append("]");
      return false;
    } else if (c.getSIMUnit().shouldStartBusinessActivity()) {
      busyReasons.append(c).append(":").append("required business activity");
      return false;
    } else if (c.isTaken()) {
      busyReasons.append(c).append(":").append("took");
      return false;
    } else if (c.isUSSDSessionOpen()) {
      busyReasons.append(c).append(":").append("opened USSD session");
      return false;
    } else if (!c.isIncomingCallReleased() && c.getCallState() != CallState.State.ACTIVE) {
      busyReasons.append(c).append(":").append("incomingCall isn't ACTIVE state");
      return false;
    } else if (!c.isOutgoingCallReleased() && c.getCallState() != CallState.State.ACTIVE) {
      busyReasons.append(c).append(":").append("outgoingCall isn't ACTIVE state");
      return false;
    } else {
      return true;
    }
  }

  private boolean acceptsCallPhoneNumber(final CallChannel c, final PhoneNumber callerPhoneNumber, final PhoneNumber calledPhoneNumber, final StringBuilder busyReasons) {
    boolean accepts = c.acceptsCallPhoneNumber(callerPhoneNumber, calledPhoneNumber);
    if (!accepts) {
      busyReasons.append(c).append(":").append("reject call phone number");
    }

    return accepts;
  }

  private boolean acceptsSmsPhoneNumber(final CallChannel c, final PhoneNumber phoneNumber, final StringBuilder busyReasons) {
    boolean accepts = c.acceptsSmsPhoneNumber(phoneNumber);
    if (!accepts) {
      busyReasons.append(c).append(":").append("reject sms phone number");
    }

    return accepts;
  }

  private boolean acceptsSmsParts(final CallChannel c, final int parts, final StringBuilder busyReasons) {
    boolean accepts = c.acceptsSmsParts(parts);
    if (!accepts) {
      busyReasons.append(c).append(":").append("reject, count of sms parts is too big");
    }

    return accepts;
  }

  private boolean acceptsSmsText(final CallChannel c, final String text, final StringBuilder busyReasons) {
    boolean accepts = c.acceptsSmsText(text);
    if (!accepts) {
      busyReasons.append(c).append(":").append("reject, text of sms denied");
    }

    return accepts;
  }

  public CallChannel takeForCall(final PhoneNumber callerPhoneNumber, final PhoneNumber calledPhoneNumber, final List<ICCID> iccids) {
    logger.trace("[{}] - attempted to take callChannel for call to calledPhoneNumber {}, callerPhoneNumber {}, iccids {}", this, callerPhoneNumber, calledPhoneNumber, iccids);
    if (iccids == null || iccids.isEmpty()) {
      return take(calledPhoneNumber, (callChannel, busyReasons) -> isFreeToCall(callChannel, busyReasons) && acceptsCallPhoneNumber(callChannel, callerPhoneNumber, calledPhoneNumber, busyReasons), null);
    } else {
      List<ICCID> iccidOnThisServer = channels.stream().map(c -> c.getSIMUnit().getUid()).collect(Collectors.toList());
      logger.trace("[{}] - iccids on this server {}", this, iccidOnThisServer);
      List<ICCID> targetIccidForThisServer = iccids.stream().filter(iccidOnThisServer::contains).collect(Collectors.toList());
      logger.trace("[{}] - target iccids for this server {}", this, targetIccidForThisServer);
      CallChannel callChannelLocal;
      for (ICCID iccid : targetIccidForThisServer) {
        callChannelLocal = take(calledPhoneNumber, (callChannel, busyReasons) -> isFreeToCall(callChannel, busyReasons) && acceptsCallPhoneNumber(callChannel, callerPhoneNumber, calledPhoneNumber, busyReasons), iccid);
        if (callChannelLocal != null) {
          logger.info("[{}] - call via callChannel {}", this, callChannelLocal);
          return callChannelLocal;
        }
      }
      return null;
    }
  }

  public CallChannel takeForSms(final PhoneNumber phoneNumber, final int parts, final String text) {
    logger.trace("[{}] - attempted to take callChannel for sms to {}", this, phoneNumber);
    CallChannel c = take(phoneNumber, (callChannel, busyReasons) -> isFreeToSms(callChannel, busyReasons) && acceptsSmsPhoneNumber(callChannel, phoneNumber, busyReasons)
    && acceptsSmsParts(callChannel, parts, busyReasons)  && acceptsSmsText(callChannel, text, busyReasons), null);
    if (c != null) {
      c.sendingSms(true);
    }
    return c;
  }

  private synchronized CallChannel take(final PhoneNumber phoneNumber, final BiPredicate<CallChannel, StringBuilder> predicate, final ICCID iccid) {
    resort();

    StringBuilder busyReasons = new StringBuilder();
    Iterator<CallChannel> iter = channels.iterator();
    while (iter.hasNext()) {
      CallChannel callChannel = iter.next();
      synchronized (callChannel) {
        if (callChannel.isReleased()) {
          continue;
        }

        if (iccid != null && !callChannel.getSIMUnit().getUid().equals(iccid)) {
          continue;
        }

        if (!predicate.test(callChannel, busyReasons)) {
          continue;
        }

        logger.trace("[{}] - take for {} found {}", this, phoneNumber, callChannel);
        callChannel.take();
        // Putting channel to the back of the list
        iter.remove();
        channels.add(callChannel);
        return callChannel;
      }
    }

    logger.trace("[{}] - failed to take for {} found only [{}]", this, phoneNumber, busyReasons);
    return null;
  }

  private void resort() {
    if (System.currentTimeMillis() - lastSortTime > resortTimeout) {
      logger.trace("[{}] - reordering call channels", this);
      Collections.sort(channels, comparator);
      lastSortTime = System.currentTimeMillis();
    }
  }

  public synchronized void forceReleaseChannels() {
    for (CallChannel c : channels) {
      logger.debug("[{}] - force releasing call channel [{}]", this, c);
      c.release("VS terminated");
    }

    int iterations = 0;
    while (!channels.isEmpty() && iterations++ < 60000 / 5000) {
      logger.debug("[{}] - wait release for call channels {}", this, channels);
      try {
        Thread.sleep(5000);
      } catch (InterruptedException e) {
        logger.warn("[{}] - while waiting release for all call channels was interrupted", this, e);
        return;
      }
    }

    if (!channels.isEmpty()) {
      logger.warn("[{}] - still remained not removed channels {}", this, channels);
    }
  }

  // set wait release to sim unit for all active channels
  public synchronized void releaseSimUnits() {
    channels.forEach(c -> c.getSIMUnit().setWaitRelease(true));
  }

  public synchronized CallChannel take(final ICCID simUID) {
    for (CallChannel c : channels) {
      if (simUID.equals(c.getSIMUnit().getUid()) && !c.isReleased()) {
        return c;
      }
    }
    return null;
  }

  public synchronized BulkIterator takeBulk(final Action action) {
    resort();

    List<CallChannel> supportsActions = new LinkedList<>();
    for (CallChannel callChannel : channels) {
      if (callChannel.isSupportsAction(action.getAction())) {
        supportsActions.add(callChannel);
      }
    }

    Collections.shuffle(supportsActions);
    return new BulkIterator(supportsActions.iterator());
  }

  public class BulkIterator {

    private final Iterator<CallChannel> iterator;
    private final StringBuilder busyReasons = new StringBuilder();

    public BulkIterator(final Iterator<CallChannel> iterator) {
      this.iterator = iterator;
    }

    public boolean hasNext() {
      return iterator.hasNext();
    }

    public CallChannel next() {
      while (hasNext()) {
        CallChannel callChannel = iterator.next();
        synchronized (callChannel) {
          if (isFreeToCall(callChannel, busyReasons.append(System.lineSeparator()))) {
            callChannel.take();
            logger.debug("[{}] - found {}", this, callChannel);
            return callChannel;
          }
        }
      }
      logger.debug("[{}] - returning null, found only [{}]", this, busyReasons);
      return null;
    }
  }

  private class CallChannelComparator implements Comparator<CallChannel> {

    @Override
    public int compare(final CallChannel o1, final CallChannel o2) {
      return Long.compare(o1.getSIMUnit().countSimFactor(), o2.getSIMUnit().countSimFactor());
    }

  }

}
