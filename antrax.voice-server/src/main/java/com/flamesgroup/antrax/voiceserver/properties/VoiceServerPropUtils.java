/*
 * Copyright (C) 2017 Flames Group SIA
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.flamesgroup.antrax.voiceserver.properties;

import com.flamesgroup.properties.PropUtils;
import com.flamesgroup.properties.ServerProperties;

import java.util.HashMap;
import java.util.Map;

public class VoiceServerPropUtils extends PropUtils {

  private static final String VOICE_SERVER_PROPERTIES = "voice-server.properties";
  private static final String JIAX2_PROPERTIES = "jiax2.properties";

  private static volatile VoiceServerPropUtils instance;

  private VoiceServerPropUtils() {
    super();
  }

  public VoiceServerProperties getVoiceServerProperties() {
    return (VoiceServerProperties) properties.get(VOICE_SERVER_PROPERTIES);
  }

  public Jiax2Properties getJiax2Properties() {
    return (Jiax2Properties) properties.get(JIAX2_PROPERTIES);
  }

  @Override
  protected Map<String, ServerProperties> getProperties() {
    Map<String, ServerProperties> localProperties = new HashMap<>();
    localProperties.put(VOICE_SERVER_PROPERTIES, new VoiceServerProperties());
    localProperties.put(JIAX2_PROPERTIES, new Jiax2Properties());
    localProperties.putAll(super.getProperties());
    return localProperties;
  }

  public static VoiceServerPropUtils getInstance() {
    if (instance == null) {
      synchronized (VoiceServerPropUtils.class) {
        if (instance == null) {
          instance = new VoiceServerPropUtils();
        }
      }
    }
    return instance;
  }

  @Override
  public String getName() {
    return VOICE_SERVER_PROPERTIES + " | " + JIAX2_PROPERTIES;
  }

}
