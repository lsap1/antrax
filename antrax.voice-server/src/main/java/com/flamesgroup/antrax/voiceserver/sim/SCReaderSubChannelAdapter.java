/*
 * Copyright (C) 2017 Flames Group SIA
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.flamesgroup.antrax.voiceserver.sim;

import com.flamesgroup.antrax.control.simserver.ISCReaderSubChannels;
import com.flamesgroup.antrax.control.simserver.IllegalChannelException;
import com.flamesgroup.commons.ChannelUID;
import com.flamesgroup.device.channel.ChannelException;
import com.flamesgroup.device.sc.APDUCommand;
import com.flamesgroup.device.sc.ATR;
import com.flamesgroup.device.simb.ISCReaderSubChannel;
import com.flamesgroup.device.simb.ISCReaderSubChannelHandler;
import com.flamesgroup.device.simb.SCReaderState;

import java.rmi.RemoteException;

public class SCReaderSubChannelAdapter implements ISCReaderSubChannel {

  private final ChannelUID channelUID;
  private final ISCReaderSubChannels scReaderSubChannels;
  private final SCReaderSubChannelsHandler scReaderSubChannelsHandler;

  public SCReaderSubChannelAdapter(final ChannelUID channelUID, final ISCReaderSubChannels scReaderSubChannels, final SCReaderSubChannelsHandler scReaderSubChannelsHandler) {
    this.channelUID = channelUID;
    this.scReaderSubChannels = scReaderSubChannels;
    this.scReaderSubChannelsHandler = scReaderSubChannelsHandler;
  }

  @Override
  public ATR start(final ISCReaderSubChannelHandler scReaderSubChannelHandler) throws ChannelException, InterruptedException {
    ATR atr;
    try {
      atr = scReaderSubChannels.start(channelUID, this.scReaderSubChannelsHandler);
    } catch (RemoteException | IllegalChannelException e) {
      throw new ChannelException(e);
    }

    scReaderSubChannelsHandler.putSCReaderSubChannelHandler(channelUID, scReaderSubChannelHandler);
    return atr;
  }

  @Override
  public void stop() throws ChannelException, InterruptedException {
    try {
      scReaderSubChannels.stop(channelUID);
    } catch (RemoteException | IllegalChannelException e) {
      throw new ChannelException(e);
    } finally {
      scReaderSubChannelsHandler.removeSCReaderSubChannelHandler(channelUID);
    }
  }

  @Override
  public void writeAPDUCommand(final APDUCommand command) throws ChannelException, InterruptedException {
    try {
      scReaderSubChannels.writeAPDUCommand(channelUID, command);
    } catch (RemoteException | IllegalChannelException e) {
      throw new ChannelException(e);
    }
  }

  @Override
  public SCReaderState getState() throws ChannelException, InterruptedException {
    throw new UnsupportedOperationException("Not supported");
  }

}
