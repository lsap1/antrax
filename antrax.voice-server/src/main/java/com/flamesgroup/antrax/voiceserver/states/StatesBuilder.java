/*
 * Copyright (C) 2017 Flames Group SIA
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.flamesgroup.antrax.voiceserver.states;

import com.flamesgroup.antrax.activity.IRemoteHistoryLogger;
import com.flamesgroup.antrax.timemachine.State;
import com.flamesgroup.antrax.voiceserver.CallChannel;
import com.flamesgroup.antrax.voiceserver.CallChannelPool;

public class StatesBuilder {

  private final IdleBeforeFirstCallState idleBeforeFirstCallState;
  private final WaitingForCallState waitingForCallState;
  private final CallingState callingState;
  private final IdleAfterSuccessfulCallState idleAfterSuccessfulCallState;
  private final IdleAfterZeroCallState idleAfterZeroCallState;
  private final IdleBeforeSelfCallState idleBeforeSelfCallState;
  private final SelfCallState selfCallState;
  private final IdleAfterSelfCallState idleAfterSelfCallState;
  private final IdleBeforeUnregistrationState idleBeforeUnregistrationState;
  private final UnregisterState unregisterState;
  private final CallChannel cc;
  private WaitingTransientState wrappedIdleBeforeFirstCallState;
  private WaitingConditionalState wrappedWaitingForCallState;
  private WaitingTransientState wrappedIdleAfterSuccessfullCallState;
  private WaitingTransientState wrappedIdleBeforeSelfCallState;
  private WaitingTransientState wrappedIdleAfterSelfCallState;
  private WaitingTransientState wrappedIdleAfterZeroCallState;

  public StatesBuilder(final CallChannel cc, final CallChannelPool callChannelPool, final IRemoteHistoryLogger historyLogger) {
    this.cc = cc;
    idleBeforeFirstCallState = new IdleBeforeFirstCallState(cc);
    waitingForCallState = new WaitingForCallState(cc);
    callingState = new CallingState(cc);
    idleAfterSuccessfulCallState = new IdleAfterSuccessfulCallState(cc);
    idleAfterZeroCallState = new IdleAfterZeroCallState(cc);
    idleBeforeSelfCallState = new IdleBeforeSelfCallState(cc);
    selfCallState = new SelfCallState(cc, callChannelPool, historyLogger);
    idleAfterSelfCallState = new IdleAfterSelfCallState(cc);

    idleBeforeUnregistrationState = new IdleBeforeUnregistrationState(cc);
    unregisterState = new UnregisterState(cc);
  }

  public void initialize() {
    idleBeforeFirstCallState.initialize(this);
    waitingForCallState.initialize(this);
    callingState.initialize(this);
    idleAfterSuccessfulCallState.initialize(this);
    idleAfterZeroCallState.initialize(this);
    idleBeforeSelfCallState.initialize(this);
    selfCallState.initialize(this);
    idleAfterSelfCallState.initialize(this);
    idleBeforeUnregistrationState.initialize(this);
    unregisterState.initialize(this);
  }

  public State getCallingState() {
    return callingState;
  }

  public State getIdleBeforeFirstCallState() {
    if (wrappedIdleBeforeFirstCallState == null) {
      wrappedIdleBeforeFirstCallState = new WaitingTransientState(idleBeforeFirstCallState, getIdleBeforeUnregistrationState(), getIdleBeforeUnregistrationState(), cc);
    }
    return wrappedIdleBeforeFirstCallState;
  }

  public State getWaitingForCallState() {
    if (wrappedWaitingForCallState == null) {
      wrappedWaitingForCallState = new WaitingConditionalState(waitingForCallState, getIdleBeforeUnregistrationState(), getIdleBeforeUnregistrationState(), cc);
    }
    return wrappedWaitingForCallState;
  }

  public State getIdleAfterSuccessfulCallState() {
    if (wrappedIdleAfterSuccessfullCallState == null) {
      wrappedIdleAfterSuccessfullCallState = new WaitingTransientState(idleAfterSuccessfulCallState, getIdleBeforeUnregistrationState(), getIdleBeforeUnregistrationState(), cc);
    }
    return wrappedIdleAfterSuccessfullCallState;
  }

  public State getIdleAfterZeroCallState() {
    if (wrappedIdleAfterZeroCallState == null) {
      wrappedIdleAfterZeroCallState = new WaitingTransientState(idleAfterZeroCallState, getIdleBeforeUnregistrationState(), getIdleBeforeUnregistrationState(), cc);
    }
    return wrappedIdleAfterZeroCallState;
  }

  public State getIdleBeforeSelfCallState() {
    if (wrappedIdleBeforeSelfCallState == null) {
      wrappedIdleBeforeSelfCallState = new WaitingTransientState(idleBeforeSelfCallState, getIdleBeforeUnregistrationState(), getIdleBeforeUnregistrationState(), cc);
    }
    return wrappedIdleBeforeSelfCallState;
  }

  public State getSelfCallState() {
    return selfCallState;
  }

  public State getIdleAfterSelfCallState() {
    if (wrappedIdleAfterSelfCallState == null) {
      wrappedIdleAfterSelfCallState = new WaitingTransientState(idleAfterSelfCallState, getIdleBeforeUnregistrationState(), getIdleBeforeUnregistrationState(), cc);
    }
    return wrappedIdleAfterSelfCallState;
  }

  public State getIdleBeforeUnregistrationState() {
    return idleBeforeUnregistrationState;
  }

  public State getUnregisterState() {
    return unregisterState;
  }
}
