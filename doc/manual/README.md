# Technical documentation

## Introduction

This document is a brief manual for the Antrax product.

Here we’ll try to provide you with answers for all the questions regarding the functionality of the system, structure characteristics and user’s interface. For your convenience, the content is divided into chapters displayed on the left side of the the text. To access the needed part just click on the corresponding title.

**Antrax** is a unique solution for voice traffic termination into GSM networks. The system is simple, intuitively clear and very comfortable. These features help to save the time needed for installation, adjustment, regulation and handling.

### Content

* [System architecture](architecture.md)
* [GSM Box](gsm-box.md)
* [SIM Box](sim-box.md)
* [The connection of GSM and SIM boxes](box-connection.md)
* [Hardware modules](hardware-modules.md)
* [Technical requirements](technical-requirements.md)
* [User Interface](user-interface/README.md)
* [Scripts](scripts/README.md)
