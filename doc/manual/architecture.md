## System architecture

The picture below shows the architecture of hardware-software  ANTRAX system.

![ANTRAX architecture](architecture.assets/antrax_architecture.png)

**Control Server** is the central software module of ANTRAX system. The main tasks of this module are configuring of other modules and coordinating of interaction between them, building of voice channels, routing of VoIP calls, executing the majority of scripts, collecting of system statistics and logs, etc. Also **Control Server** provide RMI API for ANTRAX system remote **GUI Client**. That is why **Control Server** must be launched before other modules.

**GUI Client** (where GUI is Graphical User Interface) also named **Antrax Manager** is a user's application for remote monitoring, configuring and controlling ANTRAX system. Several **GUI Client** can be connected to the system. You can find the detailed description of this module in the section [User interface](user-interface/README.md).

**Voice Server** is the software module of ANTRAX system. The main tasks of this module are initializing and communicating with **GSM Board**'s and **GSM BOX**'s hardware, connecting GSM channels with SIM channels and creating Voice channels, accepting IAX2 calls from **YATE Softswitch** and terminating them into GSM network, accepting GSM calls from GSM network and terminating them to **YATE Softswitch**, sending SMS,  executing corresponding scripts and activities, etc.
ANTRAX system may have as many **Voice Server**s as needed.

**Sim Server** is the software module of ANTRAX system. The main tasks of this module are initializing and communicating with **SIM Board**'s and **SIM BOX**'s hardware, preparing SIM channels for remote usage by **Voice Server**, etc.
ANTRAX system may have as many **Sim Server**s as needed.

**YATE Softswitch** is the external software module of ANTRAX system used to route all incoming VoIP traffic and also to normalize and convert incoming VoIP calls and audio codecs. For **YATE Softswitch**, as already understandable from its name, used [YATE](http://yate.ro).

**Database** is the external software module of ANTRAX system used to persistence store all necessary data (such as configurations, statistics, logs, etc.). For **Database** module used object-relational database management system [PostgreSQL](https://www.postgresql.org).

**GSM Board** and **SIM Board** are hardware modules of ANTRAX system used to service GSM channels (GSM modules) and SIM channels (SIM cards). Software modules communicate with these hardware modules by CUDP and MUDP protocols over [UDP](https://en.wikipedia.org/wiki/User_Datagram_Protocol) throw [Ethernet](https://en.wikipedia.org/wiki/Ethernet).

**GSM BOX** and **SIM BOX** are another hardware modules of ANTRAX system used to service GSM channels (GSM modules) and SIM channels (SIM cards). Software modules communicate with these hardware modules by CUDP and MUDP protocols throw [SPI](https://en.wikipedia.org/wiki/Serial_Peripheral_Interface_Bus) and [I²C](https://en.wikipedia.org/wiki/I²C).

GSM channels are combined into **GSM group**s, SIM channels - into **SIM group**s.

**GSM group** is a set of GSM channels, which have shared configurations. **GSM group** can contain GSM channels which can be located on different **GSM Board**s/**GSM BOX**s and served by different **Voice Server**s. Each **GSM group** must have a unique name. **GSM group** has only one configuration – link with one or more **SIM group**s.

**SIM group** is a set of SIM channels, which have shared configurations. Similarly to **GSM group**,  **SIM group** can contain SIM channels which can be located on different **SIM Board**s/**SIM BOX**s and served by different **Sim Server**s. Each **SIM group** also must have a unique name. Unlike **GSM group**,  **SIM group** has the variety of configurations, which determine the behavioral pattern of SIM channels.

The link between **GSM group** and **SIM group** means that SIM channels (SIM cards) from this **SIM group** can be registered only on GSM channels (GSM modules) from linked **GSM group**.

### SIM channel selection for connection with GSM channel

One of the tasks for **Voice Server** is a formation of Voice channels. Voice channel is created using SIM channel and GSM channel. After Voice channel creation following the registration in GSM network. After successfully registration Voice channel is ready to process calls and performs other activities (send SMS, top up the balance, etc.). Created and registered Voice channels are arranged into the list according to the [Voice Server Factor Script](scripts/voice-server-factor-script.md).

Formation of Voice channels lasts continuously and doesn't depend on presence or absence of calls. The task of **Voice Server** is a connection between all free GSM channels and SIM channels. **Voice Server** requests SIM card from **Control Server** continuously.

Destruction of Voice channel occurs according to the SIM groups setting (SIM channel is located in this SIM group and active in current Voice channel). **Voice Server** also requests  **Control Server** information about the time for Voice channel destruction.

The first channel from the list of arranged Voice channels will be active for the new call. In the case of missing free Voice channels, **Voice Server** will reject the call.

On the picture,  you can see the process of SIM channels and GSM channels selection for link creation. This **Voice Server** contains several GSM channels (*GSM group A* and *GSM group B*). GSM groups are connected with SIM groups in such way: *GSM group A* <> *SIM group A*, *GSM group B* <> *SIM group B*.

![build_voice_channel](architecture.assets/build_voice_channel.png)

Let's consider the process of GSM channels (*GSM group A*) connection with suitable SIM channels. *GSM group A* connects with *SIM group A*, thus SIM channel must belong to the *SIM group A*. As you can see on the picture, **Voice Server** hasn't got any SIM channels with A **SIM group**. That's why **Voice Server** requests SIM channels from **Control Server**. **Control Server** chooses all SIM channels from GSM group (*SIM group A*) and arranges them by [Sim Server Factor Script](scripts/sim-server-factor-script.md). First SIM channel will be taken from this arranged list. [Session Period Script](scripts/session-period-script.md) checks if selected SIM channel can go to the **Voice Server** at the moment. In the case of the positive result it is checked if selected SIM channel can go to the current **Voice Server** ([Gateway Selector Script](scripts/gateway-selector-script.md)). SIM channel is transferred into **Voice Server** if both conditions are met. In the case of failure of at least one condition ([Session Period Script](scripts/session-period-script.md) or [Gateway Selector Script](scripts/gateway-selector-script.md)), next SIM channel will be chosen from the arranged list, and the same checking will take place.

After that system checks if SIM channel can register in GSM network at the moment ([Activity Period Script](scripts/activity-period-script.md)). In the case of positive result GSM and SIM channels connect with each other and create new voice channel, which is placed on the list of voice channels on **Voice Server**. If received SIM channel can't register in GSM network at the moment, then **Voice Server** try to register SIM channel from session pool, and only then requests **Control Server** for one SIM channel one more time.

It is worth mentioning that SIM channels stay on **Voice Server** in session pool while all conditions of [Session Period Script](scripts/session-period-script.md) are being performed. Unless conditions are met, Voice channel is destroyed, and SIM channel returns back to **Sim Server**.

### Call flow

#### Main stages of call flow

![ANTRAX call flow](architecture.assets/antrax_call_flow.png)

1. The call comes to **YATE Softswitch**
2. **YATE Softswitch** through [external module protocol](http://docs.yate.ro/wiki/External_module_command_flow) sends the routing request for the call to **Control Server**
3.  After processing this request (which include different rules include Antispam) **Control Server** response with the route if any found if not then call dropped
4. **YATE Softswitch** route the call to the selected **Voice Server** with IAX2 protocol
5. Complete call connection

#### Detailed states of SIP call flow within YATE

![YATE call flow](architecture.assets/yate_sip_call_flow.png)
