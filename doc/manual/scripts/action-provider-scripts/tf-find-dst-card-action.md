## TF Find dst card action

The given script searches for a card-receiver in order to transfer money to it. The script is an intermediate part of the TF scripts group and can not be used separately from the group.

![tf_find_dst_card_action](tf-find-dst-card-action.assets/tf_find_dst_card_action.png)

### Parameters

| name| description|
| -------- | -------- |
|  event | this event is generated after card-receiver is found (do not change this option if you are unsure of your actions) |
|  action name | name of the initial action which brings about the script (do not change this option if you are unsure of your actions) |
|  balance check data | parameters of balance check |
|  minimum amount | minimum amount of money on card account to enable the card to perform as a receiver of money. When zero indexes is set up balance check will not be performed. |
|  number pattern | regular expression, which defines number of card-receiver |

### Balance check up parameters

![tf_balance_check](tf-find-dst-card-action.assets/tf_balance_check.png)

The USSD field requires specifying the combination for balance check up .
The next field must be filled up with a regular expression to analyze the answer from the operator.

 

