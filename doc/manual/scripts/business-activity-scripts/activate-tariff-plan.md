## activate tariff plan

Given script is designed to set the end date of tariff plan.

![activate-tariff-plan](activate-tariff-plan.assets/activate-tariff-plan.png)

### Parameters

| name| description|
| -------- | -------- |
|  **event** | event to set the end date of tariff plan |
|  **event on true** | event on successful date set |
|  **tariff plan days** | tariff plan days |
|  **event on false** | Event on failed date set |
