## check calls duration

The task of this script is to ensure the card performs an action on the quantifier of minutes spent in the system (taken from Sim-Servers in the column **Calls duration**)

![check_calls_duration](check-calls-duration.assets/check_calls_duration.png)

### Parameters

| name| description|
| -------- | -------- |
|  event | initial event essential for script activation |
|  duration bounds | duration frames for minutes, which the event will be generated on|
|  event on less | event generated if the minute balance is less than we assign |
|  event on more | event generated if the minute balance is more than we assign |
