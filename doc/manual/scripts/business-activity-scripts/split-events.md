## split events

Given script allows generating several events in response to initiating an event.

![split_events](split-events.assets/split_events.png)

| name| description|
| -------- | -------- |
|  lock on failure |need of locking the card on unsuccessful script performance  |
|  initial event | event which initiates the script's work |
|  time limit | maximum time of script performance|
|  event | generated event (can be several) |
 

