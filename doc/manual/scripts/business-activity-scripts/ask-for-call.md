## ask for call
 
Given script requests for an incoming call from the random card in a system. Given script operates in conjunction with call action in **Action Provider Script**.

![ask_for_call](ask-for-call.assets/ask_for_call.png)

### Parameters

| name| definition|
| -------- | -------- |
|  event | event on which call will be generated |
|  action | action name on which call will occur |
|  lock on fail | lock current card on script processing fail |
|  event on success |event on script processing success|
|  event on failure |event on script processing fail|
|  execute time | timeout script execution, after which the script will be canceled|

A concept of work is similar to ExecuteAction.

