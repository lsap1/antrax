## generate event after call

Given script is designed for the performance of any action after a particular quantity of calls.

![g_e_after_call](generate-event-after-call.assets/g_e_after_call.png)

### Parameters

| name| description|
| -------- | -------- |
|  **calls limit  ** | the quantity of calls from period, after which the **event** is performed |
|  **event** | event essential to perform |
|  **latch limit** | maximal quantity of events the script can perform |

