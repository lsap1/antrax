## Hardware modules(2U, 3U)
Antrax hardware modules include SIM-module, GSM-module, sub rack.

### GSM module

**GSM** **module** is a hardware module of Antrax system. Software  modules interact with GSM module through a stack of network protocols UDP/IP on the basis of Ethernet by means of sub rack which acts as a switchboard.

![gsmb4-111](hardware-modules.assets/gsmb4-111.jpg)

![gsmb4-22](hardware-modules.assets/gsmb4-22.jpg)

Basic characteristics of GSM modules:

  * 2 GSM channels, depending on the model can support the following frequency bands:
    * Siemens TC35i module supports the following frequency bands: GSM 900/1800
    * Siemens MC55i module supports the following frequency bands: GSM 850/900/1800/1900
    * Telit GL865-QUAD module supports the following frequency bands: GSM 850/900/1800/1900

**Listed modules works only with second-generation wireless telephone technology - 2G**

    * GSM 900/1800
    * GSM 850/900/1800/1900
  * 2 built-in SIM emulators
  * IMEI changing
  * GPRS supported
  * Possibility of hot-swap in the sub rack

Overall dimensions of ther module in millimeters:

![gsm_blue](hardware-modules.assets/gsm_blue.jpg)

### SIM module

**SIM module** is a hardware component of Antrax system. Software modules interact with SIM module through a stack of network protocols UDP/IP on the basis of Ethernet by means of sub rack, which acts as a switchboard.

![simb3](hardware-modules.assets/simb3.jpg)

![simb3-1](hardware-modules.assets/simb3-1.jpg)

![simb3-2](hardware-modules.assets/simb3-2.jpg)

Main characteristics of SIM module:

  * The independent control of 20 SIM cards of ISO7816 standard
  * Support of cards' voltage 1,8V and 3,3V
  * Possibility of hot-swap in the sub rack

Overall dimensions of the module in millimeters:

![sim_blue](hardware-modules.assets/sim_blue.jpg)

### Sub rack

![rack1](hardware-modules.assets/rack1.jpg)

 **Sub rack** is a hardware module of Antrax system. Software modules do not interact with **sub rack**. **Sub rack** performs the passive function in the system, providing power and acting as a switchboard on the basis of Ethernet for GSM boards and/or SIM boards. An auxiliary function of **sub rack** is cooling of GSM boards and SIM boards with the help of air blower.

Basic characteristics:

  * 19" 3U
  * 15 GSM or SIM boards
  * Power supply: 100-240V AC, 50/60Hz
  * 2 x Ethernet 10/100 Base-T RJ-45

Overall dimensions of the module in millimeters:

![rack1_size](hardware-modules.assets/rack1_size.jpg)

15 boards of various types can be set up in **sub rack**. For example:

  * 15 GSM boards

![rack15gsm](hardware-modules.assets/rack15gsm.jpg)

  * 15 SIM boards

![rack15gsm_sim](hardware-modules.assets/rack15gsm_sim.jpg)

  * 7 GSM boards and 8 SIM boards

![rack78](hardware-modules.assets/rack78.jpg)

  * 1 GSM board and 1 SIM board

![rack11](hardware-modules.assets/rack11.jpg)