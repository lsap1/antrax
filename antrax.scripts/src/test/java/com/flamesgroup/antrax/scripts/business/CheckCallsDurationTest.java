/*
 * Copyright (C) 2017 Flames Group SIA
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.flamesgroup.antrax.scripts.business;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import com.flamesgroup.antrax.helper.business.GenericEvent;
import com.flamesgroup.antrax.scripts.utils.TimeInterval;
import com.flamesgroup.antrax.storage.commons.impl.SimData;
import com.flamesgroup.device.gsmb.atengine.http.HTTPResponse;
import org.junit.Test;

import java.io.Serializable;
import java.util.concurrent.atomic.AtomicReference;

public class CheckCallsDurationTest {

  @Test
  public void testCheckLessCallsDurationOnUncheckedEvent() throws Exception {
    CheckCallsDuration script = new CheckCallsDuration();
    script.setEvent("checkCallsDuration");
    script.setEventOnLess("eventOnLess");
    script.setDurationBounds(new TimeInterval(30000, 40000));

    script.handleGenericEvent("checkCallsDuration", GenericEvent.EventType.UNCHECKED);
    assertTrue(script.shouldStartBusinessActivity());

    script.invokeBusinessActivity(new RegisteredInGSMChannelAdapter() {

      @Override
      public SimData getSimData() {
        return new SimData().setCallDuration(20000);
      }

      @Override
      public HTTPResponse sendHTTPRequest(final String apn, final String url, final String caCertificate) throws Exception {
        return null;
      }

      @Override
      public void fireGenericEvent(final String event, final Serializable... args) {
        assertEquals("eventOnLess", event);
      }

    });
  }

  @Test
  public void testCheckMoreCallsDurationOnUncheckedEvent() throws Exception {
    CheckCallsDuration script = new CheckCallsDuration();
    script.setEvent("checkCallsDuration");
    script.setEventOnMore("eventOnMore");
    script.setDurationBounds(new TimeInterval(30000, 40000));

    script.handleGenericEvent("checkCallsDuration", GenericEvent.EventType.UNCHECKED);
    assertTrue(script.shouldStartBusinessActivity());

    script.invokeBusinessActivity(new RegisteredInGSMChannelAdapter() {

      @Override
      public SimData getSimData() {
        return new SimData().setCallDuration(50000);
      }

      @Override
      public HTTPResponse sendHTTPRequest(final String apn, final String url, final String caCertificate) throws Exception {
        return null;
      }

      @Override
      public void fireGenericEvent(final String event, final Serializable... args) {
        assertEquals("eventOnMore", event);
      }

    });
  }

  @Test
  public void testCheckEqualCallsDurationOnUncheckedEvent() throws Exception {
    CheckCallsDuration script = new CheckCallsDuration();
    script.setEvent("checkCallsDuration");
    script.setDurationBounds(new TimeInterval(30000, 30000));

    script.handleGenericEvent("checkCallsDuration", GenericEvent.EventType.UNCHECKED);
    assertTrue(script.shouldStartBusinessActivity());

    final AtomicReference<Boolean> fired = new AtomicReference<>(Boolean.FALSE);
    script.invokeBusinessActivity(new RegisteredInGSMChannelAdapter() {

      @Override
      public SimData getSimData() {
        return new SimData().setCallDuration(30000);
      }

      @Override
      public HTTPResponse sendHTTPRequest(final String apn, final String url, final String caCertificate) throws Exception {
        return null;
      }

      @Override
      public void fireGenericEvent(final String event, final Serializable... args) {
        fired.set(Boolean.TRUE);
      }

    });
    assertFalse(fired.get());
  }

}
