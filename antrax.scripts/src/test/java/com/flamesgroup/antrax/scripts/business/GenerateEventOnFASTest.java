/*
 * Copyright (C) 2017 Flames Group SIA
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.flamesgroup.antrax.scripts.business;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import com.flamesgroup.antrax.automation.businesscripts.RegisteredInGSMChannel;
import com.flamesgroup.antrax.scripts.utils.TimePeriod;
import com.flamesgroup.device.gsmb.atengine.http.HTTPResponse;
import mockit.Expectations;
import mockit.Mocked;
import mockit.integration.junit4.JMockit;
import org.junit.Test;
import org.junit.runner.RunWith;

import java.io.Serializable;

@RunWith(JMockit.class)
public class GenerateEventOnFASTest {

  @Mocked
  private RegisteredInGSMChannel channel;

  @Test
  public void testShouldNotStartActivityOnFasLessLimit() throws Exception {
    final GenerateEventOnFAS script = new GenerateEventOnFAS();
    script.setLockOnFailure(true);
    script.setLimit(1);
    assertFalse(script.shouldStartBusinessActivity());
  }

  @Test
  public void testEventSequence() throws Exception {
    GenerateEventOnFAS script = new GenerateEventOnFAS();
    script.setLockOnFailure(false);
    script.setWaitEventTimeout(new TimePeriod(200));
    script.setEvent("event1");
    script.setLimit(1);
    script.handleFAS();
    script.handleCallEnd(1);
    assertTrue(script.shouldStartBusinessActivity());


    new Expectations() {{
      channel.fireGenericEvent(withEqual("event1"), withAny(Serializable.class), withAny(Serializable.class), withAny(Serializable.class));
    }};

    script.invokeBusinessActivity(channel);
    Thread.sleep(50);
    script.event.respondSuccess(getChannel(script));
    assertTrue(script.shouldStartBusinessActivity());
    script.invokeBusinessActivity(channel);
    assertFalse(script.shouldStartBusinessActivity());
  }

  @Test
  public void testLockRespondSuccess() throws Exception {
    final GenerateEventOnFAS script = new GenerateEventOnFAS();
    script.setLockOnFailure(true);
    script.setWaitEventTimeout(new TimePeriod(1000000));
    script.setEvent("event1");
    script.setLimit(1);
    script.handleFAS();
    script.handleCallEnd(1);
    assertTrue(script.shouldStartBusinessActivity());

    new Expectations() {{
      channel.fireGenericEvent(withEqual("event1"), withAny(Serializable.class), withAny(Serializable.class), withAny(Serializable.class));
    }};

    new Thread() {
      @Override
      public void run() {
        try {
          Thread.sleep(50);
          script.event.respondSuccess(getChannel(script));
        } catch (InterruptedException e) {
        }
      }

    }.start();
    script.invokeBusinessActivity(channel);
    Thread.sleep(100);
    assertTrue(script.shouldStartBusinessActivity());
    script.invokeBusinessActivity(channel);
    assertFalse(script.shouldStartBusinessActivity());
  }

  @Test
  public void testLockRespondFail() throws Exception {
    final GenerateEventOnFAS script = new GenerateEventOnFAS();
    script.setLockOnFailure(true);
    script.setWaitEventTimeout(new TimePeriod(1000000));
    script.setEvent("event1");
    script.setLimit(1);
    script.handleFAS();
    script.handleCallEnd(1);
    assertTrue(script.shouldStartBusinessActivity());

    new Expectations() {{
      channel.fireGenericEvent(withEqual("event1"), withAny(Serializable.class), withAny(Serializable.class), withAny(Serializable.class));
      channel.lock(withEqual("Locked: event event1 failed cause reason"));
    }};

    new Thread() {
      @Override
      public void run() {
        try {
          Thread.sleep(50);
          script.event.respondFailure(getChannel(script), "reason");
        } catch (InterruptedException e) {
        }
      }
    }.start();
    script.invokeBusinessActivity(channel);
    Thread.sleep(100);
    assertTrue(script.shouldStartBusinessActivity());
    script.invokeBusinessActivity(channel);
    assertFalse(script.shouldStartBusinessActivity());
  }

  private RegisteredInGSMChannelAdapter getChannel(final GenerateEventOnFAS script) {
    return new RegisteredInGSMChannelAdapter() {
      @Override
      public void fireGenericEvent(final String event, final Serializable... args) {
        script.handleGenericEvent(event, args);
      }

      @Override
      public HTTPResponse sendHTTPRequest(final String apn, final String url, final String caCertificate) throws Exception {
        return null;
      }
    };
  }

}
