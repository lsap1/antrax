/*
 * Copyright (C) 2017 Flames Group SIA
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.flamesgroup.antrax.scripts.business;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

import com.flamesgroup.antrax.helper.business.GenericEvent;
import com.flamesgroup.antrax.scripts.utils.TimePeriod;
import com.flamesgroup.device.gsmb.atengine.http.HTTPResponse;
import org.junit.Test;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.atomic.AtomicReference;

public class SplitEventTest {

  @Test
  public void testNoBusinessActivityAtStart() {
    SplitEvent script = new SplitEvent();
    assertFalse(script.shouldStartBusinessActivity());
  }

  @Test
  public void testRequiresBAAfterEvent() {
    SplitEvent script = createInitializedScript();
    assertTrue(script.shouldStartBusinessActivity());
  }

  private SplitEvent createInitializedScript() {
    SplitEvent script = new SplitEvent();
    script.setInitialEvent("initialEvent");
    sendInitialEvent(script);
    return script;
  }

  private void sendInitialEvent(final SplitEvent script) {
    script.handleGenericEvent("initialEvent", GenericEvent.EventType.CHECKED, "success", "failure");
  }

  @Test
  public void testRespondWithSuccessOnNoEvents() throws Exception {
    SplitEvent script = createInitializedScript();
    final AtomicReference<GenericEvent> firstEvent = new AtomicReference<>();
    script.invokeBusinessActivity(new RegisteredInGSMChannelAdapter() {
      @Override
      public void fireGenericEvent(final String event, final Serializable... args) {
        firstEvent.set(GenericEvent.wrapEvent(event, args));
      }

      @Override
      public HTTPResponse sendHTTPRequest(final String apn, final String url, final String caCertificate) throws Exception {
        return null;
      }
    });
    assertNotNull(firstEvent.get());
    assertEquals("success", firstEvent.get().getEvent());
    assertFalse(script.shouldStartBusinessActivity());
  }

  @Test
  public void testSendEvent() throws Exception {
    SplitEvent script = createInitializedScript();
    script.addEvent("event");

    checkSingleEventFullCycle(script);
  }

  private void checkSingleEventFullCycle(final SplitEvent script) throws Exception {
    final AtomicReference<GenericEvent> firstEvent = new AtomicReference<>();
    script.invokeBusinessActivity(new RegisteredInGSMChannelAdapter() {
      @Override
      public void fireGenericEvent(final String event, final Serializable... args) {
        firstEvent.set(GenericEvent.wrapEvent(event, args));
      }

      @Override
      public HTTPResponse sendHTTPRequest(final String apn, final String url, final String caCertificate) throws Exception {
        return null;
      }
    });
    assertNotNull(firstEvent.get());
    assertEquals("event", firstEvent.get().getEvent());
    assertFalse(script.shouldStartBusinessActivity());
  }

  @Test
  public void testSendSeveralEvents() throws Exception {
    SplitEvent script = createInitializedScript();
    initializeForFullCycle(script);

    final List<GenericEvent> eventsList = new ArrayList<>();
    RegisteredInGSMChannelAdapter channel = createChannel(eventsList);

    checkFullCycle(script, eventsList, channel);
    assertFalse(script.shouldStartBusinessActivity());
  }

  @Test
  public void testScriptResets() throws Exception {
    SplitEvent script = createInitializedScript();
    initializeForFullCycle(script);

    final List<GenericEvent> eventsList = new ArrayList<>();
    RegisteredInGSMChannelAdapter channel = createChannel(eventsList);
    checkFullCycle(script, eventsList, channel);
    assertFalse(script.shouldStartBusinessActivity());
    eventsList.clear();
    sendInitialEvent(script);
    checkFullCycle(script, eventsList, channel);
    assertFalse(script.shouldStartBusinessActivity());
  }

  @Test
  public void testScriptRemembersAllInitialEvents() throws Exception {
    SplitEvent script = createInitializedScript();
    initializeForFullCycle(script);
    sendInitialEvent(script);

    final List<GenericEvent> eventsList = new ArrayList<>();
    RegisteredInGSMChannelAdapter channel = createChannel(eventsList);
    checkFullCycle(script, eventsList, channel);
    eventsList.clear();
    checkFullCycle(script, eventsList, channel);
    assertFalse(script.shouldStartBusinessActivity());
  }

  @Test
  public void testEventFailure() throws Exception {
    SplitEvent script = createInitializedScript();
    script.addEvent("event");
    final List<GenericEvent> eventsList = new ArrayList<>();
    RegisteredInGSMChannelAdapter channel = createChannel(eventsList);
    script.invokeBusinessActivity(channel);
    script.handleGenericEvent(eventsList.get(0).getErrorResponce(), "reason");
    assertTrue(script.shouldStartBusinessActivity());
    script.invokeBusinessActivity(channel);
    assertFalse(script.shouldStartBusinessActivity());
    assertEquals("failure", eventsList.get(1).getEvent());

    sendInitialEvent(script);
    checkSingleEventFullCycle(script);
  }

  @Test
  public void testEventTimedOut() throws Exception {
    SplitEvent script = createInitializedScript();
    script.addEvent("event");
    script.setEventExecutionTimeLimit(new TimePeriod(TimePeriod.inMillis(200)));
    final List<GenericEvent> eventsList = new ArrayList<>();
    RegisteredInGSMChannelAdapter channel = createChannel(eventsList);
    script.invokeBusinessActivity(channel);
    assertFalse(script.shouldStartBusinessActivity());
    Thread.sleep(220);
    assertTrue(script.shouldStartBusinessActivity());
    script.invokeBusinessActivity(channel);
    assertFalse(script.shouldStartBusinessActivity());
    assertEquals("failure", eventsList.get(1).getEvent());

    sendInitialEvent(script);
    checkSingleEventFullCycle(script);
  }

  private RegisteredInGSMChannelAdapter createChannel(final List<GenericEvent> eventsList) {
    return new RegisteredInGSMChannelAdapter() {
      @Override
      public void fireGenericEvent(final String event, final Serializable... args) {
        eventsList.add(GenericEvent.wrapEvent(event, args));
      }

      @Override
      public HTTPResponse sendHTTPRequest(final String apn, final String url, final String caCertificate) throws Exception {
        return null;
      }
    };
  }

  private void initializeForFullCycle(final SplitEvent script) {
    script.addEvent("firstEvent");
    script.addEvent("secondEvent");
    script.addEvent("thirdEvent");
  }

  private void checkFullCycle(final SplitEvent script, final List<GenericEvent> eventsList, final RegisteredInGSMChannelAdapter channel) throws Exception {
    script.invokeBusinessActivity(channel);
    assertEquals("firstEvent", eventsList.get(0).getEvent());
    assertFalse(script.shouldStartBusinessActivity());
    script.handleGenericEvent(eventsList.get(0).getSuccessResponce());
    assertTrue(script.shouldStartBusinessActivity());

    script.invokeBusinessActivity(channel);
    assertTrue(script.shouldStartBusinessActivity());
    script.invokeBusinessActivity(channel);
    assertEquals("secondEvent", eventsList.get(1).getEvent());
    assertFalse(script.shouldStartBusinessActivity());
    script.handleGenericEvent(eventsList.get(1).getSuccessResponce());
    assertTrue(script.shouldStartBusinessActivity());

    script.invokeBusinessActivity(channel);
    assertTrue(script.shouldStartBusinessActivity());
    script.invokeBusinessActivity(channel);
    assertEquals("thirdEvent", eventsList.get(2).getEvent());
    assertFalse(script.shouldStartBusinessActivity());
    script.handleGenericEvent(eventsList.get(2).getSuccessResponce());
    assertTrue(script.shouldStartBusinessActivity());

    script.invokeBusinessActivity(channel);
    assertTrue(script.shouldStartBusinessActivity());
    script.invokeBusinessActivity(channel);
    assertEquals("success", eventsList.get(3).getEvent());
  }
}
