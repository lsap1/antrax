/*
 * Copyright (C) 2017 Flames Group SIA
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.flamesgroup.antrax.predictions;

import com.flamesgroup.antrax.automation.predictions.BasePrediction;
import com.flamesgroup.antrax.automation.predictions.Prediction;
import com.flamesgroup.commons.TimeUtils;

public class CallDurationPrediction extends BasePrediction {

  private static final long serialVersionUID = 3432734146746231550L;

  private final long leftDuration;

  public CallDurationPrediction(final long leftDuration) {
    this.leftDuration = leftDuration;
  }

  @Override
  public String toLocalizedString() {
    return TimeUtils.writeTime(leftDuration) + " of call time";
  }

  @Override
  protected boolean canCompareTo(final Prediction other) {
    return other instanceof CallDurationPrediction;
  }

  @Override
  protected int compareTo(final Prediction other) {
    return (int) (leftDuration - ((CallDurationPrediction) other).leftDuration);
  }

}
