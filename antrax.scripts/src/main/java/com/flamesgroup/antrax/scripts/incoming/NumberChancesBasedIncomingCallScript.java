/*
 * Copyright (C) 2017 Flames Group SIA
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.flamesgroup.antrax.scripts.incoming;

import com.flamesgroup.antrax.automation.annotations.Script;
import com.flamesgroup.antrax.automation.annotations.ScriptParam;
import com.flamesgroup.antrax.automation.businesscripts.RegisteredInGSMChannel;
import com.flamesgroup.antrax.automation.listeners.ISimCallHistory;
import com.flamesgroup.antrax.automation.listeners.RegistryAccessListener;
import com.flamesgroup.antrax.automation.listeners.SimCallHistoryListener;
import com.flamesgroup.antrax.automation.listeners.SimDataListener;
import com.flamesgroup.antrax.automation.registry.RegistryEntry;
import com.flamesgroup.antrax.automation.utils.registry.RegistryAccess;
import com.flamesgroup.antrax.storage.commons.impl.SimData;
import com.flamesgroup.unit.PhoneNumber;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.rmi.RemoteException;
import java.util.Random;

@Script(name = "call history chances based", doc = "Uses chances to determine whether to accept or drop incoming calls if sim already called to incoming number")
public class NumberChancesBasedIncomingCallScript extends ChancesBasedIncomingCallScriptBase implements SimCallHistoryListener, SimDataListener, RegistryAccessListener {

  private static final Logger logger = LoggerFactory.getLogger(NumberChancesBasedIncomingCallScript.class);

  private static final long serialVersionUID = -6428393816512955280L;

  private static final int MAX_SMS_TEMPLATES_READ_COUNT = 1024;

  private SimData simData;

  private transient ISimCallHistory simCallHistory;
  private transient RegistryAccess registryAccess;

  private String smsRegistryPath = "sms.templates";

  private boolean droppedBySimCallHistory;
  private PhoneNumber incomingNumber;

  @ScriptParam(name = "sms path in registry", doc = "path to the registry keys, which contains text for SMS")
  public void setSmsRegistryPath(final String path) {
    this.smsRegistryPath = path;
  }

  public String getSmsRegistryPath() {
    return smsRegistryPath;
  }

  @Override
  public boolean shouldDrop() {
    try {
      if (simCallHistory.containNumber(simData.getUid(), incomingNumber)) {
        return super.shouldDrop();
      } else {
        droppedBySimCallHistory = true;
        return true;
      }
    } catch (RemoteException e) {
      logger.warn("[{}] - can't check incoming number", this, e);
      return true;
    }
  }

  @Override
  public void handleIncomingCall(final PhoneNumber number) {
    this.incomingNumber = number;
    super.handleIncomingCall(number);
  }

  @Override
  public void setSimCallHistory(final ISimCallHistory simCallHistory) {
    this.simCallHistory = simCallHistory;
  }

  @Override
  public void dropAction(final RegisteredInGSMChannel channel) throws Exception {
    super.dropAction(channel);
    if (droppedBySimCallHistory) {
      droppedBySimCallHistory = false;
      if (incomingNumber.isPrivate()) {
        logger.info("[{}] - can't send SMS to private number, so only drop incoming call");
      } else {
        channel.sendSMS(incomingNumber, generateText());
      }
    }
  }

  @Override
  public void setSimData(final SimData simData) {
    this.simData = simData;
  }

  @Override
  public void setRegistryAccess(final RegistryAccess registry) {
    this.registryAccess = registry;
  }

  private String generateText() {
    RegistryEntry[] entries = registryAccess.listEntries(smsRegistryPath, MAX_SMS_TEMPLATES_READ_COUNT);
    if (entries.length == 0) {
      throw new IllegalStateException("There is no any template under register key = " + smsRegistryPath);
    }
    int index = new Random(System.currentTimeMillis()).nextInt(entries.length);
    return entries[index].getValue();
  }

}
