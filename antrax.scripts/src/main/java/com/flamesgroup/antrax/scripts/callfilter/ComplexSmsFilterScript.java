/*
 * Copyright (C) 2017 Flames Group SIA
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.flamesgroup.antrax.scripts.callfilter;

import com.flamesgroup.antrax.automation.annotations.Script;
import com.flamesgroup.antrax.automation.annotations.ScriptParam;
import com.flamesgroup.antrax.automation.annotations.StateField;
import com.flamesgroup.antrax.automation.listeners.ActivityListener;
import com.flamesgroup.antrax.automation.listeners.ActivityPeriodListener;
import com.flamesgroup.antrax.automation.listeners.GenericEventListener;
import com.flamesgroup.antrax.automation.listeners.SMSListener;
import com.flamesgroup.antrax.automation.listeners.SimDataListener;
import com.flamesgroup.antrax.automation.scripts.SmsFilterScript;
import com.flamesgroup.antrax.automation.statefulscripts.ScriptSaver;
import com.flamesgroup.antrax.automation.statefulscripts.StatefullScript;
import com.flamesgroup.antrax.helper.activity.api.SimpleActivityScript;
import com.flamesgroup.antrax.scripts.utils.ActivityScriplet;
import com.flamesgroup.antrax.scripts.utils.scriplet.ScripletBlock;
import com.flamesgroup.antrax.scripts.utils.scriplet.ScripletFactory;
import com.flamesgroup.antrax.storage.commons.GSMGroup;
import com.flamesgroup.antrax.storage.commons.impl.SimData;
import com.flamesgroup.unit.PhoneNumber;
import org.slf4j.LoggerFactory;

import java.io.Serializable;

@Script(name = "complex sms filter", doc = "filters sms numbers by both pattern and advanced rules, which can be selected from set")
public class ComplexSmsFilterScript extends SmsFilterBase implements SmsFilterScript, SMSListener, StatefullScript, SimDataListener, ActivityPeriodListener, ActivityListener, GenericEventListener {

  private static final long serialVersionUID = 907618557920959301L;

  @StateField
  private SimpleActivityScript rule;

  private ScriptSaver scriptSaver;

  private SimData simData;

  public ComplexSmsFilterScript() {
    super(LoggerFactory.getLogger(ComplexSmsFilterScript.class));
    scriptSaver = new ScriptSaver(super.getScriptSaver());
  }

  @ScriptParam(name = "rule", doc = "Additional rule which controls whether call should be accepted")
  public void setRule(final ActivityScriplet scriplet) throws Exception {
    this.rule = scriplet.createActivityScript();
    this.scriptSaver = new ScriptSaver(rule.getScriptSaver(), super.getScriptSaver());
  }

  public ActivityScriplet getRule() {
    return new ScripletBlock(ScripletFactory.createScripletFor(ScripletFactory.listScripts()[0]));
  }

  @Override
  public void handleActivityStarted(final GSMGroup gsmGroup) {
    rule.handleActivityStarted(gsmGroup);
  }

  @Override
  public void handleActivityEnded() {
    rule.handleActivityEnded();
  }

  @Override
  public void handlePeriodStart() {
    rule.handlePeriodStart();
  }

  @Override
  public void handlePeriodEnd() {
    rule.handlePeriodEnd();
  }

  @Override
  public void handleIncomingSMS(final String phoneNumber, final String text) {
    simData.setIncomingSMSCount(simData.getIncomingSMSCount() + 1);
    super.handleIncomingSMS(phoneNumber, text);
  }

  @Override
  public void handleSentSMS(final String phoneNumber, final String text, final int parts) {
    simData.setSuccessOutgoingSmsCount(simData.getSuccessOutgoingSmsCount() + parts);
    super.handleSentSMS(phoneNumber, text, parts);
  }

  @Override
  public void handleFailSentSMS(final int parts) {
    super.handleFailSentSMS(parts);
  }

  @Override
  public void setSimData(final SimData simData) {
    this.simData = simData;
    rule.setSimData(simData);
  }

  @Override
  public boolean isSmsAccepted(final PhoneNumber bPhoneNumber) {
    rule.setSimData(simData);
    return isBNumberMatches(bPhoneNumber) && rule.isActivityAllowed();
  }

  @Override
  public String substituteText(final String originText) {
    return super.baseSubstituteText(originText);
  }

  @Override
  public boolean isAcceptsSmsParts(final int parts) {
    return super.isAcceptsSmsParts(parts);
  }

  @Override
  public ScriptSaver getScriptSaver() {
    return scriptSaver;
  }

  @Override
  public void handleGenericEvent(final String event, final Serializable... args) {
    rule.handleGenericEvent(event, args);
  }

  @Override
  public String toString() {
    return String.format("%s(%s)", getClass().getSimpleName(), rule);
  }

}
