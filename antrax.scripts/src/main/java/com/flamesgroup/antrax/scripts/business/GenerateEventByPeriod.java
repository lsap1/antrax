/*
 * Copyright (C) 2017 Flames Group SIA
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.flamesgroup.antrax.scripts.business;

import static com.flamesgroup.antrax.scripts.utils.GeneratePeriodHelper.generatePeriod;

import com.flamesgroup.antrax.automation.annotations.Script;
import com.flamesgroup.antrax.automation.annotations.ScriptParam;
import com.flamesgroup.antrax.automation.annotations.StateField;
import com.flamesgroup.antrax.automation.businesscripts.RegisteredInGSMChannel;
import com.flamesgroup.antrax.automation.listeners.ActivityListener;
import com.flamesgroup.antrax.automation.listeners.GenericEventListener;
import com.flamesgroup.antrax.automation.scripts.BusinessActivityScript;
import com.flamesgroup.antrax.automation.statefulscripts.ScriptSaver;
import com.flamesgroup.antrax.automation.statefulscripts.StatefullScript;
import com.flamesgroup.antrax.helper.business.GenericEvent;
import com.flamesgroup.antrax.scripts.utils.Chance;
import com.flamesgroup.antrax.scripts.utils.TimePeriod;
import com.flamesgroup.antrax.storage.commons.GSMGroup;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.Serializable;
import java.text.SimpleDateFormat;

@Script(name = "generate event randomly by period", doc = "generates events randomly; specified number of events generates during specified time interval")
public class GenerateEventByPeriod implements BusinessActivityScript, GenericEventListener, StatefullScript, ActivityListener {

  private static final long serialVersionUID = 5347033546402471914L;

  private static final Logger logger = LoggerFactory.getLogger(GenerateEventByPeriod.class);

  private static final String FAILURE_RESPONCE_PREFIX = " failure ";
  private static final String SUCCESS_RESPONCE_PREFIX = " successed ";

  private final SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");

  @StateField
  private boolean lock = false;
  @StateField
  private long periodBegining;
  @StateField
  private int events;
  @StateField
  private long nextPeriod;

  private GenericEvent sendEvent = GenericEvent.checkedEvent("randomEvent", successedResponse("randomEvent"), failureResponse("randomEvent"));

  private boolean lockOnFailure;
  private Chance eventChance = new Chance(60);
  private TimePeriod period = new TimePeriod(TimePeriod.inHours(1));
  private int eventLimit = 5;

  @StateField
  private volatile String lockReason;

  private final ScriptSaver saver = new ScriptSaver();

  @ScriptParam(name = "send event", doc = "this event generates randomly")
  public void setSendEvent(final String event) {
    this.sendEvent = GenericEvent.checkedEvent(event, successedResponse(event), failureResponse(event));
  }

  public String getSendEvent() {
    return sendEvent.getEvent();
  }

  @ScriptParam(name = "lock on failure", doc = "if checked, sim card will be locked with message 'failedToExecute %event%'")
  public void setLockOnFailure(final boolean lock) {
    this.lockOnFailure = lock;
  }

  public boolean getLockOnFailure() {
    return lockOnFailure;
  }

  @ScriptParam(name = "event chance", doc = "chance of event generation in percent")
  public void setEventChance(final Chance eventChance) {
    this.eventChance = eventChance;
  }

  public Chance getEventChance() {
    return eventChance;
  }

  @ScriptParam(name = "event generation period", doc = "period in which eventLimit parameter will have an effect")
  public void setPeriod(final TimePeriod period) {
    this.period = period;
  }

  public TimePeriod getPeriod() {
    return period;
  }

  @ScriptParam(name = "event limit", doc = "limits numbers of generated event in specified period")
  public void setEventLimit(final int eventLimit) {
    this.eventLimit = eventLimit;
  }

  public int getEventLimit() {
    return eventLimit;
  }

  @Override
  public String describeBusinessActivity() {
    return "generates event randomly by period";
  }

  @Override
  public void invokeBusinessActivity(final RegisteredInGSMChannel channel) throws Exception {
    if (lock) {
      try {
        if (lockReason != null) {
          channel.lock(lockReason);
        } else {
          channel.lock("failedToExecute " + sendEvent.getEvent());
        }
      } finally {
        lock = false;
        lockReason = null;
      }
    } else {
      sendEvent.fireEvent(channel);
      events++;
      nextPeriod = generatePeriod(periodBegining, periodBegining + period.getPeriod(), System.currentTimeMillis(), eventLimit, events);
      logger.debug("[{}] - next period: {}", this, format.format(nextPeriod));
    }
    saver.save();
  }

  @Override
  public boolean shouldStartBusinessActivity() {
    return lock || shouldGenerateEvent();
  }

  private boolean shouldGenerateEvent() {
    long currentTime = System.currentTimeMillis();
    if (currentTime - periodBegining > period.getPeriod()) {
      periodBegining = currentTime;
      events = 0;
      logger.debug("[{}] - resetting events in period: begining period: {}", this, format.format(periodBegining));
      nextPeriod = generatePeriod(periodBegining, periodBegining + period.getPeriod(), currentTime, eventLimit, events);
      logger.debug("[{}] - next period: {}", this, format.format(nextPeriod));
      saver.save();
    }

    if (currentTime < nextPeriod || events >= eventLimit) {
      return false;
    }

    boolean chance = eventChance.countChance();
    if (!chance) { // if chance false wait next period to try generate event
      events++;
      nextPeriod = generatePeriod(periodBegining, periodBegining + period.getPeriod(), currentTime, eventLimit, events);
      logger.debug("[{}] - next period: {}", this, format.format(nextPeriod));
      saver.save();
    }
    return chance;
  }

  @Override
  public void handleGenericEvent(final String event, final Serializable... args) {
    if (this.sendEvent.isFailureResponce(event, args)) {
      if (lockOnFailure) {
        lock = true;
        lockReason = sendEvent.getFailReason(event, args);
        saver.save();
      }
    }
  }

  @Override
  public void handleActivityStarted(final GSMGroup gsmGroup) {
    long currentTime = System.currentTimeMillis();
    if (currentTime - periodBegining <= period.getPeriod() && currentTime >= nextPeriod) {
      nextPeriod = generatePeriod(periodBegining, periodBegining + period.getPeriod(), currentTime, eventLimit, events);
      logger.debug("[{}] - current time: {} exceed next period time, so next period: {}", this, format.format(currentTime), format.format(nextPeriod));
      saver.save();
    }
  }

  @Override
  public void handleActivityEnded() {
  }

  private String successedResponse(final String event) {
    return event + SUCCESS_RESPONCE_PREFIX + Math.random() + System.nanoTime();
  }

  private String failureResponse(final String event) {
    return event + FAILURE_RESPONCE_PREFIX + Math.random() + System.nanoTime();
  }

  @Override
  public ScriptSaver getScriptSaver() {
    return saver;
  }

}
