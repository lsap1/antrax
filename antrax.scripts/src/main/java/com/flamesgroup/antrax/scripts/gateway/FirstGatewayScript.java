/*
 * Copyright (C) 2017 Flames Group SIA
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.flamesgroup.antrax.scripts.gateway;

import com.flamesgroup.antrax.automation.annotations.Script;
import com.flamesgroup.antrax.automation.annotations.StateField;
import com.flamesgroup.antrax.automation.listeners.ActivityListener;
import com.flamesgroup.antrax.automation.listeners.SessionListener;
import com.flamesgroup.antrax.automation.predictions.Prediction;
import com.flamesgroup.antrax.automation.scripts.GatewaySelectorScript;
import com.flamesgroup.antrax.automation.statefulscripts.ScriptSaver;
import com.flamesgroup.antrax.automation.statefulscripts.StatefullScript;
import com.flamesgroup.antrax.predictions.GatewaysPredictionFactory;
import com.flamesgroup.antrax.storage.commons.GSMGroup;
import com.flamesgroup.antrax.storage.commons.IServerData;

@Script(name = "first gateway", doc = "Accepts only first gateway on which sim card was registered")
public class FirstGatewayScript implements GatewaySelectorScript, StatefullScript, SessionListener, ActivityListener {

  private static final long serialVersionUID = 893137855841571361L;

  @StateField
  private IServerData gateway;
  @StateField
  private boolean registered;

  private final ScriptSaver saver = new ScriptSaver();

  @Override
  public boolean isGatewayApplies(final IServerData gateway) {
    return this.gateway != null && this.gateway.equals(gateway);
  }

  @Override
  public Prediction predictNextGateway() {
    if (gateway == null) {
      return GatewaysPredictionFactory.any();
    } else {
      return GatewaysPredictionFactory.anyOf(gateway.getName());
    }
  }

  @Override
  public void handleSessionStarted(final IServerData gateway) {
    if (this.gateway != null) {
      return;
    }

    this.gateway = gateway;
    saver.save();
  }

  @Override
  public void handleSessionEnded() {
    if (registered) {
      return;
    }

    gateway = null;
    saver.save();
  }

  @Override
  public ScriptSaver getScriptSaver() {
    return saver;
  }

  @Override
  public void handleActivityStarted(final GSMGroup gsmGroup) {
    if (registered) {
      return;
    }

    registered = true;
    saver.save();
  }

  @Override
  public void handleActivityEnded() {
  }

}
