/*
 * Copyright (C) 2017 Flames Group SIA
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.flamesgroup.antrax.scripts.business;

import com.flamesgroup.antrax.automation.annotations.Script;
import com.flamesgroup.antrax.automation.annotations.ScriptParam;
import com.flamesgroup.antrax.automation.businesscripts.RegisteredInGSMChannel;
import com.flamesgroup.antrax.automation.listeners.ActivityListener;
import com.flamesgroup.antrax.automation.listeners.GenericEventListener;
import com.flamesgroup.antrax.automation.scripts.BusinessActivityScript;
import com.flamesgroup.antrax.helper.business.EventHelper;
import com.flamesgroup.antrax.helper.business.EventStatus;
import com.flamesgroup.antrax.helper.business.GenericEvent;
import com.flamesgroup.antrax.scripts.utils.TimePeriod;
import com.flamesgroup.antrax.storage.commons.GSMGroup;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.Serializable;

@Script(name = "generate event instantly", doc = "generates event instantly")
public class GenerateEventInstantly implements BusinessActivityScript, GenericEventListener, ActivityListener {

  private static final Logger logger = LoggerFactory.getLogger(GenerateEventInstantly.class);

  private boolean lockOnFinish = true;
  private boolean lockOnFailed;
  private GenericEvent event = GenericEvent.checkedEvent("instantEvent", successedResponse("instantEvent"), failureResponse("instantEvent"));
  private TimePeriod eventTimeout = new TimePeriod(TimePeriod.inMinutes(2));
  private int eventLimit = 100000;

  private volatile int eventCount = 0;

  private volatile EventHelper eventHelper;

  @ScriptParam(name = "lock on finish", doc = "lock card when event processing finished")
  public void setLockOnFinish(final boolean lockOnFinish) {
    this.lockOnFinish = lockOnFinish;
  }

  public boolean getLockOnFinish() {
    return lockOnFinish;
  }

  @ScriptParam(name = "lock on failed", doc = "lock card when some event failed")
  public void setLockOnFailed(final boolean lockOnFailed) {
    this.lockOnFailed = lockOnFailed;
  }

  public boolean getLockOnFailed() {
    return lockOnFailed;
  }

  @ScriptParam(name = "timeout", doc = "maximum time for event to complete")
  public void setEventTimeout(final TimePeriod eventTimeout) {
    this.eventTimeout = eventTimeout;
  }

  public TimePeriod getEventTimeout() {
    return eventTimeout;
  }

  @ScriptParam(name = "event limit", doc = "numbers of events that will be generated per activity")
  public void setEventLimit(final int limit) {
    this.eventLimit = limit;
  }

  public int getEventLimit() {
    return eventLimit;
  }

  @ScriptParam(name = "event", doc = "event to generate")
  public void setEvent(final String event) {
    this.event = GenericEvent.checkedEvent(event, successedResponse(event), failureResponse(event));
  }

  public String getEvent() {
    return event.getEvent();
  }

  @Override
  public String describeBusinessActivity() {
    return "event " + event;
  }

  @Override
  public void invokeBusinessActivity(final RegisteredInGSMChannel channel) throws Exception {
    if (eventHelper == null) {
      eventHelper = new EventHelper(event, eventTimeout.getPeriod());
      logger.debug("[{}] - generate event {} ", this, event);
      eventHelper.generateEvent(channel);
      eventCount++;
    } else {
      if ((eventHelper.getStatus() == EventStatus.FAILED || eventHelper.getStatus() == EventStatus.TIMEOUT) && lockOnFailed) {
        logger.debug("[{}] - {}", this, eventHelper.getFailReason());
        logger.debug("[{}] - going to lock card by failed", this);
        channel.lock(event + " locked by failed");
      } else if (eventHelper.getStatus() == EventStatus.SUCCEED) {
        if (lockOnFinish) {
          channel.lock(event + " successful finished");
        }
      }
      eventHelper = null;
    }
  }

  @Override
  public boolean shouldStartBusinessActivity() {
    if (eventHelper == null) {
      return eventCount < eventLimit;
    } else {
      return eventHelper.isFinished();
    }
  }

  private String successedResponse(final String event) {
    return event + " success " + Math.random() + System.nanoTime();
  }

  private String failureResponse(final String event) {
    return event + " failure " + Math.random() + System.nanoTime();
  }

  @Override
  public void handleGenericEvent(final String event, final Serializable... args) {
    logger.debug("[{}] - caught event {}", this, event);
    if (eventHelper != null) {
      eventHelper.handleEvent(event, args);
    }
  }

  @Override
  public void handleActivityStarted(final GSMGroup gsmGroup) {
    eventHelper = null;
    eventCount = 0;
  }

  @Override
  public void handleActivityEnded() {

  }

}
