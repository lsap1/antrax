/*
 * Copyright (C) 2017 Flames Group SIA
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.flamesgroup.antrax.scripts.callfilter;

import com.flamesgroup.antrax.automation.annotations.ScriptParam;
import com.flamesgroup.antrax.helper.callfilter.PhoneNumberMatcher;
import com.flamesgroup.unit.PhoneNumber;
import org.slf4j.Logger;

public abstract class CallFilterBase extends FilterBase {

  private static final long serialVersionUID = 7411706587865873081L;

  protected volatile String allowedANumber;
  protected volatile String deniedANumber;
  private transient PhoneNumberMatcher aPhoneNumberMatcher;

  public CallFilterBase(final Logger logger) {
    super(logger);
  }

  @ScriptParam(name = "allowed A number", doc = "regular expression which will filter good A numbers")
  public void setAllowedANumber(final String allowedANumber) {
    this.allowedANumber = allowedANumber;
    aPhoneNumberMatcher = null;
  }

  public String getAllowedANumber() {
    return allowedANumber;
  }

  @ScriptParam(name = "denied A number", doc = "regular expression which will filter bad A numbers")
  public void setDeniedANumber(final String deniedANumbers) {
    this.deniedANumber = deniedANumbers;
    aPhoneNumberMatcher = null;
  }

  protected PhoneNumberMatcher getAPhoneNumberMatcher(final PhoneNumber aPhoneNumber) {
    if (aPhoneNumberMatcher == null) {
      try {
        aPhoneNumberMatcher = new PhoneNumberMatcher(deniedANumber, allowedANumber, "");
        //try to substitute phone number to validate replacement string
        aPhoneNumberMatcher.substituteNumber(aPhoneNumber.getValue());
      } catch (Exception e) {
        logger.warn("[{}] - while compiling pattern", this, e);
        aPhoneNumberMatcher = new PhoneNumberMatcher(null, null, null);
      }
    }
    return aPhoneNumberMatcher;
  }

  protected boolean isANumberMatches(final PhoneNumber aPhoneNumber) {
    return getAPhoneNumberMatcher(aPhoneNumber).matches(aPhoneNumber.getValue());
  }

  protected boolean isNumberMatches(final PhoneNumber aPhoneNumber, final PhoneNumber bPhoneNumber) {
    return isANumberMatches(aPhoneNumber) && isBNumberMatches(bPhoneNumber);
  }

}
