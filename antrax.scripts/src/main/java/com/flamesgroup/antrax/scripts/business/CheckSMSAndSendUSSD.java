/*
 * Copyright (C) 2017 Flames Group SIA
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.flamesgroup.antrax.scripts.business;

import com.flamesgroup.antrax.automation.annotations.Script;
import com.flamesgroup.antrax.automation.annotations.ScriptParam;
import com.flamesgroup.antrax.automation.businesscripts.RegisteredInGSMChannel;
import com.flamesgroup.antrax.automation.listeners.GenericEventListener;
import com.flamesgroup.antrax.automation.listeners.SMSListener;
import com.flamesgroup.antrax.automation.scripts.BusinessActivityScript;
import com.flamesgroup.antrax.scripts.utils.TimePeriod;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.Serializable;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.concurrent.atomic.AtomicLong;

@Script(name = "check SMS and send USSD", doc = "analyzes incoming SMS and generates events")
public class CheckSMSAndSendUSSD implements BusinessActivityScript, SMSListener, GenericEventListener {

  private static final Logger logger = LoggerFactory.getLogger(CheckSMSAndSendUSSD.class);

  private String event = "check_sms_and_send_ussd";
  private TimePeriod smsTimeout = new TimePeriod(TimePeriod.inMinutes(1));

  private String codeRegex = ".*(\\d+).*";
  private String sendUssd = "*145*$1#";

  private volatile String smsText;

  private final AtomicBoolean smsWait = new AtomicBoolean();
  private final AtomicLong smsTime = new AtomicLong();

  @ScriptParam(name = "event", doc = "check sms will be started after this event occurs")
  public void setEvent(final String event) {
    this.event = event;
  }

  public String getEvent() {
    return event;
  }

  @ScriptParam(name = "sms timeout", doc = "max time to wait receiving sms")
  public void setSmsTimeout(final TimePeriod eventTimeout) {
    this.smsTimeout = eventTimeout;
  }

  public TimePeriod getSmsTimeout() {
    return smsTimeout;
  }

  @ScriptParam(name = "code regex", doc = "regex for parse code from incoming SMS")
  public void setCodeRegex(final String codeRegex) {
    this.codeRegex = codeRegex;
  }

  public String getCodeRegex() {
    return codeRegex;
  }

  @ScriptParam(name = "ussd for send code", doc = "ussd for send code, '$1' put to the place where must be code from SMS")
  public void setSendUssd(final String sendUssd) {
    this.sendUssd = sendUssd;
  }

  public String getSendUssd() {
    return sendUssd;
  }

  @Override
  public String describeBusinessActivity() {
    return "check SMS and send USSD";
  }

  @Override
  public void invokeBusinessActivity(final RegisteredInGSMChannel channel) throws Exception {
    if (smsText == null) {
      smsTime.set(0);
      logger.debug("[{}] - waiting sms timeout", this);
      return;
    }

    try {
      channel.sendUSSD(smsText.replaceFirst(codeRegex, sendUssd));
    } catch (Exception e) {
      logger.debug("[{}] - while send USSD", this, e);
    } finally {
      smsTime.set(0);
      smsText = null;
    }
  }

  @Override
  public boolean shouldStartBusinessActivity() {
    if (smsText != null) {
      return true;
    } else if (smsWait.get() && System.currentTimeMillis() - smsTime.get() > smsTimeout.getPeriod()) {
      smsWait.set(false);
      return true;
    } else {
      return false;
    }
  }

  @Override
  public void handleIncomingSMS(final String phoneNumber, final String text) {
    if (smsWait.get() && text != null && text.matches(codeRegex)) {
      logger.debug("[{}] - sms from [{}] with text [{}] matches with pattern [{}]", this, phoneNumber, text, codeRegex);
      smsWait.set(false);
      smsText = text;
    }
  }

  @Override
  public void handleSentSMS(final String phoneNumber, final String text, final int parts) {
  }

  @Override
  public void handleFailSentSMS(final int parts) {
  }

  @Override
  public void handleGenericEvent(final String event, final Serializable... args) {
    if (this.event.equals(event)) {
      logger.debug("[{}] - start waiting to receive sms", this);
      smsWait.set(true);
      smsTime.set(System.currentTimeMillis());
    }
  }

}
