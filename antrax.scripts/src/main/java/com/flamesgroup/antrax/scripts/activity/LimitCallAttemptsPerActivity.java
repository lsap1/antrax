/*
 * Copyright (C) 2017 Flames Group SIA
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.flamesgroup.antrax.scripts.activity;

import com.flamesgroup.antrax.automation.annotations.Script;
import com.flamesgroup.antrax.automation.annotations.ScriptParam;
import com.flamesgroup.antrax.helper.activity.SimpleActivityScriptAdapter;
import com.flamesgroup.antrax.scripts.utils.VariableLong;

@Script(name = "limit call attempts", doc = "limiting call attempts per activity")
public class LimitCallAttemptsPerActivity extends SimpleActivityScriptAdapter<com.flamesgroup.antrax.helper.activity.LimitCallAttemptsPerActivity> {

  private static final long serialVersionUID = 7918294888497498316L;

  @ScriptParam(name = "attempts limit", doc = "limit call attempts count interval")
  public void setAttemptsLimit(final VariableLong limit) {
    getAdept().setAttemptsLimit(limit);
  }

  public VariableLong getAttemptsLimit() {
    return new VariableLong(10, 15);
  }

  public LimitCallAttemptsPerActivity() {
    super(new com.flamesgroup.antrax.helper.activity.LimitCallAttemptsPerActivity());
  }

}
