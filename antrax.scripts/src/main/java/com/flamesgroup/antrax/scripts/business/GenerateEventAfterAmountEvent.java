/*
 * Copyright (C) 2017 Flames Group SIA
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.flamesgroup.antrax.scripts.business;

import com.flamesgroup.antrax.automation.annotations.Script;
import com.flamesgroup.antrax.automation.annotations.ScriptParam;
import com.flamesgroup.antrax.automation.annotations.StateField;
import com.flamesgroup.antrax.automation.businesscripts.RegisteredInGSMChannel;
import com.flamesgroup.antrax.automation.listeners.GenericEventListener;
import com.flamesgroup.antrax.automation.scripts.BusinessActivityScript;
import com.flamesgroup.antrax.automation.statefulscripts.ScriptSaver;
import com.flamesgroup.antrax.automation.statefulscripts.StatefullScript;
import com.flamesgroup.antrax.helper.business.GenericEvent;
import com.flamesgroup.antrax.scripts.utils.VariableLong;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.Serializable;
import java.time.LocalDate;
import java.time.ZoneId;

@Script(name = "generate event after amount events", doc = "generate event after amount events")
public class GenerateEventAfterAmountEvent implements BusinessActivityScript, GenericEventListener, StatefullScript {

  private static final long serialVersionUID = -6148277433777454064L;
  private static final Logger logger = LoggerFactory.getLogger(GenerateEventAfterAmountEvent.class);

  @StateField
  private volatile long curLimit = 0;
  @StateField
  private volatile long countEvents = 0;
  @StateField
  private volatile LocalDate prevDay;

  private final ScriptSaver saver = new ScriptSaver();

  private volatile GenericEvent caughtEvent;
  private String event = "event";
  private String generateEvent = "generateEvent";
  private VariableLong limitPerDay = new VariableLong(40, 50);
  private boolean resetCountAday = true;

  @ScriptParam(name = "amount events per day", doc = "amount events per day, after them, generate the event")
  public void setLimitPerDay(final VariableLong limitPerDay) {
    this.limitPerDay = limitPerDay;
  }

  public VariableLong getLimitPerDay() {
    return limitPerDay;
  }

  @ScriptParam(name = "event", doc = "event to count")
  public void setEvent(final String event) {
    this.event = event;
  }

  public String getEvent() {
    return event;
  }

  @ScriptParam(name = "generateEvent", doc = "generate event after amount received events")
  public void setGenerateEvent(final String generateEvent) {
    this.generateEvent = generateEvent;
  }

  public String getGenerateEvent() {
    return generateEvent;
  }

  @ScriptParam(name = "reset count a day", doc = "reset count of events every day")
  public void setResetCountAday(final boolean resetCountAday) {
    this.resetCountAday = resetCountAday;
  }

  public boolean getResetCountAday() {
    return resetCountAday;
  }

  @Override
  public void handleGenericEvent(final String event, final Serializable... args) {
    if (this.event.equals(event)) {
      caughtEvent = GenericEvent.wrapEvent(event, args);
    }
  }

  @Override
  public void invokeBusinessActivity(final RegisteredInGSMChannel channel) throws Exception {
    try {
      if (prevDay == null || LocalDate.now(ZoneId.systemDefault()).isAfter(prevDay)) {
        prevDay = LocalDate.now(ZoneId.systemDefault());
        if (resetCountAday) {
          curLimit = limitPerDay.random();
          countEvents = 0;
        }
      }
      countEvents++;
      saver.save();
      if (curLimit == countEvents) {
        caughtEvent.respondSuccess(channel);
        if (generateEvent != null && !generateEvent.isEmpty()) {
          channel.fireGenericEvent(generateEvent);
        }
      }
    } finally {
      caughtEvent = null;
    }
  }

  @Override
  public boolean shouldStartBusinessActivity() {
    return caughtEvent != null;
  }

  @Override
  public String describeBusinessActivity() {
    return "generate event after amount events";
  }

  @Override
  public ScriptSaver getScriptSaver() {
    return saver;
  }

}
