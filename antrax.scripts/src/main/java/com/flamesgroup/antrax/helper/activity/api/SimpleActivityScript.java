/*
 * Copyright (C) 2017 Flames Group SIA
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.flamesgroup.antrax.helper.activity.api;

import com.flamesgroup.antrax.automation.listeners.ActivityListener;
import com.flamesgroup.antrax.automation.listeners.ActivityPeriodListener;
import com.flamesgroup.antrax.automation.listeners.GenericEventListener;
import com.flamesgroup.antrax.automation.listeners.SessionListener;
import com.flamesgroup.antrax.automation.listeners.SimDataListener;
import com.flamesgroup.antrax.automation.predictions.Prediction;
import com.flamesgroup.antrax.automation.statefulscripts.StatefullScript;

/**
 * Simplified where two triggers (start activity and stop activity)
 * combined in one method {@link #isActivityAllowed()}
 */
public interface SimpleActivityScript extends StatefullScript, GenericEventListener, ActivityPeriodListener, SimDataListener, SessionListener, ActivityListener {
  /**
   * Determines whether activity should be started nor stoped. If it is
   * already started it will be finished on false and vice versa
   *
   * @return true to alow activity and false to forbid it
   */
  boolean isActivityAllowed();

  /**
   * Joins two scripts in one with logical or
   *
   * @param other
   * @return joined scripts
   */
  SimpleActivityScript or(SimpleActivityScript other);

  /**
   * Joins two scrips in one wth logical and
   *
   * @param other
   * @return joined scripts
   */
  SimpleActivityScript and(SimpleActivityScript other);

  Prediction predictEnd();

  Prediction predictStart();

}
