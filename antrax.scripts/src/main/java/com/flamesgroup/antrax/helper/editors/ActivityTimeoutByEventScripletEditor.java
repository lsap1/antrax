/*
 * Copyright (C) 2017 Flames Group SIA
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.flamesgroup.antrax.helper.editors;

import com.flamesgroup.antrax.automation.editors.BasePropertyEditor;
import com.flamesgroup.antrax.helper.editors.components.ActivityTimeoutByEventScripletPanel;
import com.flamesgroup.antrax.scripts.utils.ActivityTimeoutByEventScriplet;

import java.awt.*;

public class ActivityTimeoutByEventScripletEditor extends BasePropertyEditor<ActivityTimeoutByEventScriplet> {

  private final ActivityTimeoutByEventScripletPanel editor = new ActivityTimeoutByEventScripletPanel();

  @Override
  public Class<? extends ActivityTimeoutByEventScriplet> getType() {
    return ActivityTimeoutByEventScriplet.class;
  }

  @Override
  public ActivityTimeoutByEventScriplet getValue() {
    return editor.getActivityTimeoutScriplet();
  }

  @Override
  public void setValue(final ActivityTimeoutByEventScriplet value) {
    editor.setActivityTimeoutScriplet(value);
  }

  @Override
  public Component getEditorComponent() {
    return editor;
  }

}
