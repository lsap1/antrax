/*
 * Copyright (C) 2017 Flames Group SIA
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.flamesgroup.antrax.helper.editors.components;

import com.flamesgroup.antrax.automation.listeners.RegistryAccessListener;
import com.flamesgroup.antrax.automation.utils.registry.RegistryAccess;
import com.flamesgroup.antrax.helper.business.payment.PaymentHelper;
import com.flamesgroup.antrax.helper.business.payment.USSDPaymentHelper;
import com.flamesgroup.antrax.helper.editors.PaymentHelperEditor;
import com.flamesgroup.antrax.scripts.utils.UIComponentRegistryAccess;

import java.awt.*;
import java.util.Arrays;
import java.util.HashSet;
import java.util.Set;
import java.util.regex.Pattern;

import javax.swing.*;

public class USSDBasedPaymentEditorPanel extends JPanel implements PaymentHelperEditor.PaymentHelperProvider, RegistryAccessListener {

  private static final long serialVersionUID = -4129394516426319534L;

  private final JComboBox txtUssd = new JComboBox();
  private final JComboBox txtOperator = new JComboBox();
  private String[] regexesModel = new String[0];
  private final RegexesPanel regexesPanel = new RegexesPanel(200);
  private final JTextArea txtHelp = new JTextArea();
  private UIComponentRegistryAccess registry;

  public USSDBasedPaymentEditorPanel() {
    initGUI();
  }

  @Override
  public Component getEditorComponent() {
    return this;
  }

  @Override
  public void setPaymentHelper(final PaymentHelper paymentHelper) {
    USSDPaymentHelper helper = (USSDPaymentHelper) paymentHelper;
    txtUssd.setSelectedItem(helper.getUSSD());
    txtOperator.setSelectedItem(helper.getOperator());
    regexesPanel.setRegexes(helper.getResponceCheckerRegexes(), regexesModel);
  }

  @Override
  public PaymentHelper getPaymentHelper() {
    if (registry != null) {
      registry.addValue("antrax-scripts.payment.ussd", (String) txtUssd.getSelectedItem());
    }
    String operator = (String) txtOperator.getSelectedItem();
    String ussd = (String) txtUssd.getSelectedItem();
    String[] checkRegexes = regexesPanel.listRegexes();
    if (registry != null) {
      registry.addValue("antrax-scripts.payment.regexes", checkRegexes);
    }
    return new USSDPaymentHelper(operator, ussd, checkRegexes);
  }

  @Override
  public void setRegistryAccess(final RegistryAccess registry) {
    this.registry = new UIComponentRegistryAccess(registry);
    readRegistry();
  }

  private void readRegistry() {
    String[] paths = registry.listPaths(Pattern.compile("(vauchers[.][^.]*)[.].*"));
    Set<String> pathsSet = new HashSet<>(Arrays.asList(paths));
    txtOperator.setModel(new DefaultComboBoxModel(pathsSet.toArray(new String[pathsSet.size()])));
    txtUssd.setModel(new DefaultComboBoxModel(registry.listValues("antrax-scripts.payment.ussd")));
    regexesModel = registry.listValues("antrax-scripts.payment.regexes");
  }

  @Override
  public Class<? extends PaymentHelper> getProvidedHelperClass() {
    return USSDPaymentHelper.class;
  }

  private void initGUI() {
    txtUssd.setEditable(true);
    regexesPanel.setBorder(BorderFactory.createTitledBorder("Responce check regex (***)"));
    setLayout(new BoxLayout(this, BoxLayout.Y_AXIS));
    add(new LabeledPanel("operator (*):", txtOperator));
    add(new LabeledPanel("ussd (**):", txtUssd));
    add(regexesPanel);

    txtHelp.setText(getTextMessage());
    add(txtHelp);
  }

  private String getTextMessage() {
    return "* this name is used to determine registry key where to look for vauchers\n" +
    /*   */"** use $ to mark the place for vaucher. For example: *100*$#\n" +
    /*   */"*** place regexes for checking operators responces here";
  }

}
