/*
 * Copyright (C) 2017 Flames Group SIA
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.flamesgroup.antrax.helper.business.payment;

import com.flamesgroup.antrax.automation.registry.RegistryEntry;
import com.flamesgroup.antrax.automation.utils.registry.RegistryAccess;
import com.flamesgroup.antrax.automation.utils.registry.UnsyncRegistryEntryException;

public class RegistryHelper {

  private final RegistryAccess access;

  public RegistryHelper(final RegistryAccess access) {
    this.access = access;
  }

  public String getRegistryValue(final String path, final int searchDepth) throws Exception {
    for (RegistryEntry entry : access.listEntries(path, searchDepth)) {
      RegistryEntry moved;
      try {
        //move() calls to change entry update time. This action need to reordering entries list
        moved = access.move(entry, path);
      } catch (UnsyncRegistryEntryException e1) {
        continue;
      }
      return moved.getValue();
    }
    return null;
  }

}
