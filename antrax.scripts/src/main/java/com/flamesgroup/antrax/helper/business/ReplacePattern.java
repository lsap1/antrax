/*
 * Copyright (C) 2017 Flames Group SIA
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.flamesgroup.antrax.helper.business;

import java.io.Serializable;
import java.util.Objects;

public class ReplacePattern implements Serializable {

  private static final long serialVersionUID = -1788487995519980984L;

  private final String pattern;
  private final String replaceBy;

  public ReplacePattern(final String pattern, final String replaceBy) {
    Objects.requireNonNull(pattern, "pattern mustn't be null");
    Objects.requireNonNull(replaceBy, "replaceBy mustn't be null");
    this.pattern = pattern;
    this.replaceBy = replaceBy;
  }

  public String getPattern() {
    return pattern;
  }

  public String getReplaceBy() {
    return replaceBy;
  }

  @Override
  public boolean equals(final Object object) {
    if (this == object) {
      return true;
    }
    if (!(object instanceof ReplacePattern)) {
      return false;
    }
    final ReplacePattern that = (ReplacePattern) object;

    return Objects.equals(pattern, that.pattern)
        && Objects.equals(replaceBy, that.replaceBy);
  }

  @Override
  public int hashCode() {
    final int prime = 31;
    int result = Objects.hashCode(pattern);
    result = prime * result + Objects.hashCode(replaceBy);
    return result;
  }

  @Override
  public String toString() {
    return "pattern: " + pattern + " replaceBy: " + replaceBy;
  }

}
