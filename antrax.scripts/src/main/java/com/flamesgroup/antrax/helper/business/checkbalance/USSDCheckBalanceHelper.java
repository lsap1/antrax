/*
 * Copyright (C) 2017 Flames Group SIA
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.flamesgroup.antrax.helper.business.checkbalance;

import com.flamesgroup.antrax.automation.businesscripts.RegisteredInGSMChannel;
import com.flamesgroup.antrax.automation.utils.registry.RegistryAccess;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class USSDCheckBalanceHelper implements CheckBalanceHelper {

  private static final long serialVersionUID = 5375501612836189603L;

  private static final Logger logger = LoggerFactory.getLogger(USSDCheckBalanceHelper.class);

  private final String ussd;
  private final String[] regexes;

  public USSDCheckBalanceHelper(final String ussd, final String... regexes) {
    this.ussd = ussd;
    this.regexes = regexes;
  }

  @Override
  public double getBalanceValue(final RegisteredInGSMChannel channel, final RegistryAccess registry) throws Exception {
    logger.debug("[{}] - getting balance value", this);
    try {
      double balance = getBalanceValue(channel);
      channel.setBalance(balance);
      return balance;
    } catch (Exception e) {
      logger.warn("[{}] - failed to get balance value", this, e);
      throw e;
    }
  }

  private double getBalanceValue(final RegisteredInGSMChannel channel) throws Exception {
    logger.debug("[{}] - sending USSD={}", this, ussd);
    String response = channel.sendUSSD(ussd);
    channel.addUserMessage("Last USSD: " + response);
    return RegexBalanceParser.parseBalance(response, regexes);
  }

  public String getUSSD() {
    return ussd;
  }

  public String[] getRegexes() {
    return regexes;
  }


  @Override
  public String toString() {
    return ussd;
  }

}
