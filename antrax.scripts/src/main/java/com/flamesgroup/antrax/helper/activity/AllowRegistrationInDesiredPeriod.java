/*
 * Copyright (C) 2017 Flames Group SIA
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.flamesgroup.antrax.helper.activity;

import com.flamesgroup.antrax.automation.predictions.AlwaysFalsePrediction;
import com.flamesgroup.antrax.automation.predictions.AlwaysTruePrediction;
import com.flamesgroup.antrax.automation.predictions.Prediction;
import com.flamesgroup.antrax.automation.statefulscripts.ScriptSaver;
import com.flamesgroup.antrax.helper.activity.api.BaseSimpleActivityScript;
import com.flamesgroup.antrax.predictions.TimePeriodPrediction;
import com.flamesgroup.antrax.scripts.utils.TimeInterval;
import com.flamesgroup.antrax.scripts.utils.TimePeriod;

import java.util.Calendar;
import java.util.concurrent.atomic.AtomicBoolean;

public class AllowRegistrationInDesiredPeriod extends BaseSimpleActivityScript {

  private static final long serialVersionUID = -9077161994439417194L;

  private final ScriptSaver scriptSaver = new ScriptSaver();

  private final TimePeriod midnight = new TimePeriod(TimePeriod.inHours(24));

  private final AtomicBoolean activityStarted = new AtomicBoolean();

  private TimeInterval period;

  public void setPeriod(final TimeInterval period) {
    this.period = period;
  }

  @Override
  public boolean isActivityAllowed() {
    if (activityStarted.get()) {
      return true;
    } else {
      long current = getOffsetAfterMidnight();
      return current > period.getMin().getPeriod() && current < period.getMax().getPeriod();
    }
  }

  @Override
  public Prediction predictEnd() {
    return new AlwaysFalsePrediction();
  }

  @Override
  public Prediction predictStart() {
    if (isActivityAllowed()) {
      return new AlwaysTruePrediction();
    } else {
      long timeout = period.getMin().getPeriod() - getOffsetAfterMidnight();
      if (timeout < 0) {
        return new TimePeriodPrediction(midnight.getPeriod() - Math.abs(timeout));
      } else {
        return new TimePeriodPrediction(timeout);
      }
    }
  }

  @Override
  public ScriptSaver getScriptSaver() {
    return scriptSaver;
  }

  @Override
  public void handlePeriodStart() {
    activityStarted.set(true);
  }

  @Override
  public void handlePeriodEnd() {
    activityStarted.set(false);
  }

  private static long getOffsetAfterMidnight() {
    long now = System.currentTimeMillis();
    Calendar cal = Calendar.getInstance();
    cal.setTimeInMillis(now);
    cal.set(Calendar.HOUR_OF_DAY, 0);
    cal.set(Calendar.MINUTE, 0);
    cal.set(Calendar.SECOND, 0);
    cal.set(Calendar.MILLISECOND, 0);
    now -= cal.getTimeInMillis();
    return now;
  }

}
