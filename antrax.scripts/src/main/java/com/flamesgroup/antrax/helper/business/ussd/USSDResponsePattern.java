/*
 * Copyright (C) 2017 Flames Group SIA
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.flamesgroup.antrax.helper.business.ussd;

import java.io.Serializable;
import java.util.Objects;

public final class USSDResponsePattern implements Serializable {

  private static final long serialVersionUID = -360645632924957622L;

  private final String responsePattern;
  private final String responseEvent;

  public USSDResponsePattern(final String responsePattern, final String responseEvent) {
    Objects.requireNonNull(responsePattern, "responsePattern mustn't be null");
    Objects.requireNonNull(responseEvent, "responseEvent mustn't be null");

    this.responsePattern = responsePattern;
    this.responseEvent = responseEvent;
  }

  public String getResponsePattern() {
    return responsePattern;
  }

  public String getResponseEvent() {
    return responseEvent;
  }

  @Override
  public boolean equals(final Object object) {
    if (object == this) {
      return true;
    }
    if (!(object instanceof USSDResponsePattern)) {
      return false;
    }
    USSDResponsePattern ussdResponsePattern = (USSDResponsePattern) object;

    return responsePattern.equals(ussdResponsePattern.responsePattern)
        && responseEvent.equals(ussdResponsePattern.getResponseEvent());
  }

  @Override
  public int hashCode() {
    final int prime = 31;
    int result = 3;
    result = prime * result + responsePattern.hashCode();
    result = prime * result + responseEvent.hashCode();
    return result;
  }

  @Override
  public String toString() {
    return String.format("USSD response pattern: [ %s ]; Event: [ %s ]", responsePattern, responseEvent);
  }

}
