/*
 * Copyright (C) 2017 Flames Group SIA
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.flamesgroup.antrax.helper.editors;

import com.flamesgroup.antrax.automation.editors.BasePropertyEditor;
import com.flamesgroup.antrax.helper.business.checkbalance.sms.SMSCheckBalanceHelper;
import com.flamesgroup.antrax.helper.editors.components.RegexesPanel;

import java.awt.*;

import javax.swing.*;

public class SMSCheckBalanceHelperEditor extends BasePropertyEditor<SMSCheckBalanceHelper> {

  private static class EditorComponent extends JPanel {
    private static final long serialVersionUID = 7646879263017821528L;
    private final RegexesPanel regexes = new RegexesPanel(400);

    public EditorComponent() {
      add(regexes);
    }

    public SMSCheckBalanceHelper getValue() {
      return new SMSCheckBalanceHelper(regexes.listRegexes());
    }

    public void setValue(final SMSCheckBalanceHelper helper) {
      regexes.setRegexes(helper.getRegexes(), new String[0]);
    }
  }

  private final EditorComponent editor = new EditorComponent();



  @Override
  public Component getEditorComponent() {
    return editor;
  }

  @Override
  public Class<? extends SMSCheckBalanceHelper> getType() {
    return SMSCheckBalanceHelper.class;
  }

  @Override
  public SMSCheckBalanceHelper getValue() {
    return editor.getValue();
  }

  @Override
  public void setValue(final SMSCheckBalanceHelper value) {
    editor.setValue(value);
  }

  public static void main(final String[] args) {
    final JFrame frame = new JFrame();
    final SMSCheckBalanceHelperEditor paymentHelperEditor = new SMSCheckBalanceHelperEditor();
    frame.getContentPane().add(paymentHelperEditor.getEditorComponent());
    frame.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
    frame.pack();

    SwingUtilities.invokeLater(new Runnable() {
      @Override
      public void run() {
        System.out.println(paymentHelperEditor.getValue());
        frame.setVisible(true);
      }
    });
  }

}
