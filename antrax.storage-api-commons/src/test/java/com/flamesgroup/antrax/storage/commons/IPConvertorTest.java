/*
 * Copyright (C) 2017 Flames Group SIA
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.flamesgroup.antrax.storage.commons;

import static org.junit.Assert.assertEquals;

import com.flamesgroup.antrax.storage.commons.impl.IPConvertor;
import org.junit.Test;

import java.net.InetAddress;

public class IPConvertorTest {

  @Test
  public void testToLongToAdr() throws Exception {
    long cnt = 3232235520L;
    for (int i = 0; i < 256; i++) {
      for (int j = 0; j < 256; j++) {
        String adr = 192 + "." + 168 + "." + i + "." + j;
        assertEquals(cnt, IPConvertor.toLong(InetAddress.getByName(adr)));
        assertEquals(InetAddress.getByName(adr), IPConvertor.toInetAddress(cnt));
        cnt++;
      }
    }
    assertEquals(0, IPConvertor.toLong(InetAddress.getByName("0.0.0.0")));
    assertEquals(16843009, IPConvertor.toLong(InetAddress.getByName("1.1.1.1")));
    assertEquals(3232238341L, IPConvertor.toLong(InetAddress.getByName("192.168.11.5")));
    assertEquals(4294967295L, IPConvertor.toLong(InetAddress.getByName("255.255.255.255")));
    assertEquals(168430090L, IPConvertor.toLong(InetAddress.getByName("10.10.10.10")));
  }

  @Test
  public void testToInetAddress() throws Exception {
    assertEquals(InetAddress.getByName("0.0.0.0"), IPConvertor.toInetAddress(0));
    assertEquals(InetAddress.getByName("1.1.1.1"), IPConvertor.toInetAddress(16843009));
    assertEquals(InetAddress.getByName("192.168.11.5"), IPConvertor.toInetAddress(3232238341L));
    assertEquals(InetAddress.getByName("255.255.255.255"), IPConvertor.toInetAddress(4294967295L));
    assertEquals(InetAddress.getByName("10.10.10.10"), IPConvertor.toInetAddress(168430090L));
  }

}
