/*
 * Copyright (C) 2017 Flames Group SIA
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.flamesgroup.antrax.storage.commons;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import com.flamesgroup.antrax.storage.commons.impl.GSMGroupImpl;
import org.junit.Test;

public class GSMGroupTest {

  @Test
  public void testGSMGroup() throws Exception {
    GSMGroup gsmGroup = new GSMGroupImpl(2L);
    gsmGroup.setName("GROUP_1");
    assertEquals("GROUP_1", gsmGroup.getName());
    assertEquals(2L, gsmGroup.getID());
  }

  @Test
  public void testGSMGroupEquals() throws Exception {
    GSMGroup gsmGroup1 = new GSMGroupImpl(1L);
    gsmGroup1.setName("GROUP_1");
    GSMGroup gsmGroup2 = new GSMGroupImpl(2L);
    gsmGroup2.setName("GROUP_1");
    GSMGroup gsmGroup3 = new GSMGroupImpl(1L);
    gsmGroup3.setName("GROUP_1");
    GSMGroup gsmGroup4 = new GSMGroupImpl(1L);
    gsmGroup4.setName("GROUP_1");

    assertTrue(gsmGroup1.equals(gsmGroup3));
    assertFalse(gsmGroup1.equals(gsmGroup2));
    assertTrue(gsmGroup1.equals(gsmGroup4));
    assertFalse(gsmGroup2.equals(gsmGroup4));
    assertTrue(gsmGroup3.equals(gsmGroup4));

  }

  @Test
  public void testGSMGroupHashCode() throws Exception {
    GSMGroup gsmGroup1 = new GSMGroupImpl(1L);
    gsmGroup1.setName("GROUP_2");
    GSMGroup gsmGroup2 = new GSMGroupImpl(1L);
    gsmGroup2.setName("GROUP_1");
    assertEquals(gsmGroup1.hashCode(), gsmGroup2.hashCode());
  }

}
