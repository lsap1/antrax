/*
 * Copyright (C) 2017 Flames Group SIA
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.flamesgroup.antrax.storage.commons.impl;

import java.util.Objects;

public class PhoneNumberPrefix extends Domain {

  private static final long serialVersionUID = -6794569979700816741L;

  private String name;
  private String prefix;
  private int phoneNumberPrefixCount;

  public PhoneNumberPrefix() {
  }

  public PhoneNumberPrefix(final long id) {
    this.id = id;
  }

  public String getName() {
    return name;
  }

  public PhoneNumberPrefix setName(final String name) {
    this.name = name;
    return this;
  }

  public String getPrefix() {
    return prefix;
  }

  public PhoneNumberPrefix setPrefix(final String prefix) {
    this.prefix = prefix;
    return this;
  }

  public int getPhoneNumberPrefixCount() {
    return phoneNumberPrefixCount;
  }

  public PhoneNumberPrefix setPhoneNumberPrefixCount(final int phoneNumberPrefixCount) {
    this.phoneNumberPrefixCount = phoneNumberPrefixCount;
    return this;
  }

  @Override
  public boolean equals(final Object object) {
    if (this == object) {
      return true;
    }
    if (!(object instanceof PhoneNumberPrefix)) {
      return false;
    }
    if (!super.equals(object)) {
      return false;
    }
    final PhoneNumberPrefix that = (PhoneNumberPrefix) object;

    return phoneNumberPrefixCount == that.phoneNumberPrefixCount
        && Objects.equals(name, that.name)
        && Objects.equals(prefix, that.prefix);
  }

  @Override
  public int hashCode() {
    final int prime = 31;
    int result = super.hashCode();
    result = prime * result + Objects.hashCode(name);
    result = prime * result + Objects.hashCode(prefix);
    result = prime * result + phoneNumberPrefixCount;
    return result;
  }

  @Override
  public String toString() {
    return getClass().getSimpleName() + '@' + Integer.toHexString(hashCode()) +
        "[name:'" + name + "'" +
        " prefix:'" + prefix + "'" +
        " phoneNumberPrefixCount:" + phoneNumberPrefixCount +
        ']';
  }

}
