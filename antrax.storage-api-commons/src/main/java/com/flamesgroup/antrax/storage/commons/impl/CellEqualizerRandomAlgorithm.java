/*
 * Copyright (C) 2017 Flames Group SIA
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.flamesgroup.antrax.storage.commons.impl;

import java.time.LocalTime;
import java.util.Objects;

public class CellEqualizerRandomAlgorithm extends CellEqualizerAlgorithm {

  private static final long serialVersionUID = 6892468593276270046L;

  private CellEqualizerRandomConfiguration configuration;

  public CellEqualizerRandomAlgorithm() {
  }

  public CellEqualizerRandomAlgorithm(final long id) {
    super(id);
  }

  @Override
  public CellEqualizerRandomConfiguration getConfiguration() {
    return configuration;
  }

  public CellEqualizerRandomAlgorithm setConfiguration(final CellEqualizerRandomConfiguration configuration) {
    this.configuration = configuration;
    return this;
  }

  @Override
  public boolean equals(final Object object) {
    if (this == object) {
      return true;
    }
    if (!(object instanceof CellEqualizerRandomAlgorithm)) {
      return false;
    }
    if (!super.equals(object)) {
      return false;
    }
    final CellEqualizerRandomAlgorithm that = (CellEqualizerRandomAlgorithm) object;

    return Objects.equals(configuration, that.configuration);
  }

  @Override
  public int hashCode() {
    final int prime = 31;
    int result = super.hashCode();
    result = prime * result + Objects.hashCode(configuration);
    return result;
  }

  @Override
  public String toString() {
    return getClass().getSimpleName() + '@' + Integer.toHexString(hashCode()) +
        "[configuration:" + configuration +
        ']';
  }


  public static class CellEqualizerRandomConfiguration extends Configuration {

    private static final long serialVersionUID = 1083858939523291039L;

    private int minRegeneratedTime;
    private int maxRegeneratedTime;
    private int minWaitingTime;
    private int maxWaitingTime;
    private LocalTime fromLeadTime;
    private LocalTime toLeadTime;

    public CellEqualizerRandomConfiguration() {
    }

    public int getMinRegeneratedTime() {
      return minRegeneratedTime;
    }

    public CellEqualizerRandomConfiguration setMinRegeneratedTime(final int minRegeneratedTime) {
      this.minRegeneratedTime = minRegeneratedTime;
      return this;
    }

    public int getMaxRegeneratedTime() {
      return maxRegeneratedTime;
    }

    public CellEqualizerRandomConfiguration setMaxRegeneratedTime(final int maxRegeneratedTime) {
      this.maxRegeneratedTime = maxRegeneratedTime;
      return this;
    }

    public int getMinWaitingTime() {
      return minWaitingTime;
    }

    public CellEqualizerRandomConfiguration setMinWaitingTime(final int minWaitingTime) {
      this.minWaitingTime = minWaitingTime;
      return this;
    }

    public int getMaxWaitingTime() {
      return maxWaitingTime;
    }

    public CellEqualizerRandomConfiguration setMaxWaitingTime(final int maxWaitingTime) {
      this.maxWaitingTime = maxWaitingTime;
      return this;
    }

    public LocalTime getFromLeadTime() {
      return fromLeadTime;
    }

    public CellEqualizerRandomConfiguration setFromLeadTime(final LocalTime fromLeadTime) {
      this.fromLeadTime = fromLeadTime;
      return this;
    }

    public LocalTime getToLeadTime() {
      return toLeadTime;
    }

    public CellEqualizerRandomConfiguration setToLeadTime(final LocalTime toLeadTime) {
      this.toLeadTime = toLeadTime;
      return this;
    }

    @Override
    public String toString() {
      return getClass().getSimpleName() + '@' + Integer.toHexString(hashCode()) +
          "[minRegeneratedTime:" + minRegeneratedTime +
          " maxRegeneratedTime:" + maxRegeneratedTime +
          " minWaitingTime:" + minWaitingTime +
          " maxWaitingTime:" + maxWaitingTime +
          " fromLeadTime:" + fromLeadTime +
          " toLeadTime:" + toLeadTime +
          ']';
    }

  }

}
