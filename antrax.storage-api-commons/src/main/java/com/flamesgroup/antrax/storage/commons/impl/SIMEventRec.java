/*
 * Copyright (C) 2017 Flames Group SIA
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.flamesgroup.antrax.storage.commons.impl;

import com.flamesgroup.antrax.storage.enums.SIMEvent;
import com.flamesgroup.unit.ICCID;

import java.util.Date;

public class SIMEventRec extends Domain {

  private static final long serialVersionUID = 7191792152474012698L;

  private SIMEventRec parent;
  private ICCID simUid;
  private SIMEvent event;
  private String[] args;
  private Date startTime;

  public SIMEventRec(final long id) {
    this.id = id;
  }

  public SIMEventRec getParent() {
    return parent;
  }

  public SIMEventRec setParent(final SIMEventRec parent) {
    this.parent = parent;
    return this;
  }

  public ICCID getSimUid() {
    return simUid;
  }

  public SIMEventRec setSimUid(final ICCID simUid) {
    this.simUid = simUid;
    return this;
  }

  public SIMEvent getEvent() {
    return event;
  }

  public SIMEventRec setEvent(final SIMEvent event) {
    this.event = event;
    return this;
  }

  public String[] getArgs() {
    return args;
  }

  public SIMEventRec setArgs(final String[] args) {
    this.args = args;
    return this;
  }

  public Date getStartTime() {
    return startTime;
  }

  public SIMEventRec setStartTime(final Date startTime) {
    this.startTime = startTime;
    return this;
  }

  @Override
  public int hashCode() {
    return super.hashCode();
  }

  @Override
  public boolean equals(final Object obj) {
    return super.equals(obj);
  }

  @Override
  public String toString() {
    return "Event:[" + event + "] on sim:[" + simUid + "]";
  }

}
