/*
 * Copyright (C) 2017 Flames Group SIA
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.flamesgroup.antrax.storage.commons.impl;

import java.util.Objects;

public class RouteConfig extends Domain {

  private static final long serialVersionUID = 2644158189092930239L;

  private final CallAllocationAlgorithm callAllocationAlgorithm;
  private final SmsAllocationAlgorithm smsAllocationAlgorithm;

  public RouteConfig(final CallAllocationAlgorithm callAllocationAlgorithm, final SmsAllocationAlgorithm smsAllocationAlgorithm) {
    Objects.requireNonNull(callAllocationAlgorithm, "callAllocationAlgorithm mustn't be null");
    Objects.requireNonNull(smsAllocationAlgorithm, "smsAllocationAlgorithm mustn't be null");

    this.callAllocationAlgorithm = callAllocationAlgorithm;
    this.smsAllocationAlgorithm = smsAllocationAlgorithm;
  }

  public CallAllocationAlgorithm getCallAllocationAlgorithm() {
    return callAllocationAlgorithm;
  }

  public SmsAllocationAlgorithm getSmsAllocationAlgorithm() {
    return smsAllocationAlgorithm;
  }

  @Override
  public boolean equals(final Object object) {
    if (this == object) {
      return true;
    }
    if (!(object instanceof RouteConfig)) {
      return false;
    }
    RouteConfig that = (RouteConfig) object;

    return callAllocationAlgorithm == that.callAllocationAlgorithm
        && smsAllocationAlgorithm == that.smsAllocationAlgorithm;
  }

  @Override
  public int hashCode() {
    final int prime = 31;
    int result = Objects.hashCode(callAllocationAlgorithm);
    result = prime * result + Objects.hashCode(smsAllocationAlgorithm);
    return result;
  }

}
