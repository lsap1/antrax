/*
 * Copyright (C) 2017 Flames Group SIA
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.flamesgroup.antrax.storage.commons.impl;

import java.time.LocalTime;
import java.util.Objects;

public class CellEqualizerStatisticAlgorithm extends CellEqualizerAlgorithm {

  private static final long serialVersionUID = 6892468593276270046L;

  private CellEqualizerStatisticConfiguration configuration;

  public CellEqualizerStatisticAlgorithm() {
  }

  public CellEqualizerStatisticAlgorithm(final long id) {
    super(id);
  }

  @Override
  public CellEqualizerStatisticConfiguration getConfiguration() {
    return configuration;
  }

  public CellEqualizerStatisticAlgorithm setConfiguration(final CellEqualizerStatisticConfiguration configuration) {
    this.configuration = configuration;
    return this;
  }

  @Override
  public boolean equals(final Object object) {
    if (this == object) {
      return true;
    }
    if (!(object instanceof CellEqualizerStatisticAlgorithm)) {
      return false;
    }
    if (!super.equals(object)) {
      return false;
    }
    final CellEqualizerStatisticAlgorithm that = (CellEqualizerStatisticAlgorithm) object;

    return Objects.equals(configuration, that.configuration);
  }

  @Override
  public int hashCode() {
    final int prime = 31;
    int result = super.hashCode();
    result = prime * result + Objects.hashCode(configuration);
    return result;
  }

  @Override
  public String toString() {
    return getClass().getSimpleName() + '@' + Integer.toHexString(hashCode()) +
        "[configuration:" + configuration +
        ']';
  }


  public static class CellEqualizerStatisticConfiguration extends Configuration {

    private static final long serialVersionUID = 1083858939523291039L;

    private int minSuccessfulCallCount;
    private int maxSuccessfulCallCount;
    private int minUssdCount;
    private int maxUssdCount;
    private int minOutgoingSmsCount;
    private int maxOutgoingSmsCount;

    private int minWaitingTime;
    private int maxWaitingTime;
    private LocalTime fromLeadTime;
    private LocalTime toLeadTime;

    public CellEqualizerStatisticConfiguration() {
    }

    public int getMinSuccessfulCallCount() {
      return minSuccessfulCallCount;
    }

    public CellEqualizerStatisticConfiguration setMinSuccessfulCallCount(final int minSuccessfulCallCount) {
      this.minSuccessfulCallCount = minSuccessfulCallCount;
      return this;
    }

    public int getMaxSuccessfulCallCount() {
      return maxSuccessfulCallCount;
    }

    public int getMinUssdCount() {
      return minUssdCount;
    }

    public CellEqualizerStatisticConfiguration setMinUssdCount(final int minUssdCount) {
      this.minUssdCount = minUssdCount;
      return this;
    }

    public int getMaxUssdCount() {
      return maxUssdCount;
    }

    public CellEqualizerStatisticConfiguration setMaxUssdCount(final int maxUssdCount) {
      this.maxUssdCount = maxUssdCount;
      return this;
    }

    public int getMinOutgoingSmsCount() {
      return minOutgoingSmsCount;
    }

    public CellEqualizerStatisticConfiguration setMinOutgoingSmsCount(final int minOutgoingSmsCount) {
      this.minOutgoingSmsCount = minOutgoingSmsCount;
      return this;
    }

    public int getMaxOutgoingSmsCount() {
      return maxOutgoingSmsCount;
    }

    public CellEqualizerStatisticConfiguration setMaxOutgoingSmsCount(final int maxOutgoingSmsCount) {
      this.maxOutgoingSmsCount = maxOutgoingSmsCount;
      return this;
    }

    public CellEqualizerStatisticConfiguration setMaxSuccessfulCallCount(final int maxSuccessfulCallCount) {
      this.maxSuccessfulCallCount = maxSuccessfulCallCount;
      return this;
    }

    public int getMinWaitingTime() {
      return minWaitingTime;
    }

    public CellEqualizerStatisticConfiguration setMinWaitingTime(final int minWaitingTime) {
      this.minWaitingTime = minWaitingTime;
      return this;
    }

    public int getMaxWaitingTime() {
      return maxWaitingTime;
    }

    public CellEqualizerStatisticConfiguration setMaxWaitingTime(final int maxWaitingTime) {
      this.maxWaitingTime = maxWaitingTime;
      return this;
    }

    public LocalTime getFromLeadTime() {
      return fromLeadTime;
    }

    public CellEqualizerStatisticConfiguration setFromLeadTime(final LocalTime fromLeadTime) {
      this.fromLeadTime = fromLeadTime;
      return this;
    }

    public LocalTime getToLeadTime() {
      return toLeadTime;
    }

    public CellEqualizerStatisticConfiguration setToLeadTime(final LocalTime toLeadTime) {
      this.toLeadTime = toLeadTime;
      return this;
    }

    @Override
    public String toString() {
      return getClass().getSimpleName() + '@' + Integer.toHexString(hashCode()) +
          "[minSuccessfulCallCount:" + minSuccessfulCallCount +
          " maxSuccessfulCallCount:" + maxSuccessfulCallCount +
          " minUssdCount:" + minUssdCount +
          " maxUssdCount:" + maxUssdCount +
          " minOutgoingSmsCount:" + minOutgoingSmsCount +
          " maxOutgoingSmsCount:" + maxOutgoingSmsCount +
          " minWaitingTime:" + minWaitingTime +
          " maxWaitingTime:" + maxWaitingTime +
          " fromLeadTime:" + fromLeadTime +
          " toLeadTime:" + toLeadTime +
          ']';
    }

  }

}
