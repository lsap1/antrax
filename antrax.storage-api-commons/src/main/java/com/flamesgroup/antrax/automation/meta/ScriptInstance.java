/*
 * Copyright (C) 2017 Flames Group SIA
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.flamesgroup.antrax.automation.meta;

public interface ScriptInstance {

  ScriptDefinition getDefinition();

  ScriptParameter[] getParameters();

  ScriptParameter getParameter(String name);

  ScriptParameter[] getParameters(String name);

  ScriptParameter addParameter(String propName, byte[] serialziedValue);

  ScriptParameter addParameter(String propName, Object value, ClassLoader classLoader);

  ScriptParameter insertParameter(ScriptParameter previousParam);

  void removeParameter(ScriptParameter param);

  void setParameter(String propName, Object value, ClassLoader classLoader);

  void setParameters(ScriptInstance src) throws IllegalArgumentException;

  void moveParamDown(ScriptParameter param);

  void moveParamUp(ScriptParameter param);

}
