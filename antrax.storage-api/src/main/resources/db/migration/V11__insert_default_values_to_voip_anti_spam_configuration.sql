ALTER TABLE voip_anti_spam_configuration ALTER COLUMN gray_list_config SET DEFAULT '[]';
ALTER TABLE voip_anti_spam_configuration ALTER COLUMN black_list_config SET DEFAULT '[]';
ALTER TABLE voip_anti_spam_configuration ALTER COLUMN gsm_alerting_config SET DEFAULT '[]';
ALTER TABLE voip_anti_spam_configuration ALTER COLUMN voip_alerting_config SET DEFAULT '[]';
ALTER TABLE voip_anti_spam_configuration ALTER COLUMN fas_config SET DEFAULT '[]';

INSERT INTO voip_anti_spam_configuration SELECT * FROM (SELECT false) AS dummy WHERE NOT EXISTS (SELECT * FROM voip_anti_spam_configuration);
