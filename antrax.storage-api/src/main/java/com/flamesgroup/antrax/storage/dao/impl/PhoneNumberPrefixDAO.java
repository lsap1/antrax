/*
 * Copyright (C) 2017 Flames Group SIA
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.flamesgroup.antrax.storage.dao.impl;

import static com.flamesgroup.storage.jooq.Tables.PHONE_NUMBER_PREFIX;
import static com.flamesgroup.storage.jooq.Tables.PHONE_NUMBER_PREFIX_CONFIG;
import static com.flamesgroup.storage.jooq.Tables.PHONE_NUMBER_PREFIX_LIST;

import com.flamesgroup.antrax.storage.commons.impl.PhoneNumberPrefix;
import com.flamesgroup.antrax.storage.commons.impl.PhoneNumberPrefixConfig;
import com.flamesgroup.antrax.storage.connection.IDbConnection;
import com.flamesgroup.antrax.storage.connection.IDbConnectionPool;
import com.flamesgroup.antrax.storage.dao.IPhoneNumberPrefixDAO;
import com.flamesgroup.antrax.storage.exceptions.DataModificationException;
import com.flamesgroup.storage.jooq.tables.records.PhoneNumberPrefixConfigRecord;
import com.flamesgroup.storage.jooq.tables.records.PhoneNumberPrefixListRecord;
import com.flamesgroup.storage.jooq.tables.records.PhoneNumberPrefixRecord;
import com.flamesgroup.storage.map.PhoneNumberPrefixMapper;
import org.jooq.BatchBindStep;
import org.jooq.DSLContext;
import org.jooq.DeleteQuery;
import org.jooq.InsertFinalStep;
import org.jooq.InsertValuesStep2;
import org.jooq.SelectQuery;
import org.jooq.exception.DataAccessException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

public class PhoneNumberPrefixDAO implements IPhoneNumberPrefixDAO {

  private static final Logger logger = LoggerFactory.getLogger(PhoneNumberPrefixDAO.class);

  private final IDbConnectionPool dbConnectionPool;

  public PhoneNumberPrefixDAO(final IDbConnectionPool dbConnectionPool) {
    this.dbConnectionPool = dbConnectionPool;
  }

  @Override
  public List<PhoneNumberPrefix> listPhoneNumberPrefixes() {
    logger.debug("[{}] - trying to select PhoneNumberPrefixes", this);
    IDbConnection connection = dbConnectionPool.getConnection();
    try {
      DSLContext context = connection.getDSLContext();
      return context.transactionResult(configuration -> {
        SelectQuery<PhoneNumberPrefixRecord> selectQuery = context.selectQuery(PHONE_NUMBER_PREFIX);

        return selectQuery.fetchInto(PhoneNumberPrefixRecord.class).stream().map(PhoneNumberPrefixMapper::mapPhoneNumberPrefixRecordToPhoneNumberPrefix).collect(Collectors.toList());
      });
    } catch (DataAccessException e) {
      logger.warn("[{}] - while selecting list of PhoneNumberPrefix", this, e);
      return new ArrayList<>();
    } finally {
      connection.close();
    }
  }

  @Override
  public void insertPhoneNumberPrefix(final PhoneNumberPrefix phoneNumberPrefix) throws DataModificationException {
    logger.debug("[{}] - trying to insert phoneNumberPrefix: [{}]", this, phoneNumberPrefix);
    IDbConnection connection = dbConnectionPool.getConnection();
    try {
      DSLContext context = connection.getDSLContext();
      context.transaction(configuration -> {
        PhoneNumberPrefixRecord phoneNumberPrefixRecord = context.newRecord(PHONE_NUMBER_PREFIX);
        PhoneNumberPrefixMapper.mapPhoneNumberPrefixToPhoneNumberPrefixRecord(phoneNumberPrefix, phoneNumberPrefixRecord);

        int res = phoneNumberPrefixRecord.insert();

        if (res > 0) {
          logger.debug("[{}] - inserted phoneNumberPrefix: [{}]", this, phoneNumberPrefix);
        } else {
          logger.warn("[{}] - can't insert phoneNumberPrefix: [{}]", this, phoneNumberPrefix);
        }
      });
    } catch (DataAccessException e) {
      throw new DataModificationException(String.format("[%s] - execution error, while insert phoneNumberPrefix", this), e);
    } finally {
      connection.close();
    }
  }

  @Override
  public void updatePhoneNumberPrefix(final PhoneNumberPrefix phoneNumberPrefix) throws DataModificationException {
    logger.debug("[{}] - trying to update phoneNumberPrefix: [{}]", this, phoneNumberPrefix);
    IDbConnection connection = dbConnectionPool.getConnection();
    try {
      DSLContext context = connection.getDSLContext();
      context.transaction(configuration -> {
        PhoneNumberPrefixRecord phoneNumberPrefixRecord = context.newRecord(PHONE_NUMBER_PREFIX);
        PhoneNumberPrefixMapper.mapPhoneNumberPrefixToPhoneNumberPrefixRecord(phoneNumberPrefix, phoneNumberPrefixRecord);
        phoneNumberPrefixRecord.setId(phoneNumberPrefix.getID());

        int res = phoneNumberPrefixRecord.update();

        if (res > 0) {
          logger.debug("[{}] - updated phoneNumberPrefix: [{}]", this, phoneNumberPrefix);
        } else {
          logger.warn("[{}] - can't update phoneNumberPrefix: [{}]", this, phoneNumberPrefix);
        }
      });
    } catch (DataAccessException e) {
      throw new DataModificationException(String.format("[%s] - execution error, while update phoneNumberPrefix", this), e);
    } finally {
      connection.close();
    }
  }

  @Override
  public void deletePhoneNumberPrefix(final PhoneNumberPrefix phoneNumberPrefix) throws DataModificationException {
    logger.debug("[{}] - trying to delete phoneNumberPrefix: [{}]", this, phoneNumberPrefix);
    IDbConnection connection = dbConnectionPool.getConnection();
    try {
      DSLContext context = connection.getDSLContext();
      context.transaction(configuration -> {
        DeleteQuery<PhoneNumberPrefixListRecord> deleteQuery = context.deleteQuery(PHONE_NUMBER_PREFIX_LIST);
        deleteQuery.addConditions(PHONE_NUMBER_PREFIX_LIST.PHONE_NUMBER_PREFIX_ID.eq(phoneNumberPrefix.getID()));

        int res = deleteQuery.execute();

        DeleteQuery<PhoneNumberPrefixRecord> subDeleteQuery = context.deleteQuery(PHONE_NUMBER_PREFIX);
        subDeleteQuery.addConditions(PHONE_NUMBER_PREFIX.ID.eq(phoneNumberPrefix.getID()));

        res = subDeleteQuery.execute();
        if (res > 0) {
          logger.debug("[{}] - deleted phoneNumberPrefix: [{}]", this, phoneNumberPrefix);
          return;
        }
        logger.warn("[{}] - can't delete phoneNumberPrefix: [{}]", this, phoneNumberPrefix);
        throw new DataModificationException(String.format("[%s] - can't delete phoneNumberPrefix: %s", this, phoneNumberPrefix));
      });
    } catch (DataAccessException e) {
      throw new DataModificationException(String.format("[%s] - execution error, while delete phoneNumberPrefix", this), e);
    } finally {
      connection.close();
    }
  }

  @Override
  public int clearPhoneNumberPrefix(final PhoneNumberPrefix phoneNumberPrefix) throws DataModificationException {
    logger.debug("[{}] - trying to  clear phoneNumberPrefix: [{}]", this, phoneNumberPrefix);
    IDbConnection connection = dbConnectionPool.getConnection();
    try {
      DSLContext context = connection.getDSLContext();
      return context.transactionResult(configuration -> {
        DeleteQuery<PhoneNumberPrefixListRecord> deleteQuery = context.deleteQuery(PHONE_NUMBER_PREFIX_LIST);
        deleteQuery.addConditions(PHONE_NUMBER_PREFIX_LIST.PHONE_NUMBER_PREFIX_ID.eq(phoneNumberPrefix.getID()));

        int res = deleteQuery.execute();

        phoneNumberPrefix.setPhoneNumberPrefixCount(0);
        updatePhoneNumberPrefix(phoneNumberPrefix);

        logger.debug("[{}] -  cleared phoneNumberPrefix: [{}]", this, phoneNumberPrefix);
        return res;
      });
    } catch (DataAccessException e) {
      throw new DataModificationException(String.format("[%s] - execution error, while clear phoneNumberPrefix", this), e);
    } finally {
      connection.close();
    }
  }

  @Override
  public void insertAllNumbers(final Set<String> numbers, final long phoneNumberPrefixId, final boolean ignoreOrUpdateOnDuplicate) throws DataModificationException {
    logger.debug("[{}] - trying to store numbers@{}", this, numbers.hashCode());
    IDbConnection connection = dbConnectionPool.getConnection();
    try {
      DSLContext context = connection.getDSLContext();
      context.transaction(configuration -> {
        InsertValuesStep2<PhoneNumberPrefixListRecord, Long, String> values = context.insertInto(PHONE_NUMBER_PREFIX_LIST, PHONE_NUMBER_PREFIX_LIST.PHONE_NUMBER_PREFIX_ID,
            PHONE_NUMBER_PREFIX_LIST.PHONE_NUMBER)
            .values((Long) null, null);
        InsertFinalStep<PhoneNumberPrefixListRecord> finalStep;
        if (ignoreOrUpdateOnDuplicate) {
          finalStep = values.onDuplicateKeyIgnore();
        } else {
          finalStep = values.onDuplicateKeyUpdate().set(PHONE_NUMBER_PREFIX_LIST.PHONE_NUMBER_PREFIX_ID, phoneNumberPrefixId);
        }

        BatchBindStep batch = context.batch(finalStep);

        numbers.forEach(number -> batch.bind(phoneNumberPrefixId, number, phoneNumberPrefixId));

        batch.execute();

        logger.debug("[{}] - store numbers@{}", this, numbers.hashCode());
      });
    } catch (DataAccessException e) {
      throw new DataModificationException(String.format("[%s] - execution error, while store numbers", this), e);
    } finally {
      connection.close();
    }
  }

  @Override
  public void updatePhoneNumberPrefixesCount(final PhoneNumberPrefix phoneNumberPrefix) throws DataModificationException {
    logger.debug("[{}] - trying to update PhoneNumberPrefixesCount for element: [{}]", this, phoneNumberPrefix);
    IDbConnection connection = dbConnectionPool.getConnection();
    try {
      DSLContext context = connection.getDSLContext();
      context.transaction(configuration -> {
        SelectQuery<PhoneNumberPrefixListRecord> selectQuery = context.selectQuery(PHONE_NUMBER_PREFIX_LIST);
        selectQuery.addConditions(PHONE_NUMBER_PREFIX_LIST.PHONE_NUMBER_PREFIX_ID.eq(phoneNumberPrefix.getID()));

        phoneNumberPrefix.setPhoneNumberPrefixCount(context.fetchCount(selectQuery));
        updatePhoneNumberPrefix(phoneNumberPrefix);
      });
    } catch (DataAccessException e) {
      throw new DataModificationException(String.format("[%s] - execution error, while update PhoneNumberPrefixesCount", this), e);
    } finally {
      connection.close();
    }
  }

  @Override
  public List<String> listPhoneNumberForPrefix(final long phoneNumberPrefixId) {
    logger.debug("[{}] - trying to select PhoneNumbers for phoneNumberPrefixId: [{}]", this, phoneNumberPrefixId);
    IDbConnection connection = dbConnectionPool.getConnection();
    try {
      DSLContext context = connection.getDSLContext();
      return context.transactionResult(configuration -> {
        SelectQuery<PhoneNumberPrefixListRecord> selectQuery = context.selectQuery(PHONE_NUMBER_PREFIX_LIST);
        selectQuery.addSelect(PHONE_NUMBER_PREFIX_LIST.PHONE_NUMBER);
        selectQuery.addConditions(PHONE_NUMBER_PREFIX_LIST.PHONE_NUMBER_PREFIX_ID.eq(phoneNumberPrefixId));

        return selectQuery.fetchInto(String.class).stream().collect(Collectors.toList());
      });
    } catch (DataAccessException e) {
      logger.warn("[{}] - while selecting list of PhoneNumbers", this, e);
      return new ArrayList<>();
    } finally {
      connection.close();
    }
  }

  @Override
  public PhoneNumberPrefixConfig getPrefixConfig() {
    logger.debug("[{}] - trying to select PhoneNumberPrefixConfig", this);
    IDbConnection connection = dbConnectionPool.getConnection();
    try {
      DSLContext context = connection.getDSLContext();
      return context.transactionResult(configuration -> {
        SelectQuery<PhoneNumberPrefixConfigRecord> selectQuery = context.selectQuery(PHONE_NUMBER_PREFIX_CONFIG);

        PhoneNumberPrefixConfigRecord phoneNumberPrefixConfigRecord = selectQuery.fetchOneInto(PhoneNumberPrefixConfigRecord.class);
        return phoneNumberPrefixConfigRecord == null ? null : PhoneNumberPrefixMapper.mapPhoneNumberPrefixConfigRecordToPhoneNumberPrefixConfig(phoneNumberPrefixConfigRecord);
      });
    } catch (DataAccessException e) {
      logger.warn("[{}] - while selecting list of PhoneNumbers", this, e);
      return null;
    } finally {
      connection.close();
    }
  }

  @Override
  public void savePrefixConfig(final PhoneNumberPrefixConfig phoneNumberPrefixConfig) throws DataModificationException {
    logger.debug("[{}] - trying to save phoneNumberPrefixConfig: [{}]", this, phoneNumberPrefixConfig);
    IDbConnection connection = dbConnectionPool.getConnection();
    try {
      DSLContext context = connection.getDSLContext();
      context.transaction(configuration -> {
        context.deleteFrom(PHONE_NUMBER_PREFIX_CONFIG).execute();

        PhoneNumberPrefixConfigRecord phoneNumberPrefixConfigRecord = context.newRecord(PHONE_NUMBER_PREFIX_CONFIG);
        PhoneNumberPrefixMapper.mapPhoneNumberPrefixConfigToPhoneNumberPrefixConfigRecord(phoneNumberPrefixConfig, phoneNumberPrefixConfigRecord);

        int res = phoneNumberPrefixConfigRecord.insert();

        if (res > 0) {
          logger.debug("[{}] - inserted phoneNumberPrefixConfig: [{}]", this, phoneNumberPrefixConfig);
        } else {
          logger.warn("[{}] - can't insert phoneNumberPrefixConfig: [{}]", this, phoneNumberPrefixConfig);
        }
      });
    } catch (DataAccessException e) {
      throw new DataModificationException(String.format("[%s] - execution error, while save phoneNumberPrefixConfig", this), e);
    } finally {
      connection.close();
    }
  }

  @Override
  public String findPhoneNumberPrefixList(final String phoneNumber) {
    logger.debug("[{}] - trying to select PhoneNumberPrefixList for phoneNumber: [{}]", this, phoneNumber);
    IDbConnection connection = dbConnectionPool.getConnection();
    try {
      DSLContext context = connection.getDSLContext();
      return context.transactionResult(configuration -> {
        SelectQuery<PhoneNumberPrefixListRecord> selectQuery = context.selectQuery(PHONE_NUMBER_PREFIX_LIST);
        selectQuery.addConditions(PHONE_NUMBER_PREFIX_LIST.PHONE_NUMBER.eq(phoneNumber));

        PhoneNumberPrefixListRecord numberPrefixListRecord = selectQuery.fetchOneInto(PhoneNumberPrefixListRecord.class);
        if (numberPrefixListRecord == null) {
          return null;
        } else {
          SelectQuery<PhoneNumberPrefixRecord> subSelectQuery = context.selectQuery(PHONE_NUMBER_PREFIX);
          subSelectQuery.addConditions(PHONE_NUMBER_PREFIX.ID.eq(numberPrefixListRecord.getPhoneNumberPrefixId()));

          PhoneNumberPrefixRecord phoneNumberPrefixRecord = subSelectQuery.fetchOneInto(PhoneNumberPrefixRecord.class);
          PhoneNumberPrefix phoneNumberPrefix = PhoneNumberPrefixMapper.mapPhoneNumberPrefixRecordToPhoneNumberPrefix(phoneNumberPrefixRecord);

          return phoneNumberPrefix.getPrefix();
        }

      });
    } catch (DataAccessException e) {
      logger.warn("[{}] - while selecting list of PhoneNumberPrefixList", this, e);
      return null;
    } finally {
      connection.close();
    }
  }

}
