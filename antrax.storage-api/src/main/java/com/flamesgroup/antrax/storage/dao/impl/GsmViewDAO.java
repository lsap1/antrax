/*
 * Copyright (C) 2017 Flames Group SIA
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.flamesgroup.antrax.storage.dao.impl;

import static com.flamesgroup.storage.jooq.Tables.CELL_EQUALIZER_CONFIGURATION;
import static com.flamesgroup.storage.jooq.Tables.GSM_VIEW;

import com.flamesgroup.antrax.storage.commons.impl.CellEqualizerAlgorithm;
import com.flamesgroup.antrax.storage.commons.impl.CellEqualizerBasicAlgorithm;
import com.flamesgroup.antrax.storage.commons.impl.CellEqualizerRandomAlgorithm;
import com.flamesgroup.antrax.storage.commons.impl.CellEqualizerStatisticAlgorithm;
import com.flamesgroup.antrax.storage.commons.impl.GsmView;
import com.flamesgroup.antrax.storage.connection.IDbConnection;
import com.flamesgroup.antrax.storage.connection.IDbConnectionPool;
import com.flamesgroup.antrax.storage.dao.IGsmViewDAO;
import com.flamesgroup.antrax.storage.exceptions.DataModificationException;
import com.flamesgroup.storage.jooq.tables.records.CellEqualizerConfigurationRecord;
import com.flamesgroup.storage.jooq.tables.records.GsmViewRecord;
import com.flamesgroup.storage.map.CellEqualizerMapper;
import com.flamesgroup.storage.map.GsmViewMapper;
import com.google.gson.Gson;
import com.google.gson.JsonNull;
import org.jooq.DSLContext;
import org.jooq.DeleteQuery;
import org.jooq.InsertQuery;
import org.jooq.SelectQuery;
import org.jooq.exception.DataAccessException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

public class GsmViewDAO implements IGsmViewDAO {

  private static final Logger logger = LoggerFactory.getLogger(GsmViewDAO.class);

  private final IDbConnectionPool dbConnectionPool;

  public GsmViewDAO(final IDbConnectionPool dbConnectionPool) {
    this.dbConnectionPool = dbConnectionPool;
  }

  @Override
  public void upsertManualGsmView(final List<GsmView> gsmViews) throws DataModificationException {
    logger.debug("[{}] - trying to upsert GsmView@{}", this, gsmViews.hashCode());
    IDbConnection connection = dbConnectionPool.getConnection();
    try {
      DSLContext context = connection.getDSLContext();
      context.transaction(configuration -> {
        for (GsmView gsmView : gsmViews) {
          InsertQuery<GsmViewRecord> insertQuery = context.insertQuery(GSM_VIEW);

          insertQuery.onDuplicateKeyUpdate(true);

          insertQuery.addValueForUpdate(GSM_VIEW.TRUSTED, gsmView.isTrusted());
          insertQuery.addValueForUpdate(GSM_VIEW.SERVING_CELL, gsmView.isServingCell());
          insertQuery.addValueForUpdate(GSM_VIEW.NOTES, gsmView.getNotes());

          GsmViewRecord gsmViewRecord = context.newRecord(GSM_VIEW);
          GsmViewMapper.mapGsmViewToGsmViewRecord(gsmView, gsmViewRecord);

          insertQuery.addRecord(gsmViewRecord);

          int res = insertQuery.execute();

          if (res > 0) {
            logger.debug("[{}] - upsert GsmView@{}", this, gsmView.hashCode());
          } else {
            logger.warn("[{}] - upsert 0 elements of GsmView@{}", this, gsmView.hashCode());
          }
        }
      });
    } catch (DataAccessException e) {
      throw new DataModificationException(String.format("[%s] - execution error, while upsert GsmView", this), e);
    } finally {
      connection.close();
    }
  }

  @Override
  public void upsertAutomaticGsmView(final List<GsmView> gsmViews) throws DataModificationException {
    logger.debug("[{}] - trying to upsert GsmView@{}", this, gsmViews.hashCode());
    IDbConnection connection = dbConnectionPool.getConnection();
    try {
      DSLContext context = connection.getDSLContext();
      context.transaction(configuration -> {
        for (GsmView gsmView : gsmViews) {
          InsertQuery<GsmViewRecord> insertQuery = context.insertQuery(GSM_VIEW);

          insertQuery.onDuplicateKeyUpdate(true);

          insertQuery.addValueForUpdate(GSM_VIEW.LAST_APPEARANCE, gsmView.getLastAppearance());
          insertQuery.addValueForUpdate(GSM_VIEW.MCC, gsmView.getLastMcc());
          insertQuery.addValueForUpdate(GSM_VIEW.MNC, gsmView.getLastMnc());
          insertQuery.addValueForUpdate(GSM_VIEW.NETWORK_SURVEY_RX_LEV, gsmView.getNetworkSurveyLastRxLev());
          insertQuery.addValueForUpdate(GSM_VIEW.CELL_INFO_RX_LEV, gsmView.getCellInfoLastRxLev());
          insertQuery.addValueForUpdate(GSM_VIEW.NETWORK_SURVEY_NUMBER_OCCURRENCES, gsmView.getNetworkSurveyNumberOccurrences());
          insertQuery.addValueForUpdate(GSM_VIEW.SERVING_NUMBER_OCCURRENCES, gsmView.getServingNumberOccurrences());
          insertQuery.addValueForUpdate(GSM_VIEW.NEIGHBOURS_NUMBER_OCCURRENCES, gsmView.getNeighborsNumberOccurrences());
          insertQuery.addValueForUpdate(GSM_VIEW.SUCCESSFUL_CALLS_COUNT, gsmView.getSuccessfulCallsCount());
          insertQuery.addValueForUpdate(GSM_VIEW.TOTAL_CALLS_COUNT, gsmView.getTotalCallsCount());
          insertQuery.addValueForUpdate(GSM_VIEW.CALLS_DURATION, gsmView.getCallsDuration());
          insertQuery.addValueForUpdate(GSM_VIEW.OUTGOING_SMS_COUNT, gsmView.getOutgoingSmsCount());
          insertQuery.addValueForUpdate(GSM_VIEW.INCOMING_SMS_COUNT, gsmView.getIncomingSmsCount());
          insertQuery.addValueForUpdate(GSM_VIEW.USSD_COUNT, gsmView.getUssdCount());
          insertQuery.addValueForUpdate(GSM_VIEW.AMOUNT_PDD, gsmView.getAmountPdd());

          GsmViewRecord gsmViewRecord = context.newRecord(GSM_VIEW);
          GsmViewMapper.mapGsmViewToGsmViewRecord(gsmView, gsmViewRecord);

          insertQuery.addRecord(gsmViewRecord);

          int res = insertQuery.execute();

          if (res > 0) {
            logger.debug("[{}] - upsert GsmView@{}", this, gsmView.hashCode());
          } else {
            logger.warn("[{}] - upsert 0 elements of GsmView@{}", this, gsmView.hashCode());
          }
        }
      });
    } catch (DataAccessException e) {
      throw new DataModificationException(String.format("[%s] - execution error, while upsert GsmView", this), e);
    } finally {
      connection.close();
    }
  }

  @Override
  public List<GsmView> listGsmView(final Long serverId) {
    logger.debug("[{}] - trying to select GsmView for serverId [{}]", this, serverId);
    IDbConnection connection = dbConnectionPool.getConnection();
    try {
      DSLContext context = connection.getDSLContext();
      return context.transactionResult(configuration -> {
        SelectQuery<GsmViewRecord> selectQuery = context.selectQuery(GSM_VIEW);
        selectQuery.addConditions(GSM_VIEW.SERVER_ID.eq(serverId));

        return selectQuery.fetchInto(GsmViewRecord.class).stream().map(GsmViewMapper::mapGsmViewRecordToGsmView).collect(Collectors.toList());
      });
    } catch (DataAccessException e) {
      logger.warn("[{}] - while selecting list of GsmView for serverId [{}]", this, serverId, e);
      return new ArrayList<>();
    } finally {
      connection.close();
    }
  }

  @Override
  public void clearGsmView(final long serverId) {
    logger.debug("[{}] - trying to clear GsmView for serverId [{}]", this, serverId);
    IDbConnection connection = dbConnectionPool.getConnection();
    try {
      DSLContext context = connection.getDSLContext();
      context.transaction(configuration -> {
        DeleteQuery<GsmViewRecord> deleteQuery = context.deleteQuery(GSM_VIEW);
        deleteQuery.addConditions(GSM_VIEW.SERVER_ID.eq(serverId));

        int res = deleteQuery.execute();

        if (res > 0) {
          logger.debug("[{}] - clear GsmView for serverId [{}]", this, serverId);
        } else {
          logger.warn("[{}] - clear 0 elements of GsmView for serverId [{}]", this, serverId);
        }
      });
    } catch (DataAccessException e) {
      logger.warn("[{}] - while clear list of GsmView for serverId [{}]", this, serverId, e);
    } finally {
      connection.close();
    }
  }

  @Override
  public void upsertCellEqualizerBasicAlgorithmConfiguration(final CellEqualizerBasicAlgorithm cellEqualizerBasicAlgorithm) throws DataModificationException {
    logger.debug("[{}] - trying to upsert cellEqualizerBasicAlgorithm: [{}]", this, cellEqualizerBasicAlgorithm);
    IDbConnection connection = dbConnectionPool.getConnection();
    try {
      DSLContext context = connection.getDSLContext();
      context.transaction(configuration -> {
        InsertQuery<CellEqualizerConfigurationRecord> insertQuery = context.insertQuery(CELL_EQUALIZER_CONFIGURATION);
        CellEqualizerConfigurationRecord cellEqualizerConfigurationRecord = context.newRecord(CELL_EQUALIZER_CONFIGURATION);
        CellEqualizerMapper.cellEqualizerBasicAlgorithmToCellEqualizerConfigurationRecord(cellEqualizerBasicAlgorithm, cellEqualizerConfigurationRecord);
        if (cellEqualizerBasicAlgorithm.getID() > 0) {
          cellEqualizerConfigurationRecord.setId(cellEqualizerBasicAlgorithm.getID());
        }
        insertQuery.onDuplicateKeyUpdate(true);
        insertQuery.addValueForUpdate(CELL_EQUALIZER_CONFIGURATION.CLASS_NAME, cellEqualizerBasicAlgorithm.getClass().getName());
        insertQuery.addValueForUpdate(CELL_EQUALIZER_CONFIGURATION.CONFIGURATION, JsonNull.INSTANCE);

        insertQuery.addRecord(cellEqualizerConfigurationRecord);

        int res = insertQuery.execute();

        if (res > 0) {
          logger.debug("[{}] - upsert cellEqualizerBasicAlgorithm: [{}]", this, cellEqualizerBasicAlgorithm);
        } else {
          logger.warn("[{}] - upsert 0 elements of cellEqualizerBasicAlgorithm: [{}]", this, cellEqualizerBasicAlgorithm);
        }
      });
    } catch (DataAccessException e) {
      throw new DataModificationException(String.format("[%s] - execution error, while upsert cellEqualizerBasicAlgorithm", this), e);
    } finally {
      connection.close();
    }
  }

  @Override
  public void upsertCellEqualizerRandomAlgorithmConfiguration(final CellEqualizerRandomAlgorithm cellEqualizerRandomAlgorithm) throws DataModificationException {
    logger.debug("[{}] - trying to upsert cellEqualizerRandomAlgorithm: [{}]", this, cellEqualizerRandomAlgorithm);
    IDbConnection connection = dbConnectionPool.getConnection();
    try {
      DSLContext context = connection.getDSLContext();
      context.transaction(configuration -> {
        InsertQuery<CellEqualizerConfigurationRecord> insertQuery = context.insertQuery(CELL_EQUALIZER_CONFIGURATION);
        CellEqualizerConfigurationRecord cellEqualizerConfigurationRecord = context.newRecord(CELL_EQUALIZER_CONFIGURATION);
        CellEqualizerMapper.cellEqualizerRandomAlgorithmToCellEqualizerConfigurationRecord(cellEqualizerRandomAlgorithm, cellEqualizerConfigurationRecord);
        if (cellEqualizerRandomAlgorithm.getID() > 0) {
          cellEqualizerConfigurationRecord.setId(cellEqualizerRandomAlgorithm.getID());
        }
        insertQuery.onDuplicateKeyUpdate(true);
        insertQuery.addValueForUpdate(CELL_EQUALIZER_CONFIGURATION.CLASS_NAME, cellEqualizerRandomAlgorithm.getClass().getName());
        insertQuery.addValueForUpdate(CELL_EQUALIZER_CONFIGURATION.CONFIGURATION, new Gson().toJsonTree(cellEqualizerRandomAlgorithm.getConfiguration()));

        insertQuery.addRecord(cellEqualizerConfigurationRecord);

        int res = insertQuery.execute();

        if (res > 0) {
          logger.debug("[{}] - upsert cellEqualizerRandomAlgorithm: [{}]", this, cellEqualizerRandomAlgorithm);
        } else {
          logger.warn("[{}] - upsert 0 elements of cellEqualizerRandomAlgorithm: [{}]", this, cellEqualizerRandomAlgorithm);
        }
      });
    } catch (DataAccessException e) {
      throw new DataModificationException(String.format("[%s] - execution error, while upsert cellEqualizerRandomAlgorithm", this), e);
    } finally {
      connection.close();
    }
  }

  @Override
  public void upsertCellEqualizerStatisticAlgorithmConfiguration(final CellEqualizerStatisticAlgorithm cellEqualizerStatisticAlgorithm) throws DataModificationException {
    logger.debug("[{}] - trying to upsert cellEqualizerStatisticAlgorithm: [{}]", this, cellEqualizerStatisticAlgorithm);
    IDbConnection connection = dbConnectionPool.getConnection();
    try {
      DSLContext context = connection.getDSLContext();
      context.transaction(configuration -> {
        InsertQuery<CellEqualizerConfigurationRecord> insertQuery = context.insertQuery(CELL_EQUALIZER_CONFIGURATION);
        CellEqualizerConfigurationRecord cellEqualizerConfigurationRecord = context.newRecord(CELL_EQUALIZER_CONFIGURATION);
        CellEqualizerMapper.cellEqualizerStatisticAlgorithmToCellEqualizerConfigurationRecord(cellEqualizerStatisticAlgorithm, cellEqualizerConfigurationRecord);
        if (cellEqualizerStatisticAlgorithm.getID() > 0) {
          cellEqualizerConfigurationRecord.setId(cellEqualizerStatisticAlgorithm.getID());
        }
        insertQuery.onDuplicateKeyUpdate(true);
        insertQuery.addValueForUpdate(CELL_EQUALIZER_CONFIGURATION.CLASS_NAME, cellEqualizerStatisticAlgorithm.getClass().getName());
        insertQuery.addValueForUpdate(CELL_EQUALIZER_CONFIGURATION.CONFIGURATION, new Gson().toJsonTree(cellEqualizerStatisticAlgorithm.getConfiguration()));

        insertQuery.addRecord(cellEqualizerConfigurationRecord);

        int res = insertQuery.execute();

        if (res > 0) {
          logger.debug("[{}] - upsert cellEqualizerStatisticAlgorithm: [{}]", this, cellEqualizerStatisticAlgorithm);
        } else {
          logger.warn("[{}] - upsert 0 elements of cellEqualizerStatisticAlgorithm: [{}]", this, cellEqualizerStatisticAlgorithm);
        }
      });
    } catch (DataAccessException e) {
      throw new DataModificationException(String.format("[%s] - execution error, while upsert cellEqualizerStatisticAlgorithm", this), e);
    } finally {
      connection.close();
    }
  }

  @Override
  public CellEqualizerAlgorithm getCellEqualizerConfiguration(final long serverId) {
    logger.debug("[{}] - trying to select CellEqualizerConfiguration for serverId [{}]", this, serverId);
    IDbConnection connection = dbConnectionPool.getConnection();
    try {
      DSLContext context = connection.getDSLContext();
      return context.transactionResult(configuration -> {
        SelectQuery<CellEqualizerConfigurationRecord> selectQuery = context.selectQuery(CELL_EQUALIZER_CONFIGURATION);
        selectQuery.addConditions(CELL_EQUALIZER_CONFIGURATION.SERVER_ID.eq(serverId));

        CellEqualizerConfigurationRecord cellEqualizerConfigurationRecord = selectQuery.fetchOneInto(CellEqualizerConfigurationRecord.class);
        return cellEqualizerConfigurationRecord == null ? null : CellEqualizerMapper.cellEqualizerConfigurationRecordToCellEqualizerAlgorithm(cellEqualizerConfigurationRecord);
      });
    } catch (DataAccessException e) {
      logger.warn("[{}] - while selecting list of CellEqualizerConfiguration for serverId [{}]", this, serverId, e);
      return null;
    } finally {
      connection.close();
    }
  }

}
