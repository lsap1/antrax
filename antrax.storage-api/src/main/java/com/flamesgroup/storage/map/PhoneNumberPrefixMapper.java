/*
 * Copyright (C) 2017 Flames Group SIA
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.flamesgroup.storage.map;

import com.flamesgroup.antrax.storage.commons.impl.PhoneNumberPrefix;
import com.flamesgroup.antrax.storage.commons.impl.PhoneNumberPrefixConfig;
import com.flamesgroup.antrax.storage.commons.impl.PhoneNumberPrefixList;
import com.flamesgroup.storage.jooq.tables.records.PhoneNumberPrefixConfigRecord;
import com.flamesgroup.storage.jooq.tables.records.PhoneNumberPrefixListRecord;
import com.flamesgroup.storage.jooq.tables.records.PhoneNumberPrefixRecord;

import java.util.Objects;

public final class PhoneNumberPrefixMapper {

  private PhoneNumberPrefixMapper() {
  }

  public static PhoneNumberPrefixConfig mapPhoneNumberPrefixConfigRecordToPhoneNumberPrefixConfig(final PhoneNumberPrefixConfigRecord phoneNumberPrefixConfigRecord) {
    Objects.requireNonNull(phoneNumberPrefixConfigRecord, "phoneNumberPrefixConfigRecord mustn't be null");
    PhoneNumberPrefixConfig phoneNumberPrefixConfig = new PhoneNumberPrefixConfig();
    phoneNumberPrefixConfig.setDefaultPrefix(phoneNumberPrefixConfigRecord.getDefaultPrefix())
        .setEnable(phoneNumberPrefixConfigRecord.getEnable());
    return phoneNumberPrefixConfig;
  }

  public static void mapPhoneNumberPrefixConfigToPhoneNumberPrefixConfigRecord(final PhoneNumberPrefixConfig phoneNumberPrefixConfig,
      final PhoneNumberPrefixConfigRecord phoneNumberPrefixConfigRecord) {
    Objects.requireNonNull(phoneNumberPrefixConfig, "phoneNumberPrefixConfig mustn't be null");
    Objects.requireNonNull(phoneNumberPrefixConfigRecord, "phoneNumberPrefixConfigRecord mustn't be null");
    phoneNumberPrefixConfigRecord.setDefaultPrefix(phoneNumberPrefixConfig.getDefaultPrefix());
    phoneNumberPrefixConfigRecord.setEnable(phoneNumberPrefixConfig.isEnable());
  }

  public static PhoneNumberPrefix mapPhoneNumberPrefixRecordToPhoneNumberPrefix(final PhoneNumberPrefixRecord phoneNumberPrefixRecord) {
    Objects.requireNonNull(phoneNumberPrefixRecord, "phoneNumberPrefixRecord mustn't be null");
    PhoneNumberPrefix phoneNumberPrefix = new PhoneNumberPrefix(phoneNumberPrefixRecord.getId());
    phoneNumberPrefix.setName(phoneNumberPrefixRecord.getName())
        .setPrefix(phoneNumberPrefixRecord.getPrefix())
        .setPhoneNumberPrefixCount(phoneNumberPrefixRecord.getPhoneNumberCount());
    return phoneNumberPrefix;
  }

  public static void mapPhoneNumberPrefixToPhoneNumberPrefixRecord(final PhoneNumberPrefix phoneNumberPrefix, final PhoneNumberPrefixRecord phoneNumberPrefixRecord) {
    Objects.requireNonNull(phoneNumberPrefix, "phoneNumberPrefix mustn't be null");
    Objects.requireNonNull(phoneNumberPrefixRecord, "phoneNumberPrefixRecord mustn't be null");
    phoneNumberPrefixRecord.setName(phoneNumberPrefix.getName())
        .setPrefix(phoneNumberPrefix.getPrefix())
        .setPhoneNumberCount(phoneNumberPrefix.getPhoneNumberPrefixCount());
  }

  public static PhoneNumberPrefixList mapPhoneNumberPrefixListRecordToPhoneNumberPrefixList(final PhoneNumberPrefixListRecord phoneNumberPrefixListRecord,
      final PhoneNumberPrefix phoneNumberPrefix) {
    Objects.requireNonNull(phoneNumberPrefixListRecord, "phoneNumberPrefixListRecord mustn't be null");
    PhoneNumberPrefixList phoneNumberPrefixList = new PhoneNumberPrefixList(phoneNumberPrefixListRecord.getId());
    phoneNumberPrefixList.setPhoneNumber(phoneNumberPrefixListRecord.getPhoneNumber())
        .setPhoneNumberPrefix(phoneNumberPrefix);
    return phoneNumberPrefixList;
  }

  public static void mapPhoneNumberPrefixListToPhoneNumberPrefixListRecord(final PhoneNumberPrefixList phoneNumberPrefixList,
      final PhoneNumberPrefixListRecord phoneNumberPrefixListRecord) {
    Objects.requireNonNull(phoneNumberPrefixList, "phoneNumberPrefixList mustn't be null");
    Objects.requireNonNull(phoneNumberPrefixListRecord, "phoneNumberPrefixListRecord mustn't be null");
    phoneNumberPrefixListRecord.setPhoneNumber(phoneNumberPrefixList.getPhoneNumber())
        .setPhoneNumberPrefixId(phoneNumberPrefixList.getPhoneNumberPrefix().getID());
  }

}
