/*
 * Copyright (C) 2017 Flames Group SIA
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.flamesgroup.storage.convert;

import com.flamesgroup.unit.ICCID;
import org.jooq.Converter;

public final class IccidConverter implements Converter<String, ICCID> {

  private static final long serialVersionUID = 4757352188127460345L;

  @Override
  public ICCID from(final String databaseObject) {
    return databaseObject == null ? null : new ICCID(databaseObject);
  }

  @Override
  public String to(final ICCID userObject) {
    return userObject == null ? null : userObject.getValue();
  }

  @Override
  public Class<String> fromType() {
    return String.class;
  }

  @Override
  public Class<ICCID> toType() {
    return ICCID.class;
  }

}
