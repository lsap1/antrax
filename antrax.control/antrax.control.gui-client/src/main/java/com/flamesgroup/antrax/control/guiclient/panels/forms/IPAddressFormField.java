/*
 * Copyright (C) 2017 Flames Group SIA
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.flamesgroup.antrax.control.guiclient.panels.forms;

import com.flamesgroup.antrax.control.swingwidgets.field.IPTextField;

import java.awt.*;
import java.net.InetAddress;

public class IPAddressFormField extends FormField<InetAddress> {

  private IPTextField component;

  public IPAddressFormField(final int id, final String name) {
    super(id, name);
  }

  @Override
  public Component getComponent() {
    if (component == null) {
      component = new IPTextField();
    }
    return component;
  }

  public IPTextField getFieldComponent() {
    return (IPTextField) getComponent();
  }

  @Override
  public InetAddress getValue() {
    return getFieldComponent().getInetAddress();
  }

  @Override
  public void setValue(final InetAddress value) {
    getFieldComponent().setInetAddress(value);
  }

  @Override
  public void clear() {
    getFieldComponent().clear();
  }

}
