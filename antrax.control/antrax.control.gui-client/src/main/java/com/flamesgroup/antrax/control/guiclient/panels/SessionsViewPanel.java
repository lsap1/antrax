/*
 * Copyright (C) 2017 Flames Group SIA
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.flamesgroup.antrax.control.guiclient.panels;

import com.flamesgroup.antrax.control.guiclient.newapi.AppPanel;
import com.flamesgroup.antrax.control.guiclient.utils.BeansPool;
import com.flamesgroup.antrax.control.guiclient.utils.Refresher;
import com.flamesgroup.antrax.control.guiclient.utils.RefresherThread;
import com.flamesgroup.antrax.control.guiclient.utils.TransactionManager;
import com.flamesgroup.antrax.control.swingwidgets.contextsearch.JContextSearch;
import com.flamesgroup.antrax.control.swingwidgets.editor.JReflectiveBar;
import com.flamesgroup.antrax.control.swingwidgets.table.ColumnWriter;
import com.flamesgroup.antrax.control.swingwidgets.table.JUpdatableTable;
import com.flamesgroup.antrax.control.swingwidgets.table.TableBuilder;
import com.flamesgroup.antrax.control.swingwidgets.table.UpdateTableColumnModel;
import com.flamesgroup.antrax.storage.commons.SessionParams;

import java.awt.*;
import java.util.Date;

import javax.swing.*;

public class SessionsViewPanel extends JPanel implements AppPanel {

  private static final long serialVersionUID = -3001833610394282860L;
  private final JUpdatableTable<SessionParams, String> sessionInfoTable = new JUpdatableTable<>(createBuilder());
  private final Refresher refresher;
  private RefresherThread refresherThread;

  @Override
  public JComponent getComponent() {
    return this;
  }

  @Override
  public void setEditable(final boolean editable) {
  }

  @Override
  public void postInitialize(final RefresherThread refresher, final TransactionManager transactionManager) {
    this.refresherThread = refresher;
  }

  @Override
  public void setActive(final boolean active) {
    if (active) {
      refresherThread.addRefresher(refresher);
    } else {
      refresherThread.removeRefresher(refresher);
    }
  }

  @Override
  public void release() {
    sessionInfoTable.getUpdatableTableProperties().saveProperties();
    refresher.interrupt();
  }

  public SessionsViewPanel() {
    setLayout(new BorderLayout());
    add(createToolbar(), BorderLayout.NORTH);
    add(new JScrollPane(sessionInfoTable), BorderLayout.CENTER);
    refresher = new Refresher("SessionsInfo") {

      private volatile SessionParams[] infos;

      @Override
      public void updateGuiElements() {
        try {
          showInfo(infos);
        } finally {
          infos = null;
        }
      }

      @Override
      protected void refreshInformation(final BeansPool beansPool) throws Exception {
        infos = beansPool.getControlBean().getSessionInfos();
      }

      @Override
      public void handleEnabled(final BeansPool beansPool) {
      }

      @Override
      public void handleDisabled(final BeansPool beansPool) {
      }
    };
    sessionInfoTable.setName(getClass().getSimpleName());
    sessionInfoTable.getUpdatableTableProperties().restoreProperties();
  }

  private void showInfo(final SessionParams[] infos) {
    sessionInfoTable.setData(infos);
  }

  private Component createToolbar() {
    JContextSearch cs = new JContextSearch();
    cs.registerSearchItem(sessionInfoTable);

    JReflectiveBar bar = new JReflectiveBar();
    bar.addToRight(cs);
    return bar;
  }

  private TableBuilder<SessionParams, String> createBuilder() {
    return new TableBuilder<SessionParams, String>() {

      @Override
      public String getUniqueKey(final SessionParams src) {
        return src.getClientUid();
      }

      @Override
      public void buildRow(final SessionParams src, final ColumnWriter<SessionParams> dest) {
        if (src.getIp() != null) {
          dest.writeColumn(src.getIp().getHostAddress());
        } else {
          dest.writeColumn("");
        }
        dest.writeColumn(src.getUserName());
        dest.writeColumn(src.getGroup());
        dest.writeColumn(src.getSessionEndTime() == null ? "yes" : "");
        dest.writeColumn(src.getSessionStartTime());
        dest.writeColumn(src.getSessionLastAccessTime());
        dest.writeColumn(src.getSessionEndTime());
      }

      @Override
      public void buildColumns(final UpdateTableColumnModel columns) {
        columns.addColumn("IP", String.class);
        columns.addColumn("User", String.class);
        columns.addColumn("Group", String.class);
        columns.addColumn("Active", String.class);
        columns.addColumn("Start Time", Date.class);
        columns.addColumn("Last Accessed", Date.class);
        columns.addColumn("End Time", Date.class);
      }
    };
  }
}
