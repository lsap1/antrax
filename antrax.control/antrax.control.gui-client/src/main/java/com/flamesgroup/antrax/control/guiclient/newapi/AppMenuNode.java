/*
 * Copyright (C) 2017 Flames Group SIA
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.flamesgroup.antrax.control.guiclient.newapi;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import javax.swing.*;
import javax.swing.event.TreeModelEvent;
import javax.swing.event.TreeModelListener;
import javax.swing.tree.TreePath;

public class AppMenuNode {
  private final List<AppMenuNode> children = new ArrayList<>();
  private String text;
  private Icon icon;
  private Badge badge;
  private AppPanelProvider panelProvider;
  private TreeModelListener listener;
  private TreePath path;
  private AppPanel panel;
  private JTree tree;

  public AppMenuNode(final JTree tree) {
    this.path = new TreePath(this);
    this.tree = tree;
  }

  public AppMenuNode(final String text, final Icon icon, final Badge badge, final AppPanelProvider panelProvider) {
    this.text = text;
    this.icon = icon;
    this.badge = badge;
    this.panelProvider = panelProvider;
    if (this.badge != null) {
      this.badge.addBadgeValueChangeListener(new BadgeValueChangeListener() {

        @Override
        public void handleBadgeValueChanged(final Badge badge) {
          if (path != null && listener != null) {
            listener.treeNodesChanged(new TreeModelEvent(tree, path));
          }
        }
      });
    }
  }

  public void addNode(final AppMenuNode appMenuNode) {
    appMenuNode.setTree(tree);
    children.add(appMenuNode);
    appMenuNode.setTreeModelListener(listener);
    if (path != null) {
      TreePath childPath = path.pathByAddingChild(appMenuNode);
      appMenuNode.setTreePath(childPath);
      listener.treeNodesInserted(new TreeModelEvent(tree, path, new int[] {children.size() - 1}, new Object[] {appMenuNode}));
      tree.expandPath(childPath);
    }
  }

  private void setTree(final JTree tree) {
    this.tree = tree;
    for (AppMenuNode n : children) {
      n.setTree(tree);
    }
  }

  private void setTreePath(final TreePath treePath) {
    this.path = treePath;
    for (AppMenuNode n : children) {
      n.setTreePath(path.pathByAddingChild(n));
    }
  }

  public void setTreeModelListener(final TreeModelListener listener) {
    this.listener = listener;
    for (AppMenuNode n : children) {
      n.setTreeModelListener(listener);
    }
  }

  public int getChildCount() {
    return children.size();
  }

  public AppMenuNode getChild(final int index) {
    return children.get(index);
  }

  public int getIndexOf(final AppMenuNode appMenuNode) {
    return children.indexOf(appMenuNode);
  }

  public String getText() {
    return text;
  }

  public Icon getIcon() {
    return icon;
  }

  public Badge getBadge() {
    return badge;
  }

  public AppPanel getPanel() {
    if (panel == null && panelProvider != null) {
      panel = panelProvider.createPanel();
    }
    return panel;
  }

  public AppMenuNode findNode(final String text) {
    for (AppMenuNode node : children) {
      if (node.getText().equals(text)) {
        return node;
      }
    }
    return null;
  }

  public TreePath getTreePath() {
    return path;
  }

  public void setIcon(final Icon icon) {
    this.icon = icon;
    listener.treeNodesChanged(new TreeModelEvent(tree, path));
  }

  public void release() {
    if (getPanel() != null) {
      getPanel().release();
    }
    for (AppMenuNode child : children) {
      child.release();
    }
  }

  @Override
  public String toString() {
    return text;
  }

  public int getRecursiveChildCount() {
    int count = getChildCount();
    for (AppMenuNode child : children) {
      count += child.getRecursiveChildCount();
    }
    return count;
  }

  public void sortChildren() {
    TreePath selected = tree.getSelectionPath();
    Collections.sort(children, new Comparator<AppMenuNode>() {
      @Override
      public int compare(final AppMenuNode o1, final AppMenuNode o2) {
        return o1.text.compareTo(o2.text);
      }
    });
    if (listener != null) {
      listener.treeStructureChanged(new TreeModelEvent(tree, path));
    }
    tree.expandPath(path);
    tree.setSelectionPath(selected);
  }

  public void removeNode(final AppMenuNode childNode) {
    int index = getIndexOf(childNode);
    children.remove(index);
    if (listener != null) {
      listener.treeNodesRemoved(new TreeModelEvent(tree, path.getPath(), new int[] {index}, new Object[] {childNode}));
    }
    childNode.release();
  }
}
