/*
 * Copyright (C) 2017 Flames Group SIA
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.flamesgroup.antrax.control.guiclient.panels;

import com.flamesgroup.antrax.control.GuiProperties;
import com.flamesgroup.antrax.control.authorization.NotPermittedException;
import com.flamesgroup.antrax.control.guiclient.MainApp;
import com.flamesgroup.antrax.control.guiclient.refreshers.GsmViewRefresher;
import com.flamesgroup.antrax.control.guiclient.utils.BeansPool;
import com.flamesgroup.antrax.control.guiclient.utils.RefresherThread;
import com.flamesgroup.antrax.control.guiclient.utils.TransactionListener;
import com.flamesgroup.antrax.control.guiclient.utils.TransactionManager;
import com.flamesgroup.antrax.control.guiclient.utils.TransactionState;
import com.flamesgroup.antrax.control.swingwidgets.contextsearch.JContextSearch;
import com.flamesgroup.antrax.control.swingwidgets.editor.JEditIntField;
import com.flamesgroup.antrax.control.swingwidgets.editor.JReflectiveBar;
import com.flamesgroup.antrax.control.swingwidgets.scroller.ScrollBarWidgetFactory;
import com.flamesgroup.antrax.control.swingwidgets.table.JExportCsvButton;
import com.flamesgroup.antrax.storage.commons.impl.CellEqualizerAlgorithm;
import com.flamesgroup.antrax.storage.commons.impl.CellEqualizerBasicAlgorithm;
import com.flamesgroup.antrax.storage.commons.impl.CellEqualizerRandomAlgorithm;
import com.flamesgroup.antrax.storage.commons.impl.CellEqualizerStatisticAlgorithm;
import com.flamesgroup.antrax.storage.commons.impl.CellEqualizerStatisticAlgorithm.CellEqualizerStatisticConfiguration;
import com.flamesgroup.antrax.storage.commons.impl.GsmView;

import java.awt.*;
import java.awt.event.ActionListener;
import java.time.Instant;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.time.ZoneId;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Collections;
import java.util.Date;
import java.util.List;
import java.util.concurrent.atomic.AtomicBoolean;

import javax.swing.*;
import javax.swing.border.EmptyBorder;
import javax.swing.table.TableCellEditor;
import javax.swing.table.TableModel;
import javax.swing.table.TableRowSorter;

public class GsmViewPanel extends JPanel {

  private static final long serialVersionUID = -550321748191186931L;

  private GsmViewRefresher refresher;
  private static final GsmViewTable gsmViewTable = new GsmViewTable();
  private static final JComponent scrollPanel = ScrollBarWidgetFactory.createScrollPaneWithButtonsTogether(gsmViewTable);
  private static final JCheckBox multiArfcn = new JCheckBox("Multi arfcn", true);

  private static final AtomicBoolean loadProperties = new AtomicBoolean();
  private static final AtomicBoolean saveProperties = new AtomicBoolean();

  private final JCheckBox checkSelectedServing = new JCheckBox("Selected serving");

  private final JCheckBox checkSelectedTrusted = new JCheckBox("Selected trusted");

  private final JButton clearButton = new JButton("Clear");
  private final JButton resetButton = new JButton("Reset statistic");

  private final JRadioButton basicAlgorithmRadio = new JRadioButton("Basic");
  private final JRadioButton randomAlgorithmRadio = new JRadioButton("Random");
  private final JRadioButton statisticAlgorithmRadio = new JRadioButton("Statistic");
  private final JPanel algorithmDetailsPanel = new JPanel();
  private final JPanel extendPanel = new JPanel();
  private final JPanel randomPanel = new JPanel();
  private final JPanel statisticPanel = new JPanel();
  private final JEditIntField minRandomPeriodField = new JEditIntField(0, Integer.MAX_VALUE);
  private final JEditIntField maxRandomPeriodField = new JEditIntField(0, Integer.MAX_VALUE);

  private final JEditIntField minStatisticSuccessfulCallCountField = new JEditIntField(0, Integer.MAX_VALUE);
  private final JEditIntField maxStatisticSuccessfulCallCountField = new JEditIntField(0, Integer.MAX_VALUE);
  private final JEditIntField minStatisticUssdCountField = new JEditIntField(0, Integer.MAX_VALUE);
  private final JEditIntField maxStatisticUssdCountField = new JEditIntField(0, Integer.MAX_VALUE);
  private final JEditIntField minStatisticOutgoingSmsCountField = new JEditIntField(0, Integer.MAX_VALUE);
  private final JEditIntField maxStatisticOutgoingSmsCountField = new JEditIntField(0, Integer.MAX_VALUE);

  private final JEditIntField minWaitingTimeField = new JEditIntField(0, Integer.MAX_VALUE);
  private final JEditIntField maxWaitingTimeField = new JEditIntField(0, Integer.MAX_VALUE);
  private final JSpinner fromLeadTimeSpinner = new JSpinner(new SpinnerDateModel());
  private final JSpinner toLeadTimeSpinner = new JSpinner(new SpinnerDateModel());
  private final String server;
  private final AtomicBoolean isClear = new AtomicBoolean(false);
  private final AtomicBoolean firstUpdate = new AtomicBoolean(false);
  private final AtomicBoolean lastEditable = new AtomicBoolean(false);

  private JReflectiveBar tableControlPanel;

  private final JContextSearch searchField = new JContextSearch();

  private final GuiProperties guiProperties;

  private RefresherThread refresherThread;

  public GsmViewPanel(final String server) {
    super(new BorderLayout());
    this.server = server;

    initComponents();
    guiProperties = new GuiProperties(GsmViewPanel.class.getSimpleName()) {
      private static final String MULTI_ARFCN = "multiArfcn";

      @Override
      protected void restoreOutProperties() {
        String multiArfcnString = properties.getProperty(MULTI_ARFCN, "true");
        multiArfcn.setSelected(Boolean.valueOf(multiArfcnString));
      }

      @Override
      protected void saveOutProperties() {
        properties.setProperty(MULTI_ARFCN, String.valueOf(multiArfcn.isSelected()));
      }
    };

    if (!loadProperties.getAndSet(true)) {
      gsmViewTable.getUpdatableTableProperties().restoreProperties();
      guiProperties.restoreProperties();
    }
  }

  private void initComponents() {
    setBorder(new EmptyBorder(5, 5, 5, 5));
    JPanel topPanel = new JPanel();
    JPanel topControlsPanel = new JPanel();
    topPanel.setLayout(new BoxLayout(topPanel, BoxLayout.Y_AXIS));
    topControlsPanel.setLayout(new BorderLayout());
    algorithmDetailsPanel.setLayout(new BoxLayout(algorithmDetailsPanel, BoxLayout.Y_AXIS));

    JPanel algorithmPanel = new JPanel();
    algorithmPanel.setLayout(new GridLayout(2, 1));
    JLabel algorithmLabel = new JLabel("The allocation algorithm between base stations:");
    algorithmPanel.add(algorithmLabel);
    ButtonGroup radioGroup = new ButtonGroup();
    radioGroup.add(basicAlgorithmRadio);
    radioGroup.add(randomAlgorithmRadio);
    radioGroup.add(statisticAlgorithmRadio);
    basicAlgorithmRadio.setSelected(true);
    basicAlgorithmRadio.addActionListener(createActionListenerForRadioButton());
    randomAlgorithmRadio.addActionListener(createActionListenerForRadioButton());
    statisticAlgorithmRadio.addActionListener(createActionListenerForRadioButton());

    JPanel radioPanel = new JPanel(new FlowLayout(FlowLayout.LEFT));
    radioPanel.add(basicAlgorithmRadio);
    radioPanel.add(randomAlgorithmRadio);
    radioPanel.add(statisticAlgorithmRadio);
    algorithmPanel.add(radioPanel);
    topControlsPanel.add(algorithmPanel, BorderLayout.NORTH);

    topControlsPanel.add(algorithmDetailsPanel, BorderLayout.CENTER);

    randomPanel.setLayout(new GridBagLayout());
    randomPanel.setBorder(new EmptyBorder(5, 0, 0, 0));
    ((GridBagLayout) randomPanel.getLayout()).columnWidths = new int[] {0, 0};
    ((GridBagLayout) randomPanel.getLayout()).rowHeights = new int[] {0, 0, 0, 0, 0};
    ((GridBagLayout) randomPanel.getLayout()).columnWeights = new double[] {1.0, 1.0E-4};
    ((GridBagLayout) randomPanel.getLayout()).rowWeights = new double[] {0.0, 0.0, 0.0, 0.0, 1.0E-4};

    randomPanel.add(new JLabel("Period of regenerated cell equalizer"), new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0,
        GridBagConstraints.WEST, GridBagConstraints.VERTICAL,
        new Insets(0, 0, 0, 0), 0, 0));
    JPanel timeEditorPanel = new JPanel();
    timeEditorPanel.setLayout(new FlowLayout(FlowLayout.LEFT));
    timeEditorPanel.add(new JLabel("from: "));
    minRandomPeriodField.setColumns(10);
    minRandomPeriodField.setText("10");
    timeEditorPanel.add(minRandomPeriodField);
    timeEditorPanel.add(new JLabel("..."));
    maxRandomPeriodField.setColumns(10);
    maxRandomPeriodField.setText("30");
    timeEditorPanel.add(maxRandomPeriodField);
    timeEditorPanel.add(new JLabel(" min"));
    randomPanel.add(timeEditorPanel, new GridBagConstraints(0, 1, 1, 1, 0.0, 0.0,
        GridBagConstraints.WEST, GridBagConstraints.VERTICAL,
        new Insets(0, 0, 0, 0), 0, 0));

    extendPanel.setLayout(new GridBagLayout());
    extendPanel.setBorder(new EmptyBorder(5, 0, 0, 0));
    ((GridBagLayout) extendPanel.getLayout()).columnWidths = new int[] {0, 0};
    ((GridBagLayout) extendPanel.getLayout()).rowHeights = new int[] {0, 0, 0, 0, 0};
    ((GridBagLayout) extendPanel.getLayout()).columnWeights = new double[] {1.0, 1.0E-4};
    ((GridBagLayout) extendPanel.getLayout()).rowWeights = new double[] {0.0, 0.0, 0.0, 0.0, 1.0E-4};
    extendPanel.add(new JLabel("Waiting time between iterations"), new GridBagConstraints(0, 2, 1, 1, 0.0, 0.0,
        GridBagConstraints.WEST, GridBagConstraints.VERTICAL,
        new Insets(0, 0, 0, 0), 0, 0));

    JPanel waitingEditorPanel = new JPanel();
    waitingEditorPanel.setLayout(new FlowLayout(FlowLayout.LEFT));
    waitingEditorPanel.add(new JLabel("from: "));
    minWaitingTimeField.setColumns(10);
    minWaitingTimeField.setText("10");
    waitingEditorPanel.add(minWaitingTimeField);
    waitingEditorPanel.add(new JLabel("..."));
    maxWaitingTimeField.setColumns(10);
    maxWaitingTimeField.setText("30");
    waitingEditorPanel.add(maxWaitingTimeField);
    waitingEditorPanel.add(new JLabel(" sec"));
    extendPanel.add(waitingEditorPanel, new GridBagConstraints(0, 3, 1, 1, 0.0, 0.0,
        GridBagConstraints.WEST, GridBagConstraints.VERTICAL,
        new Insets(0, 0, 0, 0), 0, 0));

    extendPanel.add(new JLabel("Lead time"), new GridBagConstraints(0, 4, 1, 1, 0.0, 0.0,
        GridBagConstraints.WEST, GridBagConstraints.VERTICAL,
        new Insets(0, 0, 0, 0), 0, 0));

    JPanel leadTimeEditorPanel = new JPanel();
    leadTimeEditorPanel.setLayout(new FlowLayout(FlowLayout.LEFT));
    leadTimeEditorPanel.add(new JLabel("from: "));
    JSpinner.DateEditor fromTimeEditor = new JSpinner.DateEditor(fromLeadTimeSpinner, "HH:mm:ss");
    fromLeadTimeSpinner.setEditor(fromTimeEditor);
    Calendar calendar = Calendar.getInstance();
    calendar.set(Calendar.HOUR_OF_DAY, 8);
    calendar.set(Calendar.MINUTE, 0);
    calendar.set(Calendar.SECOND, 0);
    calendar.set(Calendar.MILLISECOND, 0);
    fromLeadTimeSpinner.setValue(calendar.getTime());
    leadTimeEditorPanel.add(fromLeadTimeSpinner);

    leadTimeEditorPanel.add(new JLabel("to: "));
    JSpinner.DateEditor toTimeEditor = new JSpinner.DateEditor(toLeadTimeSpinner, "HH:mm:ss");
    toLeadTimeSpinner.setEditor(toTimeEditor);
    calendar.set(Calendar.HOUR_OF_DAY, 23);
    toLeadTimeSpinner.setValue(calendar.getTime());
    leadTimeEditorPanel.add(toLeadTimeSpinner);

    extendPanel.add(leadTimeEditorPanel, new GridBagConstraints(0, 5, 1, 1, 0.0, 0.0,
        GridBagConstraints.WEST, GridBagConstraints.VERTICAL,
        new Insets(0, 0, 0, 0), 0, 0));

    statisticPanel.setLayout(new GridBagLayout());
    statisticPanel.setBorder(new EmptyBorder(5, 0, 0, 0));
    ((GridBagLayout) statisticPanel.getLayout()).columnWidths = new int[] {0, 0};
    ((GridBagLayout) statisticPanel.getLayout()).rowHeights = new int[] {0, 0, 0, 0, 0};
    ((GridBagLayout) statisticPanel.getLayout()).columnWeights = new double[] {1.0, 1.0E-4};
    ((GridBagLayout) statisticPanel.getLayout()).rowWeights = new double[] {0.0, 0.0, 0.0, 0.0, 1.0E-4};

    statisticPanel.add(new JLabel("Limits for channel"), new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0,
        GridBagConstraints.WEST, GridBagConstraints.VERTICAL,
        new Insets(0, 0, 0, 0), 0, 0));
    JPanel limitsEditorPanel = new JPanel();
    limitsEditorPanel.setLayout(new FlowLayout(FlowLayout.LEFT));
    limitsEditorPanel.add(new JLabel("Successful call count from:"));
    minStatisticSuccessfulCallCountField.setColumns(5);
    minStatisticSuccessfulCallCountField.setText("10");
    limitsEditorPanel.add(minStatisticSuccessfulCallCountField);
    limitsEditorPanel.add(new JLabel("..."));
    maxStatisticSuccessfulCallCountField.setColumns(5);
    maxStatisticSuccessfulCallCountField.setText("30");
    limitsEditorPanel.add(maxStatisticSuccessfulCallCountField);
    limitsEditorPanel.add(new JLabel("| USSD count from:"));
    minStatisticUssdCountField.setColumns(5);
    minStatisticUssdCountField.setText("10");
    limitsEditorPanel.add(minStatisticUssdCountField);
    limitsEditorPanel.add(new JLabel("..."));
    maxStatisticUssdCountField.setColumns(5);
    maxStatisticUssdCountField.setText("30");
    limitsEditorPanel.add(maxStatisticUssdCountField);
    limitsEditorPanel.add(new JLabel("| Outgoing SMS count from:"));
    minStatisticOutgoingSmsCountField.setColumns(5);
    minStatisticOutgoingSmsCountField.setText("10");
    limitsEditorPanel.add(minStatisticOutgoingSmsCountField);
    limitsEditorPanel.add(new JLabel("..."));
    maxStatisticOutgoingSmsCountField.setColumns(5);
    maxStatisticOutgoingSmsCountField.setText("30");
    limitsEditorPanel.add(maxStatisticOutgoingSmsCountField);
    statisticPanel.add(limitsEditorPanel, new GridBagConstraints(0, 3, 1, 1, 0.0, 0.0,
        GridBagConstraints.WEST, GridBagConstraints.VERTICAL,
        new Insets(0, 0, 0, 0), 0, 0));

    searchField.registerSearchItem(gsmViewTable);
    tableControlPanel = new JReflectiveBar();
    if (server != null) {
      topPanel.add(topControlsPanel);

      tableControlPanel.addToLeft(checkSelectedServing);
      tableControlPanel.addToLeft(checkSelectedTrusted);
      tableControlPanel.addToLeft(clearButton);
      tableControlPanel.addToLeft(getExportToCsvButton());
      resetButton.addActionListener(e -> {
        java.util.List<GsmView> selectedElems = gsmViewTable.getSelectedElems();
        if (!selectedElems.isEmpty()) {
          refresher.resetGsmViewStatistic(selectedElems, server);
        }
      });
      tableControlPanel.addToLeft(resetButton);
    }
    tableControlPanel.addToLeft(multiArfcn);
    tableControlPanel.addToRight(searchField);

    topPanel.add(tableControlPanel);

    add(topPanel, BorderLayout.NORTH);
    init();
  }

  private JButton getExportToCsvButton() {
    JExportCsvButton jExportCsvButton = new JExportCsvButton(gsmViewTable, "gsmView");
    jExportCsvButton.setText("Export");
    return jExportCsvButton;
  }

  private ActionListener createActionListenerForRadioButton() {
    return e -> repaintAlgorithmPanel();
  }

  public void repaintAlgorithmPanel() {
    algorithmDetailsPanel.removeAll();
    if (randomAlgorithmRadio.isSelected()) {
      algorithmDetailsPanel.add(randomPanel);
      algorithmDetailsPanel.add(extendPanel);
    } else if (statisticAlgorithmRadio.isSelected()) {
      algorithmDetailsPanel.add(statisticPanel);
      algorithmDetailsPanel.add(extendPanel);
    }
    revalidate();
    repaint();
  }

  public void updateCellEqualizerAlgorithm(final CellEqualizerAlgorithm cellEqualizerAlgorithm) {
    if (cellEqualizerAlgorithm instanceof CellEqualizerBasicAlgorithm) {
      basicAlgorithmRadio.setSelected(true);
    } else if (cellEqualizerAlgorithm instanceof CellEqualizerRandomAlgorithm) {
      randomAlgorithmRadio.setSelected(true);
      CellEqualizerRandomAlgorithm.CellEqualizerRandomConfiguration cellEqualizerRandomAlgorithmConfiguration =
          (CellEqualizerRandomAlgorithm.CellEqualizerRandomConfiguration) cellEqualizerAlgorithm.getConfiguration();
      minRandomPeriodField.setText(String.valueOf(cellEqualizerRandomAlgorithmConfiguration.getMinRegeneratedTime()));
      maxRandomPeriodField.setText(String.valueOf(cellEqualizerRandomAlgorithmConfiguration.getMaxRegeneratedTime()));
      minWaitingTimeField.setText(String.valueOf(cellEqualizerRandomAlgorithmConfiguration.getMinWaitingTime()));
      maxWaitingTimeField.setText(String.valueOf(cellEqualizerRandomAlgorithmConfiguration.getMaxWaitingTime()));
      LocalTime fromLeadTime = cellEqualizerRandomAlgorithmConfiguration.getFromLeadTime();
      if (fromLeadTime != null) {
        Instant instant = fromLeadTime.atDate(LocalDate.now()).atZone(ZoneId.systemDefault()).toInstant();
        fromLeadTimeSpinner.setValue(Date.from(instant));
      }
      LocalTime toLeadTime = cellEqualizerRandomAlgorithmConfiguration.getToLeadTime();
      if (toLeadTime != null) {
        Instant instant = toLeadTime.atDate(LocalDate.now()).atZone(ZoneId.systemDefault()).toInstant();
        toLeadTimeSpinner.setValue(Date.from(instant));
      }
    } else if (cellEqualizerAlgorithm instanceof CellEqualizerStatisticAlgorithm) {
      statisticAlgorithmRadio.setSelected(true);
      CellEqualizerStatisticConfiguration cellEqualizerStatisticAlgorithm = (CellEqualizerStatisticConfiguration) cellEqualizerAlgorithm.getConfiguration();
      minStatisticSuccessfulCallCountField.setText(String.valueOf(cellEqualizerStatisticAlgorithm.getMinSuccessfulCallCount()));
      maxStatisticSuccessfulCallCountField.setText(String.valueOf(cellEqualizerStatisticAlgorithm.getMaxSuccessfulCallCount()));
      minStatisticUssdCountField.setText(String.valueOf(cellEqualizerStatisticAlgorithm.getMinUssdCount()));
      maxStatisticUssdCountField.setText(String.valueOf(cellEqualizerStatisticAlgorithm.getMaxUssdCount()));
      minStatisticOutgoingSmsCountField.setText(String.valueOf(cellEqualizerStatisticAlgorithm.getMinOutgoingSmsCount()));
      maxStatisticOutgoingSmsCountField.setText(String.valueOf(cellEqualizerStatisticAlgorithm.getMaxOutgoingSmsCount()));

      minWaitingTimeField.setText(String.valueOf(cellEqualizerStatisticAlgorithm.getMinWaitingTime()));
      maxWaitingTimeField.setText(String.valueOf(cellEqualizerStatisticAlgorithm.getMaxWaitingTime()));
      LocalTime fromLeadTime = cellEqualizerStatisticAlgorithm.getFromLeadTime();
      if (fromLeadTime != null) {
        Instant instant = fromLeadTime.atDate(LocalDate.now()).atZone(ZoneId.systemDefault()).toInstant();
        fromLeadTimeSpinner.setValue(Date.from(instant));
      }
      LocalTime toLeadTime = cellEqualizerStatisticAlgorithm.getToLeadTime();
      if (toLeadTime != null) {
        Instant instant = toLeadTime.atDate(LocalDate.now()).atZone(ZoneId.systemDefault()).toInstant();
        toLeadTimeSpinner.setValue(Date.from(instant));
      }
    }
    repaintAlgorithmPanel();
    firstUpdate.set(true);
    setEditable(lastEditable.get());
  }

  private void init() {
    // disable sorting column on updating serving and trusted cells
    TableRowSorter<TableModel> sorter = new TableRowSorter<>(gsmViewTable.getModel());
    gsmViewTable.setRowSorter(sorter);
    sorter.setSortsOnUpdates(false);

    checkSelectedServing.addActionListener(e -> {
      for (int selectedRow : gsmViewTable.getSelectedRows()) {
        gsmViewTable.setServingValueAt(checkSelectedServing.isSelected(), selectedRow);
      }
    });
    checkSelectedTrusted.addActionListener(e -> {
      for (int selectedRow : gsmViewTable.getSelectedRows()) {
        gsmViewTable.setTrustedValueAt(checkSelectedTrusted.isSelected(), selectedRow);
      }
    });

    gsmViewTable.setMultiArfcn(multiArfcn.isSelected()); // first init
    multiArfcn.addActionListener(e -> gsmViewTable.setMultiArfcn(multiArfcn.isSelected()));

    clearButton.addActionListener(e -> {
      gsmViewTable.clearData();
      isClear.set(true);
    });
  }

  public void postInitialize(final RefresherThread refresherThread, final TransactionManager transactionManager) {
    this.refresherThread = refresherThread;
    this.refresher = new GsmViewRefresher(refresherThread, transactionManager, this);
    refresher.initializePermanentRefresher(this, server);
    transactionManager.addListener(new TransactionListener() {
      @Override
      public void onTransactionStateChanged(final TransactionState newState) {
        refresherThread.forceRefresh();
        if (newState == TransactionState.ROLLBACK) {
          isClear.set(false);
          TableCellEditor cellEditor = gsmViewTable.getCellEditor();
          if (cellEditor != null) {
            cellEditor.stopCellEditing();
          }
        }
      }

      @Override
      public List<Throwable> handleTransactionGoingToBeCommitted(final int transactionId, final BeansPool beansPool) {
        if (!firstUpdate.get() || server == null) {
          return Collections.emptyList();
        }
        TableCellEditor cellEditor = gsmViewTable.getCellEditor();
        if (cellEditor != null) {
          cellEditor.stopCellEditing();
        }
        saveCellEqualizer();
        try {
          beansPool.getGsmViewBean().updateCellEqualizerAlgorithm(MainApp.clientUID, server, refresher.getCellEqualizerAlgorithm());
          if (isClear.getAndSet(false)) {
            beansPool.getGsmViewBean().clearGsmView(MainApp.clientUID, server);
          } else {
            beansPool.getGsmViewBean().saveGsmView(MainApp.clientUID, server, Arrays.asList(refresher.getGsmViewData()));
          }
        } catch (NotPermittedException e) {
          return Collections.singletonList(e);
        }
        return Collections.emptyList();
      }
    });
  }

  private void saveCellEqualizer() {
    CellEqualizerAlgorithm cellEqualizerAlgorithm = refresher.getCellEqualizerAlgorithm();
    if (basicAlgorithmRadio.isSelected()) {
      CellEqualizerBasicAlgorithm cellEqualizerBasicAlgorithm = new CellEqualizerBasicAlgorithm();
      if (cellEqualizerAlgorithm != null) {
        cellEqualizerBasicAlgorithm.setServerId(cellEqualizerAlgorithm.getServerId());
        cellEqualizerBasicAlgorithm.setID(cellEqualizerAlgorithm.getID());
      }
      cellEqualizerAlgorithm = cellEqualizerBasicAlgorithm;
    } else if (randomAlgorithmRadio.isSelected()) {
      CellEqualizerRandomAlgorithm cellEqualizerRandomAlgorithmLocal = new CellEqualizerRandomAlgorithm();
      if (cellEqualizerAlgorithm != null) {
        cellEqualizerRandomAlgorithmLocal.setServerId(cellEqualizerAlgorithm.getServerId());
        cellEqualizerRandomAlgorithmLocal.setID(cellEqualizerAlgorithm.getID());
      }
      CellEqualizerRandomAlgorithm.CellEqualizerRandomConfiguration cellEqualizerRandomConfiguration = new CellEqualizerRandomAlgorithm.CellEqualizerRandomConfiguration();
      cellEqualizerRandomConfiguration.setMinRegeneratedTime(minRandomPeriodField.getIntValue());
      cellEqualizerRandomConfiguration.setMaxRegeneratedTime(maxRandomPeriodField.getIntValue());
      cellEqualizerRandomConfiguration.setMinWaitingTime(minWaitingTimeField.getIntValue());
      cellEqualizerRandomConfiguration.setMaxWaitingTime(maxWaitingTimeField.getIntValue());
      Instant instant = Instant.ofEpochMilli(((Date) fromLeadTimeSpinner.getValue()).getTime());
      cellEqualizerRandomConfiguration.setFromLeadTime(LocalDateTime.ofInstant(instant, ZoneId.systemDefault()).toLocalTime());
      instant = Instant.ofEpochMilli(((Date) toLeadTimeSpinner.getValue()).getTime());
      cellEqualizerRandomConfiguration.setToLeadTime(LocalDateTime.ofInstant(instant, ZoneId.systemDefault()).toLocalTime());
      cellEqualizerRandomAlgorithmLocal.setConfiguration(cellEqualizerRandomConfiguration);
      cellEqualizerAlgorithm = cellEqualizerRandomAlgorithmLocal;
    } else if (statisticAlgorithmRadio.isSelected()) {
      CellEqualizerStatisticAlgorithm cellEqualizerStatisticAlgorithmLocal = new CellEqualizerStatisticAlgorithm();
      if (cellEqualizerAlgorithm != null) {
        cellEqualizerStatisticAlgorithmLocal.setServerId(cellEqualizerAlgorithm.getServerId());
        cellEqualizerStatisticAlgorithmLocal.setID(cellEqualizerAlgorithm.getID());
      }
      CellEqualizerStatisticConfiguration cellEqualizerStatisticConfiguration = new CellEqualizerStatisticConfiguration();
      cellEqualizerStatisticConfiguration.setMinSuccessfulCallCount(minStatisticSuccessfulCallCountField.getIntValue());
      cellEqualizerStatisticConfiguration.setMaxSuccessfulCallCount(maxStatisticSuccessfulCallCountField.getIntValue());
      cellEqualizerStatisticConfiguration.setMinUssdCount(minStatisticUssdCountField.getIntValue());
      cellEqualizerStatisticConfiguration.setMaxUssdCount(maxStatisticUssdCountField.getIntValue());
      cellEqualizerStatisticConfiguration.setMinOutgoingSmsCount(minStatisticOutgoingSmsCountField.getIntValue());
      cellEqualizerStatisticConfiguration.setMaxOutgoingSmsCount(maxStatisticOutgoingSmsCountField.getIntValue());

      cellEqualizerStatisticConfiguration.setMinWaitingTime(minWaitingTimeField.getIntValue());
      cellEqualizerStatisticConfiguration.setMaxWaitingTime(maxWaitingTimeField.getIntValue());
      Instant instant = Instant.ofEpochMilli(((Date) fromLeadTimeSpinner.getValue()).getTime());
      cellEqualizerStatisticConfiguration.setFromLeadTime(LocalDateTime.ofInstant(instant, ZoneId.systemDefault()).toLocalTime());
      instant = Instant.ofEpochMilli(((Date) toLeadTimeSpinner.getValue()).getTime());
      cellEqualizerStatisticConfiguration.setToLeadTime(LocalDateTime.ofInstant(instant, ZoneId.systemDefault()).toLocalTime());
      cellEqualizerStatisticAlgorithmLocal.setConfiguration(cellEqualizerStatisticConfiguration);
      cellEqualizerAlgorithm = cellEqualizerStatisticAlgorithmLocal;
    }
    refresher.setCellEqualizerAlgorithm(cellEqualizerAlgorithm);
  }

  public void setActive(final boolean active) {
    if (active) {
      add(scrollPanel, BorderLayout.CENTER);
      tableControlPanel.addToLeft(multiArfcn);
      searchField.applyFilter();
      refresher.setGsmViewData(this);
      refresherThread.addRefresher(refresher);
    } else {
      saveCellEqualizer();
      remove(scrollPanel);
      refresher.clearPrevGsmView();
      tableControlPanel.remove(multiArfcn);
      refresherThread.removeRefresher(refresher);
    }
  }

  public void release() {
    if (!saveProperties.getAndSet(true)) {
      gsmViewTable.getUpdatableTableProperties().saveProperties();
      guiProperties.saveProperties();
    }
    if (refresher != null) {
      refresher.release();
    }
  }

  public GsmViewTable getGsmViewTable() {
    return gsmViewTable;
  }

  public void setEditable(final boolean editable) {
    lastEditable.set(editable);
    boolean localEditable = editable && firstUpdate.get();
    gsmViewTable.setEditable(localEditable);
    checkSelectedServing.setEnabled(localEditable);
    checkSelectedTrusted.setEnabled(localEditable);
    multiArfcn.setEnabled(localEditable);
    clearButton.setEnabled(localEditable);
    basicAlgorithmRadio.setEnabled(localEditable);
    randomAlgorithmRadio.setEnabled(localEditable);
    statisticAlgorithmRadio.setEnabled(localEditable);
    minRandomPeriodField.setEnabled(localEditable);
    maxRandomPeriodField.setEnabled(localEditable);
    minWaitingTimeField.setEnabled(localEditable);
    maxWaitingTimeField.setEnabled(localEditable);
    minStatisticSuccessfulCallCountField.setEnabled(localEditable);
    maxStatisticSuccessfulCallCountField.setEnabled(localEditable);
    minStatisticUssdCountField.setEnabled(localEditable);
    maxStatisticUssdCountField.setEnabled(localEditable);
    minStatisticOutgoingSmsCountField.setEnabled(localEditable);
    maxStatisticOutgoingSmsCountField.setEnabled(localEditable);
    fromLeadTimeSpinner.setEnabled(localEditable);
    toLeadTimeSpinner.setEnabled(localEditable);
  }

}
