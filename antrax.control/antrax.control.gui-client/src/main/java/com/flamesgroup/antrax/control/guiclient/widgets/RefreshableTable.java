/*
 * Copyright (C) 2017 Flames Group SIA
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.flamesgroup.antrax.control.guiclient.widgets;

import com.flamesgroup.antrax.control.swingwidgets.table.JMetalTable;

import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.util.LinkedList;
import java.util.List;

import javax.swing.*;
import javax.swing.event.RowSorterEvent;
import javax.swing.event.RowSorterListener;
import javax.swing.event.TableModelEvent;
import javax.swing.event.TableModelListener;
import javax.swing.table.TableModel;

public abstract class RefreshableTable extends JMetalTable {

  private static final long serialVersionUID = -5703433308932289435L;

  public interface RefreshableTableListener {
    void onDataChanged();

    void onSorted();
  }

  private final List<RefreshableTableListener> refreshListeners = new LinkedList<>();
  private RowSorterListener rowSorterListener;
  private TableModelListener modelListener;

  public RefreshableTable() {
    super();
    register();
  }

  private void register() {
    rowSorterListener = new RowSorterListener() {
      @Override
      public void sorterChanged(final RowSorterEvent e) {
        refreshIndicators();
        fireRowsSorted();
      }
    };

    modelListener = new TableModelListener() {
      @Override
      public void tableChanged(final TableModelEvent e) {
        refreshIndicators();
        fireDataRefreshed();
      }
    };

    RowSorter<?> rowSorter = getRowSorter();
    if (rowSorter != null) {
      rowSorter.addRowSorterListener(rowSorterListener);
    }

    TableModel model = getModel();
    if (model != null) {
      model.addTableModelListener(modelListener);
    }

    addPropertyChangeListener(new PropertyChangeListener() {
      @Override
      public void propertyChange(final PropertyChangeEvent evt) {
        if ("rowSorter".equals(evt.getPropertyName())) {
          RowSorter<?> oldRowSorter = (RowSorter<?>) evt.getOldValue();
          RowSorter<?> rowSorter = (RowSorter<?>) evt.getNewValue();
          if (oldRowSorter != null) {
            oldRowSorter.removeRowSorterListener(rowSorterListener);
          }
          if (rowSorter != null) {
            rowSorter.addRowSorterListener(rowSorterListener);
          }
        } else if ("model".equals(evt.getPropertyName())) {
          TableModel oldModel = (TableModel) evt.getOldValue();
          TableModel model = (TableModel) evt.getNewValue();
          if (oldModel != null) {
            oldModel.removeTableModelListener(modelListener);
          }
          if (model != null) {
            model.addTableModelListener(modelListener);
          }
        }
      }
    });
  }

  public void addRefreshableTableListener(final RefreshableTableListener l) {
    refreshListeners.add(l);
  }

  public void removeRefreshableTableListener(final RefreshableTableListener l) {
    refreshListeners.remove(l);
  }

  protected void fireDataRefreshed() {
    for (RefreshableTableListener l : refreshListeners) {
      l.onDataChanged();
    }
  }

  protected void fireRowsSorted() {
    for (RefreshableTableListener l : refreshListeners) {
      l.onSorted();
    }
  }

  protected abstract void refreshIndicators();

}
