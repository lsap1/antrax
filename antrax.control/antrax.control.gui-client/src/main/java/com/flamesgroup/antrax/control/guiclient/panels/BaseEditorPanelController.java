/*
 * Copyright (C) 2017 Flames Group SIA
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.flamesgroup.antrax.control.guiclient.panels;

import com.flamesgroup.antrax.control.guiclient.utils.ActionCallbackHandler;
import com.flamesgroup.antrax.control.guiclient.utils.ActionRefresher;
import com.flamesgroup.antrax.control.guiclient.utils.BeansPool;
import com.flamesgroup.antrax.control.guiclient.utils.Refresher;
import com.flamesgroup.antrax.control.guiclient.utils.RefresherThread;
import com.flamesgroup.antrax.control.guiclient.utils.WaitingReference;
import com.flamesgroup.antrax.control.swingwidgets.MessageUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.awt.*;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

public class BaseEditorPanelController<T> {

  private final Logger logger = LoggerFactory.getLogger(BaseEditorPanelController.class);

  private final Map<T, T> elementsForUpdate = new HashMap<>();
  private final Set<T> elementsForRemove = new HashSet<>();

  private final BeanCommunication<T> communication;

  public BaseEditorPanelController(final BeanCommunication<T> communication) {
    this.communication = communication;
  }

  public synchronized T createElem(final Component invoker, final RefresherThread refresherThread, final int transactionId) {
    final WaitingReference<T> retval = new WaitingReference<>(null);

    ActionCallbackHandler<T> handler = new ActionCallbackHandler<T>() {
      @Override
      public void onActionRefresherFailure(final Refresher refresher, final List<Throwable> caught) {
        retval.release();
        MessageUtils.showError(invoker, "Failed to create element", caught.get(0).getMessage());
      }

      @Override
      public void onActionRefresherSuccess(final Refresher refresher, final T result) {
        logger.debug("[{}] - item is successfully created. Notify about it", this);
        retval.setAndRelease(result);
      }

      @Override
      public void onRefreshUI(final Refresher refresher) {
      }
    };
    ActionRefresher<T> action = new ActionRefresher<T>("createElem", refresherThread, handler) {

      @Override
      protected T performAction(final BeansPool beansPool) throws Exception {
        logger.debug("[{}] - creating elem", this);
        T elem = communication.createElem(beansPool, transactionId);
        elementsForUpdate.put(elem, elem);
        return elem;
      }
    };
    action.execute();

    logger.debug("[{}] - waiting until elem is created", this);
    retval.waitValue();

    logger.debug("[{}] - created {}", this, retval.get());
    return retval.get();
  }

  public synchronized T copyElem(final T srcElem, final T dstElem, final Component btn, final RefresherThread refresherThread, final int transactionId) {
    final WaitingReference<T> retval = new WaitingReference<>(null);
    ActionCallbackHandler<T> handler = new ActionCallbackHandler<T>() {
      @Override
      public void onActionRefresherFailure(final Refresher refresher, final List<Throwable> caught) {
        retval.release();
        MessageUtils.showError(btn, "Failed to copy element", caught.get(0).getMessage());
      }

      @Override
      public void onActionRefresherSuccess(final Refresher refresher, final T result) {
        logger.debug("[{}] - item is successfully created. Notify about it", this);
        retval.setAndRelease(result);
      }

      @Override
      public void onRefreshUI(final Refresher refresher) {
      }
    };

    ActionRefresher<T> action = new ActionRefresher<T>("copyElem", refresherThread, handler) {

      @Override
      protected T performAction(final BeansPool beansPool) throws Exception {
        logger.debug("[{}] - creating elem copy", this);
        T copyElem = communication.copyElement(srcElem, dstElem);

        logger.debug("[{}] - updating elem copy", this);
        elementsForUpdate.put(copyElem, copyElem);
        return copyElem;
      }
    };
    action.execute();

    logger.debug("[{}] - waiting until elem is copied", this);
    retval.waitValue();

    logger.debug("[{}] - copied {}", this, retval.get());
    return retval.get();
  }

  public synchronized void removeElem(final T elem) {
    if (elem == null) {
      throw new NullPointerException();
    }
    logger.debug("[{}] - Removed {}", this, elem);
    elementsForUpdate.remove(elem);

    elementsForRemove.add(elem);
  }

  public synchronized void updateElem(final T elem) {
    if (elem == null) {
      throw new NullPointerException();
    }
    logger.debug("[{}] - updated {}", this, elem);
    elementsForUpdate.put(elem, elem);
  }

  public synchronized List<Throwable> syncChanges(final BeansPool pool, final int transactionId) {
    logger.debug("[{}] - synchronizing", this);
    List<Throwable> throwables = new ArrayList<>();
    for (T e : elementsForUpdate.keySet()) {
      logger.debug("[{}] - sent {} to update", this, e);
      try {
        communication.updateElem(e, pool, transactionId);
      } catch (Exception ex) {
        throwables.add(ex);
      }
    }
    for (T e : elementsForRemove) {
      logger.debug("[{}] - sent {} to remove", this, e);
      try {
        communication.removeElem(e, pool, transactionId);
      } catch (Exception ex) {
        throwables.add(ex);
      }
    }
    clearChanges();
    return throwables;
  }

  public synchronized int clearChanges() {
    int retval = elementsForUpdate.size() + elementsForRemove.size();
    elementsForUpdate.clear();
    elementsForRemove.clear();
    return retval;
  }

}
