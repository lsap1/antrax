/*
 * Copyright (C) 2017 Flames Group SIA
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.flamesgroup.antrax.control.guiclient.scripts.panels;

import com.flamesgroup.antrax.automation.meta.ScriptConflict;
import com.flamesgroup.antrax.automation.utils.registry.RegistryAccess;
import com.flamesgroup.antrax.plugins.core.AntraxPluginsStore;

import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.*;

public class ScriptConflictsResolverDialog extends JDialog {

  private static final long serialVersionUID = 8116250644275513668L;
  private final ScriptConflictsResolverPanel scriptsPanel = new ScriptConflictsResolverPanel();

  private JButton applyButton;
  private boolean applied;

  public ScriptConflictsResolverDialog(final Window parent) {
    super(parent);
    setModalityType(ModalityType.APPLICATION_MODAL);
    setTitle("Update of scripts in system may corrupt existing Sim Groups. To prevent this you should manually control all values being copied from old scripts to the future one.");
    setLayout(new BorderLayout());

    scriptsPanel.setBorder(BorderFactory.createEmptyBorder(10, 10, 10, 10));
    add(scriptsPanel, BorderLayout.CENTER);
    add(createButtonsPanel(), BorderLayout.SOUTH);
    setPreferredSize(new Dimension(800, 600));
    pack();
  }

  private JPanel createButtonsPanel() {
    JPanel retval = new JPanel();
    retval.add(getApplyButton());
    retval.add(createCancelButton());
    return retval;
  }

  private JButton createCancelButton() {
    JButton retval = new JButton("Cancel");
    retval.addActionListener(new ActionListener() {

      @Override
      public void actionPerformed(final ActionEvent e) {
        ScriptConflictsResolverDialog.this.setVisible(false);
      }
    });
    return retval;
  }

  private JButton getApplyButton() {
    if (applyButton == null) {
      applyButton = new JButton("Apply");
      applyButton.addActionListener(new ActionListener() {

        @Override
        public void actionPerformed(final ActionEvent e) {
          applyConflicts();
        }
      });
    }
    return applyButton;
  }

  private void applyConflicts() {
    if (scriptsPanel.isAllConflictsResolved()) {
      applied = true;
      setVisible(false);
    } else {
      JOptionPane.showMessageDialog(scriptsPanel, scriptsPanel.getUnresolvedConflict().toString(), "You should resolve all conflicts", JOptionPane.ERROR_MESSAGE);
    }
  }

  public void setConflicts(final ScriptConflict... conflicts) {
    scriptsPanel.setConflicts(conflicts);
  }

  public void initialize(final AntraxPluginsStore leftPS, final AntraxPluginsStore rightPS, final RegistryAccess registry) {
    scriptsPanel.initialize(leftPS, rightPS, registry);
  }

  public boolean showDialog() {
    setPreferredSize(getOwner().getSize());
    pack();
    setLocationRelativeTo(getOwner());
    setVisible(true);
    return applied;
  }

  public ScriptConflict[] getResolvedConflicts() {
    return scriptsPanel.getResolvedConflicts();
  }
}
