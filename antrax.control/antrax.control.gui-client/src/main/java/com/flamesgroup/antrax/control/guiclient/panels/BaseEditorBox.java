/*
 * Copyright (C) 2017 Flames Group SIA
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.flamesgroup.antrax.control.guiclient.panels;

import com.flamesgroup.antrax.control.swingwidgets.editor.JEditPanel;

import java.awt.*;
import java.awt.event.ContainerEvent;
import java.awt.event.ContainerListener;
import java.awt.event.FocusEvent;
import java.awt.event.FocusListener;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.swing.*;
import javax.swing.text.JTextComponent;

public abstract class BaseEditorBox<T> extends JEditPanel {

  private static final long serialVersionUID = -6343256220497839686L;

  private boolean editable;

  private final List<ElementModifiedEventHandler<T>> handlers = new ArrayList<>();
  private final FocusListener focusListener = new FocusListener() {

    @Override
    public void focusLost(final FocusEvent e) {
      if (e.isTemporary()) {
        return;
      }

      if (!isContainsChild(e.getOppositeComponent())) {
        fireFocusLost();
      }
    }

    @Override
    public void focusGained(final FocusEvent e) {
    }
  };

  private ContainerListener containerListener;

  private List<T> elems;
  private final Map<Component, Object> values = new HashMap<>();
  private final List<Component> boxComponents = new ArrayList<>();

  /**
   * Writes all elem properties to the boxes editors
   *
   * @param src source of data to write
   */
  protected abstract void writeElemToEditors(T src);

  /**
   * Reads all boxes editors values and write them to elem properties
   *
   * @param dest destination of reading
   */
  protected abstract void readEditorsToElem(T dest);

  /**
   * Creates and adds all GUI elements. This method is called from
   * constructor, so properties of class is not instantiated yet.
   * <p/>
   * Example of bad usage:
   * <p/>
   * <p>
   * <pre>
   * class BadBox extends BaseEditorBox&lt;String&gt; {
   *   private JTextField field = new JTextFeld();
   * &lt;p/&gt;
   *   protected void createGUIElements() {
   *     add(field); // NullPointerException! because field is not instantiated yet
   *  }
   * }
   * </pre>
   * <p/>
   * Example of good usage:
   * <p/>
   * <p>
   * <pre>
   * class BadBox extends BaseEditorBox&lt;String&gt; {
   *   private JTextField field;
   * &lt;p/&gt;
   *   protected void createGUIElements() {
   *     field = new JTextFeld();
   *     add(field); // Ok
   *  }
   * }
   * </pre>
   */
  protected abstract void createGUIElements();

  public boolean supportsMultipleEditing() {
    return false;
  }

  public void setMultipleEditing(final boolean multiple) {
  }

  public void addElementModifiedHandler(final ElementModifiedEventHandler<T> handler) {
    handlers.add(handler);
  }

  public void fireElementModified(final T elem) {
    for (ElementModifiedEventHandler<T> h : handlers) {
      h.handleElementModified(elem);
    }
  }

  private boolean isContainsChild(final Component component) {
    return hasMeAsAParent(component);
  }

  private boolean hasMeAsAParent(Component component) {
    while (component != null) {
      if (component == this) {
        return true;
      }
      component = component.getParent();
    }
    return false;
  }

  private void addFocusListener(Component component) {
    if (component == null) {
      return;
    }

    if (component instanceof JScrollPane) {
      component = ((JScrollPane) component).getViewport().getView();
    }

    boolean contains = false;
    for (FocusListener fl : component.getFocusListeners()) {
      if (fl == focusListener) {
        contains = true;
      }
    }
    if (!contains) {
      component.addFocusListener(focusListener);
    }

    if (component instanceof JComponent) {
      for (Component cmp : ((JComponent) component).getComponents()) {
        addFocusListener(cmp);
      }
    }
  }

  private void removeFocusListener(Component component) {
    if (component == null) {
      return;
    }

    if (component instanceof JScrollPane) {
      component = ((JScrollPane) component).getViewport().getView();
    }

    component.removeFocusListener(focusListener);

    if (component instanceof JComponent) {
      for (Component cmp : ((JComponent) component).getComponents()) {
        removeFocusListener(cmp);
      }
    }
  }

  ContainerListener getContainerListener() {
    if (containerListener == null) {
      containerListener = new ContainerListener() {

        @Override
        public void componentRemoved(final ContainerEvent e) {
          removeFocusListener(e.getChild());
        }

        @Override
        public void componentAdded(final ContainerEvent e) {
          addFocusListener(e.getChild());
          addComponentsContainerListeners(e.getChild());
        }
      };
    }

    return containerListener;
  }

  private void addComponentsContainerListeners(Component component) {
    if (component == null) {
      return;
    }

    if (component instanceof JScrollPane) {
      component = ((JScrollPane) component).getViewport().getView();
    }

    if (component instanceof Container) {
      ((Container) component).addContainerListener(getContainerListener());
    }

    if (component instanceof JComponent) {
      for (Component cmp : ((JComponent) component).getComponents()) {
        addComponentsContainerListeners(cmp);
      }
    }
  }

  public BaseEditorBox() {
    addComponentsContainerListeners(this);
    createGUIElements();
  }

  private void fireFocusLost() {
    if (elems != null && wasChanges()) {
      for (T elem : elems) {
        if (!preventChanges(elem)) {
          readEditorsToElem(elem);
          fireElementModified(elem);
          rememberEditorValues();
        }
      }
    }
  }

  public void forceCheckChanges() {
    fireFocusLost();
  }

  protected boolean preventChanges(final T elem) {
    return false;
  }

  public void setData(final List<T> elems) {
    fireFocusLost();
    if (elems.size() > 1) {
      setMultipleEditing(true);
    } else {
      setMultipleEditing(false);
    }
    this.elems = elems;
    if (elems.size() == 0) {
      clearEditors();
    } else {
      for (T e : elems) {
        writeElemToEditors(e);
      }
      rememberEditorValues();
    }
  }

  protected Object getCustomComponentValue(final Component component) {
    return null;
  }

  protected void clearCustomComponent(final Component component) {
  }

  private List<Component> getBoxComponents() {
    boxComponents.clear();
    readBoxComponents(getComponents(), boxComponents);
    return boxComponents;
  }

  private void readBoxComponents(final Component[] components, final List<Component> boxComponents) {
    for (Component c : components) {
      if (c instanceof JScrollPane) {
        c = ((JScrollPane) c).getViewport().getView();
      }

      if (c instanceof JComponent) {
        readBoxComponents(((JComponent) c).getComponents(), boxComponents);
      }
      boxComponents.add(c);
    }
  }

  private Object getComponentValue(final Component c) {
    if (c instanceof JTextComponent) {
      return ((JTextComponent) c).getText();
    } else if (c instanceof JComboBox) {
      return ((JComboBox) c).getSelectedItem();
    } else if (c instanceof JCheckBox) {
      return ((JCheckBox) c).isSelected();
    } else if (c instanceof JSpinner) {
      return ((JSpinner) c).getValue();
    } else if (c instanceof JRadioButton) {
      return ((JRadioButton) c).isSelected();
    } else {
      return getCustomComponentValue(c);
    }
  }

  public void setEditable(final boolean value) {
    this.editable = value;
    for (Component c : getBoxComponents()) {
      if (c instanceof JTextComponent) {
        ((JTextComponent) c).setEditable(value);
      } else if (c instanceof JComboBox) {
        c.setEnabled(value);
      } else if (c instanceof JCheckBox) {
        c.setEnabled(value);
      } else if (c instanceof JSpinner) {
        c.setEnabled(value);
      } else if (c instanceof JLabel) {
        c.setEnabled(value);
      } else {
        setCustomComponentEditable(c, value);
      }
    }
  }

  protected void setCustomComponentEditable(final Component component, final boolean value) {
  }

  public boolean isEditable() {
    return editable;
  }

  private void clearEditors() {
    for (Component c : getBoxComponents()) {
      if (c instanceof JTextComponent) {
        ((JTextComponent) c).setText("");
      } else if (c instanceof JComboBox) {
        ((JComboBox) c).setSelectedItem(null);
      } else if (c instanceof JCheckBox) {
        ((JCheckBox) c).setSelected(false);
      } else if (c instanceof JSpinner) {
        JSpinner spinner = (JSpinner) c;
        if (spinner.getValue() instanceof Number) {
          spinner.setValue(0);
        }
      } else {
        clearCustomComponent(c);
      }
    }
  }

  private void rememberEditorValues() {
    values.clear();
    rememberRecursiveEditorValues(getComponents());
  }

  private void rememberRecursiveEditorValues(final Component[] components) {
    for (Component c : components) {
      if (c instanceof JScrollPane) {
        c = ((JScrollPane) c).getViewport().getView();
      }

      if (c instanceof JComponent) {
        rememberRecursiveEditorValues(((JComponent) c).getComponents());
      }
      values.put(c, getComponentValue(c));
    }
  }

  private boolean isComponentChanged(final Component component, final Object value) {
    Object componentValue = getComponentValue(component);
    if (value == null) {
      return (componentValue != null);
    }
    return (!value.equals(componentValue));
  }

  private boolean wasChanges() {
    for (Component key : values.keySet()) {
      Object value = values.get(key);
      if (isComponentChanged(key, value)) {
        return true;
      }
    }
    return false;
  }

}
