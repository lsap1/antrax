/*
 * Copyright (C) 2017 Flames Group SIA
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.flamesgroup.antrax.control.guiclient.newapi.panels;

import com.flamesgroup.antrax.control.authorization.NotPermittedException;
import com.flamesgroup.antrax.control.guiclient.MainApp;
import com.flamesgroup.antrax.control.guiclient.newapi.AppPanel;
import com.flamesgroup.antrax.control.guiclient.newapi.AppPanelProvider;
import com.flamesgroup.antrax.control.guiclient.panels.CfgSimGroupPanel;
import com.flamesgroup.antrax.control.guiclient.utils.BeansPool;

public class SimGroupsPanelProvider implements AppPanelProvider {

  @Override
  public boolean checkPermissions(final BeansPool pool) throws NotPermittedException {
    return check(pool, "updateSIMGroup") && check(pool, "deleteSIMGroup") && check(pool, "getSIMGroupsRevision") && check(pool, "listSIMGroups")
        && check(pool, "getPluginsStore") && check(pool, "listScriptDefinitions") && check(pool, "createSIMGroup")
        && check(pool, "getPluginsStoreRevision") && check(pool, "getSIMGroupsRevision");
  }

  private boolean check(final BeansPool pool, final String action) throws NotPermittedException {
    return pool.getConfigBean().checkPermission(MainApp.clientUID, action);
  }

  @Override
  public AppPanel createPanel() {
    return new CfgSimGroupPanel();
  }
}
