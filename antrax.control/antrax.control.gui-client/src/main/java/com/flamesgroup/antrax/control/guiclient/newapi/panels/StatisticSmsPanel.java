/*
 * Copyright (C) 2017 Flames Group SIA
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.flamesgroup.antrax.control.guiclient.newapi.panels;

import com.flamesgroup.antrax.control.communication.SmsStatistic;
import com.flamesgroup.antrax.control.guiclient.newapi.AppPanel;
import com.flamesgroup.antrax.control.guiclient.utils.BeansPool;
import com.flamesgroup.antrax.control.guiclient.utils.RefresherThread;
import com.flamesgroup.antrax.control.guiclient.utils.TransactionManager;
import com.flamesgroup.antrax.control.swingwidgets.contextsearch.JContextSearch;
import com.flamesgroup.antrax.control.swingwidgets.editor.JEditLabel;
import com.flamesgroup.antrax.control.swingwidgets.editor.JReflectiveBar;
import com.flamesgroup.antrax.control.swingwidgets.table.ColumnWriter;
import com.flamesgroup.antrax.control.swingwidgets.table.JExportCsvButton;
import com.flamesgroup.antrax.control.swingwidgets.table.JUpdatableTable;
import com.flamesgroup.antrax.control.swingwidgets.table.TableBuilder;
import com.flamesgroup.antrax.control.swingwidgets.table.UpdateTableColumnModel;

import java.awt.*;
import java.util.List;
import java.util.concurrent.atomic.AtomicReference;

import javax.swing.*;
import javax.swing.border.EmptyBorder;
import javax.swing.table.DefaultTableCellRenderer;

public class StatisticSmsPanel extends JPanel implements AppPanel {

  private static final long serialVersionUID = 3761625756058142742L;

  private final AtomicReference<RefresherThread> refresherThread = new AtomicReference<>();
  private final AtomicReference<ExecutiveRefresher> refresher = new AtomicReference<>();
  private final JUpdatableTable<SmsStatistic, String> tableStatistic = new JUpdatableTable<>(new SmsStatisticTableBuilder());
  private final JEditLabel totalSendSmsADay = new JEditLabel("0");
  private final JEditLabel totalLimitSendSmsADay = new JEditLabel("0");
  private final JEditLabel totalResidueSendSmsADay = new JEditLabel("0");

  public StatisticSmsPanel() {
    setLayout(new BorderLayout());
    tableStatistic.setName(getClass().getSimpleName() + "_SMS_STATISTIC");
    tableStatistic.getUpdatableTableProperties().restoreProperties();
    JContextSearch searchField = new JContextSearch();
    searchField.registerSearchItem(tableStatistic);
    JReflectiveBar searchPanel = new JReflectiveBar();
    searchPanel.addToRight(searchField);

    JExportCsvButton jExportCsvButton = new JExportCsvButton(tableStatistic, "sms_statistic");
    jExportCsvButton.setText("Export");
    searchPanel.addToLeft(jExportCsvButton);

    JPanel headerPanel = new JPanel();
    headerPanel.setLayout(new BoxLayout(headerPanel, BoxLayout.Y_AXIS));
    headerPanel.add(searchPanel);

    add(headerPanel, BorderLayout.NORTH);
    add(new JScrollPane(tableStatistic), BorderLayout.CENTER);
    JReflectiveBar bottomPanel = new JReflectiveBar();
    JEditLabel totalSendSmsADayLabel = new JEditLabel("Total send SMS a day: ");
    Font f = totalSendSmsADayLabel.getFont();
    totalSendSmsADayLabel.setFont(f.deriveFont(f.getStyle() | Font.BOLD));
    bottomPanel.addToRight(totalSendSmsADayLabel);
    bottomPanel.addToRight(totalSendSmsADay);

    JEditLabel totalLimitSendSmsADayLabel = new JEditLabel("Total limit send SMS a day: ");
    totalLimitSendSmsADayLabel.setFont(f.deriveFont(f.getStyle() | Font.BOLD));
    bottomPanel.addToRight(totalLimitSendSmsADayLabel);
    bottomPanel.addToRight(totalLimitSendSmsADay);

    JEditLabel totalResidueSendSmsADayLabel = new JEditLabel("Total residue send SMS a day: ");
    totalResidueSendSmsADayLabel.setFont(f.deriveFont(f.getStyle() | Font.BOLD));
    bottomPanel.addToRight(totalResidueSendSmsADayLabel);
    bottomPanel.addToRight(totalResidueSendSmsADay);

    add(bottomPanel, BorderLayout.SOUTH);

    tableStatistic.getColumnModel().getColumn(3).setCellRenderer(new DefaultTableCellRenderer() {
      private static final long serialVersionUID = -7928046453890721266L;

      @Override
      public Component getTableCellRendererComponent(final JTable table, final Object value, final boolean isSelected, final boolean hasFocus, final int row, final int column) {
        final Component c = super.getTableCellRendererComponent(table, value, isSelected, hasFocus, row, column);
        SmsStatistic elemAt = (SmsStatistic) ((JUpdatableTable) table).getElemAt(row);
        float percent = ((float) (elemAt.getLimitADay() - elemAt.getAmountSendSmsADay()) / elemAt.getLimitADay()) * 100;
        JLabel cmp = new JLabel(value.toString());
        cmp.setOpaque(true);
        cmp.setBorder(new EmptyBorder(0, 5, 0, 0));
        if (elemAt.getAmountSendSmsADay() >= elemAt.getLimitADay()) {
          cmp.setBackground(Color.RED);
          return cmp;
        } else if (percent < 20) {
          cmp.setBackground(Color.YELLOW);
          return cmp;
        }
        return c;
      }
    });
  }

  @Override
  public JComponent getComponent() {
    return this;
  }

  @Override
  public void setEditable(final boolean editable) {

  }

  @Override
  public void setActive(final boolean active) {
    if (active) {
      refresherThread.get().addRefresher(refresher.get());
    } else {
      refresherThread.get().removeRefresher(refresher.get());
    }
  }

  @Override
  public void postInitialize(final RefresherThread refresher, final TransactionManager transactionManager) {
    this.refresherThread.set(refresher);
    this.refresher.set(createRefresher());
  }

  @Override
  public void release() {
    tableStatistic.getUpdatableTableProperties().saveProperties();
  }

  private ExecutiveRefresher createRefresher() {
    ExecutiveRefresher smsStatisticPanelRefresher = new ExecutiveRefresher("SmsStatisticPanelRefresher", refresherThread.get(), null, this);
    smsStatisticPanelRefresher.addPermanentExecution(new RefresherExecution() {

      private List<SmsStatistic> smsStatistics;

      @Override
      public String describeExecution() {
        return "update tableStatistic";
      }

      @Override
      public void invokeExecution(final BeansPool beansPool, final TransactionManager transactionManager) throws Exception {
        smsStatistics = beansPool.getControlBean().listSmsStatistic();
      }

      @Override
      public void updateGUIComponent() {
        tableStatistic.setData(smsStatistics.toArray(new SmsStatistic[0]));
        int totalSendADay = 0;
        int totalLimitADay = 0;
        for (SmsStatistic smsStatistic : smsStatistics) {
          totalSendADay += smsStatistic.getAmountSendSmsADay();
          totalLimitADay += smsStatistic.getLimitADay();
        }
        totalSendSmsADay.setText(String.valueOf(totalSendADay));
        totalLimitSendSmsADay.setText(String.valueOf(totalLimitADay));
        totalResidueSendSmsADay.setText(String.valueOf(totalLimitADay - totalSendADay));
      }
    });
    return smsStatisticPanelRefresher;
  }


  private class SmsStatisticTableBuilder implements TableBuilder<SmsStatistic, String> {


    private SmsStatisticTableBuilder() {
    }

    @Override
    public String getUniqueKey(final SmsStatistic src) {
      return src.getIpAddress();
    }

    @Override
    public void buildRow(final SmsStatistic src, final ColumnWriter<SmsStatistic> dest) {
      dest.writeColumn(src.getIpAddress());
      dest.writeColumn(src.getAmountSendSmsADay());
      dest.writeColumn(src.getLimitADay());
      dest.writeColumn(src.getLimitADay() - src.getAmountSendSmsADay());
    }

    @Override
    public void buildColumns(final UpdateTableColumnModel columns) {
      columns.addColumn("IP address", String.class);
      columns.addColumn("The amount send SMS a day", Integer.class);
      columns.addColumn("Limit send SMS a day", Integer.class);
      columns.addColumn("Residue send SMS a day", Integer.class);
    }

  }

}
