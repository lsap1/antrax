/*
 * Copyright (C) 2017 Flames Group SIA
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package com.flamesgroup.antrax.control.guiclient.panels.audiolist;

import com.flamesgroup.antrax.automation.audiocapture.AudioFile;
import com.flamesgroup.antrax.control.authorization.NotPermittedException;
import com.flamesgroup.antrax.control.guiclient.MainApp;
import com.flamesgroup.antrax.control.guiclient.newapi.panels.ExecutiveRefresher;
import com.flamesgroup.antrax.control.guiclient.newapi.panels.RefresherExecution;
import com.flamesgroup.antrax.control.guiclient.utils.BeansPool;
import com.flamesgroup.antrax.control.guiclient.utils.RefresherThread;
import com.flamesgroup.antrax.control.guiclient.utils.TransactionManager;
import com.flamesgroup.antrax.control.swingwidgets.dialog.WaitingDialog;
import com.flamesgroup.antrax.storage.commons.IServerData;

import java.awt.*;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;
import java.util.zip.ZipEntry;
import java.util.zip.ZipOutputStream;

public class DownloadAudioRefresher extends ExecutiveRefresher {

  private final DownloadAudioServerTable downloadAudioServerTable;
  private final DownloadAudioTable downloadAudioTable;
  private final Set<IServerData> servers = new HashSet<>();
  private List<AudioFile> audioFiles = new ArrayList<>();
  private volatile boolean updating;
  private Component invoker;

  public DownloadAudioRefresher(final DownloadAudioServerTable downloadAudioServerTable, final DownloadAudioTable downloadAudioTable, final RefresherThread refresherThread, final TransactionManager transactionManager,
      final Component invoker) {
    super("DownloadAudioRefresher", refresherThread, transactionManager, invoker);
    this.downloadAudioServerTable = downloadAudioServerTable;
    this.downloadAudioTable = downloadAudioTable;
    this.invoker = invoker;
  }

  public void init() {
    addPermanentExecution(new RefresherExecution() {
      @Override
      public String describeExecution() {
        return "Refresh audio files";
      }

      @Override
      public void invokeExecution(final BeansPool beansPool, final TransactionManager transactionManager) throws Exception {
        servers.addAll(beansPool.getConfigBean().listServers(MainApp.clientUID).stream().filter(IServerData::isVoiceServerEnabled).collect(Collectors.toSet()));
        if (!downloadAudioServerTable.getSelectedElems().isEmpty()) {
          IServerData selectedServer = downloadAudioServerTable.getSelectedElem();
          audioFiles = beansPool.getControlBean().getAudioFiles(selectedServer);
        }
      }

      @Override
      public void updateGUIComponent() {
        updating = true;
        downloadAudioServerTable.setData(servers.toArray(new IServerData[servers.size()]));
        downloadAudioTable.setData(audioFiles.toArray(new AudioFile[audioFiles.size()]));

        downloadAudioTable.setEnabled(true);
        updating = false;
      }
    });
  }

  @Override
  public void handleEnabled(final BeansPool beansPool) {

  }

  @Override
  public void handleDisabled(final BeansPool beansPool) {

  }

  public boolean isUpdating() {
    return updating;
  }

  public void downloadAudioFiles(final File selectedFile, final IServerData selectedServer, final List<AudioFile> selectedAudioFile) {
    addExecution(new RefresherExecution() {
      @Override
      public String describeExecution() {
        return "Download audio files";
      }

      @Override
      public void invokeExecution(final BeansPool beansPool, final TransactionManager transactionManager) throws Exception {
        WaitingDialog wd = new WaitingDialog(null, MainApp.getAppName(), "Downloading audio file from: " + selectedServer.getName());
        wd.show(invoker, () -> {
          try (ZipOutputStream out = new ZipOutputStream(new FileOutputStream(selectedFile))) {
            for (AudioFile audioFile : selectedAudioFile) {
              byte[] data = beansPool.getControlBean().downloadAudioFileFromServer(selectedServer, audioFile.getName());
              ZipEntry e = new ZipEntry(audioFile.getName());
              out.putNextEntry(e);

              out.write(data, 0, data.length);

              out.closeEntry();
            }
          } catch (IOException | NotPermittedException e) {
            e.printStackTrace();
          }
        });
      }

      @Override
      public void updateGUIComponent() {

      }
    });
  }
}
