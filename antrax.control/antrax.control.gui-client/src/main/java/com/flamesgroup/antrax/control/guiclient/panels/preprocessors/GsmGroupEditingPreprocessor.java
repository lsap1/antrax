/*
 * Copyright (C) 2017 Flames Group SIA
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.flamesgroup.antrax.control.guiclient.panels.preprocessors;

import com.flamesgroup.antrax.control.guiclient.commadapter.GsmGroupEditAdapter;
import com.flamesgroup.antrax.control.guiclient.panels.EditingPreprocessor;
import com.flamesgroup.antrax.control.guiclient.panels.OperationCanceledException;
import com.flamesgroup.antrax.control.guiclient.utils.RefresherThread;
import com.flamesgroup.antrax.control.swingwidgets.MessageUtils;
import com.flamesgroup.antrax.control.swingwidgets.table.JUpdatableTable;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.awt.*;
import java.util.List;
import java.util.stream.Collectors;

public class GsmGroupEditingPreprocessor implements EditingPreprocessor<GsmGroupEditAdapter> {

  private static final Logger logger = LoggerFactory.getLogger(GsmGroupEditingPreprocessor.class);

  private final JUpdatableTable<GsmGroupEditAdapter, Long> table;

  public GsmGroupEditingPreprocessor(final JUpdatableTable<GsmGroupEditAdapter, Long> table) {
    this.table = table;
  }

  @Override
  public GsmGroupEditAdapter beforeAdd(final GsmGroupEditAdapter elem, final RefresherThread refresherThread, final int transactionId, final Component invoker) throws OperationCanceledException {
    List<String> groupNames = table.getElems().stream().map(e -> e.getGsmGroup().getName()).collect(Collectors.toList());
    elem.getGsmGroup().setName(checkOnUniqueAndAddStarToEnd(groupNames, "New group"));
    return elem;
  }

  @Override
  public void beforeRemove(final GsmGroupEditAdapter elem, final RefresherThread refresherThread, final int transactionId, final Component invoker) throws OperationCanceledException {
  }

  @Override
  public void beforeUpdate(final GsmGroupEditAdapter elem, final RefresherThread refresherThread, final int transactionId, final Component invoker) throws OperationCanceledException {
    if (table.getElems().stream().map(e -> e.getGsmGroup().getName()).filter(e -> e.equals(elem.getGsmGroup().getName())).count() > 1) {
      logger.info("[{}] - contains duplicate: [{}]", this, elem.getGsmGroup().getName());
      MessageUtils.showWarn(invoker.getParent(), "Warn", "Please use unique name for GSM group");
      throw new OperationCanceledException();
    }
  }

  private String checkOnUniqueAndAddStarToEnd(final List<String> groupNames, final String newGroupName) {
    for (String groupName : groupNames) {
      if (newGroupName.equals(groupName)) {
        return checkOnUniqueAndAddStarToEnd(groupNames, newGroupName + "*");
      }
    }
    return newGroupName;
  }
}
