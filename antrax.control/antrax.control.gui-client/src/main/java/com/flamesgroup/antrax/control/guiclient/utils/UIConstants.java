/*
 * Copyright (C) 2017 Flames Group SIA
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.flamesgroup.antrax.control.guiclient.utils;

import java.awt.*;

public final class UIConstants {

  public static final Color BORDER_COLOR = new Color(0x404040);
  public static final Color COLOR_NO_GROUP = Color.CYAN.darker();
  public static final Color COLOR_PIN_REMOVING = new Color(0x800080);
  public static final Color COLOR_PIN_REQUIRED = new Color(0xb91d09);
  public static final Color WARNING_COLOR = new Color(0xF88017);

}
