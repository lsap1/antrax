/*
 * Copyright (C) 2017 Flames Group SIA
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.flamesgroup.antrax.control.guiclient.widgets;

import com.flamesgroup.antrax.control.swingwidgets.field.AbstractField;
import com.flamesgroup.unit.PhoneNumber;

import javax.swing.*;

public class SimUIDField extends AbstractField {

  private static final long serialVersionUID = -2077811647115713271L;

  public SimUIDField() {
    this(false);
  }

  public SimUIDField(final boolean required) {
    setColumns(12);
    setRequired(required);
    setHorizontalAlignment(SwingConstants.LEADING);
  }

  public void setContent(final PhoneNumber phoneNumber) {
    if (phoneNumber == null) {
      setText("");
    } else {
      setText((phoneNumber.getValue() != null) ? phoneNumber.getValue() : "");
    }
  }

  @Override
  public String getContent() {
    String text = getText();
    return (text == null) ? "" : text.trim();
  }

  /**
   * Checks the validity
   */
  @Override
  protected boolean isValid(final String text) {
    if (text != null && text.matches("[0-9]+")) {
      return (text.length() >= 1);
    }
    return false;
  }

  @Override
  protected String getFormattedText(final String text) {
    return text.trim();
  }

}
