/*
 * Copyright (C) 2017 Flames Group SIA
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.flamesgroup.antrax.control.guiclient.panels.registry;

import com.flamesgroup.antrax.control.swingwidgets.table.ColumnWriter;
import com.flamesgroup.antrax.control.swingwidgets.table.JUpdatableTable;
import com.flamesgroup.antrax.control.swingwidgets.table.TableBuilder;
import com.flamesgroup.antrax.control.swingwidgets.table.UpdateTableColumnModel;

public class RegistryPathTable extends JUpdatableTable<String, String> {
  private static final long serialVersionUID = -8750014571344065247L;

  public RegistryPathTable() {
    super(new TableBuilder<String, String>() {

      @Override
      public void buildColumns(final UpdateTableColumnModel columns) {
        columns.addColumn("path", String.class);
      }

      @Override
      public void buildRow(final String src, final ColumnWriter<String> dest) {
        dest.writeColumn(src);
      }

      @Override
      public String getUniqueKey(final String src) {
        return src;
      }
    });
  }

}
