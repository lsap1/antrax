/*
 * Copyright (C) 2017 Flames Group SIA
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.flamesgroup.antrax.control.guiclient.newapi.panels;

import com.flamesgroup.antrax.control.guiclient.MainApp;
import com.flamesgroup.antrax.control.guiclient.newapi.AppPanel;
import com.flamesgroup.antrax.control.guiclient.utils.BeansPool;
import com.flamesgroup.antrax.control.guiclient.utils.RefresherThread;
import com.flamesgroup.antrax.control.guiclient.utils.TransactionManager;
import com.flamesgroup.antrax.control.guiclient.utils.UserProperty;
import com.flamesgroup.antrax.control.swingwidgets.IconPool;
import com.flamesgroup.antrax.control.swingwidgets.MessageUtils;
import com.flamesgroup.antrax.control.swingwidgets.contextsearch.JContextSearch;
import com.flamesgroup.antrax.control.swingwidgets.dialog.WaitingDialog;
import com.flamesgroup.antrax.control.swingwidgets.table.JUpdatableTable;
import com.flamesgroup.antrax.control.swingwidgets.table.PrefixListTableBuilder;
import com.flamesgroup.antrax.control.utils.VoipAntiSpamHelper;
import com.flamesgroup.antrax.storage.commons.impl.PhoneNumberPrefix;
import com.flamesgroup.antrax.storage.commons.impl.PhoneNumberPrefixConfig;
import com.flamesgroup.commons.Pair;

import java.awt.*;
import java.awt.event.ActionListener;
import java.awt.event.FocusEvent;
import java.awt.event.FocusListener;
import java.io.BufferedWriter;
import java.io.File;
import java.nio.file.Files;
import java.util.HashSet;
import java.util.LinkedHashSet;
import java.util.Scanner;
import java.util.Set;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.concurrent.atomic.AtomicReference;

import javax.swing.*;
import javax.swing.border.EmptyBorder;

public class CfgPrefixListPanel extends JPanel implements AppPanel {

  private static final long serialVersionUID = 2415934085108198147L;

  private final JContextSearch searchField = new JContextSearch();
  private final JButton addButton = new JButton();
  private final JButton editButton = new JButton();
  private final JButton delButton = new JButton();
  private final JButton clearButton = new JButton();
  private final JButton importButton = new JButton();
  private final JButton exportButton = new JButton();
  private final JCheckBox enableCheckBox = new JCheckBox();
  private final JTextField defaultPrefixTextField = new JTextField();
  private final JUpdatableTable<PhoneNumberPrefix, String> phoneNumberPrefixTable = new JUpdatableTable<>(new PrefixListTableBuilder());
  private final JScrollPane phoneNumberPrefixTableScrollPane = new JScrollPane();

  private final AtomicReference<RefresherThread> refresherThread = new AtomicReference<>();
  private final AtomicReference<ExecutiveRefresher> refresher = new AtomicReference<>();
  private final Set<PhoneNumberPrefix> phoneNumberPrefixes = new LinkedHashSet<>();

  private final AtomicReference<UserProperty> sourceDirectory = new AtomicReference<>(new UserProperty(System.getProperty("user.home"), "scripts.source-dir"));
  private final AtomicBoolean isProcessing = new AtomicBoolean(false);

  private PhoneNumberPrefixConfig phoneNumberPrefixConfig = null;

  public CfgPrefixListPanel() {
    initComponents();
    phoneNumberPrefixTable.setName(getClass().getSimpleName());
    phoneNumberPrefixTable.getUpdatableTableProperties().restoreProperties();
  }

  @Override
  public JComponent getComponent() {
    return this;
  }

  @Override
  public void setEditable(final boolean editable) {
  }

  @Override
  public void setActive(final boolean active) {
    if (active) {
      refresherThread.get().addRefresher(refresher.get());
    } else {
      refresherThread.get().removeRefresher(refresher.get());
    }
  }

  @Override
  public void postInitialize(final RefresherThread refresher, final TransactionManager transactionManager) {
    this.refresherThread.set(refresher);
    this.refresher.set(createRefresher());
  }

  @Override
  public void release() {
    phoneNumberPrefixTable.getUpdatableTableProperties().saveProperties();
    if (refresher.get() != null) {
      this.refresher.get().interrupt();
    }
  }

  private ExecutiveRefresher createRefresher() {
    ExecutiveRefresher voipAntiSpamConfigurationPanelRefresher = new ExecutiveRefresher("PrefixListPanelRefresher", refresherThread.get(), null, this);
    voipAntiSpamConfigurationPanelRefresher.addPermanentExecution(new RefresherExecution() {

      @Override
      public String describeExecution() {
        return "update table";
      }

      @Override
      public void invokeExecution(final BeansPool beansPool, final TransactionManager transactionManager) throws Exception {
        if (isProcessing.get()) {
          return;
        }
        phoneNumberPrefixes.clear();
        phoneNumberPrefixes.addAll(beansPool.getPrefixListBean().listPhoneNumberPrefixes(MainApp.clientUID));
        phoneNumberPrefixConfig = beansPool.getPrefixListBean().getPrefixConfig(MainApp.clientUID);
      }

      @Override
      public void updateGUIComponent() {
        if (isProcessing.get()) {
          return;
        }
        phoneNumberPrefixTable.setData(phoneNumberPrefixes.toArray(new PhoneNumberPrefix[phoneNumberPrefixes.size()]));
        if (phoneNumberPrefixConfig != null) {
          enableCheckBox.setSelected(phoneNumberPrefixConfig.isEnable());
          defaultPrefixTextField.setText(phoneNumberPrefixConfig.getDefaultPrefix());
        }
      }

    });
    return voipAntiSpamConfigurationPanelRefresher;
  }

  private void initComponents() {
    JPanel baseControlPanel = new JPanel();
    JPanel controlPanel = new JPanel();
    JPanel mainPanel = new JPanel();
    JPanel checkBoxPanel = new JPanel();
    JPanel searchPanel = new JPanel();

    //======== this ========
    setBorder(new EmptyBorder(10, 10, 0, 10));
    setLayout(new BorderLayout());

    //======== baseControlPanel ========
    {
      baseControlPanel.setLayout(new BorderLayout());

      //======== searchPanel ========
      {
        searchPanel.setBorder(new EmptyBorder(10, 10, 10, 10));
        searchPanel.setLayout(new BorderLayout());

        //---- textField1 ----
        searchField.registerSearchItem(phoneNumberPrefixTable);
        searchPanel.add(searchField, BorderLayout.CENTER);
      }
      baseControlPanel.add(searchPanel, BorderLayout.EAST);

      //======== controlPanel ========
      {
        controlPanel.setLayout(new FlowLayout());

        //---- addButton ----
        addButton.setIcon(IconPool.getShared("/img/buttons/button-add.gif"));
        addButton.setToolTipText("Add");
        addButton.addActionListener(createActionListenerAdd());
        controlPanel.add(addButton);

        //---- delButton ----
        delButton.setIcon(IconPool.getShared("/img/buttons/button-remove.png"));
        delButton.setToolTipText("Del");
        delButton.addActionListener(createActionListenerDelete());
        controlPanel.add(delButton);

        //---- editButton ----
        editButton.setIcon(IconPool.getShared("/img/buttons/button-edit.gif"));
        editButton.setToolTipText("Edit");
        editButton.addActionListener(createActionListenerEdit());
        controlPanel.add(editButton);

        //---- clearButton ----
        clearButton.setIcon(IconPool.getShared("/img/buttons/clear.png"));
        clearButton.setToolTipText("Clear");
        clearButton.addActionListener(createActionListenerClear());
        controlPanel.add(clearButton);

        //---- importButton ----
        importButton.setIcon(IconPool.getShared("/img/buttons/import.png"));
        importButton.setToolTipText("Import");
        importButton.addActionListener(createActionListenerImportFile());
        controlPanel.add(importButton);

        //---- exportButton ----
        exportButton.setIcon(IconPool.getShared("/img/buttons/export.png"));
        exportButton.setToolTipText("Export");
        exportButton.addActionListener(createActionListenerExportFile());
        controlPanel.add(exportButton);
      }
      baseControlPanel.add(controlPanel, BorderLayout.WEST);
    }
    add(baseControlPanel, BorderLayout.NORTH);

    //======== mainPanel ========
    {
      mainPanel.setLayout(new BorderLayout());

      //======== checkBocPanel ========
      {
        checkBoxPanel.setBorder(new EmptyBorder(5, 5, 5, 5));
        checkBoxPanel.setLayout(new GridLayout(2, 1));

        //---- enableCheckBox ----
        enableCheckBox.setText("Enable Prefix mark lists");
        enableCheckBox.addActionListener(createActionListenerCheckBoxSaveConfig());
        checkBoxPanel.add(enableCheckBox);

        JPanel defaultPrefixPanel = new JPanel();
        defaultPrefixPanel.setLayout(new FlowLayout(FlowLayout.LEFT));

        JLabel defaultPrefixLabel = new JLabel("Default prefix:");
        defaultPrefixPanel.add(defaultPrefixLabel);

        defaultPrefixTextField.setColumns(20);
        defaultPrefixTextField.addFocusListener(createActionListenerDefaultPrefixSaveConfig());
        defaultPrefixPanel.add(defaultPrefixTextField);
        checkBoxPanel.add(defaultPrefixPanel);
      }
      mainPanel.add(checkBoxPanel, BorderLayout.NORTH);
      phoneNumberPrefixTableScrollPane.setViewportView(phoneNumberPrefixTable);
      mainPanel.add(phoneNumberPrefixTableScrollPane, BorderLayout.CENTER);
    }
    add(mainPanel, BorderLayout.CENTER);
    setControlsEnable(true);
  }

  private FocusListener createActionListenerDefaultPrefixSaveConfig() {
    return new FocusListener() {
      @Override
      public void focusGained(final FocusEvent e) {
        isProcessing.set(true);
      }

      @Override
      public void focusLost(final FocusEvent e) {
        refresher.get().addExecution(new RefresherExecution() {

          @Override
          public String describeExecution() {
            return "update configuration";
          }

          @Override
          public void invokeExecution(final BeansPool beansPool, final TransactionManager transactionManager) throws Exception {
            if (phoneNumberPrefixConfig == null) {
              phoneNumberPrefixConfig = new PhoneNumberPrefixConfig();
            }
            phoneNumberPrefixConfig.setDefaultPrefix(defaultPrefixTextField.getText());
            beansPool.getPrefixListBean().savePrefixConfig(MainApp.clientUID, phoneNumberPrefixConfig);
          }

          @Override
          public void updateGUIComponent() {
          }
        });
        isProcessing.set(false);
      }
    };
  }

  private ActionListener createActionListenerCheckBoxSaveConfig() {
    return e -> refresher.get().addExecution(new RefresherExecution() {

      @Override
      public String describeExecution() {
        return "update configuration";
      }

      @Override
      public void invokeExecution(final BeansPool beansPool, final TransactionManager transactionManager) throws Exception {
        if (phoneNumberPrefixConfig == null) {
          phoneNumberPrefixConfig = new PhoneNumberPrefixConfig();
        }
        phoneNumberPrefixConfig.setEnable(enableCheckBox.isSelected());
        if (phoneNumberPrefixConfig.getDefaultPrefix() == null) {
          phoneNumberPrefixConfig.setDefaultPrefix("");
        }
        beansPool.getPrefixListBean().savePrefixConfig(MainApp.clientUID, phoneNumberPrefixConfig);
      }

      @Override
      public void updateGUIComponent() {

      }
    });
  }

  private ActionListener createActionListenerAdd() {
    return e -> {
      PhoneNumberPrefix newElem = createShowInputDialogAdd();
      if (newElem == null) {
        return;
      }

      refresher.get().addExecution(new RefresherExecution() {

        @Override
        public void updateGUIComponent() {
        }

        @Override
        public void invokeExecution(final BeansPool beansPool, final TransactionManager transactionManager) throws Exception {
          beansPool.getPrefixListBean().addPhoneNumberPrefix(MainApp.clientUID, newElem);
        }

        @Override
        public String describeExecution() {
          return "add new element";
        }
      });
    };
  }

  private ActionListener createActionListenerEdit() {
    return e -> {
      PhoneNumberPrefix selectedElem = phoneNumberPrefixTable.getSelectedElem();
      if (selectedElem == null) {
        JOptionPane.showMessageDialog(this, "Please, choose element from table.");
        return;
      }
      PhoneNumberPrefix editElem = createShowInputDialogEdit(selectedElem);
      if (editElem == null) {
        return;
      }

      refresher.get().addExecution(new RefresherExecution() {

        @Override
        public void updateGUIComponent() {
        }

        @Override
        public void invokeExecution(final BeansPool beansPool, final TransactionManager transactionManager1) throws Exception {
          beansPool.getPrefixListBean().editPhoneNumberPrefix(MainApp.clientUID, editElem);
        }

        @Override
        public String describeExecution() {
          return "edit element";
        }
      });
    };
  }

  private ActionListener createActionListenerDelete() {
    return e -> {
      PhoneNumberPrefix selectedElem = phoneNumberPrefixTable.getSelectedElem();
      if (selectedElem == null) {
        JOptionPane.showMessageDialog(this, "Please, choose element from table.");
        return;
      }
      refresher.get().addExecution(new RefresherExecution() {

        @Override
        public void updateGUIComponent() {
        }

        @Override
        public void invokeExecution(final BeansPool beansPool, final TransactionManager transactionManager) throws Exception {
          isProcessing.set(true);
          setControlsEnable(false);
          try {
            beansPool.getPrefixListBean().deletePhoneNumberPrefix(MainApp.clientUID, selectedElem);
          } finally {
            isProcessing.set(false);
            setControlsEnable(true);
          }
        }

        @Override
        public String describeExecution() {
          return "delete element";
        }
      });

    };
  }

  private ActionListener createActionListenerClear() {
    return e -> {
      PhoneNumberPrefix selectedElem = phoneNumberPrefixTable.getSelectedElem();
      if (selectedElem == null) {
        JOptionPane.showMessageDialog(this, "Please, choose element from table.");
        return;
      }
      refresher.get().addExecution(new RefresherExecution() {

        @Override
        public void updateGUIComponent() {
        }

        @Override
        public void invokeExecution(final BeansPool beansPool, final TransactionManager transactionManager) throws Exception {
          isProcessing.set(true);
          setControlsEnable(false);
          try {
            int res = beansPool.getPrefixListBean().clearPhoneNumberPrefixes(MainApp.clientUID, selectedElem);
            JOptionPane.showMessageDialog(CfgPrefixListPanel.this, "Clear " + res + " element(s)");
          } finally {
            isProcessing.set(false);
            setControlsEnable(true);
          }
        }

        @Override
        public String describeExecution() {
          return "clear element";
        }
      });
    };
  }


  private ActionListener createActionListenerImportFile() {
    return e -> {
      final int COUNT_OF_PART_FOR_IMPORT = 1_000;
      PhoneNumberPrefix selectedElem = phoneNumberPrefixTable.getSelectedElem();
      if (selectedElem == null) {
        JOptionPane.showMessageDialog(this, "Please, choose element from table.");
        return;
      }
      Boolean ignoreOrUpdateOnDuplicate = createChoseIgnoreDialog();
      if (ignoreOrUpdateOnDuplicate == null) {
        return;
      }

      File file = selectFile("Select file for import");

      if (file == null) {
        return;
      }

      refresher.get().addExecution(new RefresherExecution() {

        @Override
        public void updateGUIComponent() {
        }

        @Override
        public void invokeExecution(final BeansPool beansPool, final TransactionManager transactionManager) throws Exception {
          setControlsEnable(false);
          isProcessing.set(true);
          WaitingDialog wd = new WaitingDialog(null, MainApp.getAppName(), "Import...");
          wd.show(CfgPrefixListPanel.this, () -> {
            try {
              try (Scanner scanner = new Scanner(file)) {
                Set<String> numbers = new HashSet<>();
                while (scanner.hasNext()) {
                  numbers.addAll(VoipAntiSpamHelper.parseNumbersFromString(scanner.nextLine()));
                  if (numbers.size() >= COUNT_OF_PART_FOR_IMPORT) {
                    beansPool.getPrefixListBean().importPhoneNumberPrefix(MainApp.clientUID, numbers, selectedElem.getID(), ignoreOrUpdateOnDuplicate);
                    numbers.clear();
                  }
                }
                if (!numbers.isEmpty()) {
                  beansPool.getPrefixListBean().importPhoneNumberPrefix(MainApp.clientUID, numbers, selectedElem.getID(), ignoreOrUpdateOnDuplicate);
                }
              }

              beansPool.getPrefixListBean().updatePhoneNumberPrefixesCount(MainApp.clientUID, selectedElem, ignoreOrUpdateOnDuplicate);
              while (true) {
                Pair<Boolean, Exception> booleanExceptionPair = beansPool.getPrefixListBean().checkPhoneNumberPrefixesCountExecuting(MainApp.clientUID);
                if (!booleanExceptionPair.first()) {
                  if (booleanExceptionPair.second() != null) {
                    throw booleanExceptionPair.second();
                  }
                  break;
                }
                Thread.sleep(TimeUnit.SECONDS.toMillis(5));
              }
              MessageUtils.showInfo(CfgPrefixListPanel.this, MainApp.getAppName(), "Import completed");

            } catch (Exception e) {
              MessageUtils.showError(CfgPrefixListPanel.this, "Error while, import", e);
            } finally {
              setControlsEnable(true);
              isProcessing.set(false);
            }
          });
        }

        @Override
        public String describeExecution() {
          return "import from file";
        }

      });
    };
  }

  private ActionListener createActionListenerExportFile() {
    return e -> {
      PhoneNumberPrefix selectedElem = phoneNumberPrefixTable.getSelectedElem();
      if (selectedElem == null) {
        JOptionPane.showMessageDialog(this, "Please, choose element from table.");
        return;
      }

      File file = selectFile("Select file for export");

      if (file == null) {
        return;
      }

      refresher.get().addExecution(new RefresherExecution() {

        @Override
        public void updateGUIComponent() {
        }

        @Override
        public void invokeExecution(final BeansPool beansPool, final TransactionManager transactionManager) throws Exception {
          setControlsEnable(false);
          isProcessing.set(true);
          WaitingDialog wd = new WaitingDialog(null, MainApp.getAppName(), "Export...");
          wd.show(CfgPrefixListPanel.this, () -> {
            try {
              String s = beansPool.getPrefixListBean().exportNumbers(MainApp.clientUID, selectedElem);
              if (s == null) {
                MessageUtils.showInfo(CfgPrefixListPanel.this, MainApp.getAppName(), "Can't export, get NULL data");
              } else {
                try (BufferedWriter writer = Files.newBufferedWriter(file.toPath())) {
                  writer.write(s);
                }
                MessageUtils.showInfo(CfgPrefixListPanel.this, MainApp.getAppName(), "Export completed");
              }
            } catch (Exception e) {
              MessageUtils.showError(CfgPrefixListPanel.this, "Error while, export", e);
            } finally {
              setControlsEnable(true);
              isProcessing.set(false);
            }
          });
        }

        @Override
        public String describeExecution() {
          return "export to file";
        }

      });
    };
  }


  private void setControlsEnable(final boolean enable) {
    addButton.setEnabled(enable);
    editButton.setEnabled(enable);
    delButton.setEnabled(enable);
    clearButton.setEnabled(enable);
    importButton.setEnabled(enable);
    exportButton.setEnabled(enable);

    enableCheckBox.setEnabled(enable);
    defaultPrefixTextField.setEnabled(enable);
  }

  private File selectFile(final String title) {
    File current = new File(sourceDirectory.get().getValue());
    JFileChooser chooser = new JFileChooser();
    chooser.setDialogTitle(title);
    chooser.setFileSelectionMode(JFileChooser.FILES_ONLY);
    chooser.setAcceptAllFileFilterUsed(false);
    chooser.setSelectedFile(current);

    if (chooser.showOpenDialog(CfgPrefixListPanel.this) == JFileChooser.APPROVE_OPTION) {
      sourceDirectory.get().setValue(chooser.getCurrentDirectory().getAbsolutePath());
      return chooser.getSelectedFile();
    } else {
      return null;
    }
  }

  private Boolean createChoseIgnoreDialog() {
    JPanel contentPanel = new JPanel(new GridLayout(2, 1));
    JRadioButton ignoreOnDuplicate = new JRadioButton("Ignore on duplicate");
    ignoreOnDuplicate.setSelected(true);
    JRadioButton updateOnDuplicate = new JRadioButton("Update on duplicate");
    ButtonGroup buttonGroup = new ButtonGroup();
    buttonGroup.add(ignoreOnDuplicate);
    buttonGroup.add(updateOnDuplicate);
    contentPanel.add(ignoreOnDuplicate);
    contentPanel.add(updateOnDuplicate);

    Object[] buttons = {"Cancel", "Choose"};
    int res = JOptionPane.showOptionDialog(CfgPrefixListPanel.this,
        contentPanel,
        "Choose ignore or update on duplicate",
        JOptionPane.YES_NO_OPTION,
        JOptionPane.PLAIN_MESSAGE,
        null,
        buttons,
        buttons[0]);
    if (res == 1) {
      return ignoreOnDuplicate.isSelected();
    }
    return null;
  }


  private PhoneNumberPrefix createShowInputDialogAdd() {
    return createShowInputDialog(new Object[] {"Cancel", "Add"}, "Add new prefix", null);
  }

  private PhoneNumberPrefix createShowInputDialogEdit(final PhoneNumberPrefix phoneNumberPrefix) {
    return createShowInputDialog(new Object[] {"Cancel", "Edit"}, "Change selected prefix", phoneNumberPrefix);
  }

  private PhoneNumberPrefix createShowInputDialog(final Object[] buttons, final String title, final PhoneNumberPrefix phoneNumberPrefix) {
    PhoneNumberPrefix phoneNumberPrefixLocal = phoneNumberPrefix;
    JPanel contentPanel = new JPanel(new GridLayout(4, 1));

    JLabel lName = new JLabel();
    lName.setText("Name");
    contentPanel.add(lName);

    JTextField tName = new JTextField();
    contentPanel.add(tName);

    JLabel lPrefix = new JLabel();
    lPrefix.setText("Prefix");
    contentPanel.add(lPrefix);

    JTextField tPrefix = new JTextField();
    contentPanel.add(tPrefix);

    if (phoneNumberPrefixLocal != null) {
      tName.setText(phoneNumberPrefixLocal.getName());
      tPrefix.setText(phoneNumberPrefixLocal.getPrefix());
    }

    int res = JOptionPane.showOptionDialog(CfgPrefixListPanel.this,
        contentPanel,
        title,
        JOptionPane.YES_NO_OPTION,
        JOptionPane.PLAIN_MESSAGE,
        null,
        buttons,
        buttons[0]);
    if (res == 1) {
      if (!tName.getText().isEmpty() && !tPrefix.getText().isEmpty()) {
        if (phoneNumberPrefixLocal == null) {
          phoneNumberPrefixLocal = new PhoneNumberPrefix();
        }
        phoneNumberPrefixLocal.setName(tName.getText());
        phoneNumberPrefixLocal.setPrefix(tPrefix.getText());
        return phoneNumberPrefixLocal;
      }
    }
    return null;
  }

}
