package com.flamesgroup.antrax.control.guiclient.widgets.renderers;

import com.flamesgroup.commons.Pair;

import java.awt.*;
import java.time.Instant;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.time.temporal.ChronoUnit;

import javax.swing.*;
import javax.swing.table.DefaultTableCellRenderer;

public class TariffPlanExpireRenderer extends DefaultTableCellRenderer {

  private static final long serialVersionUID = 8109593013776591478L;

  public TariffPlanExpireRenderer() {
    setHorizontalAlignment(SwingConstants.CENTER);
  }

  @Override
  public Component getTableCellRendererComponent(final JTable table, final Object value, final boolean isSelected, final boolean hasFocus, final int row, final int column) {
    Component component = super.getTableCellRendererComponent(table, value, isSelected, hasFocus, row, column);
    String labelText;
    if (value instanceof Pair<?, ?>) {
      Long expireLong = (Long) ((Pair) value).first();
      if (expireLong > 0) {
        LocalDate currentDate = Instant.ofEpochMilli((Long) ((Pair) value).second()).atZone(ZoneId.systemDefault()).toLocalDate();
        LocalDate expireDate = Instant.ofEpochMilli(expireLong).atZone(ZoneId.systemDefault()).toLocalDate();
        long daysBetween = ChronoUnit.DAYS.between(currentDate, expireDate);
        if (daysBetween > 0) {
          if (daysBetween > 1) {
            labelText = "in " + daysBetween + " days";
          } else {
            labelText = "in " + daysBetween + " day";
          }
        } else if (daysBetween < 0) {
          daysBetween = Math.abs(daysBetween);
          if (daysBetween > 1) {
            labelText = daysBetween + " days ago";
          } else {
            labelText = daysBetween + " day ago";
          }
        } else {
          labelText = "today";
        }
      } else {
        labelText = "";
      }
    } else {
      labelText = "";
    }
    setText(labelText);

    return component;
  }

}
