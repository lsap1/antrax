/*
 * Copyright (C) 2017 Flames Group SIA
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.flamesgroup.antrax.control.guiclient.newapi.panels.voipantispam;

import com.flamesgroup.antrax.control.swingwidgets.editor.JEditIntField;
import com.flamesgroup.commons.voipantispam.BlackListConfig;

import java.awt.*;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;

import javax.swing.*;

public class BlackListDialog extends JDialog {

  private static final long serialVersionUID = 5187763398993178258L;

  private final JEditIntField period;
  private final JEditIntField maxRoutingRequestPerPeriod;

  private final JButton cancelButton;
  private final JButton okButton;

  private int componentsIndex;
  private boolean approved;

  public BlackListDialog(final Window owner, final BlackListConfig blackListConfig) {
    this(owner);

    period.setValue(blackListConfig.getPeriod());
    maxRoutingRequestPerPeriod.setValue(blackListConfig.getMaxRoutingRequestPerPeriod());
  }

  public BlackListDialog(final Window owner) {
    super(owner);

    cancelButton = createCancelBtn();
    okButton = createOkBtn();

    GridBagLayout layout = new GridBagLayout();
    getContentPane().setLayout(layout);
    layout.rowWeights = new double[] {0, 0, 0, 1};

    add(new JLabel("Period: ", JLabel.RIGHT), period = new JEditIntField(0, Integer.MAX_VALUE));
    add(new JLabel("Max routing request per period: ", JLabel.RIGHT), maxRoutingRequestPerPeriod = new JEditIntField(0, Integer.MAX_VALUE));
    add(createButtonsPanel(), new GridBagConstraints(0, componentsIndex, 2, 1, 1, 0, GridBagConstraints.CENTER, GridBagConstraints.BOTH, new Insets(2, 2, 2, 2), 0, 0));

    setTitle("Black list config");
    setMinimumSize(new Dimension(350, 150));
    setResizable(false);
    pack();

    ActionListener cancelListener = e -> cancelButton.doClick();
    ActionListener okListener = e -> okButton.doClick();

    KeyStroke stroke = KeyStroke.getKeyStroke(KeyEvent.VK_ESCAPE, 0);
    getRootPane().registerKeyboardAction(cancelListener, stroke, JComponent.WHEN_IN_FOCUSED_WINDOW);

    period.addActionListener(okListener);
    maxRoutingRequestPerPeriod.addActionListener(okListener);
  }

  private void add(final JLabel lbl, final Component editor) {
    getContentPane().add(lbl, new GridBagConstraints(0, componentsIndex, 1, 1, 0, 0, GridBagConstraints.WEST, GridBagConstraints.HORIZONTAL, new Insets(2, 2, 2, 2), 0, 0));
    getContentPane().add(editor, new GridBagConstraints(1, componentsIndex, 1, 1, 1, 0, GridBagConstraints.WEST, GridBagConstraints.HORIZONTAL, new Insets(2, 2, 2, 2), 0, 0));
    componentsIndex++;
  }

  private JPanel createButtonsPanel() {
    JPanel retval = new JPanel();
    SpringLayout layout = new SpringLayout();
    retval.setLayout(layout);
    retval.add(okButton);
    retval.add(cancelButton);
    layout.putConstraint(SpringLayout.EAST, okButton, 5, SpringLayout.HORIZONTAL_CENTER, retval);
    layout.putConstraint(SpringLayout.WEST, cancelButton, 5, SpringLayout.HORIZONTAL_CENTER, retval);
    layout.putConstraint(SpringLayout.SOUTH, okButton, 0, SpringLayout.SOUTH, retval);
    layout.putConstraint(SpringLayout.SOUTH, cancelButton, 0, SpringLayout.SOUTH, retval);

    retval.setPreferredSize(okButton.getPreferredSize());
    return retval;
  }

  private JButton createCancelBtn() {
    JButton retval = new JButton("Cancel");
    retval.addActionListener(e -> setVisible(false));
    return retval;
  }

  private JButton createOkBtn() {
    final JButton retval = new JButton("OK");
    retval.addActionListener(e -> {
      approved = true;
      setVisible(false);
    });
    return retval;
  }

  private BlackListConfig getBlackListConfig() {
    if (approved) {
      return new BlackListConfig(period.getIntValue(), maxRoutingRequestPerPeriod.getIntValue());
    } else {
      return null;
    }
  }

  private static BlackListConfig getBlackListConfig(final BlackListDialog blackListDialog) {
    blackListDialog.setModalityType(ModalityType.DOCUMENT_MODAL);
    blackListDialog.setDefaultCloseOperation(JDialog.DISPOSE_ON_CLOSE);
    blackListDialog.setLocationRelativeTo(blackListDialog.getOwner());
    blackListDialog.setVisible(true);
    blackListDialog.dispose();

    return blackListDialog.getBlackListConfig();
  }

  public static BlackListConfig createNewBlackListConfig(final Component parent) {
    Window windowAncestor = parent == null ? null : SwingUtilities.getWindowAncestor(parent);
    return getBlackListConfig(new BlackListDialog(windowAncestor));
  }

  public static BlackListConfig createEditBlackListConfig(final Component parent, final BlackListConfig blackListConfig) {
    Window windowAncestor = parent == null ? null : SwingUtilities.getWindowAncestor(parent);
    return getBlackListConfig(new BlackListDialog(windowAncestor, blackListConfig));
  }

}
