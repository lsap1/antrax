/*
 * Copyright (C) 2017 Flames Group SIA
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.flamesgroup.antrax.control.guiclient.panels;

import com.flamesgroup.antrax.automation.meta.ScriptDefinition;
import com.flamesgroup.antrax.control.guiclient.scripts.insteditor.PropertyEditorsSource;
import com.flamesgroup.antrax.plugins.core.AntraxPluginsStore;
import com.flamesgroup.antrax.storage.commons.SIMGroup;

public class SimGroupWrapper {
  private SIMGroup simGroup;
  private PropertyEditorsSource propEditorsSource;
  private AntraxPluginsStore pluginsStore;
  private ScriptDefinition[] definitions;

  private SimGroupWrapper() {

  }

  public SIMGroup getSimGroup() {
    return simGroup;
  }

  public PropertyEditorsSource getPropEditorsSource() {
    return propEditorsSource;
  }

  public AntraxPluginsStore getPluginsStore() {
    return pluginsStore;
  }

  public ScriptDefinition[] getDefinitions() {
    return definitions;
  }

  public SimGroupWrapper wrap(final SIMGroup simGroup) {
    SimGroupWrapper retval = new SimGroupWrapper();
    retval.pluginsStore = pluginsStore;
    retval.propEditorsSource = propEditorsSource;
    retval.definitions = definitions;
    retval.simGroup = simGroup;
    return retval;
  }

  @Override
  public String toString() {
    return getSimGroup().toString();
  }

  public static SimGroupWrapper[] wrap(final AntraxPluginsStore ps, final PropertyEditorsSource propEditors, final ScriptDefinition[] definitions, final SIMGroup[] simGroup) {
    assert ps != null;
    assert propEditors != null;
    assert definitions != null;
    assert simGroup != null;

    SimGroupWrapper[] retval = new SimGroupWrapper[simGroup.length];
    for (int i = 0; i < simGroup.length; i++) {
      retval[i] = wrap(ps, propEditors, definitions, simGroup[i]);
    }
    return retval;
  }

  public static SimGroupWrapper wrap(final AntraxPluginsStore ps,
      final PropertyEditorsSource propEditors, final ScriptDefinition[] definitions,
      final SIMGroup simGroup) {
    assert ps != null;
    assert propEditors != null;
    assert definitions != null;
    assert simGroup != null;
    SimGroupWrapper wrapper = new SimGroupWrapper();
    wrapper.pluginsStore = ps;
    wrapper.propEditorsSource = propEditors;
    wrapper.definitions = definitions;
    wrapper.simGroup = simGroup;
    return wrapper;
  }

}
