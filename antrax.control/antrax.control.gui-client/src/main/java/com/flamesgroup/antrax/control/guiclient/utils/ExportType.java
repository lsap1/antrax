/*
 * Copyright (C) 2017 Flames Group SIA
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.flamesgroup.antrax.control.guiclient.utils;

import com.flamesgroup.antrax.control.swingwidgets.ExtFileFilter;
import com.flamesgroup.antrax.control.swingwidgets.IconPool;

import javax.swing.*;

public enum ExportType {
  CSV(new ExtFileFilter(".csv", "*.csv files"), IconPool.getShared("/img/doc_csv.png")),
  PROPERTY(new ExtFileFilter(".properties", "*.properties Property files"), IconPool.getShared("/img/buttons/export.png"));

  private final ExtFileFilter extension;
  private final Icon icon;

  ExportType(final ExtFileFilter extension, final Icon icon) {
    this.extension = extension;
    this.icon = icon;
  }

  public ExtFileFilter getFileExtension() {
    return extension;
  }

  public Icon getIcon() {
    return icon;
  }
}
