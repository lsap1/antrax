/*
 * Copyright (C) 2017 Flames Group SIA
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.flamesgroup.antrax.control.guiclient.utils;

public class TimeUtils {

  public static final String DEFAULT_DATE_FORMAT = "dd.MM.yyyy HH:mm:ss";

  public static final int SEC_PER_MINUTE = 60;
  public static final int SEC_PER_HOUR = 60 * SEC_PER_MINUTE;
  public static final int SEC_PER_DAY = 24 * SEC_PER_HOUR;
  private static final String TIME_DELIMITER = ":";

  private static final int time_parts[] = {SEC_PER_DAY, SEC_PER_HOUR, SEC_PER_MINUTE,};

  public static String writeTimeFull(final int timeSec) {
    int remainder = timeSec < 0 ? (-1) * timeSec : timeSec;
    int i = 0;
    String strTime = timeSec < 0 ? "-" : "";
    for (int part : time_parts) {
      int tm = remainder / part;

      String dlm = (i == 1 && !strTime.isEmpty()) ? "d " : ":";
      strTime += (strTime.isEmpty() ? "" : dlm) + write(tm, (i == 0));

      remainder -= tm * /* SEC_PER_MINUTE / */part;
      i++;
    }
    // seconds
    strTime += ":" + write(remainder, false);
    return strTime;
  }

  public static String writeTime(final int timeSec) {
    int remainder = timeSec < 0 ? (-1) * timeSec : timeSec;
    String strTime = timeSec < 0 ? "-" : "";
    int tm = remainder / SEC_PER_MINUTE;

    strTime += write(tm, false);

    remainder -= tm * SEC_PER_MINUTE;
    // seconds
    strTime += ":" + write(remainder, false);
    return strTime;
  }

  private static String write(final int time, final boolean isDays) {
    if (isDays) {
      return (time > 0) ? String.valueOf(time) : "";
    } else {
      return (time > 9) ? Integer.toString(time) : "0" + time;
    }
  }

  public static String writeTime(final long millis) {
    int time = (int) (millis / 1000);
    return writeTime(time);
  }

  public static String writeTimeFull(final long millis) {
    int time = (int) (millis / 1000);
    return writeTimeFull(time);
  }

  /**
   * Convert input string from timeStump format: hh*:mm*:ss* to long<br>
   * <b>*</b> - any count of int
   *
   * @param str
   * @return
   */
  public static long strtimeToLong(String str) {
    if (str == null) {
      str = "0";
    }
    String[] parts = str.split(TIME_DELIMITER);

    long factor = 1000;
    long milisec = 0;

    if (parts.length > 3) {
      return 0;
    }

    for (int i = parts.length - 1; i >= 0; i--) {
      if (!CharUtils.isDigit(parts[i])) {
        milisec = 0;
      }
      milisec += factor * Long.parseLong(parts[i]);
      factor *= 60;
    }

    return milisec;
  }

  public static String longToStrTime(final Long timeMillis) {
    try {
      long time = timeMillis / 1000;
      String seconds = Integer.toString((int) (time % 60));
      String minutes = Integer.toString((int) ((time % 3600) / 60));
      String hours = Integer.toString((int) (time / 3600));
      for (int i = 0; i < 2; i++) {
        if (seconds.length() < 2) {
          seconds = "0" + seconds;
        }
        if (minutes.length() < 2) {
          minutes = "0" + minutes;
        }
        if (hours.length() < 2) {
          hours = "0" + hours;
        }
      }
      return hours + ":" + minutes + ":" + seconds;
    } catch (Exception ignored) {
      return "";
    }
  }
}
