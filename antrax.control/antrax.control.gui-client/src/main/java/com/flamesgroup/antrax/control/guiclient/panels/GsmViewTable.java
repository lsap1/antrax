/*
 * Copyright (C) 2017 Flames Group SIA
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.flamesgroup.antrax.control.guiclient.panels;

import com.flamesgroup.antrax.control.guiclient.utils.TimeUtils;
import com.flamesgroup.antrax.control.guiclient.widgets.exporters.DateCellExporter;
import com.flamesgroup.antrax.control.guiclient.widgets.exporters.PercentCellExporter;
import com.flamesgroup.antrax.control.guiclient.widgets.exporters.TimePeriodCellExporter;
import com.flamesgroup.antrax.control.guiclient.widgets.renderers.CheckBoxRenderer;
import com.flamesgroup.antrax.control.guiclient.widgets.renderers.DateTableCellRenderer;
import com.flamesgroup.antrax.control.guiclient.widgets.renderers.PercentTCRenderer;
import com.flamesgroup.antrax.control.guiclient.widgets.renderers.TimeoutTCRenderer;
import com.flamesgroup.antrax.control.guiclient.widgets.tooltip.GsmViewTableTooltips;
import com.flamesgroup.antrax.control.swingwidgets.table.ColumnWriter;
import com.flamesgroup.antrax.control.swingwidgets.table.JUpdatableTable;
import com.flamesgroup.antrax.control.swingwidgets.table.TableBuilder;
import com.flamesgroup.antrax.control.swingwidgets.table.UpdateTableColumnModel;
import com.flamesgroup.antrax.storage.commons.impl.GsmView;
import com.flamesgroup.commons.CellKey;
import com.flamesgroup.utils.OperatorsStorage;

import java.awt.*;
import java.util.Date;
import java.util.concurrent.atomic.AtomicBoolean;

import javax.swing.*;
import javax.swing.table.DefaultTableCellRenderer;

public class GsmViewTable extends JUpdatableTable<GsmView, CellKey> {

  private static final long serialVersionUID = -2282735513505070369L;

  private static final String SERVING_COLUMN = "Serving";
  public static final String TRUSTED_COLUMN = "Trusted";
  public static final String LAC_COLUMN = "LAC";
  public static final String CELL_COLUMN = "CELL";
  public static final String BSIC_COLUMN = "BSIC";
  public static final String ARFCN_COLUMN = "ARFCN";
  public static final String NS_LAST_RX_LEV_COLUMN = "NS Last RxLev";
  public static final String S_N_LAST_RX_LEV_COLUMN = "S/N Last RxLev";
  public static final String OPERATOR_COLUMN = "Operator";
  public static final String NETWORK_SURVEY_NUMBER_OCCURRENCES_COLUMN = "NS Number occurrences";
  public static final String SERVING_NUMBER_OCCURRENCES_COLUMN = "S Number occurrences";
  public static final String NEIGHBOURS_NUMBER_OCCURRENCES_COLUMN = "N Number occurrences";
  public static final String SUCCESSFUL_CALLS_COUNT_COLUMN = "Successful Calls Count";
  public static final String TOTAL_CALLS_COUNT_COLUMN = "Total Calls Count";
  public static final String AVERAGE_PDD_COLUMN = "Average PDD";
  public static final String CALLS_DURATION_COLUMN = "Calls Duration";
  public static final String ASR_COLUMN = "ASR";
  public static final String ACD_COLUMN = "ACD";
  public static final String OUTGOING_SMS_COUNT_COLUMN = "Outgoing Sms Count";
  public static final String INCOMING_SMS_COUNT_COLUMN = "Incoming Sms Count";
  public static final String USSD_COUNT_COLUMN = "Ussd Count";
  public static final String FIRST_APPEARANCE_COLUMN = "First appearance";
  public static final String LAST_APPEARANCE_COLUMN = "Last appearance";
  public static final String NOTES_COLUMN = "Notes";
  public static final String SERVER_COLUMN = "Server";

  private final AtomicBoolean editing = new AtomicBoolean();
  private final AtomicBoolean multiArfcn = new AtomicBoolean();

  private final String[] columnToolTips = {
      GsmViewTableTooltips.getServingTooltip(),
      GsmViewTableTooltips.getTrustedTooltip(),
      GsmViewTableTooltips.getLacTooltip(),
      GsmViewTableTooltips.getCellTooltip(),
      GsmViewTableTooltips.getBsicTooltip(),
      GsmViewTableTooltips.getArfcnTooltip(),
      GsmViewTableTooltips.getNetworkSurveyLastRxLevelTooltip(),
      GsmViewTableTooltips.getCellMonitoringLastRxLevelTooltip(),
      GsmViewTableTooltips.getOperatorTooltip(),
      GsmViewTableTooltips.getNetworkSurveyNumberOccurrencesTooltip(),
      GsmViewTableTooltips.getServingCellMonitoringNumberOccurrencesTooltip(),
      GsmViewTableTooltips.getCellMonitoringNumberOccurrencesTooltip(),
      GsmViewTableTooltips.getSuccessfulCallsTooltip(),
      GsmViewTableTooltips.getTotalCallsTooltip(),
      GsmViewTableTooltips.getAveragePddTooltip(),
      GsmViewTableTooltips.getCallsDurationTooltip(),
      GsmViewTableTooltips.getAsrTooltip(),
      GsmViewTableTooltips.getAcdTooltip(),
      GsmViewTableTooltips.getOutgoingSmsCountTooltip(),
      GsmViewTableTooltips.getIncomingSmsCountTooltip(),
      GsmViewTableTooltips.getUssdCountTooltip(),
      GsmViewTableTooltips.getFirstAppearanceTooltip(),
      GsmViewTableTooltips.getLastAppearanceTooltip(),
      GsmViewTableTooltips.getServerTooltip(),
      GsmViewTableTooltips.getNotesTooltip()
  };

  public GsmViewTable() {
    super(new GsmViewTableBuilder());
    setName("GsmView");

    setColumnToolTips(columnToolTips);
  }

  @Override
  public boolean isCellEditable(final int row, final int column) {
    return editing.get() && (column == getColumnModel().getColumnIndex(SERVING_COLUMN) ||
        column == getColumnModel().getColumnIndex(TRUSTED_COLUMN) ||
        column == getColumnModel().getColumnIndex(NOTES_COLUMN));
  }

  public void setEditable(final boolean editable) {
    this.editing.set(editable);
  }

  public boolean isEditable() {
    return editing.get();
  }

  public void setMultiArfcn(final boolean multiArfcn) {
    this.multiArfcn.set(multiArfcn);
  }

  @Override
  public void setValueAt(final Object aValue, final int row, final int column) {
    setValueAt0(aValue, row, column); // set value to selected row
    if (multiArfcn.get()) {
      GsmView selectedGsmView = getElemAt(row);
      for (GsmView gsmView : getElems()) {
        if (gsmView.getCellKey().getArfcn() == selectedGsmView.getCellKey().getArfcn()) {
          int viewRowOf = getViewRowOf(gsmView);
          if (viewRowOf != -1) {
            setValueAt0(aValue, viewRowOf, column); // set value to next row (select multi arfcn)
          }
        }
      }
    }
  }

  public void setValueAt0(final Object aValue, final int row, final int column) {
    super.setValueAt(aValue, row, column);
    if (isCellEditable(row, column)) {
      if (column == getColumnModel().getColumnIndex(SERVING_COLUMN)) {
        getElemAt(row).setServingCell((Boolean) aValue);
      } else if (column == getColumnModel().getColumnIndex(TRUSTED_COLUMN)) {
        getElemAt(row).setTrusted((Boolean) aValue);
      } else if (column == getColumnModel().getColumnIndex(NOTES_COLUMN)) {
        getElemAt(row).setNotes(String.valueOf(aValue));
      }
    }
  }

  public void setServingValueAt(final boolean selected, final int selectedRow) {
    setValueAt0(selected, selectedRow, getColumnModel().getColumnIndex(SERVING_COLUMN));
  }

  public void setTrustedValueAt(final boolean selected, final int selectedRow) {
    setValueAt0(selected, selectedRow, getColumnModel().getColumnIndex(TRUSTED_COLUMN));
  }

  private static class GsmViewTableBuilder implements TableBuilder<GsmView, CellKey> {

    private final DateTableCellRenderer dateRenderer = new DateTableCellRenderer(TimeUtils.DEFAULT_DATE_FORMAT);

    @Override
    public CellKey getUniqueKey(final GsmView src) {
      return src.getCellKey();
    }

    @Override
    public void buildRow(final GsmView src, final ColumnWriter<GsmView> dest) {
      dest.writeColumn(src.isServingCell());
      dest.writeColumn(src.isTrusted());
      dest.writeColumn(src.getCellKey().getLac());
      dest.writeColumn(src.getCellKey().getCellId());
      dest.writeColumn(src.getCellKey().getBsic());
      dest.writeColumn(src.getCellKey().getArfcn());
      dest.writeColumn(src.getNetworkSurveyLastRxLev());
      dest.writeColumn(src.getCellInfoLastRxLev());
      dest.writeColumn(getOperator(src));
      dest.writeColumn(src.getNetworkSurveyNumberOccurrences());
      dest.writeColumn(src.getServingNumberOccurrences());
      dest.writeColumn(src.getNeighborsNumberOccurrences());
      dest.writeColumn(src.getSuccessfulCallsCount());
      dest.writeColumn(src.getTotalCallsCount());
      dest.writeColumn(calculatePdd(src.getAmountPdd(), src.getTotalCallsCount()));
      dest.writeColumn(src.getCallsDuration());
      dest.writeColumn(calculateASR(src.getSuccessfulCallsCount(), src.getTotalCallsCount()));
      dest.writeColumn(calculateACD(src.getCallsDuration(), src.getSuccessfulCallsCount()));
      dest.writeColumn(src.getOutgoingSmsCount());
      dest.writeColumn(src.getIncomingSmsCount());
      dest.writeColumn(src.getUssdCount());
      dest.writeColumn(src.getFirstAppearance());
      dest.writeColumn(src.getLastAppearance());
      dest.writeColumn(src.getServerName());
      dest.writeColumn(src.getNotes());
    }

    @Override
    public void buildColumns(final UpdateTableColumnModel builder) {
      builder.addColumn(SERVING_COLUMN, Boolean.class).setRenderer(new CheckBoxRenderer());
      builder.addColumn(TRUSTED_COLUMN, Boolean.class).setRenderer(new CheckBoxRenderer());
      builder.addColumn(LAC_COLUMN, Integer.class);
      builder.addColumn(CELL_COLUMN, Integer.class);
      builder.addColumn(BSIC_COLUMN, Integer.class);
      builder.addColumn(ARFCN_COLUMN, Integer.class);
      builder.addColumn(NS_LAST_RX_LEV_COLUMN, Integer.class);
      builder.addColumn(S_N_LAST_RX_LEV_COLUMN, Integer.class);
      builder.addColumn(OPERATOR_COLUMN, String.class).setRenderer(new MncRenderer());
      builder.addColumn(NETWORK_SURVEY_NUMBER_OCCURRENCES_COLUMN, Integer.class);
      builder.addColumn(SERVING_NUMBER_OCCURRENCES_COLUMN, Integer.class);
      builder.addColumn(NEIGHBOURS_NUMBER_OCCURRENCES_COLUMN, Integer.class);
      builder.addColumn(SUCCESSFUL_CALLS_COUNT_COLUMN, Integer.class);
      builder.addColumn(TOTAL_CALLS_COUNT_COLUMN, Integer.class);
      builder.addColumn(AVERAGE_PDD_COLUMN, Long.class).setRenderer(new TimeoutTCRenderer()).setCellExporter(new TimePeriodCellExporter());
      builder.addColumn(CALLS_DURATION_COLUMN, Long.class).setRenderer(new TimeoutTCRenderer()).setCellExporter(new TimePeriodCellExporter());
      builder.addColumn(ASR_COLUMN, Integer.class).setRenderer(new PercentTCRenderer()).setPreferredWidth(50).setCellExporter(new PercentCellExporter());
      builder.addColumn(ACD_COLUMN, Long.class).setRenderer(new TimeoutTCRenderer()).setCellExporter(new TimePeriodCellExporter());
      builder.addColumn(OUTGOING_SMS_COUNT_COLUMN, Integer.class);
      builder.addColumn(INCOMING_SMS_COUNT_COLUMN, Integer.class);
      builder.addColumn(USSD_COUNT_COLUMN, Integer.class);
      builder.addColumn(FIRST_APPEARANCE_COLUMN, Date.class).setRenderer(dateRenderer).setCellExporter(new DateCellExporter());
      builder.addColumn(LAST_APPEARANCE_COLUMN, Date.class).setRenderer(dateRenderer).setCellExporter(new DateCellExporter());
      builder.addColumn(SERVER_COLUMN, String.class);
      builder.addColumn(NOTES_COLUMN, String.class);
    }

    private int calculateASR(final int successfulCallsCount, final int totalCallsCount) {
      return (totalCallsCount == 0) ? 0 : 100 * successfulCallsCount / totalCallsCount;
    }

    private long calculateACD(final long callsDuration, final int successfulCallsCount) {
      return (successfulCallsCount == 0) ? 0 : (callsDuration / successfulCallsCount);
    }

    private long calculatePdd(final long amountPdd, final int totalCallsCount) {
      return totalCallsCount == 0 ? 0 : amountPdd / totalCallsCount;
    }

    private String getOperator(final GsmView gsmView) {
      OperatorsStorage.Operator operator = OperatorsStorage.getInstance().findOperatorByCode(String.format("%03x%02x", gsmView.getLastMcc(), gsmView.getLastMnc()));
      if (operator == null) {
        operator = OperatorsStorage.getInstance().findOperatorByCode(String.format("%03x%03x", gsmView.getLastMcc(), gsmView.getLastMnc()));
      }
      if (operator == null) {
        return "UNDEFINED";
      }
      return operator.getName();
    }

  }

  private static class MncRenderer extends DefaultTableCellRenderer {

    private static final long serialVersionUID = -2251944398331684575L;

    @Override
    public Component getTableCellRendererComponent(final JTable table, final Object value, final boolean isSelected, final boolean hasFocus, final int row, final int column) {
      Component tableCellRendererComponent = super.getTableCellRendererComponent(table, value, isSelected, hasFocus, row, column);
      Object elemAt = ((JUpdatableTable) table).getElemAt(row);
      GsmView gsmView = (GsmView) elemAt;
      setToolTipText("MNC: " + String.valueOf(gsmView.getLastMnc()));
      return tableCellRendererComponent;
    }

  }

}
