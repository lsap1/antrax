/*
 * Copyright (C) 2017 Flames Group SIA
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.flamesgroup.antrax.control.guiclient.widgets;

import com.flamesgroup.antrax.control.guiclient.utils.RefresherThread;

import java.awt.*;
import java.lang.reflect.InvocationTargetException;

import javax.swing.*;

public class StatusBar extends JPanel {
  private static final long serialVersionUID = -4948803562071230360L;

  private final JProgressBar refreshProgressBar = new JProgressBar();
  private final JProgressBar timeProgressBar = new JProgressBar();

  private Thread showProgressThread = null;

  public StatusBar() {
    timeProgressBar.setPreferredSize(new Dimension(100, 12));
    refreshProgressBar.setPreferredSize(new Dimension(50, 12));
    setLayout(new BorderLayout());
    JPanel progressPane = new JPanel();
    add(new JPanel(), BorderLayout.LINE_START);
    add(progressPane, BorderLayout.LINE_END);

    progressPane.add(timeProgressBar, BorderLayout.LINE_START);
    progressPane.add(refreshProgressBar, BorderLayout.LINE_END);
  }

  public synchronized void stopRefresh() {
    if (showProgressThread != null) {
      showProgressThread.interrupt();
      showProgressThread = null;
    }
  }

  public synchronized void setRefresherThread(final RefresherThread refresherThread) {
    if (showProgressThread != null) {
      showProgressThread.interrupt();
    }

    showProgressThread = new Thread("StatusBarThread") {
      @Override
      public void run() {
        while (!interrupted()) {
          try {
            timeProgressBar.setValue(refresherThread.getTimeProgress());

            final int refreshProgress = refresherThread.getRefreshProgress();
            try {
              SwingUtilities.invokeAndWait(new Runnable() {
                @Override
                public void run() {
                  synchronized (refreshProgressBar.getTreeLock()) {
                    if (refreshProgress == 100) {
                      if (refreshProgressBar.isIndeterminate())
                        refreshProgressBar.setIndeterminate(false);
                    } else {
                      if (!refreshProgressBar.isIndeterminate())
                        refreshProgressBar.setIndeterminate(true);
                    }
                  }
                }
              });
            } catch (InvocationTargetException ignored) {
            }

            synchronized (refresherThread) {
              refresherThread.wait(50);
            }
          } catch (InterruptedException ignored) {
            break;
          }
        }
      }
    };
    showProgressThread.start();
  }
}
