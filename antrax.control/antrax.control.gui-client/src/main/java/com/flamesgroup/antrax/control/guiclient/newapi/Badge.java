/*
 * Copyright (C) 2017 Flames Group SIA
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.flamesgroup.antrax.control.guiclient.newapi;

import java.util.LinkedList;
import java.util.List;

public class Badge {
  private String error;
  private String notice;
  private String info;
  private String warning;
  private String infoHelp;
  private String noticeHelp;
  private String errorHelp;
  private String warningHelp;
  private final List<BadgeValueChangeListener> listeners = new LinkedList<>();

  public Badge() {
  }

  public Badge(final String info, final String notice, final String error) {
    this.error = error;
    this.info = info;
    this.notice = notice;
  }

  public String getError() {
    return error;
  }

  public String getNotice() {
    return notice;
  }

  public String getInfo() {
    return info;
  }

  public String getWarning() {
    return warning;
  }

  public void setError(final String error) {
    this.error = error;
    fireBadgeChanged();
  }

  public void setInfo(final String info) {
    this.info = info;
    fireBadgeChanged();
  }

  public void setNotice(final String notice) {
    this.notice = notice;
    fireBadgeChanged();
  }

  public void setWarning(final String warning) {
    this.warning = warning;
    fireBadgeChanged();
  }

  public void setInfoHelp(final String infoHelp) {
    this.infoHelp = infoHelp;
    fireBadgeChanged();
  }

  public void setNoticeHelp(final String noticeHelp) {
    this.noticeHelp = noticeHelp;
    fireBadgeChanged();
  }

  public void setErrorHelp(final String errorHelp) {
    this.errorHelp = errorHelp;
    fireBadgeChanged();
  }

  public void setWarningHelp(final String warningHelp) {
    this.warningHelp = warningHelp;
    fireBadgeChanged();
  }

  public String getErrorHelp() {
    return errorHelp;
  }

  public String getInfoHelp() {
    return infoHelp;
  }

  public String getNoticeHelp() {
    return noticeHelp;
  }

  public String getWarningHelp() {
    return warningHelp;
  }

  private void fireBadgeChanged() {
    for (BadgeValueChangeListener l : listeners) {
      l.handleBadgeValueChanged(this);
    }
  }

  public void addBadgeValueChangeListener(final BadgeValueChangeListener listener) {
    this.listeners.add(listener);
  }
}
