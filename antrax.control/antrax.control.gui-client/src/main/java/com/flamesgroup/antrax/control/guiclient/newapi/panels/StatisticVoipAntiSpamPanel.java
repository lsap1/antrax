/*
 * Copyright (C) 2017 Flames Group SIA
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.flamesgroup.antrax.control.guiclient.newapi.panels;

import com.flamesgroup.antrax.commons.VoipAntiSpamListNumbersStatus;
import com.flamesgroup.antrax.control.GuiProperties;
import com.flamesgroup.antrax.control.authorization.NotPermittedException;
import com.flamesgroup.antrax.control.communication.StorageException;
import com.flamesgroup.antrax.control.communication.TransactionException;
import com.flamesgroup.antrax.control.guiclient.MainApp;
import com.flamesgroup.antrax.control.guiclient.newapi.AppPanel;
import com.flamesgroup.antrax.control.guiclient.utils.BeansPool;
import com.flamesgroup.antrax.control.guiclient.utils.RefresherThread;
import com.flamesgroup.antrax.control.guiclient.utils.TransactionManager;
import com.flamesgroup.antrax.control.guiclient.utils.UserProperty;
import com.flamesgroup.antrax.control.guiclient.widgets.tooltip.BlackListStatisticTableTooltips;
import com.flamesgroup.antrax.control.guiclient.widgets.tooltip.GrayListStatisticTableTooltips;
import com.flamesgroup.antrax.control.guiclient.widgets.tooltip.WhiteListStatisticTableTooltips;
import com.flamesgroup.antrax.control.swingwidgets.MessageUtils;
import com.flamesgroup.antrax.control.swingwidgets.contextsearch.JSearchField;
import com.flamesgroup.antrax.control.swingwidgets.dialog.WaitingDialog;
import com.flamesgroup.antrax.control.swingwidgets.pagging.JPaginationPanel;
import com.flamesgroup.antrax.control.swingwidgets.table.BlackListNumberTableBuilder;
import com.flamesgroup.antrax.control.swingwidgets.table.GrayListNumberTableBuilder;
import com.flamesgroup.antrax.control.swingwidgets.table.JUpdatableTable;
import com.flamesgroup.antrax.control.swingwidgets.table.WhiteListNumberTableBuilder;
import com.flamesgroup.antrax.control.utils.VoipAntiSpamHelper;
import com.flamesgroup.antrax.storage.commons.impl.BlackListNumber;
import com.flamesgroup.antrax.storage.commons.impl.GrayListNumber;
import com.flamesgroup.antrax.storage.commons.impl.VoipAntiSpamPlotData;
import com.flamesgroup.antrax.storage.commons.impl.WhiteListNumber;
import com.flamesgroup.commons.Pair;
import javafx.application.Platform;
import javafx.collections.ObservableList;
import javafx.embed.swing.JFXPanel;
import javafx.scene.Scene;
import javafx.scene.chart.LineChart;
import javafx.scene.chart.NumberAxis;
import javafx.scene.chart.XYChart;

import java.awt.*;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.io.BufferedWriter;
import java.io.File;
import java.nio.file.Files;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashSet;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Scanner;
import java.util.Set;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.concurrent.atomic.AtomicReference;
import java.util.stream.Collectors;

import javax.swing.*;
import javax.swing.border.EmptyBorder;
import javax.swing.border.EtchedBorder;
import javax.swing.table.DefaultTableCellRenderer;
import javax.swing.table.JTableHeader;

public class StatisticVoipAntiSpamPanel extends JPanel implements AppPanel {

  private static final long serialVersionUID = 3703523190391825459L;

  private enum TableListNumbersType {
    WHITE, GRAY, BLACK
  }

  private final JUpdatableTable<WhiteListNumber, Date> whiteListTable = new JUpdatableTable<>(new WhiteListNumberTableBuilder());
  private final JUpdatableTable<GrayListNumber, Date> grayListTable = new JUpdatableTable<>(new GrayListNumberTableBuilder());
  private final JUpdatableTable<BlackListNumber, Date> blackListTable = new JUpdatableTable<>(new BlackListNumberTableBuilder());
  private final GuiProperties guiProperties;

  private final JScrollPane mainScrollPane = new JScrollPane();
  private final JButton wlClearButton = new JButton();
  private final JButton wlAddButton = new JButton();
  private final JButton wlBulkDelButton = new JButton();
  private final JButton wlSelectedDelButton = new JButton();
  private final JButton wlImportFromFileButton = new JButton();
  private final JButton wlExportToFileButton = new JButton();
  private final JComboBox<String> wlShowComboBox = new JComboBox<>();
  private final JPaginationPanel wlJPaginationPanel = new JPaginationPanel();
  private final JScrollPane wlTableScrollPane = new JScrollPane();
  private final JButton glClearButton = new JButton();
  private final JButton glBulkDelButton = new JButton();
  private final JButton glSelectedDelButton = new JButton();
  private final JButton glSelectedToBlackListButton = new JButton();
  private final JComboBox<String> glShowComboBox = new JComboBox<>();
  private final JScrollPane glTableScrollPane = new JScrollPane();
  private final JPaginationPanel glJPaginationPanel = new JPaginationPanel();
  private final JButton blClearButton = new JButton();
  private final JButton blAddButton = new JButton();
  private final JButton blBulkDelButton = new JButton();
  private final JButton blSelectedDelButton = new JButton();
  private final JButton blImportFromFileButton = new JButton();
  private final JButton blExportToFileButton = new JButton();
  private final JComboBox<String> blShowComboBox = new JComboBox<>();
  private final JScrollPane blTableScrollPane = new JScrollPane();
  private final JPaginationPanel blJPaginationPanel = new JPaginationPanel();
  private final AtomicReference<UserProperty> sourceDirectory = new AtomicReference<>(new UserProperty(System.getProperty("user.home"), "scripts.source-dir"));

  private final AtomicReference<RefresherThread> refresherThread = new AtomicReference<>();
  private final AtomicReference<ExecutiveRefresher> refresher = new AtomicReference<>();
  private final Set<WhiteListNumber> whiteListNumbers = new LinkedHashSet<>();
  private final Set<GrayListNumber> grayListNumbers = new LinkedHashSet<>();
  private final Set<BlackListNumber> blackListNumbers = new LinkedHashSet<>();
  private final AtomicBoolean isProcessing = new AtomicBoolean(false);
  private final AtomicInteger wlCurrentPage = new AtomicInteger(1);
  private final AtomicInteger glCurrentPage = new AtomicInteger(1);
  private final AtomicInteger blCurrentPage = new AtomicInteger(1);
  private final AtomicInteger wlCountEntriesToShow = new AtomicInteger(10);
  private final AtomicInteger glCountEntriesToShow = new AtomicInteger(10);
  private final AtomicInteger blCountEntriesToShow = new AtomicInteger(10);
  private final StringBuffer wlSearchText = new StringBuffer();
  private final StringBuffer glSearchText = new StringBuffer();
  private final StringBuffer blSearchText = new StringBuffer();
  private final AtomicReference<LineChart<Number, Number>> routingRequestChart = new AtomicReference<>();
  private final AtomicReference<LineChart<Number, Number>> blockNumberChart = new AtomicReference<>();
  private final List<VoipAntiSpamPlotData> routingRequestData = new ArrayList<>();
  private final List<VoipAntiSpamPlotData> blockNumberData = new ArrayList<>();
  private Pair<String, SortOrder> wlSort = null;
  private Pair<String, SortOrder> glSort = null;
  private Pair<String, SortOrder> blSort = null;

  private final String[] whiteListColumnToolTips = {
      WhiteListStatisticTableTooltips.getNumberTooltip(),
      WhiteListStatisticTableTooltips.getStatusTooltip(),
      WhiteListStatisticTableTooltips.getAddTimeTooltip(),
      WhiteListStatisticTableTooltips.getRoutingRequestCountTooltip()
  };

  private final String[] grayListColumnToolTips = {
      GrayListStatisticTableTooltips.getNumberTooltip(),
      GrayListStatisticTableTooltips.getStatusTooltip(),
      GrayListStatisticTableTooltips.getBlockTimeTooltip(),
      GrayListStatisticTableTooltips.getBlockCountTooltip(),
      GrayListStatisticTableTooltips.getBlockTimeLeftTooltip(),
      GrayListStatisticTableTooltips.getRoutingRequestCountTooltip()
  };

  private final String[] blackListColumnToolTips = {
      BlackListStatisticTableTooltips.getNumberTooltip(),
      BlackListStatisticTableTooltips.getStatusTooltip(),
      BlackListStatisticTableTooltips.getAddTimeTooltip(),
      BlackListStatisticTableTooltips.getRoutingRequestCountTooltip()
  };

  public StatisticVoipAntiSpamPanel() {
    initComponents();

    whiteListTable.setColumnToolTips(whiteListColumnToolTips);
    grayListTable.setColumnToolTips(grayListColumnToolTips);
    blackListTable.setColumnToolTips(blackListColumnToolTips);

    whiteListTable.setName(getClass().getSimpleName() + "_WhiteList");
    grayListTable.setName(getClass().getSimpleName() + "_GrayList");
    blackListTable.setName(getClass().getSimpleName() + "_BlackList");
    whiteListTable.getTableHeader().addMouseListener(createMouseAdapter(TableListNumbersType.WHITE));
    grayListTable.getTableHeader().addMouseListener(createMouseAdapter(TableListNumbersType.GRAY));
    grayListTable.getColumnModel().getColumn(1).setCellRenderer(new CellRenderer(TableListNumbersType.GRAY));
    grayListTable.getColumnModel().getColumn(4).setCellRenderer(new CellRenderer(TableListNumbersType.GRAY));
    blackListTable.getTableHeader().addMouseListener(createMouseAdapter(TableListNumbersType.BLACK));
    blackListTable.getColumnModel().getColumn(1).setCellRenderer(new CellRenderer(TableListNumbersType.BLACK));
    blackListTable.getUpdatableTableProperties().restoreProperties();
    grayListTable.getUpdatableTableProperties().restoreProperties();
    whiteListTable.getUpdatableTableProperties().restoreProperties();
    guiProperties = new GuiProperties(getClass().getSimpleName()) {

      private static final String WHITE_LIST_SHOW_ENTRIES = "whiteListShowEntries";
      private static final String GRAY_LIST_SHOW_ENTRIES = "grayListShowEntries";
      private static final String BLACK_LIST_SHOW_ENTRIES = "blackListShowEntries";

      @Override
      protected void restoreOutProperties() {
        String whiteListShowEntries = properties.getProperty(WHITE_LIST_SHOW_ENTRIES, "10");
        wlShowComboBox.getModel().setSelectedItem(whiteListShowEntries);
        String grayListShowEntries = properties.getProperty(GRAY_LIST_SHOW_ENTRIES, "10");
        glShowComboBox.getModel().setSelectedItem(grayListShowEntries);
        String blackListShowEntries = properties.getProperty(BLACK_LIST_SHOW_ENTRIES, "10");
        blShowComboBox.getModel().setSelectedItem(blackListShowEntries);
      }

      @Override
      protected void saveOutProperties() {
        properties.setProperty(WHITE_LIST_SHOW_ENTRIES, wlShowComboBox.getModel().getSelectedItem().toString());
        properties.setProperty(GRAY_LIST_SHOW_ENTRIES, glShowComboBox.getModel().getSelectedItem().toString());
        properties.setProperty(BLACK_LIST_SHOW_ENTRIES, blShowComboBox.getModel().getSelectedItem().toString());
      }
    };
    guiProperties.restoreProperties();
  }

  @Override
  public JComponent getComponent() {
    return this;
  }

  @Override
  public void setEditable(final boolean editable) {
  }

  @Override
  public void setActive(final boolean active) {
    if (active) {
      refresherThread.get().addRefresher(refresher.get());
    } else {
      refresherThread.get().removeRefresher(refresher.get());
    }
  }

  @Override
  public void postInitialize(final RefresherThread refresher, final TransactionManager transactionManager) {
    this.refresherThread.set(refresher);
    this.refresher.set(createRefresher());
  }

  @Override
  public void release() {
    if (refresher.get() != null) {
      this.refresher.get().interrupt();
    }
    blackListTable.getUpdatableTableProperties().saveProperties();
    grayListTable.getUpdatableTableProperties().saveProperties();
    whiteListTable.getUpdatableTableProperties().saveProperties();
    guiProperties.saveProperties();
  }

  private ExecutiveRefresher createRefresher() {
    ExecutiveRefresher voipAntiSpamConfigurationPanelRefresher = new ExecutiveRefresher("VoipAntiSpamConfigurationPanelRefresher", refresherThread.get(), null, this);
    voipAntiSpamConfigurationPanelRefresher.addPermanentExecution(new RefresherExecution() {

      @Override
      public String describeExecution() {
        return "update table";
      }

      @Override
      public void invokeExecution(final BeansPool beansPool, final TransactionManager transactionManager) throws Exception {
        if (isProcessing.get()) {
          return;
        }
        wlJPaginationPanel
            .setPages(getPages((float) beansPool.getVoipAntiSpamStatisticBean().getCountWhiteListNumbers(MainApp.clientUID, wlSearchText.toString()) / wlCountEntriesToShow.get()));
        glJPaginationPanel
            .setPages(getPages((float) beansPool.getVoipAntiSpamStatisticBean().getCountGrayListNumbers(MainApp.clientUID, glSearchText.toString()) / glCountEntriesToShow.get()));
        blJPaginationPanel
            .setPages(getPages((float) beansPool.getVoipAntiSpamStatisticBean().getCountBlackListNumbers(MainApp.clientUID, blSearchText.toString()) / blCountEntriesToShow.get()));

        whiteListNumbers.clear();
        whiteListNumbers.addAll(beansPool.getVoipAntiSpamStatisticBean().listWhiteListNumbers(MainApp.clientUID,
            wlSearchText.toString(),
            wlSort,
            (wlCurrentPage.get() - 1) * wlCountEntriesToShow.get(),
            wlCountEntriesToShow.get()));

        grayListNumbers.clear();
        grayListNumbers.addAll(beansPool.getVoipAntiSpamStatisticBean().listGrayListNumbers(MainApp.clientUID,
            glSearchText.toString(),
            glSort,
            (glCurrentPage.get() - 1) * glCountEntriesToShow.get(),
            glCountEntriesToShow.get()));

        blackListNumbers.clear();
        blackListNumbers.addAll(beansPool.getVoipAntiSpamStatisticBean().listBlackListNumbers(MainApp.clientUID,
            blSearchText.toString(),
            blSort,
            (blCurrentPage.get() - 1) * blCountEntriesToShow.get(),
            blCountEntriesToShow.get()));

        routingRequestData.clear();
        routingRequestData.addAll(beansPool.getVoipAntiSpamStatisticBean().getPlotDataForRoutingRequest(MainApp.clientUID));

        blockNumberData.clear();
        blockNumberData.addAll(beansPool.getVoipAntiSpamStatisticBean().getPlotDataForBlockNumber(MainApp.clientUID));
      }

      @Override
      public void updateGUIComponent() {
        setControlsEnable(!isProcessing.get());
        whiteListTable.setData(whiteListNumbers.toArray(new WhiteListNumber[whiteListNumbers.size()]));
        wlTableScrollPane.setPreferredSize(new Dimension(0, whiteListTable.getRowHeight() * (whiteListTable.getRowCount() + 2)));

        grayListTable.setData(grayListNumbers.toArray(new GrayListNumber[grayListNumbers.size()]));
        glTableScrollPane.setPreferredSize(new Dimension(0, grayListTable.getRowHeight() * (grayListTable.getRowCount() + 2)));


        blackListTable.setData(blackListNumbers.toArray(new BlackListNumber[blackListNumbers.size()]));
        blTableScrollPane.setPreferredSize(new Dimension(0, blackListTable.getRowHeight() * (blackListTable.getRowCount() + 2)));

        mainScrollPane.revalidate();

        Platform.runLater(() -> {
          routingRequestChart.get().getData().clear();
          if (!routingRequestData.isEmpty()) {
            XYChart.Series<Number, Number> series = new XYChart.Series<>();
            ObservableList<XYChart.Data<Number, Number>> data = series.getData();
            for (VoipAntiSpamPlotData voipAntiSpamPlotData : routingRequestData) {
              data.add(new XYChart.Data<>(voipAntiSpamPlotData.getTime(), voipAntiSpamPlotData.getValue()));
            }
            routingRequestChart.get().getData().add(series);
          }

          blockNumberChart.get().getData().clear();
          if (!blockNumberData.isEmpty()) {
            XYChart.Series<Number, Number> series = new XYChart.Series<>();
            ObservableList<XYChart.Data<Number, Number>> data = series.getData();
            for (VoipAntiSpamPlotData voipAntiSpamPlotData : blockNumberData) {
              data.add(new XYChart.Data<>(voipAntiSpamPlotData.getTime(), voipAntiSpamPlotData.getValue()));
            }
            blockNumberChart.get().getData().add(series);
          }
        });
      }
    });
    return voipAntiSpamConfigurationPanelRefresher;
  }

  private int getPages(final float f) {
    if ((f - Math.floor(f)) > 0) {
      return (int) (f + 1);
    } else {
      return (int) f;
    }
  }

  private JFXPanel createRoutingRequestChart() {
    final JFXPanel fxPanel = new JFXPanel();
    Platform.runLater(() -> {
      final NumberAxis xAxis = new NumberAxis(0, 23, 1);
      final NumberAxis yAxis = new NumberAxis();

      yAxis.setLabel("count");
      xAxis.setLabel("time/hour");
      yAxis.setAutoRanging(true);
      yAxis.setForceZeroInRange(false);
      //creating the chart
      routingRequestChart.set(new LineChart<>(xAxis, yAxis));
      routingRequestChart.get().setTitle("Routing request");
      routingRequestChart.get().setAnimated(false);
      routingRequestChart.get().setLegendVisible(false);
      Scene scene = new Scene(routingRequestChart.get(), 400, 300);
      scene.getStylesheets().add("css/voipantispamcharts.css");
      fxPanel.setScene(scene);
    });
    return fxPanel;
  }

  private JFXPanel createBlockNumberChart() {
    final JFXPanel fxPanel = new JFXPanel();
    Platform.runLater(() -> {
      final NumberAxis xAxis = new NumberAxis(0, 23, 1);
      final NumberAxis yAxis = new NumberAxis();
      yAxis.setLabel("count");
      xAxis.setLabel("time/hour");
      yAxis.setAutoRanging(true);
      yAxis.setForceZeroInRange(false);
      //creating the chart
      blockNumberChart.set(new LineChart<>(xAxis, yAxis));
      blockNumberChart.get().setTitle("Block number");
      blockNumberChart.get().setAnimated(false);
      blockNumberChart.get().setLegendVisible(false);
      Scene scene = new Scene(blockNumberChart.get(), 400, 300);
      scene.getStylesheets().add("css/voipantispamcharts.css");
      fxPanel.setScene(scene);
    });
    return fxPanel;
  }

  private void initComponents() {
    JPanel mainPanel = new JPanel();
    JPanel chartsPanel = new JPanel();
    JPanel wlPanel = new JPanel();
    JLabel wlLabel = new JLabel();
    JPanel wlControlsPanel = new JPanel();
    JPanel wlTablePanel = new JPanel();
    JPanel wlTableTopPanel = new JPanel();
    JPanel wlPaginationPanel = new JPanel();
    JLabel wlShowLabel = new JLabel();
    JLabel wlEntriesLabel = new JLabel();
    JSearchField wlSearchField = new JSearchField();
    JPanel glPanel = new JPanel();
    JLabel glLabel = new JLabel();
    JPanel glControlsPanel = new JPanel();
    JPanel glTablePanel = new JPanel();
    JPanel glTableTopPanel = new JPanel();
    JPanel glPaginationPanel = new JPanel();
    JLabel glShowLabel = new JLabel();
    JSearchField glSearchField = new JSearchField();
    JLabel glEntriesLabel = new JLabel();
    JPanel blPanel = new JPanel();
    JLabel blLabel = new JLabel();
    JPanel blControlsPanel = new JPanel();
    JPanel blTablePanel = new JPanel();
    JPanel blTableTopPanel = new JPanel();
    JPanel blPaginationPanel = new JPanel();
    JLabel blShowLabel = new JLabel();
    JSearchField blSearchField = new JSearchField();
    JLabel blEntriesLabel = new JLabel();

    String[] showEntries = {"10", "25", "50", "100"};

    //======== this ========
    setLayout(new BorderLayout());

    //======== mainScrollPane ========
    {

      //======== mainPanel ========
      {
        mainPanel.setBorder(new EmptyBorder(10, 10, 10, 10));
        mainPanel.setLayout(new GridBagLayout());
        ((GridBagLayout) mainPanel.getLayout()).columnWidths = new int[] {0, 0};
        ((GridBagLayout) mainPanel.getLayout()).rowHeights = new int[] {0, 0, 0, 0, 0};
        ((GridBagLayout) mainPanel.getLayout()).columnWeights = new double[] {1.0, 1.0E-4};
        ((GridBagLayout) mainPanel.getLayout()).rowWeights = new double[] {1.0, 1.0, 1.0, 1.0, 1.0E-4};

        //======== chartsPanel ========
        {
          chartsPanel.setLayout(new GridLayout(1, 2));
          chartsPanel.add(createRoutingRequestChart());
          chartsPanel.add(createBlockNumberChart());
        }
        mainPanel.add(chartsPanel, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0,
            GridBagConstraints.CENTER, GridBagConstraints.BOTH,
            new Insets(0, 0, 10, 0), 0, 0));

        //======== wlPanel ========
        {
          wlPanel.setLayout(new GridBagLayout());
          ((GridBagLayout) wlPanel.getLayout()).columnWidths = new int[] {0, 0};
          ((GridBagLayout) wlPanel.getLayout()).rowHeights = new int[] {0, 32, 0, 0};
          ((GridBagLayout) wlPanel.getLayout()).columnWeights = new double[] {1.0, 1.0E-4};
          ((GridBagLayout) wlPanel.getLayout()).rowWeights = new double[] {0.0, 0.0, 0.0, 1.0E-4};

          //---- wlLabel ----
          wlLabel.setText("White list");
          wlLabel.setHorizontalAlignment(SwingConstants.LEFT);
          wlLabel.setFont(wlLabel.getFont().deriveFont(wlLabel.getFont().getStyle() | Font.BOLD));
          wlPanel.add(wlLabel, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0,
              GridBagConstraints.CENTER, GridBagConstraints.BOTH,
              new Insets(0, 0, 0, 0), 0, 0));

          //======== wlControlsPanel ========
          {
            wlControlsPanel.setLayout(new FlowLayout(FlowLayout.LEFT));

            //---- wlClearButton ----
            wlClearButton.setText("Clear");
            wlClearButton.addActionListener(createActionListenerClear(TableListNumbersType.WHITE));
            wlControlsPanel.add(wlClearButton);

            //---- wlAddButton ----
            wlAddButton.setText("Add");
            wlAddButton.addActionListener(createActionListenerAdd(TableListNumbersType.WHITE));
            wlControlsPanel.add(wlAddButton);

            //---- wlBulkDelButton ----
            wlBulkDelButton.setText("Bulk del");
            wlBulkDelButton.addActionListener(createActionListenerBulkDelete(TableListNumbersType.WHITE));
            wlControlsPanel.add(wlBulkDelButton);

            //---- wlSelectedDelButton ----
            wlSelectedDelButton.setText("Selected Del");
            wlSelectedDelButton.addActionListener(createActionListenerDeleteSelected(TableListNumbersType.WHITE));
            wlControlsPanel.add(wlSelectedDelButton);

            //---- wlImportFromFileButton ----
            wlImportFromFileButton.setText("Import from file");
            wlImportFromFileButton.addActionListener(createActionListenerImportFile(TableListNumbersType.WHITE));
            wlImportFromFileButton.setToolTipText("Hint: As a delimiter at file, use any non-numeric character except '#'");
            wlControlsPanel.add(wlImportFromFileButton);

            //---- wlExportToFileButton ----
            wlExportToFileButton.setText("Export to file");
            wlExportToFileButton.addActionListener(createActionListenerExportFile(TableListNumbersType.WHITE));
            wlControlsPanel.add(wlExportToFileButton);
          }
          wlPanel.add(wlControlsPanel, new GridBagConstraints(0, 1, 1, 1, 0.0, 0.0,
              GridBagConstraints.NORTH, GridBagConstraints.HORIZONTAL,
              new Insets(0, 0, 0, 0), 0, 0));

          //======== wlTablePanel ========
          {
            wlTablePanel.setBorder(new EtchedBorder(EtchedBorder.RAISED));
            wlTablePanel.setLayout(new BorderLayout());

            //======== wlTableTopPanel ========
            {
              wlTableTopPanel.setBorder(new EtchedBorder());
              wlTableTopPanel.setBackground(new Color(214, 217, 223));
              wlTableTopPanel.setLayout(new BorderLayout());

              //======== wlPaginationPanel ========
              {
                wlPaginationPanel.setLayout(new FlowLayout(FlowLayout.LEFT));

                //---- wlShowLabel ----
                wlShowLabel.setText("Show");
                wlPaginationPanel.add(wlShowLabel);

                //---- wlShowComboBox ----
                wlShowComboBox.setModel(new DefaultComboBoxModel<>(showEntries));
                wlShowComboBox.addActionListener(createActionListenerShow(TableListNumbersType.WHITE));
                wlPaginationPanel.add(wlShowComboBox);

                //---- wlEntriesLabel ----
                wlEntriesLabel.setText("entries");
                wlPaginationPanel.add(wlEntriesLabel);
              }
              wlTableTopPanel.add(wlPaginationPanel, BorderLayout.WEST);

              //---- wlSearchField ----
              JPanel wlSearchPanel = new JPanel();
              wlSearchField.setColumns(15);
              wlSearchField.setToolTipText("Searching at all columns");
              wlSearchField.addCancelListener(createActionListenerSearch(TableListNumbersType.WHITE, wlSearchField));
              wlSearchPanel.add(wlSearchField);
              wlTableTopPanel.add(wlSearchPanel, BorderLayout.EAST);
            }
            wlTablePanel.add(wlTableTopPanel, BorderLayout.NORTH);

            //======== wlTableBottomPanel(pagination) ========
            wlJPaginationPanel.setLayout(new FlowLayout(FlowLayout.RIGHT));
            wlJPaginationPanel.addListener(createListenerChangePagination(TableListNumbersType.WHITE));
            wlTablePanel.add(wlJPaginationPanel, BorderLayout.SOUTH);

            //======== wlTableScrollPane ========
            {
              wlTableScrollPane.setPreferredSize(new Dimension(0, whiteListTable.getRowHeight() * (whiteListTable.getRowCount() + 2)));
              wlTableScrollPane.setViewportView(whiteListTable);
            }
            wlTablePanel.add(wlTableScrollPane, BorderLayout.CENTER);
          }
          wlPanel.add(wlTablePanel, new GridBagConstraints(0, 2, 1, 1, 0.0, 0.0,
              GridBagConstraints.CENTER, GridBagConstraints.BOTH,
              new Insets(0, 0, 0, 0), 0, 0));
        }
        mainPanel.add(wlPanel, new GridBagConstraints(0, 1, 1, 1, 0.0, 0.0,
            GridBagConstraints.CENTER, GridBagConstraints.BOTH,
            new Insets(0, 0, 10, 0), 0, 0));

        //======== glPanel ========
        {
          glPanel.setLayout(new GridBagLayout());
          ((GridBagLayout) glPanel.getLayout()).columnWidths = new int[] {0, 0};
          ((GridBagLayout) glPanel.getLayout()).rowHeights = new int[] {0, 32, 0, 0};
          ((GridBagLayout) glPanel.getLayout()).columnWeights = new double[] {1.0, 1.0E-4};
          ((GridBagLayout) glPanel.getLayout()).rowWeights = new double[] {0.0, 0.0, 0.0, 1.0E-4};

          //---- glLabel ----
          glLabel.setText("Gray list");
          glLabel.setHorizontalAlignment(SwingConstants.LEFT);
          glLabel.setFont(glLabel.getFont().deriveFont(glLabel.getFont().getStyle() | Font.BOLD));
          glPanel.add(glLabel, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0,
              GridBagConstraints.CENTER, GridBagConstraints.BOTH,
              new Insets(0, 0, 0, 0), 0, 0));

          //======== glControlsPanel ========
          {
            glControlsPanel.setLayout(new FlowLayout(FlowLayout.LEFT));

            //---- glClearButton ----
            glClearButton.setText("Clear");
            glClearButton.addActionListener(createActionListenerClear(TableListNumbersType.GRAY));
            glControlsPanel.add(glClearButton);

            //---- glBulkDelButton ----
            glBulkDelButton.setText("Bulk del");
            glBulkDelButton.addActionListener(createActionListenerBulkDelete(TableListNumbersType.GRAY));
            glControlsPanel.add(glBulkDelButton);

            //---- glSelectedDelButton ----
            glSelectedDelButton.setText("Selected Del");
            glSelectedDelButton.addActionListener(createActionListenerDeleteSelected(TableListNumbersType.GRAY));
            glControlsPanel.add(glSelectedDelButton);

            //---- glSelectedToBlackListButton ----
            glSelectedToBlackListButton.setText("Selected to black list");
            glSelectedToBlackListButton.addActionListener(createActionListenerMoveToBlackList());
            glControlsPanel.add(glSelectedToBlackListButton);
          }
          glPanel.add(glControlsPanel, new GridBagConstraints(0, 1, 1, 1, 0.0, 0.0,
              GridBagConstraints.NORTH, GridBagConstraints.HORIZONTAL,
              new Insets(0, 0, 0, 0), 0, 0));

          //======== glTablePanel ========
          {
            glTablePanel.setBorder(new EtchedBorder(EtchedBorder.RAISED));
            glTablePanel.setLayout(new BorderLayout());

            //======== glTableTopPanel ========
            {
              glTableTopPanel.setBorder(new EtchedBorder());
              glTableTopPanel.setBackground(new Color(214, 217, 223));
              glTableTopPanel.setLayout(new BorderLayout());

              //======== glPaginationPanel ========
              {
                glPaginationPanel.setLayout(new FlowLayout(FlowLayout.LEFT));

                //---- glShowLabel ----
                glShowLabel.setText("Show");
                glPaginationPanel.add(glShowLabel);

                //---- glShowComboBox ----
                glShowComboBox.setModel(new DefaultComboBoxModel<>(showEntries));
                glShowComboBox.addActionListener(createActionListenerShow(TableListNumbersType.GRAY));
                glPaginationPanel.add(glShowComboBox);

                //---- glEntriesLabel ----
                glEntriesLabel.setText("entries");
                glPaginationPanel.add(glEntriesLabel);
              }
              glTableTopPanel.add(glPaginationPanel, BorderLayout.WEST);

              //---- glSearchField ----
              JPanel glSearchPanel = new JPanel();
              glSearchField.setColumns(15);
              glSearchField.setToolTipText("Searching at all columns");
              glSearchField.addCancelListener(createActionListenerSearch(TableListNumbersType.GRAY, glSearchField));
              glSearchPanel.add(glSearchField);
              glTableTopPanel.add(glSearchPanel, BorderLayout.EAST);
            }
            glTablePanel.add(glTableTopPanel, BorderLayout.NORTH);

            //======== glTableBottomPanel(Pagination) ========
            glJPaginationPanel.setLayout(new FlowLayout(FlowLayout.RIGHT));
            glJPaginationPanel.addListener(createListenerChangePagination(TableListNumbersType.GRAY));
            glTablePanel.add(glJPaginationPanel, BorderLayout.SOUTH);

            //======== glTableScrollPane ========
            {
              glTableScrollPane.setPreferredSize(new Dimension(0, grayListTable.getRowHeight() * (grayListTable.getRowCount() + 2)));
              glTableScrollPane.setViewportView(grayListTable);
            }
            glTablePanel.add(glTableScrollPane, BorderLayout.CENTER);
          }
          glPanel.add(glTablePanel, new GridBagConstraints(0, 2, 1, 1, 0.0, 0.0,
              GridBagConstraints.CENTER, GridBagConstraints.BOTH,
              new Insets(0, 0, 0, 0), 0, 0));
        }
        mainPanel.add(glPanel, new GridBagConstraints(0, 2, 1, 1, 0.0, 0.0,
            GridBagConstraints.CENTER, GridBagConstraints.BOTH,
            new Insets(0, 0, 10, 0), 0, 0));

        //======== blPanel ========
        {
          blPanel.setLayout(new GridBagLayout());
          ((GridBagLayout) blPanel.getLayout()).columnWidths = new int[] {0, 0};
          ((GridBagLayout) blPanel.getLayout()).rowHeights = new int[] {0, 32, 0, 0, 0};
          ((GridBagLayout) blPanel.getLayout()).columnWeights = new double[] {1.0, 1.0E-4};
          ((GridBagLayout) blPanel.getLayout()).rowWeights = new double[] {0.0, 0.0, 0.0, 0.0, 1.0E-4};

          //---- blLabel ----
          blLabel.setText("Black list");
          blLabel.setHorizontalAlignment(SwingConstants.LEFT);
          blLabel.setFont(blLabel.getFont().deriveFont(blLabel.getFont().getStyle() | Font.BOLD));
          blPanel.add(blLabel, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0,
              GridBagConstraints.CENTER, GridBagConstraints.BOTH,
              new Insets(0, 0, 0, 0), 0, 0));

          //======== blControlsPanel ========
          {
            blControlsPanel.setLayout(new FlowLayout(FlowLayout.LEFT));

            //---- blClearButton ----
            blClearButton.setText("Clear");
            blClearButton.addActionListener(createActionListenerClear(TableListNumbersType.BLACK));
            blControlsPanel.add(blClearButton);

            //---- blAddButton ----
            blAddButton.setText("Add");
            blAddButton.addActionListener(createActionListenerAdd(TableListNumbersType.BLACK));
            blControlsPanel.add(blAddButton);

            //---- blBulkDelButton ----
            blBulkDelButton.setText("Bulk del");
            blBulkDelButton.addActionListener(createActionListenerBulkDelete(TableListNumbersType.BLACK));
            blControlsPanel.add(blBulkDelButton);

            //---- blSelectedDelButton ----
            blSelectedDelButton.setText("Selected Del");
            blSelectedDelButton.addActionListener(createActionListenerDeleteSelected(TableListNumbersType.BLACK));
            blControlsPanel.add(blSelectedDelButton);

            //---- blImportFromFileButton ----
            blImportFromFileButton.setText("Import from file");
            blImportFromFileButton.addActionListener(createActionListenerImportFile(TableListNumbersType.BLACK));
            blImportFromFileButton.setToolTipText("Hint: As a delimiter at file, use any non-numeric character except '#'");
            blControlsPanel.add(blImportFromFileButton);

            //---- blExportToFileButton ----
            blExportToFileButton.setText("Export to file");
            blExportToFileButton.addActionListener(createActionListenerExportFile(TableListNumbersType.BLACK));
            blControlsPanel.add(blExportToFileButton);
          }
          blPanel.add(blControlsPanel, new GridBagConstraints(0, 1, 1, 1, 0.0, 0.0,
              GridBagConstraints.NORTH, GridBagConstraints.HORIZONTAL,
              new Insets(0, 0, 0, 0), 0, 0));

          //======== blTablePanel ========
          {
            blTablePanel.setBorder(new EtchedBorder(EtchedBorder.RAISED));
            blTablePanel.setLayout(new BorderLayout());

            //======== blTableTopPanel ========
            {
              blTableTopPanel.setBorder(new EtchedBorder());
              blTableTopPanel.setBackground(new Color(214, 217, 223));
              blTableTopPanel.setLayout(new BorderLayout());

              //======== blPaginationPanel ========
              {
                blPaginationPanel.setLayout(new FlowLayout(FlowLayout.LEFT));

                //---- blShowLabel ----
                blShowLabel.setText("Show");
                blPaginationPanel.add(blShowLabel);

                //---- blShowComboBox ----
                blShowComboBox.setModel(new DefaultComboBoxModel<>(showEntries));
                blShowComboBox.addActionListener(createActionListenerShow(TableListNumbersType.BLACK));
                blPaginationPanel.add(blShowComboBox);

                //---- blEntriesLabel ----
                blEntriesLabel.setText("entries");
                blPaginationPanel.add(blEntriesLabel);
              }
              blTableTopPanel.add(blPaginationPanel, BorderLayout.WEST);

              //---- blSearchField ----
              JPanel blSearchPanel = new JPanel();
              blSearchField.setColumns(15);
              blSearchField.setToolTipText("Searching at all columns");
              blSearchField.addCancelListener(createActionListenerSearch(TableListNumbersType.BLACK, blSearchField));
              blSearchPanel.add(blSearchField);
              blTableTopPanel.add(blSearchPanel, BorderLayout.EAST);
            }
            blTablePanel.add(blTableTopPanel, BorderLayout.NORTH);

            //======== blTableBottomPanel ========
            blJPaginationPanel.setLayout(new FlowLayout(FlowLayout.RIGHT));
            blJPaginationPanel.addListener(createListenerChangePagination(TableListNumbersType.BLACK));
            blTablePanel.add(blJPaginationPanel, BorderLayout.SOUTH);

            //======== blTableScrollPane ========
            {
              blTableScrollPane.setPreferredSize(new Dimension(0, blackListTable.getRowHeight() * (blackListTable.getRowCount() + 2)));
              blTableScrollPane.setViewportView(blackListTable);
            }
            blTablePanel.add(blTableScrollPane, BorderLayout.CENTER);
          }
          blPanel.add(blTablePanel, new GridBagConstraints(0, 2, 1, 1, 0.0, 0.0,
              GridBagConstraints.CENTER, GridBagConstraints.BOTH,
              new Insets(0, 0, 0, 0), 0, 0));
        }
        mainPanel.add(blPanel, new GridBagConstraints(0, 3, 1, 1, 0.0, 0.0,
            GridBagConstraints.CENTER, GridBagConstraints.BOTH,
            new Insets(0, 0, 0, 0), 0, 0));
      }
      mainScrollPane.setViewportView(mainPanel);
    }
    add(mainScrollPane, BorderLayout.CENTER);
    setControlsEnable(false);
  }

  private void setControlsEnable(final boolean enable) {
    wlClearButton.setEnabled(enable);
    wlAddButton.setEnabled(enable);
    wlBulkDelButton.setEnabled(enable);
    wlSelectedDelButton.setEnabled(enable);
    wlImportFromFileButton.setEnabled(enable);
    wlExportToFileButton.setEnabled(enable);

    glClearButton.setEnabled(enable);
    glBulkDelButton.setEnabled(enable);
    glSelectedDelButton.setEnabled(enable);
    glSelectedToBlackListButton.setEnabled(enable);

    blClearButton.setEnabled(enable);
    blAddButton.setEnabled(enable);
    blBulkDelButton.setEnabled(enable);
    blSelectedDelButton.setEnabled(enable);
    blImportFromFileButton.setEnabled(enable);
    blExportToFileButton.setEnabled(enable);

    wlJPaginationPanel.setEnabled(enable);
    glJPaginationPanel.setEnabled(enable);
    blJPaginationPanel.setEnabled(enable);
  }

  private ActionListener createActionListenerSearch(final TableListNumbersType tableListNumbersType, final JSearchField searchField) {
    return e -> {
      switch (tableListNumbersType) {
        case WHITE:
          wlSearchText.delete(0, wlSearchText.capacity());
          if (e.getActionCommand().equals("Search field updated")) {
            wlSearchText.append(searchField.getText());
          }
          break;
        case GRAY:
          glSearchText.delete(0, glSearchText.capacity());
          if (e.getActionCommand().equals("Search field updated")) {
            glSearchText.append(searchField.getText());
          }
          break;
        case BLACK:
          blSearchText.delete(0, blSearchText.capacity());
          if (e.getActionCommand().equals("Search field updated")) {
            blSearchText.append(searchField.getText());
          }
          break;
      }
    };
  }

  private MouseAdapter createMouseAdapter(final TableListNumbersType tableListNumbersType) {
    return new MouseAdapter() {

      @Override
      public void mouseClicked(final MouseEvent e) {
        JTableHeader tableHeader = (JTableHeader) e.getSource();
        int selectedColumn;
        selectedColumn = tableHeader.columnAtPoint(e.getPoint());
        if (selectedColumn != -1) {
          SortOrder sortOrder;
          switch (tableListNumbersType) {
            case WHITE:
              sortOrder = whiteListTable.getRowSorter().getSortKeys().stream().filter(o -> o.getColumn() == selectedColumn).findFirst().get().getSortOrder();
              wlSort = new Pair<>(whiteListTable.getColumnName(selectedColumn), sortOrder);
              break;
            case GRAY:
              sortOrder = grayListTable.getRowSorter().getSortKeys().stream().filter(o -> o.getColumn() == selectedColumn).findFirst().get().getSortOrder();
              glSort = new Pair<>(grayListTable.getColumnName(selectedColumn), sortOrder);
              break;
            case BLACK:
              sortOrder = blackListTable.getRowSorter().getSortKeys().stream().filter(o -> o.getColumn() == selectedColumn).findFirst().get().getSortOrder();
              blSort = new Pair<>(blackListTable.getColumnName(selectedColumn), sortOrder);
              break;
          }
          refresherThread.get().forceRefresh();
        }
      }
    };
  }

  private ActionListener createActionListenerShow(final TableListNumbersType tableListNumbersType) {
    return e -> {
      switch (tableListNumbersType) {
        case WHITE:
          wlCountEntriesToShow.set(Integer.parseInt(wlShowComboBox.getModel().getSelectedItem().toString()));
          break;
        case GRAY:
          glCountEntriesToShow.set(Integer.parseInt(glShowComboBox.getModel().getSelectedItem().toString()));
          break;
        case BLACK:
          blCountEntriesToShow.set(Integer.parseInt(blShowComboBox.getModel().getSelectedItem().toString()));
          break;
      }
      if (refresherThread.get() != null) {
        refresherThread.get().forceRefresh();
      }
    };
  }

  private JPaginationPanel.PagerListener createListenerChangePagination(final TableListNumbersType tableListNumbersType) {
    return e -> {
      switch (tableListNumbersType) {
        case WHITE:
          wlCurrentPage.set(e);
          break;
        case GRAY:
          glCurrentPage.set(e);
          break;
        case BLACK:
          blCurrentPage.set(e);
          break;
      }
      refresherThread.get().forceRefresh();
    };
  }

  private ActionListener createActionListenerClear(final TableListNumbersType tableListNumbersType) {
    return e -> refresher.get().addExecution(new RefresherExecution() {

      @Override
      public void updateGUIComponent() {
      }

      @Override
      public void invokeExecution(final BeansPool beansPool, final TransactionManager transactionManager) throws Exception {
        switch (tableListNumbersType) {
          case WHITE:
            beansPool.getVoipAntiSpamStatisticBean().deleteAllWhiteListNumbers(MainApp.clientUID);
            break;
          case GRAY:
            beansPool.getVoipAntiSpamStatisticBean().deleteAllGrayListNumbers(MainApp.clientUID);
            break;
          case BLACK:
            beansPool.getVoipAntiSpamStatisticBean().deleteAllBlackListNumbers(MainApp.clientUID);
            break;
        }
      }

      @Override
      public String describeExecution() {
        return "clear " + tableListNumbersType.toString().toLowerCase() + "ListNumbers ";
      }
    });
  }

  private ActionListener createActionListenerAdd(final TableListNumbersType tableListNumbersType) {
    return e -> {
      Set<String> numbers = createShowInputDialogAdd("Bulk add " + tableListNumbersType.toString().toLowerCase() + " list numbers");

      if (numbers == null || numbers.isEmpty()) {
        return;
      }

      refresher.get().addExecution(new RefresherExecution() {

        @Override
        public void updateGUIComponent() {
        }

        @Override
        public void invokeExecution(final BeansPool beansPool, final TransactionManager transactionManager1) throws Exception {
          insertToListNumbers(tableListNumbersType, numbers, beansPool);
        }

        @Override
        public String describeExecution() {
          return "add new " + tableListNumbersType.toString().toLowerCase() + "ListNumbers ";
        }
      });
    };
  }

  private ActionListener createActionListenerBulkDelete(final TableListNumbersType tableListNumbersType) {
    return e -> {
      Set<String> numbers = createShowInputDialogDelete("Bulk add " + tableListNumbersType.toString().toLowerCase() + " list numbers");

      if (numbers == null || numbers.isEmpty()) {
        return;
      }

      refresher.get().addExecution(new RefresherExecution() {

        @Override
        public void updateGUIComponent() {
        }

        @Override
        public void invokeExecution(final BeansPool beansPool, final TransactionManager transactionManager1) throws Exception {
          switch (tableListNumbersType) {
            case WHITE:
              beansPool.getVoipAntiSpamStatisticBean().deleteWhiteListNumbers(MainApp.clientUID, numbers);
              break;
            case GRAY:
              beansPool.getVoipAntiSpamStatisticBean().deleteGrayListNumbers(MainApp.clientUID, numbers);
              break;
            case BLACK:
              beansPool.getVoipAntiSpamStatisticBean().deleteBlackListNumbers(MainApp.clientUID, numbers);
              break;
          }
        }

        @Override
        public String describeExecution() {
          return "delete " + tableListNumbersType.toString().toLowerCase() + "ListNumbers ";
        }
      });
    };
  }

  private ActionListener createActionListenerDeleteSelected(final TableListNumbersType tableListNumbersType) {
    return e -> refresher.get().addExecution(new RefresherExecution() {

      @Override
      public void updateGUIComponent() {
      }

      @Override
      public void invokeExecution(final BeansPool beansPool, final TransactionManager transactionManager) throws Exception {
        Set<String> selectedElements;
        switch (tableListNumbersType) {
          case WHITE:
            selectedElements = whiteListTable.getSelectedElems().stream().map(whiteListNumber -> whiteListNumber.getNumber()).collect(Collectors.toSet());
            if (selectedElements == null || selectedElements.isEmpty()) {
              return;
            }
            beansPool.getVoipAntiSpamStatisticBean().deleteWhiteListNumbers(MainApp.clientUID, selectedElements);
            break;
          case GRAY:
            selectedElements = grayListTable.getSelectedElems().stream().map(grayListNumber -> grayListNumber.getNumber()).collect(Collectors.toSet());
            if (selectedElements == null || selectedElements.isEmpty()) {
              return;
            }
            beansPool.getVoipAntiSpamStatisticBean().deleteGrayListNumbers(MainApp.clientUID, selectedElements);
            break;
          case BLACK:
            selectedElements = blackListTable.getSelectedElems().stream().map(blackListNumber -> blackListNumber.getNumber()).collect(Collectors.toSet());
            if (selectedElements == null || selectedElements.isEmpty()) {
              return;
            }
            beansPool.getVoipAntiSpamStatisticBean().deleteBlackListNumbers(MainApp.clientUID, selectedElements);
            break;
        }
      }

      @Override
      public String describeExecution() {
        return "delete selected" + tableListNumbersType.toString().toLowerCase() + "ListNumbers ";
      }
    });
  }

  private ActionListener createActionListenerImportFile(final TableListNumbersType tableListNumbersType) {
    return e -> {
      final int COUNT_OF_PART_FOR_IMPORT = 10_000;
      File file = selectFile("Select file for import");

      if (file == null) {
        return;
      }

      refresher.get().addExecution(new RefresherExecution() {

        @Override
        public void updateGUIComponent() {
        }

        @Override
        public void invokeExecution(final BeansPool beansPool, final TransactionManager transactionManager) throws Exception {
          setControlsEnable(false);
          isProcessing.set(true);
          WaitingDialog wd = new WaitingDialog(null, MainApp.getAppName(), "Import " + tableListNumbersType.toString().toLowerCase() + "ListNumbers...");
          wd.show(StatisticVoipAntiSpamPanel.this, () -> {
            try {
              try (Scanner scanner = new Scanner(file)) {
                Set<String> numbers = new HashSet<>();
                while (scanner.hasNext()) {
                  numbers.addAll(VoipAntiSpamHelper.parseNumbersFromString(scanner.nextLine()));
                  if (numbers.size() >= COUNT_OF_PART_FOR_IMPORT) {
                    importList(beansPool, numbers);
                    numbers.clear();
                  }
                }
                if (!numbers.isEmpty()) {
                  importList(beansPool, numbers);
                }
              }

              MessageUtils.showInfo(StatisticVoipAntiSpamPanel.this, MainApp.getAppName(), "Import completed");

            } catch (Exception e) {
              MessageUtils.showError(StatisticVoipAntiSpamPanel.this, "Error while, import " + tableListNumbersType.toString().toLowerCase() + "ListNumbers", e);
            } finally {
              setControlsEnable(true);
              isProcessing.set(false);
            }
          });
        }

        @Override
        public String describeExecution() {
          return "import " + tableListNumbersType.toString().toLowerCase() + "ListNumbers from file";
        }

        private void importList(final BeansPool beansPool, final Set<String> numbers) throws TransactionException, NotPermittedException, StorageException {
          switch (tableListNumbersType) {
            case WHITE:
              beansPool.getVoipAntiSpamStatisticBean().importWhiteListNumbers(MainApp.clientUID, numbers);
              break;
            case BLACK:
              beansPool.getVoipAntiSpamStatisticBean().importBlackListNumbers(MainApp.clientUID, numbers);
              break;
          }
        }

      });
    };
  }

  private ActionListener createActionListenerExportFile(final TableListNumbersType tableListNumbersType) {
    return e -> {
      Set<String> columns = createShowExportDialog();

      if (columns == null) {
        return;
      }

      File file = selectFile("Select file for export");

      if (file == null) {
        return;
      }

      refresher.get().addExecution(new RefresherExecution() {

        @Override
        public void updateGUIComponent() {
        }

        @Override
        public void invokeExecution(final BeansPool beansPool, final TransactionManager transactionManager) throws Exception {
          setControlsEnable(false);
          isProcessing.set(true);
          WaitingDialog wd = new WaitingDialog(null, MainApp.getAppName(), "Export " + tableListNumbersType.toString().toLowerCase() + "ListNumbers...");
          wd.show(StatisticVoipAntiSpamPanel.this, () -> {
            try {
              StringBuilder stringBuilder = null;
              switch (tableListNumbersType) {
                case WHITE:
                  stringBuilder = beansPool.getVoipAntiSpamStatisticBean().exportWhiteListNumbers(MainApp.clientUID, columns);
                  break;
                case BLACK:
                  stringBuilder = beansPool.getVoipAntiSpamStatisticBean().exportBlackListNumbers(MainApp.clientUID, columns);
                  break;
              }
              if (stringBuilder == null) {
                MessageUtils.showInfo(StatisticVoipAntiSpamPanel.this, MainApp.getAppName(), "Can't export get NULL data");
              } else {
                try (BufferedWriter writer = Files.newBufferedWriter(file.toPath())) {
                  writer.write(stringBuilder.toString());
                }
                MessageUtils.showInfo(StatisticVoipAntiSpamPanel.this, MainApp.getAppName(), "Export completed");
              }
            } catch (Exception e) {
              MessageUtils.showError(StatisticVoipAntiSpamPanel.this, "Error while, import " + tableListNumbersType.toString().toLowerCase() + "ListNumbers", e);
            } finally {
              setControlsEnable(true);
              isProcessing.set(false);
            }
          });
        }

        @Override
        public String describeExecution() {
          return "Export " + tableListNumbersType.toString().toLowerCase() + "ListNumbers to file";
        }
      });
    };
  }

  private ActionListener createActionListenerMoveToBlackList() {
    return e -> refresher.get().addExecution(new RefresherExecution() {

      @Override
      public void updateGUIComponent() {
      }

      @Override
      public void invokeExecution(final BeansPool beansPool, final TransactionManager transactionManager) throws Exception {
        Set<String> selectedElements = grayListTable.getSelectedElems().stream().map(grayListNumber -> grayListNumber.getNumber()).collect(Collectors.toSet());
        if (selectedElements == null || selectedElements.isEmpty()) {
          return;
        }
        beansPool.getVoipAntiSpamStatisticBean().moveToBlackListNumbers(MainApp.clientUID, selectedElements);
      }

      @Override
      public String describeExecution() {
        return "move to blackListNumbers ";
      }
    });
  }

  private void insertToListNumbers(final TableListNumbersType tableListNumbersType, final Set<String> numbers, final BeansPool beansPool) throws Exception {
    switch (tableListNumbersType) {
      case WHITE:
        Set<WhiteListNumber> whiteListNumbersLocal = new HashSet<>();
        for (String number : numbers) {
          whiteListNumbersLocal.add(new WhiteListNumber(number, VoipAntiSpamListNumbersStatus.MANUAL, 0, new Date()));
          beansPool.getVoipAntiSpamStatisticBean().insertWhiteListNumbers(MainApp.clientUID, whiteListNumbersLocal);
          whiteListNumbersLocal.clear();
        }
        break;
      case BLACK:
        Set<BlackListNumber> blackListNumbersLocal = new HashSet<>();
        for (String number : numbers) {
          blackListNumbersLocal.add(new BlackListNumber(number, VoipAntiSpamListNumbersStatus.MANUAL, 0, "", new Date()));
        }
        beansPool.getVoipAntiSpamStatisticBean().insertBlackListNumbers(MainApp.clientUID, blackListNumbersLocal);
        break;
    }
  }

  private Set<String> createShowInputDialogAdd(final String title) {
    return createShowInputDialog(new Object[] {"Cancel", "Add",}, title);
  }

  private Set<String> createShowInputDialogDelete(final String title) {
    return createShowInputDialog(new Object[] {"Cancel", "Delete"}, title);
  }

  private Set<String> createShowInputDialog(final Object[] buttons, final String title) {
    JPanel contentPanel = new JPanel(new BorderLayout());
    JLabel infoMessage = new JLabel("Hint: As a delimiter, use any non-numeric character except '#'");
    infoMessage.setBorder(new EmptyBorder(0, 10, 10, 0));
    infoMessage.setFont(infoMessage.getFont().deriveFont(infoMessage.getFont().getStyle() | Font.BOLD));
    contentPanel.add(infoMessage, BorderLayout.NORTH);
    final JTextArea textArea = new JTextArea();
    textArea.setLineWrap(true);
    JScrollPane scrollPane = new JScrollPane(textArea);
    scrollPane.setPreferredSize(new Dimension(350, 150));
    scrollPane.setHorizontalScrollBarPolicy(ScrollPaneConstants.HORIZONTAL_SCROLLBAR_NEVER);
    scrollPane.setVerticalScrollBarPolicy(ScrollPaneConstants.VERTICAL_SCROLLBAR_AS_NEEDED);
    contentPanel.add(scrollPane, BorderLayout.CENTER);
    int res = JOptionPane.showOptionDialog(StatisticVoipAntiSpamPanel.this,
        contentPanel,
        title,
        JOptionPane.YES_NO_OPTION,
        JOptionPane.PLAIN_MESSAGE,
        null,
        buttons,
        buttons[0]);
    String stringNumbers = null;
    if (res == 1) {
      stringNumbers = textArea.getText();
    }
    Set<String> numbers = null;
    if (stringNumbers != null) {
      numbers = VoipAntiSpamHelper.parseNumbersFromString(stringNumbers);
    }
    return numbers;
  }

  private File selectFile(final String title) {
    File current = new File(sourceDirectory.get().getValue());
    JFileChooser chooser = new JFileChooser();
    chooser.setDialogTitle(title);
    chooser.setFileSelectionMode(JFileChooser.FILES_ONLY);
    chooser.setAcceptAllFileFilterUsed(false);
    chooser.setSelectedFile(current);

    if (chooser.showOpenDialog(StatisticVoipAntiSpamPanel.this) == JFileChooser.APPROVE_OPTION) {
      sourceDirectory.get().setValue(chooser.getCurrentDirectory().getAbsolutePath());
      return chooser.getSelectedFile();
    } else {
      return null;
    }
  }

  private Set<String> createShowExportDialog() {
    JPanel contentPanel = new JPanel(new BorderLayout());
    JLabel infoMessage = new JLabel("Select Columns");
    infoMessage.setBorder(new EmptyBorder(0, 10, 10, 0));
    infoMessage.setFont(infoMessage.getFont().deriveFont(infoMessage.getFont().getStyle() | Font.BOLD));
    contentPanel.add(infoMessage, BorderLayout.NORTH);
    JPanel checkBoxPanel = new JPanel(new GridLayout(3, 1));
    JCheckBox numbersCheckBox = new JCheckBox("numbers");
    checkBoxPanel.add(numbersCheckBox);
    JCheckBox statusCheckBox = new JCheckBox("status");
    checkBoxPanel.add(statusCheckBox);
    JCheckBox addTimeCheckBox = new JCheckBox("add time");
    checkBoxPanel.add(addTimeCheckBox);
    JCheckBox routingRequestCountCheckBox = new JCheckBox("routing request count");
    checkBoxPanel.add(routingRequestCountCheckBox);

    Object[] buttons = new Object[] {"Cancel", "Export",};

    contentPanel.add(checkBoxPanel, BorderLayout.CENTER);
    int res = JOptionPane.showOptionDialog(StatisticVoipAntiSpamPanel.this,
        contentPanel,
        "Export to CSV file",
        JOptionPane.YES_NO_OPTION,
        JOptionPane.PLAIN_MESSAGE,
        null,
        buttons,
        buttons[0]);
    if (res == 1) {
      Set<String> columns = new HashSet<>();
      if (numbersCheckBox.isSelected()) {
        columns.add("numbers");
      }
      if (statusCheckBox.isSelected()) {
        columns.add("status");
      }
      if (addTimeCheckBox.isSelected()) {
        columns.add("add time");
      }
      if (routingRequestCountCheckBox.isSelected()) {
        columns.add("routing request count");
      }
      return columns;
    }
    return null;
  }

  private class CellRenderer extends DefaultTableCellRenderer {

    private static final long serialVersionUID = 1381362840946191421L;

    private final TableListNumbersType tableListNumbersType;

    private CellRenderer(final TableListNumbersType tableListNumbersType) {
      this.tableListNumbersType = tableListNumbersType;
    }

    @Override
    public Component getTableCellRendererComponent(final JTable table, final Object value, final boolean isSelected, final boolean hasFocus, final int row, final int column) {
      Component tableCellRendererComponent = super.getTableCellRendererComponent(table, value, isSelected, hasFocus, row, column);
      Object elemAt = ((JUpdatableTable) table).getElemAt(row);
      switch (tableListNumbersType) {
        case WHITE:
          break;
        case GRAY:
          GrayListNumber grayListNumber = (GrayListNumber) elemAt;
          if (column == 1 && grayListNumber.getStatus() != VoipAntiSpamListNumbersStatus.MANUAL && !grayListNumber.getStatusDescription().equals("")) {
            setToolTipText(getToolTipText(grayListNumber.getStatusDescription()));
          }
          if (column == 4 && grayListNumber.getBlockTimeLeft().getTime() > System.currentTimeMillis()) {
            JLabel jLabel = new JLabel(value.toString());
            jLabel.setOpaque(true);
            jLabel.setBackground(Color.LIGHT_GRAY);
            jLabel.setBorder(new EmptyBorder(0, 5, 0, 0));
            return jLabel;
          }
          break;
        case BLACK:
          BlackListNumber blackListNumber = (BlackListNumber) elemAt;
          if (blackListNumber.getStatus() != VoipAntiSpamListNumbersStatus.MANUAL && !blackListNumber.getStatusDescription().equals("")) {
            setToolTipText(getToolTipText(blackListNumber.getStatusDescription()));
          }
          break;
      }
      return tableCellRendererComponent;
    }

    private String getToolTipText(final String s) {
      return "<html><b>Filter parameters</b><br> " + s + "</html>";
    }

  }

}
