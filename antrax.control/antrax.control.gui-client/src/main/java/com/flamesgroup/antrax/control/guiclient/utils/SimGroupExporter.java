/*
 * Copyright (C) 2017 Flames Group SIA
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.flamesgroup.antrax.control.guiclient.utils;

import com.flamesgroup.antrax.automation.meta.ScriptDefinition;
import com.flamesgroup.antrax.automation.meta.ScriptInstance;
import com.flamesgroup.antrax.automation.meta.ScriptParameter;
import com.flamesgroup.antrax.commons.Timeout;
import com.flamesgroup.antrax.commons.impl.InvariableTimeout;
import com.flamesgroup.antrax.control.authorization.NotPermittedException;
import com.flamesgroup.antrax.control.communication.StorageException;
import com.flamesgroup.antrax.control.communication.TransactionException;
import com.flamesgroup.antrax.control.guiclient.MainApp;
import com.flamesgroup.antrax.control.swingwidgets.table.ExportException;
import com.flamesgroup.antrax.storage.commons.SIMGroup;
import com.flamesgroup.antrax.storage.commons.impl.SIMGroupImpl;
import com.flamesgroup.antrax.storage.commons.impl.ScriptCommons;
import com.flamesgroup.commons.Pair;

import java.io.Closeable;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.lang.reflect.Method;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Properties;
import java.util.Set;
import java.util.TreeSet;
import java.util.concurrent.atomic.AtomicInteger;

public class SimGroupExporter {

  private static void close(final Closeable closable) {
    if (closable != null) {
      try {
        closable.close();
      } catch (IOException e) {
        // ignore
      }
    }
  }

  public void exportToFile(final SIMGroup simGroup, final File file) throws ExportException {
    FileOutputStream stream = null;
    try {
      stream = new FileOutputStream(file);
      export(simGroup).store(stream, null);
    } catch (IOException e) {
      throw new ExportException(e);
    } finally {
      close(stream);
    }
  }

  public static Properties export(final SIMGroup simGroup) {
    Properties prop = new Properties();
    for (Method m : SIMGroup.class.getMethods()) {
      String name = m.getName();
      if (name.startsWith("get") || name.startsWith("list") || name.startsWith("is") || name.startsWith("can")) {
        name = name.replaceFirst("get|list|is", "");
        name = name.replaceFirst("can", "Can");
        Object value;
        try {
          value = m.invoke(simGroup);
        } catch (Exception e) {
          throw new RuntimeException(e);
        }
        Class<?> type = m.getReturnType();
        if (type == String.class) {
          prop.setProperty(name, String.valueOf(value));
        } else if (type == Integer.TYPE) {
          prop.setProperty(name, String.valueOf(value));
        } else if (type == Timeout.class) {
          exportTimeout(prop, name, (Timeout) value);
        } else if (type == ScriptInstance[].class) {
          exportScriptInstances(prop, name, (ScriptInstance[]) value);
        } else if (type == ScriptInstance.class) {
          exportScriptInstance(prop, name, (ScriptInstance) value);
        } else if (type == ScriptCommons[].class) {
          exportScriptCommons(prop, name, (ScriptCommons[]) value);
        } else if (type == Boolean.TYPE) {
          prop.setProperty(name, String.valueOf(value));
        } else if (type == Long.TYPE) {
          // skipping: one of the getID or getRevision (no need to save it)
        } else {
          throw new IllegalStateException("SimGroup updated and contains new type which is not supported be export engine. Type is: " + type);
        }
      }
    }
    return prop;
  }

  private static void exportTimeout(final Properties prop, final String propName, final Timeout timeout) {
    if (timeout == null) {
      prop.setProperty(propName, "null");
    } else {
      prop.setProperty(propName, String.format("%s:%s", timeout.getMediana(), timeout.getDelta()));
    }
  }

  private static void exportScriptCommons(final Properties prop, final String key, final ScriptCommons[] commons) {
    if (commons == null) {
      prop.setProperty(key, "null");
    } else {
      for (int i = 0; i < commons.length; ++i) {
        prop.setProperty(String.format("%s.%d", key, i), mask(commons[i].getName(), ':') + ':' + ByteArrayStringCoder.encode(commons[i].getSerializedValue()));
      }
    }
  }

  private static String mask(final String name, final char symb) {
    StringBuilder retval = new StringBuilder();
    for (int i = 0; i < name.length(); ++i) {
      char c = name.charAt(i);
      if (c == symb || c == '$') {
        retval.append('$');
      }
      retval.append(c);
    }
    return retval.toString();
  }

  private static Pair<String, String> unmaskUntilMet(final String value, final char symb) {
    StringBuilder retval = new StringBuilder();
    boolean escaped = false;
    int i;
    for (i = 0; i < value.length(); ++i) {
      char c = value.charAt(i);
      if (c == '$') {
        escaped = true;
        continue;
      }
      if (c == symb && !escaped) {
        ++i;
        break;
      }
      retval.append(c);
      escaped = false;
    }
    return new Pair<>(retval.toString(), value.substring(i));
  }

  private static void exportScriptInstances(final Properties prop, final String scriptPropName, final ScriptInstance... scriptInstances) {
    if (scriptInstances == null) {
      prop.setProperty(scriptPropName, "null");
    } else {
      int index = 0;
      for (ScriptInstance si : scriptInstances) {
        exportScriptInstance(prop, String.format("%s.%s", scriptPropName, index), si);
        index++;
      }
    }
  }

  private static void exportScriptInstance(final Properties prop, final String scriptPropName, final ScriptInstance si) {
    if (si == null) {
      return;
    }

    prop.setProperty(scriptPropName, si.getDefinition().getName());
    Map<String, AtomicInteger> paramIndex = new HashMap<>();
    for (ScriptParameter param : si.getParameters()) {
      String paramPropName = scriptPropName + "." + param.getDefinition().getName();
      if (param.getDefinition().isArray()) {
        AtomicInteger index = paramIndex.get(paramPropName);
        if (index == null) {
          index = new AtomicInteger(0);
          paramIndex.put(paramPropName, index);
        } else {
          index.incrementAndGet();
        }

        paramPropName += "." + String.valueOf(index.get());
      }
      String serializedData = ByteArrayStringCoder.encode(param.getSerializedValue());
      prop.setProperty(paramPropName, serializedData);
    }
  }

  public static SIMGroup importSimGroup(final Properties prop, final ClassLoader codebaseClassloader, final BeansPool pool, final int transactionId) throws ExportException {
    Set<String> alreadyImported = new HashSet<>();
    SIMGroup simGroup;
    try {
      simGroup = pool.getConfigBean().createSIMGroup(MainApp.clientUID, transactionId);
    } catch (NotPermittedException | TransactionException | StorageException e) {
      throw new ExportException("Can't create sim group for import", e);
    }

    for (Object pName : new TreeSet<>(prop.keySet())) {
      String propName = ((String) pName).replaceAll("\\.\\d+$", "");
      if (!alreadyImported.add(propName)) {
        continue;
      }
      String propValue = prop.getProperty(propName);
      Object value;
      Method m = findMethod(SIMGroup.class, "set" + propName);
      if (m == null) {
        continue;
      }
      Class<?> type = m.getParameterTypes()[0];
      if (type == String.class) {
        value = propValue;
      } else if (type == Integer.TYPE) {
        value = Integer.parseInt(propValue);
      } else if (type == ScriptInstance[].class) {
        value = importScriptInstances(prop, propName, codebaseClassloader, pool);
      } else if (type == Timeout.class) {
        value = getTimeoutProperty(prop, propName);
      } else if (type == ScriptCommons[].class) {
        value = importScriptCommons(prop, propName, codebaseClassloader);
      } else if (type == Boolean.TYPE) {
        value = getBoolProperty(prop, propName);
      } else if (type == ScriptInstance.class) {
        value = importScriptInstance(prop, propName, codebaseClassloader, pool);
      } else {
        throw new IllegalStateException("Cann not parse value of type " + type);
      }
      try {
        m.invoke(simGroup, value);
      } catch (Exception e) {
        throw new RuntimeException(e);
      }
    }
    return simGroup;
  }

  private static Method findMethod(final Class<?> clazz, final String string) {
    for (Method m : clazz.getMethods()) {
      if (string.equals(m.getName())) {
        return m;
      }
    }
    return null;
  }

  public SIMGroup importFromFile(final File file, final ClassLoader codebaseClassloader, final BeansPool pool, final int transactionId) throws ExportException {
    FileInputStream stream = null;
    Properties prop = new Properties();
    try {
      prop.load((stream = new FileInputStream(file)));
    } catch (FileNotFoundException e) {
      throw new ExportException(e);
    } catch (IOException e) {
      throw new ExportException(e);
    } finally {
      close(stream);
    }
    return importSimGroup(prop, codebaseClassloader, pool, transactionId);
  }

  private static ScriptCommons[] importScriptCommons(final Properties prop, final String key, final ClassLoader codebaseClassloader) {
    List<ScriptCommons> retval = new LinkedList<>();
    for (int i = 0; ; ++i) {
      String propName = String.format("%s.%s", key, i);
      String propValue = prop.getProperty(propName);
      if (propValue == null) {
        break;
      }
      ScriptCommons common = new ScriptCommons();
      Pair<String, String> pair = unmaskUntilMet(propValue, ':');
      common.setName(pair.first());
      common.setSerializedValue(ByteArrayStringCoder.decode(pair.second()));
      retval.add(common);
    }
    return retval.toArray(new ScriptCommons[retval.size()]);
  }

  private static ScriptInstance[] importScriptInstances(final Properties prop, final String scriptName, final ClassLoader codebaseClassloader, final BeansPool pool) throws ExportException {
    List<ScriptInstance> instances = new LinkedList<>();
    for (int i = 0; ; i++) {
      String scriptPropName = String.format("%s.%s", scriptName, i);
      ScriptInstance si = importScriptInstance(prop, scriptPropName, codebaseClassloader, pool);
      if (si == null) {
        break;
      } else {
        instances.add(si);
      }
    }

    return !instances.isEmpty() ? instances.toArray(new ScriptInstance[instances.size()]) : null;
  }

  private static ScriptInstance importScriptInstance(final Properties prop, final String scriptPropName, final ClassLoader codebaseClassloader, final BeansPool pool) throws ExportException {
    String definitionName = prop.getProperty(scriptPropName);
    if (definitionName == null) {
      return null;
    }

    try {
      ScriptDefinition def = pool.getConfigBean().getScriptDefinition(MainApp.clientUID, definitionName);
      if (def == null) {
        throw new ExportException("Error import script, can't find script definition '" + definitionName + "'");
      }
      Map<String, List<ParamInfo>> mapParams = loadParameters(prop, scriptPropName);

      ScriptInstance si = def.createInstance();
      applyParameters(si, mapParams);
      return si;
    } catch (NotPermittedException e) {
      throw new ExportException("Access denied, while find script definition '" + definitionName + "'", e);
    } catch (StorageException e) {
      throw new ExportException("Storage error, while import script '" + scriptPropName + "'", e);
    }
  }

  private static void applyParameters(final ScriptInstance si, final Map<String, List<ParamInfo>> mapParams) throws ExportException {
    for (String paramName : mapParams.keySet()) {
      List<ParamInfo> paramsList = mapParams.get(paramName);

      ScriptParameter scriptParam = si.getParameter(paramName);
      if (scriptParam == null) {
        throw new ExportException("Can't import script '" + si.getDefinition().getName() + "' - no parameter '" + paramName + "' any more");
      }

      boolean isArray = scriptParam.getDefinition().isArray();
      for (ParamInfo pi : paramsList) {
        // if this is array and NOT first array parameter
        if (isArray && pi.getIndex() > 0) {
          scriptParam = si.addParameter(paramName, pi.getValue());
        } else {
          scriptParam.setSerializedValue(pi.getValue());
        }
      }
    }
  }

  private static Map<String, List<ParamInfo>> loadParameters(final Properties prop, String scriptPropName) {
    scriptPropName += ".";

    Map<String, List<ParamInfo>> map = new HashMap<>();
    for (Object key : prop.keySet()) {
      if (!(key instanceof String)) {
        continue;
      }
      String strKey = (String) key;
      if (!strKey.startsWith(scriptPropName) || strKey.equals(scriptPropName)) {
        continue;
      }

      // <scrip-prefix> <=> <scrip-name>{.<script-index>}
      // <scrip-prefix>.<param-name>{.<param-index>}
      String paramProp = strKey.substring(scriptPropName.length());
      String[] parts = paramProp.split("\\.");
      String paramName = parts.length > 0 ? parts[0] : paramProp;
      int index = 0;
      if (parts.length > 1) {
        index = Integer.parseInt(parts[1]);
      }

      List<ParamInfo> params = map.get(paramName);
      if (params == null) {
        params = new LinkedList<>();
        map.put(paramName, params);
      }

      String value = prop.getProperty(strKey);
      params.add(new ParamInfo(index, ByteArrayStringCoder.decode(value)));
    }

    sortParametersByIndex(map);
    return map;
  }

  private static void sortParametersByIndex(final Map<String, List<ParamInfo>> paramsMap) {
    for (List<ParamInfo> params : paramsMap.values()) {
      Collections.sort(params, new Comparator<ParamInfo>() {

        @Override
        public int compare(final ParamInfo p1, final ParamInfo p2) {
          return p1.getIndex() - p2.getIndex();
        }

      });
    }
  }

  private static String getStringProperty(final Properties prop, final String key) throws ExportException {
    String value = prop.getProperty(key);
    if (value == null) {
      throw new ExportException(String.format("Unknown property key '%s'", key));
    }

    return value;
  }

  private static boolean getBoolProperty(final Properties prop, final String key) throws ExportException {
    return Boolean.valueOf(getStringProperty(prop, key));
  }

  private static Timeout getTimeoutProperty(final Properties prop, final String key) throws ExportException {
    String strValue = getStringProperty(prop, key);
    if ("null".equals(strValue)) {
      return null;
    }
    String[] parts = strValue.split(":");
    if (parts.length < 2) {
      throw new ExportException(String.format("Timeout property must have two values separated by colon '%s' = [%s]", key, strValue));
    }

    try {
      long mediana = Long.valueOf(parts[0]);
      long delta = Long.valueOf(parts[1]);

      return new InvariableTimeout(mediana, delta);
    } catch (NumberFormatException ignored) {
      throw new ExportException(String.format("Invalid value for timeout property '%s' =[%s]", key, strValue));
    }
  }

  private static class ParamInfo {

    private final int index;
    private final byte[] value;

    public ParamInfo(final int index, final byte[] value) {
      this.index = index;
      this.value = value;
    }

    public int getIndex() {
      return index;
    }

    public byte[] getValue() {
      return value;
    }

  }
}
