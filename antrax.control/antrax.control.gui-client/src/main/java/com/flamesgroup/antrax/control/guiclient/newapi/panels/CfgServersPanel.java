/*
 * Copyright (C) 2017 Flames Group SIA
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.flamesgroup.antrax.control.guiclient.newapi.panels;

import com.flamesgroup.antrax.control.authorization.NotPermittedException;
import com.flamesgroup.antrax.control.communication.StorageException;
import com.flamesgroup.antrax.control.communication.TransactionException;
import com.flamesgroup.antrax.control.guiclient.MainApp;
import com.flamesgroup.antrax.control.guiclient.newapi.panels.servers.CfgServersBox;
import com.flamesgroup.antrax.control.guiclient.panels.BaseEditorBox;
import com.flamesgroup.antrax.control.guiclient.panels.BaseEditorPanel;
import com.flamesgroup.antrax.control.guiclient.panels.BeanCommunication;
import com.flamesgroup.antrax.control.guiclient.panels.EditingPreprocessor;
import com.flamesgroup.antrax.control.guiclient.panels.OperationCanceledException;
import com.flamesgroup.antrax.control.guiclient.panels.preprocessors.ServerEditingPreprocessor;
import com.flamesgroup.antrax.control.guiclient.panels.tablebuilders.ServerRowBuilder;
import com.flamesgroup.antrax.control.guiclient.utils.BeansPool;
import com.flamesgroup.antrax.control.guiclient.utils.RefresherThread;
import com.flamesgroup.antrax.control.guiclient.utils.TransactionListener;
import com.flamesgroup.antrax.control.guiclient.utils.TransactionManager;
import com.flamesgroup.antrax.control.guiclient.utils.TransactionState;
import com.flamesgroup.antrax.control.guiclient.widgets.tooltip.ServerConfigTableTooltips;
import com.flamesgroup.antrax.control.swingwidgets.MessageUtils;
import com.flamesgroup.antrax.control.swingwidgets.table.TableBuilder;
import com.flamesgroup.antrax.storage.commons.IServerData;
import com.flamesgroup.commons.CallRouteConfig;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.regex.Pattern;

public class CfgServersPanel extends BaseEditorPanel<IServerData, Long> {

  private static final Logger logger = LoggerFactory.getLogger(CfgServersPanel.class);
  private static final long serialVersionUID = -6310722905805157009L;

  private final List<IServerData> servers = new ArrayList<>();

  private final String[] columnToolTips = {
      ServerConfigTableTooltips.getNameTooltip(),
      ServerConfigTableTooltips.getPublicInterfaceTooltip(),
      ServerConfigTableTooltips.getVoiceServerTooltip(),
      ServerConfigTableTooltips.getSimServerTooltip(),
      ServerConfigTableTooltips.getAudioCaptureTooltip()
  };

  public CfgServersPanel() {
    getTable().setColumnToolTips(columnToolTips);
  }

  @Override
  protected BeanCommunication<IServerData> createBeanCommunication() {
    return new ServerBeanCommunication();
  }

  @Override
  protected BaseEditorBox<IServerData> createEditorBox(final RefresherThread refresherThread, final TransactionManager transactionManager) {
    return new CfgServersBox();
  }

  @Override
  protected TableBuilder<IServerData, Long> createTableBuilder() {
    return new ServerRowBuilder();
  }

  @Override
  protected boolean isEditorBoxStretched() {
    return true;
  }

  @Override
  protected EditingPreprocessor<IServerData> createEditingPreprocessor() {
    return new ServerEditingPreprocessor(getTable());
  }

  @Override
  public void postInitialize(final RefresherThread refresher, final TransactionManager transactionManager) {
    transactionManager.addListener(new TransactionListener() {
      @Override
      public void onTransactionStateChanged(final TransactionState newState) {
      }

      @Override
      public List<Throwable> handleTransactionGoingToBeCommitted(final int transactionId, final BeansPool beansPool) {
        beansPool.getConfigBean().incServersRevision(MainApp.clientUID);
        return Collections.emptyList();
      }
    });

    super.postInitialize(refresher, transactionManager);
  }

  @Override
  protected void addElement(IServerData elem) {
    int transactionId = getTransactionId();
    logger.debug("[{}] - preprocessing add new elem", this);
    try {
      elem = getPreprocessor().beforeAdd(elem, transactionId, CfgServersPanel.this);
    } catch (OperationCanceledException ignored) {
      return;
    }

    if (servers.contains(elem)) {
      MessageUtils.showError(this, "Duplicate server", "Server [" + elem.getName() + "] already exist");
      return;
    }

    logger.debug("[{}] - updating new elem: {}", this, elem);
    if (elem != null) {
      logger.debug("[{}] - insert in table new elem: {}", this, elem);
      getTable().selectRow(getTable().insertElem(elem));
    }
  }

  private class ServerBeanCommunication implements BeanCommunication<IServerData> {

    private long lastRevision = -1;

    @Override
    public IServerData createElem(final BeansPool beansPool, final int transactionId) throws NotPermittedException, TransactionException, StorageException {
      return beansPool.getConfigBean().createServer(MainApp.clientUID, transactionId);
    }

    @Override
    public IServerData[] listElems(final BeansPool pool, final int transactionId) throws NotPermittedException, TransactionException, StorageException {
      lastRevision = pool.getConfigBean().getServersRevision(MainApp.clientUID);
      servers.clear();
      List<IServerData> serversLocal = pool.getConfigBean().listServers(MainApp.clientUID);
      servers.addAll(serversLocal);
      return serversLocal.toArray(new IServerData[serversLocal.size()]);
    }

    @Override
    public void removeElem(final IServerData elem, final BeansPool pool, final int transactionId) throws NotPermittedException, TransactionException, StorageException {
      pool.getConfigBean().deleteServer(MainApp.clientUID, transactionId, elem);
    }

    @Override
    public boolean checkReadRequired(final BeansPool pool, final int transactionId) throws Exception {
      long rev = pool.getConfigBean().getServersRevision(MainApp.clientUID);
      return rev != lastRevision;
    }

    @Override
    public IServerData updateElem(final IServerData elem, final BeansPool pool, final int transactionId) throws NotPermittedException, TransactionException, StorageException {
      if (elem.isVoiceServerEnabled()) {
        CallRouteConfig callRouteConfig = elem.getVoiceServerConfig().getCallRouteConfig();
        try {
          Pattern.compile(callRouteConfig.getCalledRegex()); // TODO: dirty fix for incorrect called regex
        } catch (Exception e) {
          throw new IllegalStateException("Incorrect called regex for [" + elem.getName() + "]:\n" + e.getMessage());
        }
      }

      return pool.getConfigBean().updateServer(MainApp.clientUID, transactionId, elem);
    }

    @Override
    public IServerData copyElement(final IServerData srcElem, final IServerData dstElem) {
      return dstElem;
    }

    @Override
    public boolean refreshForced() {
      return false;
    }
  }

}
