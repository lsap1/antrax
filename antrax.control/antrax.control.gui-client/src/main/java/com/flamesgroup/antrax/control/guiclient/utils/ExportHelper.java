/*
 * Copyright (C) 2017 Flames Group SIA
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.flamesgroup.antrax.control.guiclient.utils;

import com.flamesgroup.antrax.control.guiclient.MainApp;
import com.flamesgroup.antrax.control.swingwidgets.ExtFileFilter;
import com.flamesgroup.antrax.control.swingwidgets.MessageUtils;
import com.flamesgroup.antrax.control.swingwidgets.dialog.FileSelectDialog;
import com.flamesgroup.antrax.control.swingwidgets.dialog.WaitingDialog;
import com.flamesgroup.antrax.control.swingwidgets.table.ExportException;
import com.flamesgroup.antrax.control.swingwidgets.table.JUpdatableTable;
import com.flamesgroup.antrax.control.swingwidgets.table.TableExportException;
import com.flamesgroup.antrax.control.swingwidgets.table.TableExporter;
import com.flamesgroup.antrax.storage.commons.SIMGroup;

import java.awt.*;
import java.io.File;
import java.text.Format;
import java.text.MessageFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.concurrent.atomic.AtomicReference;

import javax.swing.*;

public class ExportHelper {

  private static final Format fileFormatter = new SimpleDateFormat("dd.MM.yy_HH.mm.ss");

  public void exportTable(final JUpdatableTable<?, ?> table, final ExportType type, final String fileNamePrefix, final Component invoker) {
    if (type != ExportType.CSV) {
      throw new IllegalArgumentException("support only csv export");
    }
    FileSelectDialog dialog = getSelectionDialog(invoker, type.getFileExtension());
    final File file = dialog.saveFile(generateFileName(type, fileNamePrefix));
    if (file == null) { // user canceled
      return;
    }

    final WaitingDialog wd = new WaitingDialog(type.getIcon(), MainApp.getAppName(), "Export " + table.getName() + "...");
    wd.show(invoker, () -> {
      try {
        TableExporter.writeCSVFile(table, file);
        MessageUtils.showInfo(invoker, MainApp.getAppName(), "Table exported to file: '" + file.getName() + "'");
      } catch (TableExportException e) {
        MessageUtils.showError(invoker, "Error while, export table", e);
      }
    });
  }

  public void exportSimGroup(final SIMGroup simGroup, final Component invoker) {
    ExportType exportType = ExportType.PROPERTY;
    String fileNamePrefix = simGroup.getName();

    FileSelectDialog dialog = getSelectionDialog(invoker, exportType.getFileExtension());
    final File file = dialog.saveFile(generateFileName(exportType, fileNamePrefix));
    if (file == null) { // user canceled
      return;
    }

    final WaitingDialog wd = new WaitingDialog(exportType.getIcon(), MainApp.getAppName(), "Export SIM group '" + simGroup.getName() + "'...");
    wd.show(invoker, new Runnable() {
      @Override
      public void run() {
        try {
          new SimGroupExporter().exportToFile(simGroup, file);
          MessageUtils.showInfo(invoker, MainApp.getAppName(), "SIM group '" + simGroup.getName() + "' exported to file: '" + file.getName() + "'");
        } catch (Exception e) {
          MessageUtils.showError(invoker, "Error while, export SIM group '" + simGroup.getName() + "'", e);
        }
      }
    });
  }

  public SIMGroup importSimGroup(final Component invoker, final RefresherThread refresherThread, final int transactionId, final boolean storeGroup) {
    ExportType exportType = ExportType.PROPERTY;
    final File file = getSelectionDialog(invoker, exportType.getFileExtension()).openFile();
    if (file == null) { // user canceled
      return null;
    }

    final AtomicReference<SIMGroup> simGroup = new AtomicReference<>(null);
    final WaitingDialog wd = new WaitingDialog(exportType.getIcon(), MainApp.getAppName(), MessageFormat.format("Import SIM group from ''{0}''...", file.getName()));
    wd.show(invoker, new Runnable() {
      @Override
      public void run() {
        try {
          simGroup.set(loadSimGroup(file, refresherThread, transactionId, storeGroup));
        } catch (ExportException e) {
          MessageUtils.showError(invoker, e.getMessage(), e);
        } catch (Exception e) {
          MessageUtils.showError(invoker, MessageFormat.format("Error while, import SIM group from ''{0}''", file.getName()), e);
        }
      }
    });
    return simGroup.get();
  }

  private SIMGroup loadSimGroup(final File file, final RefresherThread refresherThread, final int transactionId, final boolean storeGroup) throws ExportException {
    WaitingReference<SIMGroup> wr = RefresherUtils.executeWaitableAction(refresherThread, new CallableRefresher<SIMGroup>() {

      @Override
      public SIMGroup performAction(final BeansPool pool) throws Exception {
        ClassLoader cl = pool.getConfigBean().getPluginsStore(MainApp.clientUID, transactionId).getClassLoader();
        SIMGroup group = new SimGroupExporter().importFromFile(file, cl, pool, transactionId);
        if (storeGroup) {
          group = pool.getConfigBean().updateSIMGroup(MainApp.clientUID, transactionId, group);
        }
        return group;
      }
    });
    if (wr.get() == null && wr.getErrorCaught() != null) {
      throw new ExportException(wr.getErrorCaught());
    }
    return wr.get();
  }

  private FileSelectDialog getSelectionDialog(final Component invoker, final ExtFileFilter fileFilter) {
    Component root = SwingUtilities.getRoot(invoker);
    File currentDir = new File(System.getProperty("user.home"));
    return new FileSelectDialog(root, currentDir, fileFilter.getExtension(), fileFilter.getExtensionDescription());
  }

  private static String generateFileName(final ExportType type, final String fileNamePrefix) {
    String strDate = fileFormatter.format(new Date());
    String ext = type.getFileExtension().getExtension();
    return String.format("%s-%s%s", fileNamePrefix, strDate, ext).replaceAll(" ", "_");
  }

}
