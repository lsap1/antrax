/*
 * Copyright (C) 2017 Flames Group SIA
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.flamesgroup.antrax.control.guiclient.newapi.panels.servers;

import com.flamesgroup.antrax.control.guiclient.panels.BaseEditorBox;
import com.flamesgroup.antrax.control.swingwidgets.JLabeledComponent;
import com.flamesgroup.antrax.control.swingwidgets.editor.IPAddressField;
import com.flamesgroup.antrax.storage.commons.IServerData;

import java.awt.*;
import java.util.ArrayList;
import java.util.List;

import javax.swing.*;

public class CfgServersBox extends BaseEditorBox<IServerData> {

  private static final long serialVersionUID = 1500904348023955658L;

  private JTextField serverName;
  private IPAddressField publicInterface;
  private VoiceServerEditor voiceServerEditor;
  private SimServerEditor simServerEditor;

  @Override
  protected void createGUIElements() {
    setLayout(new BoxLayout(this, BoxLayout.Y_AXIS));
    serverName = new JTextField(20);
    publicInterface = new IPAddressField();
    voiceServerEditor = new VoiceServerEditor();
    voiceServerEditor.getComponent().setBorder(BorderFactory.createTitledBorder("Voice Server"));
    simServerEditor = new SimServerEditor();
    simServerEditor.getComponent().setBorder(BorderFactory.createTitledBorder("Sim Server"));

    JLabeledComponent lblServerName = new JLabeledComponent("Server Name (requires restart of all services)", serverName);
    JLabeledComponent lblPublicInterface = new JLabeledComponent("Public Interface", publicInterface);

    {
      List<JLabeledComponent> componets = new ArrayList<>();
      componets.addAll(voiceServerEditor.listLabeledComponents());
      componets.addAll(simServerEditor.listLabeledComponents());
      componets.add(lblServerName);
      componets.add(lblPublicInterface);
      JLabeledComponent[] c = componets.toArray(new JLabeledComponent[componets.size()]);
      JLabeledComponent.fitLabelWidths(c);
      JLabeledComponent.fitComponentWidths(c);
    }

    add(lblServerName);
    add(lblPublicInterface);
    add(voiceServerEditor.getComponent());
    add(simServerEditor.getComponent());
  }

  @Override
  protected void readEditorsToElem(final IServerData dest) {
    dest.setName(serverName.getText());
    dest.setPublicAddress(publicInterface.getAddress());
    voiceServerEditor.readEditorsToElem(dest);
    simServerEditor.readEditorsToElem(dest);
  }

  @Override
  protected void writeElemToEditors(final IServerData src) {
    serverName.setText(src.getName());
    publicInterface.setIpAddress(src.getPublicAddress());
    voiceServerEditor.writeElemToEditors(src);
    simServerEditor.writeElemToEditors(src);
  }

  @Override
  public void setEditable(final boolean value) {
    serverName.setEditable(value);
    publicInterface.setEditable(value);
    voiceServerEditor.setEditable(value);
    simServerEditor.setEditable(value);
  }

  @Override
  protected void clearCustomComponent(final Component component) {

  }

  @Override
  protected Object getCustomComponentValue(final Component component) {
    if (component == voiceServerEditor.getComponent()) {
      return voiceServerEditor.exportToObject();
    } else if (component == simServerEditor.getComponent()) {
      return simServerEditor.exportToObject();
    }
    return null;
  }

  public static void main(final String[] args) {
    final JFrame frame = new JFrame();
    frame.getContentPane().add(new CfgServersBox());
    frame.pack();
    frame.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
    SwingUtilities.invokeLater(new Runnable() {

      @Override
      public void run() {
        frame.setVisible(true);
      }
    });
  }

}
