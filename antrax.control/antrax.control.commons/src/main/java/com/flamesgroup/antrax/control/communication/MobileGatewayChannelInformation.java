/*
 * Copyright (C) 2017 Flames Group SIA
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.flamesgroup.antrax.control.communication;

import com.flamesgroup.antrax.distributor.ChannelConfig;
import com.flamesgroup.antrax.storage.commons.GSMChannel;
import com.flamesgroup.commons.ChannelUID;
import com.flamesgroup.commons.GSMNetworkInfo;

import java.io.Serializable;

public interface MobileGatewayChannelInformation extends Serializable {

  ChannelUID getGSMChannelUID();

  int getSuccessfulCallsCount();

  int getTotalCallsCount();

  long getCallsDuration();

  int getSuccessOutgoingSmsCount();

  int getTotalOutgoingSmsCount();

  int getIncomingTotalSmsCount();

  int getSignalStrength();

  int getBitErrorRate();

  GSMNetworkInfo getGsmNetworkInfo();

  boolean isLive();

  ChannelConfig getChannelConfig();

  GSMChannel getGSMChannel();

  void reset();

}
