/*
 * Copyright (C) 2017 Flames Group SIA
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.flamesgroup.antrax.control.communication;

import com.flamesgroup.antrax.commons.TimePeriodWriter;
import com.flamesgroup.antrax.storage.state.CallChannelState;
import com.flamesgroup.antrax.storage.state.CallState;
import com.flamesgroup.commons.CellKey;
import com.flamesgroup.commons.ChannelUID;
import com.flamesgroup.commons.GSMNetworkInfo;
import com.flamesgroup.commons.TimeUtils;
import com.flamesgroup.unit.CellInfo;
import com.flamesgroup.utils.ServerSyncTimeProvider;
import com.flamesgroup.utils.TimeProvider;

import java.io.PrintStream;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

public class VoiceServerChannelsInfo implements IVoiceServerChannelsInfo, Comparable<IVoiceServerChannelsInfo> {

  private static final long serialVersionUID = 7842670124438559172L;

  // TODO: remove and set correct timeout on server
  private final TimeProvider timeProvider = new ServerSyncTimeProvider();

  private boolean registered;

  private final MobileGatewayChannelInformationImpl gsmChannelInfo;
  private final Map<CellKey, CellInfoStatistic> cellStatistic = new ConcurrentHashMap<>();
  private SimChannelInformationImpl simChannelInfo;

  private long callsDuration;
  private int successfulCallsCount;
  private int totalCallsCount;

  private int successOutgoingSmsCount;
  private int totalOutgoingSmsCount;

  private CallState callState = new CallState(CallState.State.IDLE);
  private CallChannelState channelState;

  private String stateAdvInfo;
  private CellKey lastCellKey;
  private CellInfoStatistic lastServingCellStatistic;

  public VoiceServerChannelsInfo(final MobileGatewayChannelInformationImpl gsmChannelInfo) {
    this.gsmChannelInfo = gsmChannelInfo;
  }

  @Override
  public String getStateAdvInfo() {
    return stateAdvInfo;
  }

  @Override
  public MobileGatewayChannelInformation getMobileGatewayChannelInfo() {
    return gsmChannelInfo;
  }

  @Override
  public Map<CellKey, CellInfoStatistic> getCellStatistic() {
    return cellStatistic;
  }

  @Override
  public SimChannelInformation getSimChannelInfo() {
    return simChannelInfo;
  }

  @Override
  public long getCallsDuration() {
    return callsDuration;
  }

  @Override
  public int getSuccessfulCallsCount() {
    return successfulCallsCount;
  }

  @Override
  public int getTotalCallsCount() {
    return totalCallsCount;
  }

  @Override
  public int getSuccessOutgoingSmsCount() {
    return successOutgoingSmsCount;
  }

  @Override
  public int getTotalOutgoingSmsCount() {
    return totalOutgoingSmsCount;
  }

  @Override
  public CallState getCallState() {
    return callState;
  }

  @Override
  public CallChannelState getChannelState() {
    return channelState;
  }

  @Override
  public boolean isRegistered() {
    return registered;
  }

  public void setRegistered(final boolean registered) {
    this.registered = registered;
  }

  public void setChannelState(final CallChannelState channelState) {
    this.channelState = channelState;
  }

  public void addCallsDuration(final long duration) {
    callsDuration += duration;
    gsmChannelInfo.addCallsDuration(duration);
    if (lastServingCellStatistic != null) {
      lastServingCellStatistic.incCallsDuration(duration);
    }
  }

  public void incSuccessfulCallsCount() {
    successfulCallsCount++;
    gsmChannelInfo.incSuccessfulCallsCount();
    if (lastServingCellStatistic != null) {
      lastServingCellStatistic.incSuccessfulCallsCount();
    }
  }

  public void incTotalCallsCount() {
    totalCallsCount++;
    gsmChannelInfo.incTotalCallsCount();
    if (lastServingCellStatistic != null) {
      lastServingCellStatistic.incTotalCallsCount();
    }
  }

  public void incSuccessOutgoingSmsCount(final int parts) {
    successOutgoingSmsCount += parts;
    gsmChannelInfo.incSuccessOutgoingSmsCount(parts);
    if (lastServingCellStatistic != null) {
      lastServingCellStatistic.incSuccessOutgoingSmsCount(parts);
    }
  }

  public void incTotalOutgoingSmsCount(final int parts) {
    totalOutgoingSmsCount += parts;
    gsmChannelInfo.incTotalOutgoingSmsCount(parts);
    if (lastServingCellStatistic != null) {
      lastServingCellStatistic.incTotalOutgoingSmsCount(parts);
    }
  }

  public void incIncomingTotalSmsCount(final int parts) {
    gsmChannelInfo.incIncomingTotalSmsCount(parts);
    if (lastServingCellStatistic != null) {
      lastServingCellStatistic.incIncomingSmsCount(parts);
    }
  }

  public void incUssdCount() {
    if (lastServingCellStatistic != null) {
      lastServingCellStatistic.incUssdCount();
    }
  }

  public void incPdd(final long pdd) {
    if (lastServingCellStatistic != null) {
      lastServingCellStatistic.incAmountPdd(pdd);
    }
  }

  public void setGSMNetworkInfoChange(final GSMNetworkInfo gsmNetworkInfo) {
    gsmChannelInfo.setGsmNetworkInfo(gsmNetworkInfo);
    CellInfo servingCellInfo = gsmNetworkInfo.getCellInfo()[0];
    CellKey cellKey = new CellKey(servingCellInfo.getLac(), servingCellInfo.getCellId(), servingCellInfo.getBsic(), servingCellInfo.getArfcn());
    if (!cellKey.equals(lastCellKey)) {
      if (lastCellKey != null) {
        cellStatistic.put(lastCellKey, lastServingCellStatistic);
      }
      lastCellKey = cellKey;
      lastServingCellStatistic = cellStatistic.get(cellKey);
      if (lastServingCellStatistic == null) {
        lastServingCellStatistic = new CellInfoStatistic();
        cellStatistic.put(lastCellKey, lastServingCellStatistic);
      }
    }
  }

  @Override
  public long getServerTimeMillis() {
    return timeProvider.currentTimeMillis();
  }

  @Override
  public int compareTo(final IVoiceServerChannelsInfo that) {
    int retval = compare(gsmChannelInfo.getGSMChannelUID(), that.getMobileGatewayChannelInfo().getGSMChannelUID());
    if (retval != 0) {
      return retval;
    }
    return compare(getSimChannelInfo().getSimChannelUID(), that.getSimChannelInfo().getSimChannelUID());
  }

  private int compare(final ChannelUID chan1, final ChannelUID chan2) {
    if (chan1 == null) {
      return (chan2 == null) ? 0 : 1;
    } else if (chan2 == null) {
      return -1;
    }
    return chan1.compareTo(chan2);
  }

  public void setSimChannelInfo(final SimChannelInformationImpl simChannelInfo) {
    this.simChannelInfo = simChannelInfo;
  }

  @Override
  public String report() {
    getMobileGatewayChannelInfo().isLive();
    ChannelUID gsm = getMobileGatewayChannelInfo().getGSMChannelUID();
    ChannelUID cem = getSimChannelInfo() == null ? null : getSimChannelInfo().getSimChannelUID();
    CallChannelState.State ccState = getChannelState() == null ? null : getChannelState().getState();
    String ccStateTime = getChannelState() == null ? null : (String) getChannelState().writeTime(getTimeWriter(), System.currentTimeMillis());
    CallState.State callState = getCallState() == null ? null : getCallState().getState();
    String callStateTime = getCallState() == null || !getMobileGatewayChannelInfo().isLive() ? "Unlive(configured but not plugged)" : (String) getCallState().writeTime(getTimeWriter());

    return String.format("%-13s %-11s %-25s %-9s %-8s %s%n", gsm, cem, ccState, ccStateTime, callState, callStateTime);
  }

  @Override
  public void report(final PrintStream out) {
    out.append(report());
  }

  private TimePeriodWriter getTimeWriter() {
    return new TimePeriodWriter() {

      @Override
      public Object writePassedTime(final long time) {
        return TimeUtils.writeTime(time);
      }

      @Override
      public Object writeLeftTime(final long time) {
        return "-" + TimeUtils.writeTime(time);
      }
    };
  }

  public void reset() {
    registered = false;
    callsDuration = 0;
    successfulCallsCount = 0;
    totalCallsCount = 0;
    successOutgoingSmsCount = 0;
    totalOutgoingSmsCount = 0;
    callState = new CallState(CallState.State.IDLE);
    channelState = null;
  }

  public void setChannelStateAdvInfo(final String advInfo) {
    stateAdvInfo = advInfo;
  }

}
