/*
 * Copyright (C) 2017 Flames Group SIA
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.flamesgroup.antrax.activity;

import com.flamesgroup.antrax.control.communication.IVoiceServerStatus;
import com.flamesgroup.antrax.distributor.Server;
import com.flamesgroup.antrax.storage.commons.impl.CellEqualizerAlgorithm;
import com.flamesgroup.antrax.storage.commons.impl.GsmView;

import java.rmi.Remote;
import java.rmi.RemoteException;
import java.util.Date;
import java.util.List;

public interface IGsmViewManager extends Remote {

  List<GsmView> listGsmView(String voiceServerName) throws RemoteException;

  void updateGsmView(final String serverName, List<GsmView> gsmViews) throws RemoteException;

  void updateRuntimeGsmView(final String serverName, List<GsmView> gsmViews) throws RemoteException;

  void clearGsmView(String server) throws RemoteException;

  Date getStartNetworkSurvey(String voiceServerName) throws RemoteException;

  Boolean isUpdateCellAdjustments(String server) throws RemoteException;

  void setServerForUpdateCellAdjustments(String server, Boolean update) throws RemoteException;

  Boolean isUpdateImsiCatcher(String server) throws RemoteException;

  void setServerForUpdateImsiCatcher(String server, Boolean update) throws RemoteException;

  CellEqualizerAlgorithm getCellEqualizerAlgorithm(String server) throws RemoteException;

  void ping(Server server, IVoiceServerStatus voiceServerStatus) throws RemoteException;

  void resetGsmViewStatistic(List<GsmView> gsmViews, String server) throws RemoteException;

}
