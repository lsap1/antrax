/*
 * Copyright (C) 2017 Flames Group SIA
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.flamesgroup.antrax.control.authorization;

import java.io.ByteArrayOutputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.Serializable;
import java.math.BigInteger;

public class ClientUID implements Serializable {

  private static final long serialVersionUID = -4591156918204158337L;

  private final BigInteger uid;

  public ClientUID() {
    double random = Math.random();
    long timestamp = System.currentTimeMillis();
    ByteArrayOutputStream byteOut = new ByteArrayOutputStream();
    DataOutputStream dataOut = new DataOutputStream(byteOut);
    try {
      dataOut.writeLong(timestamp);
      dataOut.writeDouble(random);
    } catch (IOException ignored) {
    }
    uid = new BigInteger(byteOut.toByteArray());
  }

  @Override
  public boolean equals(final Object obj) {
    if (!(obj instanceof ClientUID)) {
      return false;
    }
    ClientUID that = (ClientUID) obj;
    return this.uid.equals(that.uid);
  }

  @Override
  public int hashCode() {
    return uid.hashCode();
  }

  @Override
  public String toString() {
    return uid.toString(16);
  }

  public static void main(final String[] args) {
    System.out.println(new ClientUID());
  }

}
