/*
 * Copyright (C) 2017 Flames Group SIA
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.flamesgroup.antrax.control.simserver;

import com.flamesgroup.antrax.activity.IRemoteVSConfigurator;
import com.flamesgroup.antrax.activity.ISimHistoryLogger;
import com.flamesgroup.antrax.automation.listeners.ISimCallHistory;
import com.flamesgroup.antrax.automation.utils.registry.RegistryAccessAdapter;
import com.flamesgroup.antrax.control.commons.ScriptStateSaver;
import com.flamesgroup.antrax.control.server.utils.DAOProvider;
import com.flamesgroup.antrax.control.simserver.core.Configuration;
import com.flamesgroup.antrax.control.simserver.core.SimUnitConfigurator;
import com.flamesgroup.antrax.control.simserver.core.SimUnitPool;
import com.flamesgroup.antrax.control.simserver.core.SimUnitSessionPool;
import com.flamesgroup.antrax.control.simserver.dao.ConfigurationDAO;
import com.flamesgroup.antrax.control.simserver.dao.ConfigurationDAOImpl;
import com.flamesgroup.antrax.control.simserver.engine.ConfigurationEngine;
import com.flamesgroup.antrax.control.simserver.engine.ExpiredSessionsCleanupEngine;
import com.flamesgroup.antrax.control.simserver.engine.SimCardsStatusReportingEngine;
import com.flamesgroup.antrax.storage.dao.IConfigViewDAO;
import com.flamesgroup.antrax.storage.dao.ISimpleConfigEditDAO;

public class UnitManagerBuilder {

  private final SimUnitManager simUnitManager = new SimUnitManager();
  private final GsmUnitManager gsmUnitManager = new GsmUnitManager();

  private final DAOProvider daoProvider;
  private final ISimActivityLogger simActivityLogger;
  private final ISimHistoryLogger simHistoryLogger;
  private final IRemoteVSConfigurator remoteVSConfigurator;
  private final DeclaredChannelPool declaredChannelPool;
  private final ISimCallHistory simCallHistory;

  private SimUnitPool simUnitPool;
  private Configuration configuration;
  private ConfigurationEngine configurationEngine;
  private SimUnitConfigurator configurator;
  private ExpiredSessionsCleanupEngine sessionCleanupEngine;
  private ConfigurationDAO configurationDAO;
  private SimUnitSessionPool simUnitSessionsPool;
  private ISimpleConfigEditDAO simpleConfigEditDAO;
  private IConfigViewDAO configViewDAO;
  private SimCardsStatusReportingEngine simStatusReportingEngine;
  private ScriptStateSaver scriptStateSaver;

  public UnitManagerBuilder(final DAOProvider daoProvider, final ISimActivityLogger simActivityLogger, final ISimHistoryLogger simHistoryLogger,
      final IRemoteVSConfigurator remoteVSConfigurator, final DeclaredChannelPool declaredChannelPool, final ISimCallHistory simCallHistory) {
    this.daoProvider = daoProvider;
    this.simActivityLogger = simActivityLogger;
    this.simHistoryLogger = simHistoryLogger;
    this.remoteVSConfigurator = remoteVSConfigurator;
    this.declaredChannelPool = declaredChannelPool;
    this.simCallHistory = simCallHistory;
  }

  public SimUnitManager buildSimUnitManager() {
    simUnitManager.setUnitSessionPool(getSimUnitSessionsPool());
    simUnitManager.setSimUnitConfigurator(getSimUnitConfigurator());
    simUnitManager.setSimUnitPool(getSimUnitPool());
    simUnitManager.setConfiguration(getConfiguration());
    simUnitManager.setConfigurationEngine(getConfigurationEngine());
    simUnitManager.setSessionCleanupEngine(getSessionCleanupEngine());
    simUnitManager.setSimStatusReportingEngine(getSimStatusReportingEngine());
    simUnitManager.setScriptStateSaver(getScriptStateSaver());
    simUnitManager.setDeclaredChannelPool(declaredChannelPool);
    return simUnitManager;
  }

  public GsmUnitManager buildGsmUnitManager() {
    gsmUnitManager.setConfigViewDAO(daoProvider.getConfigViewDAO());
    gsmUnitManager.setConfigEditDAO(daoProvider.getSimpleConfigEditDAO());
    gsmUnitManager.setSimUnitSessionPool(getSimUnitSessionsPool());
    gsmUnitManager.setDeclaredChannelPool(declaredChannelPool);
    gsmUnitManager.setCellMonitorDAO(daoProvider.getCellMonitorDAO());
    gsmUnitManager.setNetworkSurveyDAO(daoProvider.getNetworkSurveyDAO());
    return gsmUnitManager;
  }

  private SimCardsStatusReportingEngine getSimStatusReportingEngine() {
    if (simStatusReportingEngine == null) {
      simStatusReportingEngine = new SimCardsStatusReportingEngine(getSimUnitPool());
    }
    return simStatusReportingEngine;
  }

  private ExpiredSessionsCleanupEngine getSessionCleanupEngine() {
    if (sessionCleanupEngine == null) {
      sessionCleanupEngine = new ExpiredSessionsCleanupEngine(getSimUnitSessionsPool());
    }
    return sessionCleanupEngine;
  }

  private SimUnitSessionPool getSimUnitSessionsPool() {
    if (simUnitSessionsPool == null) {
      simUnitSessionsPool = new SimUnitSessionPool();
    }
    return simUnitSessionsPool;
  }

  private ConfigurationEngine getConfigurationEngine() {
    if (configurationEngine == null) {
      configurationEngine = new ConfigurationEngine(getConfiguration(), getConfigurationDAO(), getSimUnitPool(), new RegistryAccessAdapter(daoProvider.getRegistryDAO()), simCallHistory);
    }
    return configurationEngine;
  }

  private SimUnitConfigurator getSimUnitConfigurator() {
    if (configurator == null) {
      configurator = new SimUnitConfigurator(simActivityLogger, getSimpleConfigEditDAO(), getConfigViewDAO(), simHistoryLogger, getScriptStateSaver());
    }
    return configurator;
  }

  private ConfigurationDAO getConfigurationDAO() {
    if (configurationDAO == null) {
      configurationDAO = new ConfigurationDAOImpl(daoProvider);
    }
    return configurationDAO;
  }

  private ScriptStateSaver getScriptStateSaver() {
    if (scriptStateSaver == null) {
      scriptStateSaver = new ScriptStateSaver(remoteVSConfigurator);
    }
    return scriptStateSaver;
  }

  private ISimpleConfigEditDAO getSimpleConfigEditDAO() {
    if (simpleConfigEditDAO == null) {
      simpleConfigEditDAO = daoProvider.getSimpleConfigEditDAO();
    }
    return simpleConfigEditDAO;
  }

  private IConfigViewDAO getConfigViewDAO() {
    if (configViewDAO == null) {
      configViewDAO = daoProvider.getConfigViewDAO();
    }
    return configViewDAO;
  }

  private SimUnitPool getSimUnitPool() {
    if (simUnitPool == null) {
      simUnitPool = new SimUnitPool(getConfiguration());
    }
    return simUnitPool;
  }

  private Configuration getConfiguration() {
    if (configuration == null) {
      configuration = new Configuration();
    }
    return configuration;
  }

}
