/*
 * Copyright (C) 2017 Flames Group SIA
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.flamesgroup.antrax.control.server.routing;

public final class TrunkOptions {

  private final String sendIntervalMillis;
  private final String maxFrameLength;

  public TrunkOptions(final String sendIntervalMillis, final String maxFrameLength) {
    this.sendIntervalMillis = sendIntervalMillis;
    this.maxFrameLength = maxFrameLength;
  }

  public String getSendIntervalMillis() {
    return sendIntervalMillis;
  }

  public String getMaxFrameLength() {
    return maxFrameLength;
  }

}
