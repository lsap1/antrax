/*
 * Copyright (C) 2017 Flames Group SIA
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.flamesgroup.antrax.control.properties;

import com.flamesgroup.properties.PropUtils;
import com.flamesgroup.properties.ServerProperties;

import java.util.HashMap;
import java.util.Map;

public class ControlPropUtils extends PropUtils {

  private static final String CONTROL_SERVER_PROPERTY = "control-server.properties";
  private static final String CALL_ROUTE_ERRORS_PROPERTY = "call.route.errors.properties";

  private static volatile ControlPropUtils instance;

  private ControlPropUtils() {
    super();
  }

  public ControlServerProperties getControlServerProperties() {
    return (ControlServerProperties) properties.get(CONTROL_SERVER_PROPERTY);
  }

  public CallRouteErrorsProperties getCallRouteErrorsProperties() {
    return (CallRouteErrorsProperties) properties.get(CALL_ROUTE_ERRORS_PROPERTY);
  }

  @Override
  protected Map<String, ServerProperties> getProperties() {
    Map<String, ServerProperties> localProperties = new HashMap<>();
    localProperties.put(CONTROL_SERVER_PROPERTY, new ControlServerProperties());
    localProperties.put(CALL_ROUTE_ERRORS_PROPERTY, new CallRouteErrorsProperties());
    localProperties.putAll(super.getProperties());
    return localProperties;
  }

  public static ControlPropUtils getInstance() {
    if (instance == null) {
      synchronized (ControlPropUtils.class) {
        if (instance == null) {
          instance = new ControlPropUtils();
        }
      }
    }
    return instance;
  }

  @Override
  public String getName() {
    return CONTROL_SERVER_PROPERTY + "|" + CALL_ROUTE_ERRORS_PROPERTY;
  }

}
