/*
 * Copyright (C) 2017 Flames Group SIA
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.flamesgroup.antrax.control.simserver.core;

import com.flamesgroup.antrax.control.simserver.NoSuchSimError;
import com.flamesgroup.antrax.storage.commons.GSMGroup;
import com.flamesgroup.antrax.storage.commons.IServerData;
import com.flamesgroup.antrax.storage.commons.SIMGroup;
import com.flamesgroup.commons.ChannelUID;
import com.flamesgroup.unit.ICCID;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Set;

public class SimUnitPool {

  private static final Logger logger = LoggerFactory.getLogger(SimUnitPool.class);

  private enum DenyReason {
    NO_PIN,
    TAKEN,
    NOT_READY_START_SESSION,
    GSM_TO_SIM_GROUP_DENY,
    BAD_GATEWAY,
    NOT_PLUGGED,
    NO_READY_IMEI_GENERATOR,
    REACHED_REGISTRATION_LIMIT
  }

  private final Configuration configuration;
  private final List<SimUnit> units = new ArrayList<>();
  private final SimUnitComparator simUnitComparator = new SimUnitComparator();
  private final Map<String, RegistrationPair> registrationParameters = new HashMap<>();

  public SimUnitPool(final Configuration configuration) {
    this.configuration = configuration;
  }

  public synchronized SimUnit take(final GSMGroup gsmGroup, final IServerData gateway) {
    Set<DenyReason> denyReason = new HashSet<>();
    Collections.sort(units, simUnitComparator);
    for (SimUnit simUnit : units) {
      if (simUnit.isPinEnabled()) {
        denyReason.add(DenyReason.NO_PIN);
        continue;
      }
      if (simUnit.isTaken()) {
        denyReason.add(DenyReason.TAKEN);
        continue;
      }
      if (!simUnit.isReadyToStartSession()) {
        denyReason.add(DenyReason.NOT_READY_START_SESSION);
        continue;
      }
      if (!simUnit.isReadyIMEIGenerator()) {
        denyReason.add(DenyReason.NO_READY_IMEI_GENERATOR);
        continue;
      }
      if (!configuration.applies(gsmGroup, simUnit.getSIMGroup())) {
        denyReason.add(DenyReason.GSM_TO_SIM_GROUP_DENY);
        continue;
      }
      if (!simUnit.appliesGateway(gateway)) {
        denyReason.add(DenyReason.BAD_GATEWAY);
        continue;
      }
      if (!simUnit.isPlugged()) {
        denyReason.add(DenyReason.NOT_PLUGGED);
        continue;
      }

      SIMGroup simGroup = simUnit.getSIMGroup();
      long registrationPeriod = simGroup.getRegistrationPeriod() * 60 * 1000;
      if (registrationPeriod > 0) {
        String simGroupName = simGroup.getName();
        if (!registrationParameters.containsKey(simGroupName)) {
          registrationParameters.put(simGroupName, new RegistrationPair());
        }

        long currentTime = System.currentTimeMillis();
        RegistrationPair registrationPair = registrationParameters.get(simGroupName);
        if (currentTime - registrationPair.getPeriod() > registrationPeriod) {
          registrationPair.setPeriod(currentTime).setCount(0);
        }

        int registrationCount = registrationPair.getCount();
        if (registrationCount >= simGroup.getRegistrationCount()) {
          denyReason.add(DenyReason.REACHED_REGISTRATION_LIMIT);
          continue;
        }

        registrationPair.setCount(registrationCount + 1);
      }

      simUnit.take(gsmGroup, gateway);
      return simUnit;
    }
    logger.trace("[{}] - can't found any suitable sim card. Found only {}", this, denyReason);
    return null;
  }

  public synchronized List<SimUnit> listUnits() {
    return new ArrayList<>(units);
  }

  public SimUnit getSimUnit(final ChannelUID channel) {
    for (SimUnit u : listUnits()) {
      if (u.getChannelUID().equals(channel)) {
        return u;
      }
    }
    return null;
  }

  public SimUnit getSimUnit(final ICCID uid) throws NoSuchSimError {
    for (SimUnit u : listUnits()) {
      if (uid.equals(u.getUID())) {
        return u;
      }
    }
    throw new NoSuchSimError(uid);
  }

  public SimUnit[] listSimUnits(final ICCID[] uids) {
    List<SimUnit> retval = new LinkedList<>();
    List<ICCID> uidsList = Arrays.asList(uids);
    for (SimUnit u : listUnits()) {
      if (uidsList.contains(u.getUID())) {
        retval.add(u);
      }
    }
    return retval.toArray(new SimUnit[retval.size()]);
  }

  public synchronized void handleSimUnitReturned(final SimUnit simUnit) {
    simUnit.untake();
  }

  public synchronized void addSimUnit(final SimUnit simUnit) {
    if (units.removeIf(u -> u.getChannelUID().equals(simUnit.getChannelUID()))) {
      logger.debug("[{}] - already contained [{}], so replace old to new", this, simUnit);
    }

    units.add(simUnit);
    logger.debug("[{}] - sim unit added [{}]", this, simUnit);
  }

  public synchronized void removeSimUnit(final SimUnit simUnit) {
    if (units.removeIf(u -> u.getChannelUID().equals(simUnit.getChannelUID()))) {
      simUnit.unplug();
      logger.debug("[{}] - sim unit removed [{}]", this, simUnit);
    }
  }

  private class RegistrationPair {

    private long period;
    private int count;

    public long getPeriod() {
      return period;
    }

    public RegistrationPair setPeriod(final long period) {
      this.period = period;
      return this;
    }

    public int getCount() {
      return count;
    }

    public RegistrationPair setCount(final int count) {
      this.count = count;
      return this;
    }

  }

  private class SimUnitComparator implements Comparator<SimUnit> {

    @Override
    public int compare(final SimUnit o1, final SimUnit o2) {
      return Long.compare(o1.countFactor(), o2.countFactor());
    }

  }

}
