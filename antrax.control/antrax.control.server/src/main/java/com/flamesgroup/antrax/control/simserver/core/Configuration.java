/*
 * Copyright (C) 2017 Flames Group SIA
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.flamesgroup.antrax.control.simserver.core;

import com.flamesgroup.antrax.plugins.core.AntraxPluginsStore;
import com.flamesgroup.antrax.storage.commons.GSMGroup;
import com.flamesgroup.antrax.storage.commons.IServerData;
import com.flamesgroup.antrax.storage.commons.SIMGroup;

import java.util.HashMap;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

public class Configuration {

  private final AntraxPluginsStore pluginsStore;

  private long groupLinksRevision;
  private Map<GSMGroup, Set<SIMGroup>> groupLinks = new HashMap<>();

  private long gatewaysRevision;
  private Map<String, IServerData> gateways;

  private final Lock groupLinksLock = new ReentrantLock();
  private final Lock gatewaysLock = new ReentrantLock();

  public Configuration() {
    this.pluginsStore = new AntraxPluginsStore();
  }

  public AntraxPluginsStore getPluginsStore() {
    return pluginsStore;
  }

  public boolean applies(final GSMGroup gsmGroup, final SIMGroup simGroup) {
    groupLinksLock.lock();
    try {
      Set<SIMGroup> simLinks = groupLinks.get(gsmGroup);
      return simLinks != null && simLinks.contains(simGroup);
    } finally {
      groupLinksLock.unlock();
    }
  }

  public void setLinks(final Map<GSMGroup, Set<SIMGroup>> groupLinks, final long groupLinksRevision) {
    groupLinksLock.lock();
    try {
      this.groupLinks = groupLinks;
      this.groupLinksRevision = groupLinksRevision;
    } finally {
      groupLinksLock.unlock();
    }
  }

  public long getGroupLinksRevision() {
    groupLinksLock.lock();
    try {
      return groupLinksRevision;
    } finally {
      groupLinksLock.unlock();
    }
  }

  public IServerData getGateway(final String serverName) {
    gatewaysLock.lock();
    try {
      if (gateways == null) {
        return null;
      } else {
        return gateways.get(serverName);
      }
    } finally {
      gatewaysLock.unlock();
    }
  }

  public void setGateways(final Map<String, IServerData> gateways, final long gatewaysRevision) {
    gatewaysLock.lock();
    try {
      this.gateways = gateways;
      this.gatewaysRevision = gatewaysRevision;
    } finally {
      gatewaysLock.unlock();
    }
  }

  public long getGatewaysRevision() {
    gatewaysLock.lock();
    try {
      return gatewaysRevision;
    } finally {
      gatewaysLock.unlock();
    }
  }

}
