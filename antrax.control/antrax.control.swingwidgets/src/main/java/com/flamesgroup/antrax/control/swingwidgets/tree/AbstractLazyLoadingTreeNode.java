package com.flamesgroup.antrax.control.swingwidgets.tree;

import javax.swing.*;
import javax.swing.tree.DefaultMutableTreeNode;
import javax.swing.tree.DefaultTreeModel;
import javax.swing.tree.MutableTreeNode;
import javax.swing.tree.TreeModel;

public abstract class AbstractLazyLoadingTreeNode extends DefaultMutableTreeNode
    implements LazyLoadingTreeNode {

  private static final long serialVersionUID = -521416621843549275L;

  private final JTree tree;

  public AbstractLazyLoadingTreeNode(final Object userObject, final JTree tree) {
    super(userObject);
    this.tree = tree;
    setAllowsChildren(true);
  }

  protected JTree getTree() {
    return tree;
  }

  public DefaultTreeModel getTreeModel() {
    TreeModel model = tree.getModel();
    if (model instanceof DefaultTreeModel) {
      return (DefaultTreeModel) model;
    } else {
      throw new IllegalStateException("AbstractLazyLoadingTreeNode works only with DefaultTreeModel's");
    }
  }

  @Override
  public boolean isChildrenLoaded() {
    return getAllowsChildren() && getChildCount() > 0;
  }

  @Override
  public void setChildren(final MutableTreeNode... nodes) {
    int childCount = getChildCount();
    if (childCount > 0) {
      for (int i = 0; i < childCount; i++) {
        getTreeModel().removeNodeFromParent((MutableTreeNode) getChildAt(0));
      }
    }
    for (int i = 0; nodes != null && i < nodes.length; i++) {
      getTreeModel().insertNodeInto(nodes[i], this, i);
    }
    getTreeModel().reload(this);
  }

  @Override
  public boolean isLeaf() {
    return !getAllowsChildren();
  }

  @Override
  public abstract MutableTreeNode[] loadChildren(TreeModel model);

}
