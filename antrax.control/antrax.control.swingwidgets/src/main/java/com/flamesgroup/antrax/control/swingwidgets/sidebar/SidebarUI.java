package com.flamesgroup.antrax.control.swingwidgets.sidebar;

import com.flamesgroup.antrax.control.swingwidgets.IconPool;

import java.awt.*;

import javax.swing.*;
import javax.swing.plaf.basic.BasicTreeUI;
import javax.swing.tree.AbstractLayoutCache;
import javax.swing.tree.TreePath;

public class SidebarUI extends BasicTreeUI {

  public final static String CELL_MARGIN_LEFT = "Sidebar.cellMarginLeft";
  public final static String CELL_MARGIN_RIGHT = "Sidebar.cellMarginRight";
  public final static String NODE_MARGIN_Y = "Sidebar.badgeMraginY";
  public final static String BADGE_SPACE = "Sidebar.badgeSpace";
  public final static String TEXT_INDENT = "Sidebar.textIndent";
  public final static String ICON_INDENT = "Sidebar.iconIndent";
  public final static String TEXT_COLOR = "Sidebar.textColor";
  public final static String TEXT_SELECTED_COLOR = "Sidebar.textSelectedColor";
  public final static String TEXT_SHADOW_COLOR = "Sidebar.textShadowColor";

  private static final Icon expandedIcon = IconPool.getShared("arrow-down.png");
  private static final Icon collapsedIcon = IconPool.getShared("arrow-right.png");

  @Override
  protected AbstractLayoutCache.NodeDimensions createNodeDimensions() {

    return new NodeDimensionsHandler() {
      @Override
      public Rectangle getNodeDimensions(final Object value, final int row,
          final int depth, final boolean expanded, final Rectangle size) {

        Rectangle dimensions = super.getNodeDimensions(value, row, depth, expanded, size);
        Container ct = tree.getParent();
        if (ct instanceof JViewport) {
          int viewPortWidth = ((JViewport) ct).getWidth();
          dimensions.width = (viewPortWidth <= 0) ? 0 : viewPortWidth - getRowX(row, depth);
        }
        return dimensions;
      }
    };
  }

  @Override
  protected void paintHorizontalLine(final Graphics g, final JComponent c, final int y, final int left, final int right) {
    // do nothing.
  }

  @Override
  protected void paintVerticalPartOfLeg(final Graphics g, final Rectangle clipBounds, final Insets insets, final TreePath path) {
    // do nothing.
  }

  @Override
  protected void installDefaults() {
    super.installDefaults();
    setExpandedIcon(expandedIcon);
    setCollapsedIcon(collapsedIcon);
    setRightChildIndent(5);
  }

}
