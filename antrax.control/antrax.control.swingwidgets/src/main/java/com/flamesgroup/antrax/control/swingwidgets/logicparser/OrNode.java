package com.flamesgroup.antrax.control.swingwidgets.logicparser;

public class OrNode extends OperatorNode {

  @Override
  public int getPrecedence() {
    return Precedence.LOGICAL;
  }

  @Override
  public boolean evaluate() {
    if (getLeft().evaluate()) { // short circuit
      return true;
    }
    return getRight().evaluate();
  }

  @Override
  public String toString() {
    return getLeft() + " " + getRight() + " " + LogicToken.OR.getLexeme();
  }

}
