package com.flamesgroup.antrax.control.swingwidgets.logicparser;

/**
 * A node in the expression parse tree.
 */
public interface Node {

  /**
   * @return true if the node evaluates to true.
   */
  boolean evaluate();

}
