package com.flamesgroup.antrax.control.swingwidgets.table;

public interface TableBuilder<T, K extends Comparable<K>> {

  /**
   * @param src source for key extraction
   * @return non null unique key
   */
  K getUniqueKey(T src);

  void buildRow(T src, ColumnWriter<T> dest);

  void buildColumns(UpdateTableColumnModel columns);

}
