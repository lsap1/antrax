package com.flamesgroup.antrax.control.swingwidgets.editor;

import java.awt.event.ActionListener;

import javax.swing.*;

public class JReflectiveButton extends JButton {

  private static final long serialVersionUID = -4204065476517677464L;

  private int padding;

  public static class JReflectiveButtonBuilder {
    private String text;
    private Icon icon;
    private ActionListener actionListener;
    private int padding = 10;
    private String toolTipText;
    private boolean enabled;

    public JReflectiveButtonBuilder() {
      setText("");
      setEnabled(true);
    }

    public JReflectiveButtonBuilder setText(final String text) {
      this.text = text;
      return this;
    }

    public JReflectiveButtonBuilder setIcon(final Icon icon) {
      this.icon = icon;
      return this;
    }

    public JReflectiveButtonBuilder setPadding(final int padding) {
      this.padding = padding;
      return this;
    }

    public JReflectiveButtonBuilder addActionListener(final ActionListener actionListener) {
      this.actionListener = actionListener;
      return this;
    }

    public JReflectiveButtonBuilder setToolTipText(final String toolTipText) {
      this.toolTipText = toolTipText;
      return this;
    }

    public JReflectiveButtonBuilder setEnabled(final boolean enabled) {
      this.enabled = enabled;
      return this;
    }

    public JReflectiveButton build() {
      return new JReflectiveButton(this);
    }
  }

  private JReflectiveButton(final JReflectiveButtonBuilder builder) {
    super();
    setOpaque(false);
    //    setFont(new Font("Helvetica", Font.PLAIN, 12));
    setPadding(builder.padding);
    setText(builder.text);
    setIcon(builder.icon);
    setToolTipText(builder.toolTipText);
    setEnabled(builder.enabled);
    if (builder.actionListener != null) {
      addActionListener(builder.actionListener);
    }

  }

  public void setPadding(final int padding) {
    this.padding = padding;
  }

  public int getPadding() {
    return padding;
  }

}
