package com.flamesgroup.antrax.control.swingwidgets.badge;

import com.flamesgroup.antrax.control.swingwidgets.SwingHelper;

import java.awt.*;
import java.awt.geom.Rectangle2D;

import javax.swing.*;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;

public class JBadge extends JLabel {

  private static final long serialVersionUID = -5962306601968190852L;

  private static final Color DEFAULT_COLOR = new Color(0x8596bd);
  private static final Color DEFAULT_TEXT_COLOR = Color.WHITE;

  private BadgeModel model = new DefaultBadgeModel();
  private ChangeListener changeListener;

  private Color textColor;
  private Color selectedColor;

  public static class JBadgeBuilder {
    private Icon icon;
    private String text = "";
    private String toolTipText = "";
    private Color color = DEFAULT_COLOR;
    private Color textColor = DEFAULT_TEXT_COLOR;
    private boolean visible = true;
    private BadgeModel model = new DefaultBadgeModel();

    public JBadgeBuilder() {
    }

    public JBadgeBuilder setText(final String text) {
      this.text = text;
      return this;
    }

    public JBadgeBuilder setIcon(final Icon icon) {
      this.icon = icon;
      return this;
    }

    public JBadgeBuilder setToolTipText(final String toolTipText) {
      this.toolTipText = toolTipText;
      return this;
    }

    public JBadgeBuilder setColor(final Color color) {
      this.color = color;
      return this;
    }

    public JBadgeBuilder setTextColor(final Color textColor) {
      this.textColor = textColor;
      return this;
    }

    public JBadgeBuilder setVisible(final boolean visible) {
      this.visible = visible;
      return this;
    }

    public JBadgeBuilder setModel(final BadgeModel model) {
      this.model = model;
      return this;
    }

    public JBadge build() {
      return new JBadge(this);
    }

  }

  private JBadge(final JBadgeBuilder builder) {
    super();

    setModel(builder.model);
    setVisible(builder.visible);
    setText(builder.text);
    setColor(builder.color);
    setTextColor(builder.textColor);
    setToolTipText(builder.toolTipText);
    if (builder.icon != null) {
      setIcon(builder.icon);
    }
  }

  public Color getColor() {
    return model.getColor();
  }

  public void setColor(Color color) {
    if (color == null) {
      color = DEFAULT_COLOR;
    }
    model.setColor(color);
  }

  public Color getSelectedColor() {
    return selectedColor;
  }

  public Color getTextColor() {
    return textColor;
  }

  public void setTextColor(Color textColor) {
    Color oldValue = getTextColor();
    if (textColor == null) {
      textColor = DEFAULT_TEXT_COLOR;
    }
    this.textColor = textColor;
    firePropertyChange("badgeTextColor", oldValue, textColor);
  }

  @Override
  public String getText() {
    if (model == null) {
      return super.getText();
    }
    return model.getText();
  }

  @Override
  public void setIcon(final Icon icon) {
    if (model == null) {
      super.setIcon(icon);
    } else {
      model.setIcon(icon);
    }
  }

  @Override
  public Icon getIcon() {
    if (model == null) {
      return super.getIcon();
    } else {
      return model.getIcon();
    }
  }

  @Override
  public void setText(final String text) {
    if (model == null) {
      super.setText(text);
    } else {
      model.setText(text);
    }
  }

  @Override
  public boolean isVisible() {
    if (model == null) {
      return super.isVisible();
    } else {
      return model.isVisible();
    }
  }

  @Override
  public void setVisible(final boolean visible) {
    if (model == null) {
      super.setVisible(visible);
    } else {
      model.setVisible(visible);
    }
  }

  public BadgeModel getModel() {
    return model;
  }

  public void setModel(final BadgeModel newModel) {
    BadgeModel oldModel = getModel();

    if (oldModel != null) {
      oldModel.removeChangeListener(changeListener);
      changeListener = null;
    }

    model = newModel;

    if (newModel != null) {
      changeListener = createChangeListener();
      newModel.addChangeListener(changeListener);
    }

    if (newModel != oldModel) {
      revalidate();
      repaint();
    }
  }

  private void onStatusChanged(final ChangeEvent e) {
    super.setVisible(model.isVisible());
    super.setText(model.getText());
  }

  private ChangeListener createChangeListener() {
    return new ChangeListener() {
      @Override
      public void stateChanged(final ChangeEvent e) {
        onStatusChanged(e);
      }
    };
  }

  private void drawBadgeBox(final Graphics2D gc, final Rectangle bounds, final int w, final int h, final int arc) {

    int x = bounds.x + bounds.width - w;
    int y = bounds.y;
    //    Color[] colors = {Color.GRAY, Color.WHITE.brighter(), getColor()};
    //    int[] dt = {0, 1, 2};

    Color[] colors = {getColor()};
    int[] dt = {0};

    for (int i = 0; i < colors.length; i++) {
      int delta = dt[i];
      int r = arc - delta;
      gc.setColor(colors[i]);
      gc.fillRoundRect(x + delta, y + delta,
          w - delta * 2,
          h - delta * 2,
          r, r);
    }
  }

  @Override
  protected void paintComponent(final Graphics g) {
    Graphics2D gc = (Graphics2D) g.create();
    try {
      String badgeText = getText();
      if (badgeText == null || badgeText.equals("")) {
        badgeText = "";
      }

      Rectangle bounds = new Rectangle(0, 0, getWidth(), getHeight());
      gc.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);

      int badgeWidth;
      Icon icon = getIcon();
      if (icon != null) { // icon mode
        badgeWidth = icon.getIconWidth();
        int x = bounds.x + bounds.width - badgeWidth;
        int y = bounds.y;
        icon.paintIcon(this, gc, x, y);
      } else { // text mode
        // --------- Draw the badge background

        int h = bounds.height;
        // Get the bounds of the badge text in order to calculate the center
        Rectangle2D strBounds = gc.getFontMetrics().getStringBounds(badgeText, gc);

        int defaultBadgeWidth = h * 3 / 4;
        int strWidth = (badgeText.equals("") || (int) strBounds.getWidth() < defaultBadgeWidth)
            ? defaultBadgeWidth
            : (int) strBounds.getWidth();
        badgeWidth = (2 * strWidth + h) / 2;

        //gc.setColor(getColor());
        //gc.setComposite(AlphaComposite.SrcOver);

        drawBadgeBox(gc, bounds, badgeWidth, h, h);

        // --------- Draw badge text

        // Calculate the bottom left point to draw the text at
        int tx = bounds.x + bounds.width - badgeWidth / 2 - (int) strBounds.getWidth() / 2;
        int ty = (bounds.height - (int) strBounds.getY()) / 2;

        // Set the color to use for the text - note this color is always
        // the same, though it won't always show because of the composite
        // set below
        gc.setColor(getTextColor());
        // if the badge is selected, punch out the text so that the
        //    underlying color shows through as the font color
        // else use use a standard alpha composite to simply draw on top of
        //    whatever is currently there
        //gc.setComposite(isSelected() ? AlphaComposite.DstOut : AlphaComposite.SrcOver);

        // draw the badge text.
        SwingHelper.drawString(gc, badgeText, tx, ty);
      }

      setSize(badgeWidth, getHeight());
    } finally {
      if (gc != null) {
        gc.dispose();
      }
    }
  }
}
