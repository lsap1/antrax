package com.flamesgroup.antrax.control.swingwidgets.contextsearch;

public interface SearchFilter {

  boolean isApplied(String str);

}
