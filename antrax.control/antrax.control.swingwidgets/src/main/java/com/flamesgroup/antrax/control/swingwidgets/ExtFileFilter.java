package com.flamesgroup.antrax.control.swingwidgets;

import java.io.File;

import javax.swing.filechooser.FileFilter;

public class ExtFileFilter extends FileFilter {

  private final String ext;
  private final String description;

  public ExtFileFilter(final String ext, final String description) {
    this.ext = ext;
    this.description = description;
  }

  public String getExtension() {
    return ext;
  }

  public String getExtensionDescription() {
    return description;
  }

  @Override
  public boolean accept(final File file) {
    if (file != null) {
      if (file.isDirectory()) {
        return true;
      }
      String extension = getFileExtension(file);
      return (extension == null) ? ext.isEmpty() : ext.equals(extension);
    }
    return false;
  }

  @Override
  public String getDescription() {
    return description;
  }

  private String getFileExtension(final File file) {
    if (file != null) {
      String filename = file.getName();
      int i = filename.lastIndexOf('.');
      if (i > 0 && i < filename.length() - 1) {
        return filename.substring(i).toLowerCase();
      }
    }
    return null;
  }

}
