package com.flamesgroup.antrax.control.swingwidgets.field;

import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.FocusAdapter;
import java.awt.event.FocusEvent;
import java.awt.event.FocusListener;
import java.awt.event.KeyEvent;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.net.InetAddress;
import java.net.UnknownHostException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import javax.swing.*;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;
import javax.swing.event.DocumentEvent;
import javax.swing.event.DocumentListener;
import javax.swing.text.AttributeSet;
import javax.swing.text.BadLocationException;
import javax.swing.text.Document;
import javax.swing.text.PlainDocument;
import javax.swing.text.SimpleAttributeSet;

public class IPTextField extends JPanel {

  private static final long serialVersionUID = -8276408644251804303L;

  private static final int IP_V4_OCTETS_COUNT = 4;
  private static final int MIN_OCTET_VALUE = 0;
  private static final int MAX_OCTET_VALUE = 255;
  private static final int MAX_OCTET_CHARS_LENGTH = 3;

  public final String PROPERTY_SUBNET_MASK = "subnetMask";
  public final String PROPERTY_EDITABLE = "editable";
  public final String PROPERTY_ENABLED = "enabled";

  protected transient ChangeEvent changeEvent;
  private JTextField[] octetFields;
  private FocusListener octetFieldsFocusListener;
  private boolean editable;
  private int[] subnetMask;

  private final List<FocusListener> focusListeners = new ArrayList<>(1);

  public IPTextField() {
    editable = true;
    changeEvent = null;
    init();
  }

  @Override
  public synchronized void addFocusListener(final FocusListener l) {
    super.addFocusListener(l);
    focusListeners.add(l);
  }

  @Override
  public synchronized void removeFocusListener(final FocusListener l) {
    super.removeFocusListener(l);
    focusListeners.remove(l);
  }

  @Override
  public Dimension getPreferredSize() {
    Dimension size = super.getPreferredSize();

    if (octetFields.length > 0) {
      size.height = octetFields[0].getPreferredSize().height + 2 * 3;
    }
    return size;
  }

  @Override
  public Dimension getMaximumSize() {
    return getPreferredSize();
  }

  private void init() {
    setLayout(new BoxLayout(this, 0));
    setOpaque(true);
    setFocusable(true);

    subnetMask = new int[IP_V4_OCTETS_COUNT];
    Arrays.fill(subnetMask, 0);

    octetFieldsFocusListener = new OctetFieldsFocusAdapter();
    octetFields = new JTextField[IP_V4_OCTETS_COUNT];
    JLabel dotLabels[] = new JLabel[octetFields.length - 1];

    for (int l = 0; l < octetFields.length; l++) {
      octetFields[l] = createOctetField((l != 0) ? octetFields[l - 1] : null);
      add(octetFields[l]);
      if (l != octetFields.length - 1) {
        add(dotLabels[l] = createDotLabel());
      }
    }

    PropertyChangeListener propertyChangeListener = new PropertyChangeListener() {

      @Override
      public void propertyChange(final PropertyChangeEvent evt) {
        if (PROPERTY_ENABLED.equals(evt.getPropertyName())) {
          boolean enabled = Boolean.TRUE.equals(evt.getNewValue());
          for (JTextField octet : octetFields) {
            octet.setEnabled(enabled);
          }
          updateUI();
        } else if (PROPERTY_EDITABLE.equals(evt.getPropertyName())) {
          boolean octetsEditable = Boolean.TRUE.equals(evt.getNewValue());
          for (JTextField octet : octetFields) {
            octet.setEditable(octetsEditable);
          }
          updateUI();
        } else if (PROPERTY_SUBNET_MASK.equals(evt.getPropertyName())) {
          int[] mask = (int[]) evt.getNewValue();
          int[] octetValues = getValue();
          int length = Math.min(mask.length, octetFields.length);
          for (int i = 0; i < length; i++) {
            if (octetValues[i] < mask[i]) {
              octetFields[i].setText(String.valueOf(octetValues[i]));
            }
          }
        }
      }

    };
    addPropertyChangeListener(propertyChangeListener);

    updateUI();
  }

  public boolean isEditable() {
    return editable;
  }

  public void setInetAddress(final InetAddress addr) {
    setText(addr.getHostAddress());
  }

  public InetAddress getInetAddress() {
    try {
      return InetAddress.getByName(getText());
    } catch (UnknownHostException e) {
      throw new IllegalStateException(IPTextField.class.getSimpleName() + " invalid IP", e);
    }
  }

  public void setValue(final int[] ip) {
    setText(convertIPToString(ip));
  }

  public int[] getValue() {
    String[] rawText = getRawText();
    return convertStringToIP(rawText);
  }

  public void setText(final String strIP) {
    int ip[] = convertStringToIP(strIP);
    for (int i = 0; i < ip.length; i++) {
      int octet = ip[i];
      int mask = getSubnetMask()[i];
      if (octet < mask) {
        octet = mask;
      }
      octetFields[i].setText(String.valueOf(octet));
    }
  }

  public String getText() {
    return convertIPToString(getValue());
  }

  public String[] getRawText() {
    String octetText[] = new String[octetFields.length];
    int i = 0;
    for (JTextField octet : octetFields) {
      octetText[i++] = octet.getText().trim();
    }
    return octetText;
  }

  public void setEditable(final boolean editable) {
    if (editable != this.editable) {
      boolean oldVal = this.editable;
      this.editable = editable;
      firePropertyChange("editable", Boolean.valueOf(oldVal), Boolean.valueOf(this.editable));
      repaint();
    }
  }

  // ai <==> mask
  public void setSubnetMask(final int[] mask) {
    if (mask == null || mask.length != IP_V4_OCTETS_COUNT) {
      throw new IllegalArgumentException("The subnetMask must have " + IP_V4_OCTETS_COUNT + " octets.");
    }

    int firstZeroIndex = -1;
    for (int i = 0; i < mask.length; i++) {
      if (mask[i] < MIN_OCTET_VALUE || mask[i] > MAX_OCTET_VALUE) {
        throw new IllegalArgumentException("The subnetMask's "
            + Arrays.toString(mask)
            + " mask "
            + mask[i]
            + " is not a valid subnet mask.");
      }

      if (firstZeroIndex >= 0 && firstZeroIndex < i
          && mask[i] != MIN_OCTET_VALUE) {
        throw new IllegalArgumentException(
            "The subnetMask's "
                + Arrays.toString(mask)
                + " mask "
                + mask[i]
                + " is not allowed at this position. Only zero is allowed here in order to make a valid subnet mask.");
      }

      if (firstZeroIndex < 0 && mask[i] == MIN_OCTET_VALUE) {
        firstZeroIndex = i;
      }
    }

    int[] oldSubnetMask = subnetMask;
    subnetMask = mask;
    firePropertyChange(PROPERTY_SUBNET_MASK, oldSubnetMask, subnetMask);
  }

  public void clear() {
    for (JTextField octet : octetFields) {
      octet.setText("");
    }
  }

  public void setSubnetMask(final String s) {
    setSubnetMask(convertStringToIP(s));
  }

  public int[] getSubnetMask() {
    return subnetMask;
  }

  public void addChangeListener(final ChangeListener changelistener) {
    listenerList.add(ChangeListener.class, changelistener);
  }

  public void removeChangeListener(final ChangeListener changelistener) {
    listenerList.remove(ChangeListener.class, changelistener);
  }

  public ChangeListener[] getChangeListeners() {
    return (ChangeListener[]) listenerList.getListeners(ChangeListener.class);
  }

  public static String convertIPToString(final int[] ip) {
    if (ip == null) {
      throw new IllegalArgumentException("The value passed in is null");
    }

    if (ip.length < IP_V4_OCTETS_COUNT) {
      throw new IllegalArgumentException("The value passed in is lower than min octets count for IP v4");
    }

    final int len = IP_V4_OCTETS_COUNT;
    StringBuffer stringbuffer = new StringBuffer(len * MAX_OCTET_CHARS_LENGTH);
    for (int i = 0; i < len; i++) {
      stringbuffer.append(ip[i] & 0xff);
      if (i < len - 1) {
        stringbuffer.append('.');
      }
    }
    return stringbuffer.toString();
  }

  private static int[] convertStringToIP(final String[] strOctets) {
    if (strOctets.length < IP_V4_OCTETS_COUNT) {
      throw new IllegalArgumentException(
          "The value passed in is lower than min octets count for IP v4");
    }
    int[] intOctets = new int[strOctets.length];

    int i = 0;
    for (String strOctet : strOctets) {
      if (strOctet.isEmpty()) {
        intOctets[i] = MIN_OCTET_VALUE;
      } else {
        try {
          intOctets[i] = strOctetToInt(strOctet);
        } catch (NumberFormatException ignored) {
          intOctets[i] = MIN_OCTET_VALUE;
        }
      }
      i++;
    }

    return intOctets;
  }

  public static int[] convertStringToIP(final String str) {
    if (str == null) {
      throw new IllegalArgumentException("The value passed in is null");
    }
    return convertStringToIP(str.split("\\."));
  }

  private static int strOctetToInt(final String strOctet) throws NumberFormatException {
    int octet = Integer.parseInt(strOctet);
    if (octet > MAX_OCTET_VALUE) {
      octet = MAX_OCTET_VALUE;
    }
    return octet;
  }

  @Override
  public void updateUI() {
    super.updateUI();
    JTextField jtextfield = new JTextField();
    jtextfield.setEnabled(isEnabled());
    jtextfield.setEditable(isEditable());
    jtextfield.updateUI();

    setBackground(jtextfield.getBackground());
    setForeground(jtextfield.getForeground());
    setBorder(jtextfield.getBorder());
  }

  private JLabel createDotLabel() {
    NullLabel createDotLabel = new NullLabel(".");
    createDotLabel.setCursor(Cursor.getPredefinedCursor(Cursor.TEXT_CURSOR));
    return createDotLabel;
  }

  private JTextField createOctetField(final JTextField previousOctetField) {
    JTextField octetField;
    octetField = new JTextField(new IPDocument(), "", 3);
    octetField.setOpaque(false);
    octetField.addFocusListener(octetFieldsFocusListener);
    octetField.setBorder(BorderFactory.createEmptyBorder());
    octetField.setHorizontalAlignment(SwingConstants.CENTER);

    if (previousOctetField != null) {
      IPDocument prevDoc = ((IPDocument) previousOctetField.getDocument());
      prevDoc.setNextTextField(octetField);
    }

    octetField.getDocument().addDocumentListener(new DocumentListener() {

      @Override
      public void insertUpdate(final DocumentEvent documentevent) {
        onDocumentEdited();
      }

      @Override
      public void removeUpdate(final DocumentEvent documentevent) {
        onDocumentEdited();
      }

      @Override
      public void changedUpdate(final DocumentEvent documentevent) {
      }

    });

    replaceAction(octetField, JComponent.WHEN_FOCUSED,
        KeyStroke.getKeyStroke(KeyEvent.VK_BACK_SPACE, 0),
        new BackSpaceDelegateAction());
    replaceAction(octetField, JComponent.WHEN_FOCUSED,
        KeyStroke.getKeyStroke(KeyEvent.VK_LEFT, 0),
        new LeftDelegateAction());
    replaceAction(octetField, JComponent.WHEN_FOCUSED,
        KeyStroke.getKeyStroke(KeyEvent.VK_RIGHT, 0),
        new RightDelegateAction());
    replaceAction(octetField, JComponent.WHEN_FOCUSED,
        KeyStroke.getKeyStroke(KeyEvent.VK_HOME, 0),
        new HomeDelegateAction());
    replaceAction(octetField, JComponent.WHEN_FOCUSED,
        KeyStroke.getKeyStroke(KeyEvent.VK_END, 0),
        new EndDelegateAction());

    return octetField;
  }

  protected void onDocumentEdited() {
    Object[] changeListeners = listenerList.getListenerList();
    if (changeEvent == null) {
      changeEvent = new ChangeEvent(this);
    }

    for (int i = changeListeners.length - 2; i >= 0; i -= 2) {
      if (changeListeners[i] == ChangeListener.class) {
        ((ChangeListener) changeListeners[i + 1]).stateChanged(changeEvent);
      }
    }
  }

  private boolean isHasFieldBeforeFocused() {
    JTextField octetField = getFieldBeforeFocused();
    if (octetField == null) {
      return false;
    }
    octetField.requestFocusInWindow();
    return true;
  }

  private JTextField getFieldBeforeFocused() {
    for (int i = octetFields.length - 1; i >= 0; i--) {
      if (i > 0 && octetFields[i].hasFocus()) {
        return octetFields[i - 1];
      }
    }
    return null;
  }

  private JTextField getFieldAfterFocused() {
    for (int i = 0; i < octetFields.length; i++) {
      if (octetFields[i].hasFocus() && i != octetFields.length - 1) {
        return octetFields[i + 1];
      }
    }
    return null;
  }

  private boolean isHasFieldAfterFocused() {
    JTextField jtextfield = getFieldAfterFocused();
    if (jtextfield == null) {
      return false;
    }
    jtextfield.requestFocusInWindow();
    return true;
  }

  private JTextField getFirstEditableOctetField() {
    for (int i = 0; i < octetFields.length; i++) {
      if (octetFields[i].isEditable()) {
        return octetFields[i];
      }
    }
    return null;
  }

  private boolean isHasLeftEditableField() {
    JTextField jtextfield = getFirstEditableOctetField();
    if (jtextfield == null) {
      return false;
    }

    jtextfield.requestFocusInWindow();
    jtextfield.setCaretPosition(0);
    return true;
  }

  private JTextField getLastEditableOctetField() {
    for (int i = octetFields.length - 1; i >= 0; i--) {
      if (octetFields[i].isEditable()) {
        return octetFields[i];
      }
    }
    return null;
  }

  private boolean isHasRightEditableField() {
    JTextField jtextfield = getLastEditableOctetField();
    if (jtextfield == null) {
      return false;
    }

    jtextfield.requestFocusInWindow();
    jtextfield.setCaretPosition(jtextfield.getText().length());
    return true;
  }

  private int getOctetFieldIndex(final JTextField octetField) {
    for (int i = 0; i < octetFields.length; i++) {
      if (octetField == octetFields[i]) {
        return i;
      }
    }
    return -1;
  }

  @Override
  public void requestFocus() {
    JTextField octetField = getFirstEditableOctetField();
    if (octetField != null) {
      octetField.requestFocus();
    }
  }

  @Override
  public boolean requestFocus(final boolean temporary) {
    JTextField octetField = getFirstEditableOctetField();
    if (octetField == null) {
      return super.requestFocus(temporary);
    } else {
      return octetField.requestFocus(temporary);
    }
  }

  @Override
  public boolean requestFocusInWindow() {
    JTextField octetField = getFirstEditableOctetField();
    if (octetField == null) {
      return super.requestFocusInWindow();
    } else {
      return octetField.requestFocusInWindow();
    }
  }

  @Override
  public boolean hasFocus() {
    for (JTextField octetField : octetFields) {
      if (octetField.hasFocus()) {
        return true;
      }
    }
    return false;
  }

  private void setOctetDocumentValue(final String value, final JTextField octetField) {
    if (octetField != null) {
      octetField.requestFocus();
      Document document = octetField.getDocument();
      try {
        document.remove(0, document.getLength());
        document.insertString(0, value, new SimpleAttributeSet());
      } catch (BadLocationException ignored) {
      }
    }
  }

  private void replaceAction(final JComponent component, final int condition, final KeyStroke keyStroke, final DelegateAction delegate) {
    Object bindObj = component.getInputMap(condition).get(keyStroke);
    if (bindObj != null) {
      final Action action = component.getActionMap().get(bindObj);
      if (action != delegate) {
        ActionListener delegateActionListener = new ActionListener() {
          @Override
          public void actionPerformed(final ActionEvent e) {
            if (!delegate.delegateActionPerformed(e)) {
              action.actionPerformed(e);
            }
          }

        };
        component.registerKeyboardAction(delegateActionListener, keyStroke, condition);
      }
    }
  }

  private void fireFocusGained(final FocusEvent focusEvent) {
    for (FocusListener l : focusListeners) {
      l.focusGained(focusEvent);
    }
  }

  private void fireFocusLost(final FocusEvent focusEvent) {
    for (FocusListener l : focusListeners) {
      l.focusLost(focusEvent);
    }
  }

  /**
   * IP address octet field focus adapter
   */
  private class OctetFieldsFocusAdapter extends FocusAdapter {

    private boolean isLocalComponent(final Component child) {
      return isLocalComponent(IPTextField.this, child);
    }

    private boolean isLocalComponent(final JComponent root, final Component child) {
      if (child == null) {
        return false;
      }

      for (Component cmp : root.getComponents()) {
        if (cmp == child) {
          return true;
        } else if (cmp instanceof JComponent) {
          if (isLocalComponent((JComponent) cmp, child)) {
            return true;
          }
        }
      }

      return false;
    }

    @Override
    public void focusGained(final FocusEvent focusEvent) {
      JTextField jtextfield = (JTextField) focusEvent.getSource();
      jtextfield.selectAll();

      if (!isLocalComponent(focusEvent.getOppositeComponent())) {
        FocusEvent event = new FocusEvent(IPTextField.this, focusEvent.getID(),
            focusEvent.isTemporary(), focusEvent.getOppositeComponent());
        fireFocusGained(event);
      }
    }

    @Override
    public void focusLost(final FocusEvent focusEvent) {

      if (!focusEvent.isTemporary()) {
        JTextField octetField = (JTextField) focusEvent.getSource();
        String strOctet = octetField.getText();

        int octet;
        try {
          octet = strOctetToInt(strOctet);
        } catch (NumberFormatException ignored) {
          octet = MIN_OCTET_VALUE;
        }

        int index = getOctetFieldIndex(octetField);
        if (octet < getSubnetMask()[index]) {
          octet = getSubnetMask()[index];
        }

        String parsedStrOctet = String.valueOf(octet);
        if (!parsedStrOctet.equals(strOctet)) {
          octetField.setText(parsedStrOctet);
        }
      }

      if (!isLocalComponent(focusEvent.getOppositeComponent())) {
        FocusEvent event = new FocusEvent(IPTextField.this, focusEvent.getID(),
            focusEvent.isTemporary(), focusEvent.getOppositeComponent());
        fireFocusLost(event);
      }
    }

  }

  /**
   * IP address field document
   */
  private class IPDocument extends PlainDocument {

    private static final long serialVersionUID = -4401508940364743913L;

    protected JTextField nextOctetField;

    public void setNextTextField(final JTextField octetField) {
      nextOctetField = octetField;
    }

    @Override
    public void insertString(final int offs, final String str, final AttributeSet attrSet)
        throws BadLocationException {

      if (str == null) {
        return;
      }

      int charsLeft = MAX_OCTET_CHARS_LENGTH - getLength();
      if (charsLeft == 0) {
        setOctetDocumentValue(str, nextOctetField);
      } else {
        int capacity = charsLeft;
        char[] buffer = new char[capacity];
        int buffIndex = 0;
        int si = 0;
        for (; capacity > 0 && si < str.length(); si++) {
          char ch = str.charAt(si);
          if (Character.isDigit(ch)) {
            buffer[buffIndex++] = ch;
            capacity--;
          }
        }

        // is some digit chars founded ?
        if (buffIndex > 0) {
          super.insertString(offs, new String(buffer, 0, buffIndex), attrSet);

          // has more text ?
          if (si < str.length()) {
            String value = str.substring(si);
            if (isHasFieldAfterFocused()) {
              setOctetDocumentValue(value, nextOctetField);
            }
          }
        }
      }
    }

  }

  /**
   * [<- Backspace]
   * <p>
   * Backspace key delegate
   */
  private class BackSpaceDelegateAction implements DelegateAction {

    private static final long serialVersionUID = 5684220958259910737L;

    @Override
    public boolean delegateActionPerformed(final ActionEvent actionevent) {
      JTextField jtextfield = (JTextField) actionevent.getSource();
      if (jtextfield.getText().length() == 0) {
        return isHasFieldBeforeFocused();
      }
      return false;
    }
  }

  /**
   * [<-]
   * <p>
   * Left key delegate
   */
  private class LeftDelegateAction implements DelegateAction {

    private static final long serialVersionUID = -3686633650581264306L;

    @Override
    public boolean delegateActionPerformed(final ActionEvent event) {
      JTextField jtextfield = (JTextField) event.getSource();
      if (jtextfield.getCaretPosition() == 0) {
        return isHasFieldBeforeFocused();
      }
      return false;
    }

  }

  /**
   * [->]
   * <p>
   * Right key delegate
   */
  private class RightDelegateAction implements DelegateAction {

    private static final long serialVersionUID = 6287281680603370486L;

    @Override
    public boolean delegateActionPerformed(final ActionEvent actionevent) {
      JTextField jtextfield = (JTextField) actionevent.getSource();
      if (jtextfield.getCaretPosition() == jtextfield.getText().length()) {
        return isHasFieldAfterFocused();
      }
      return false;
    }

  }

  /**
   * [Home]
   * <p>
   * Home key delegate
   */
  private class HomeDelegateAction implements DelegateAction {

    private static final long serialVersionUID = -9218010673990916319L;

    @Override
    public boolean delegateActionPerformed(final ActionEvent actionevent) {
      JTextField jtextfield = (JTextField) actionevent.getSource();
      if (jtextfield.getCaretPosition() == 0) {
        return isHasLeftEditableField();
      }
      return false;
    }

  }

  /**
   * [End]
   * <p>
   * End key delegate
   */
  private class EndDelegateAction implements DelegateAction {

    private static final long serialVersionUID = -7914304622629239662L;

    @Override
    public boolean delegateActionPerformed(final ActionEvent actionevent) {
      JTextField jtextfield = (JTextField) actionevent.getSource();
      if (jtextfield.getCaretPosition() == jtextfield.getText().length()) {
        return isHasRightEditableField();
      }
      return false;
    }
  }


}
