package com.flamesgroup.antrax.control.swingwidgets.layouts;

import java.awt.*;

import javax.swing.*;

public class FormLayout {

  private final GridBagConstraints constraints;
  private final Container container;

  public FormLayout(final Container container) {
    this.container = container;
    container.setLayout(new GridBagLayout());
    constraints = new GridBagConstraints();
    constraints.insets = new Insets(3, 3, 3, 3);
    constraints.gridx = 0;
    constraints.gridy = 0;
  }

  public void addField(final String label, final JComponent component) {
    addField(new JLabel(label), component);
  }

  public void addField(final JLabel label, final JComponent component) {
    constraints.gridx = 0;
    constraints.weightx = 0;
    constraints.weighty = 0;
    Class<?> componentClass = component.getClass();

    constraints.fill = GridBagConstraints.NONE;
    if (JScrollPane.class == componentClass)
      constraints.anchor = GridBagConstraints.FIRST_LINE_END;
    else
      constraints.anchor = GridBagConstraints.LINE_END;
    container.add(label, constraints);

    constraints.gridx = 1;

    if (JScrollPane.class == componentClass) {
      constraints.weightx = 1;
      constraints.weighty = 1;
      constraints.fill = GridBagConstraints.BOTH;
      container.add(component, constraints);
    } else if (JTextField.class == componentClass) {
      JTextField textField = (JTextField) component;
      if (textField.getColumns() == 0) {
        // if you don't specify a number of columns
        // we make this text field to be as wide as possible
        constraints.fill = GridBagConstraints.BOTH;
      } else {
        // this fixes a problem when the text field doesn't
        // fit in the window width
        textField.setMinimumSize(textField.getPreferredSize());
        constraints.anchor = GridBagConstraints.LINE_START;
      }
      container.add(component, constraints);
    } else {
      constraints.anchor = GridBagConstraints.LINE_START;
      container.add(component, constraints);
    }
    label.setLabelFor(component); // accessibility

    constraints.gridy++;
  }
}
