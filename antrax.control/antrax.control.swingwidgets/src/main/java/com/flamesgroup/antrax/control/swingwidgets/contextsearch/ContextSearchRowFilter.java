package com.flamesgroup.antrax.control.swingwidgets.contextsearch;

import javax.swing.*;

public class ContextSearchRowFilter extends RowFilter<Object, Object> {

  private final int[] columns;
  private SearchFilter searchFilter;

  /**
   * @throws IllegalArgumentException if any of the values in columns are < 0.
   */
  private void checkIndices(final int[] columns) {
    for (int i = 0; i < columns.length; i++) {
      if (columns[i] < 0) {
        throw new IllegalArgumentException("Index must be >= 0");
      }
    }
  }

  public ContextSearchRowFilter() {
    this.columns = null;
  }

  public ContextSearchRowFilter(final SearchFilter searchFilter) {
    this.columns = null;
    this.searchFilter = searchFilter;

  }

  public ContextSearchRowFilter(final int[] columns) {
    checkIndices(columns);
    this.columns = columns;
  }

  protected synchronized SearchFilter getSearchFilter() {
    return searchFilter;
  }

  public synchronized void setSearchFilter(final SearchFilter searchFilter) {
    this.searchFilter = searchFilter;
  }

  @Override
  public boolean include(final Entry<? extends Object, ? extends Object> entry) {
    if (getSearchFilter() == null) {
      return true;
    }

    StringBuffer buffer = new StringBuffer();

    int columnsCount = entry.getValueCount();
    if (columns != null && columns.length > 0) {
      for (int i = 0; i < columns.length; i++) {
        int index = columns[i];
        if (index < columnsCount) {
          buffer.append(getEntryValue(entry, index));
        }
      }
    } else {
      for (int i = 0; i < columnsCount; i++) {
        buffer.append(getEntryValue(entry, i));
      }
    }

    return getSearchFilter().isApplied(buffer.toString());
  }

  protected String getEntryValue(final Entry<? extends Object, ? extends Object> entry, final int column) {
    return entry.getStringValue(column);
  }

}
