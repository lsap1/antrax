package com.flamesgroup.antrax.control.swingwidgets;

import com.flamesgroup.antrax.control.swingwidgets.table.JMetalTable;

import java.awt.*;

import javax.swing.*;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.JTableHeader;
import javax.swing.table.TableModel;
import javax.swing.table.TableRowSorter;

public class TestTableCustomHeader {
  public static void main(final String[] args) {
    new TestTableCustomHeader();
  }

  public TestTableCustomHeader() {
    JFrame frame = new JFrame("Appending Column Example!");
    frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
    JPanel panel = new JPanel();
    String data[][] = {{"Vinod", "MCA", "Computer"},
        {"Deepak", "PGDCA", "History"},
        {"Ranjan", "M.SC.", "Biology"},
        {"Radha", "BCA", "Computer"}};
    String col[] = {"Name", "Course", "Subject"};
    DefaultTableModel model = new DefaultTableModel(data, col);
    model.addColumn("Grade");
    JMetalTable table = new JMetalTable();
    table.setModel(model);
    table.setRowSorter(new TableRowSorter<TableModel>(model));
    //table.setTableHeader()


    JTableHeader header = table.getTableHeader();
    header.setBackground(Color.yellow);
    JScrollPane pane = new JScrollPane(table);
    panel.add(pane);
    frame.add(panel);
    frame.setUndecorated(true);
    frame.getRootPane().setWindowDecorationStyle(JRootPane.PLAIN_DIALOG);
    frame.setSize(500, 550);
    frame.setVisible(true);
    frame.setLocationRelativeTo(null);
  }
}
