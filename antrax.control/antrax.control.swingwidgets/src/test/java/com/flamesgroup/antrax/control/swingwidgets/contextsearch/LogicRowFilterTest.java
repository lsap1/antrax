package com.flamesgroup.antrax.control.swingwidgets.contextsearch;

import static org.junit.Assert.assertEquals;

import com.flamesgroup.antrax.control.swingwidgets.logicparser.LogicExpressionSyntaxException;
import com.flamesgroup.antrax.control.swingwidgets.table.ColumnWriter;
import com.flamesgroup.antrax.control.swingwidgets.table.JUpdatableTable;
import com.flamesgroup.antrax.control.swingwidgets.table.TableBuilder;
import com.flamesgroup.antrax.control.swingwidgets.table.UpdateTableColumnModel;
import org.junit.Test;

public class LogicRowFilterTest {

  final static class StringWithColonTableBuilder implements TableBuilder<String, String> {
    private final String[] columnNames;

    public StringWithColonTableBuilder(final String... columnNames) {
      this.columnNames = columnNames;
    }

    @Override
    public String getUniqueKey(final String src) {
      return src.split(":")[0];
    }

    @Override
    public void buildRow(final String src, final ColumnWriter<String> dest) {
      for (String s : src.split(":")) {
        dest.writeColumn(s);
      }
    }

    @Override
    public void buildColumns(final UpdateTableColumnModel builder) {
      for (String name : columnNames) {
        builder.addColumn(name, String.class);
      }
    }
  }

  @Test
  public void andFilterTest() throws LogicExpressionSyntaxException {
    StringWithColonTableBuilder rowBuilder = new StringWithColonTableBuilder("bekki", "cem", "state");
    JUpdatableTable<String, String> table = new JUpdatableTable<>(rowBuilder);

    table.setData(new String[] {
        "cem-120.1:bekki-16.0:call",
        "cem-120.2::ready",
        "cem-121.3:bekki-17.0:dial",
    });

    LogicSearchFilter filter = new LogicSearchFilter();
    filter.setFilterExpression("cem-120 & bekki");
    table.applySearchFilter(filter);

    assertEquals(1, table.getRowSorter().getViewRowCount());
    assertEquals("cem-120.1:bekki-16.0:call", table.getElemAt(0));
  }

}
