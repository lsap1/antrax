package com.flamesgroup.antrax.control.swingwidgets.table;

import static org.junit.Assert.assertEquals;

import org.junit.Test;

public class UpdatableTableModelTest {
  final static class StringWithColonTableBuilder implements TableBuilder<String, String> {
    private final String[] columnNames;

    public StringWithColonTableBuilder(final String... columnNames) {
      this.columnNames = columnNames;
    }

    @Override
    public String getUniqueKey(final String src) {
      return src.split(":")[0];
    }

    @Override
    public void buildRow(final String src, final ColumnWriter<String> dest) {
      for (String s : src.split(":")) {
        dest.writeColumn(s);
      }
    }

    @Override
    public void buildColumns(final UpdateTableColumnModel builder) {
      for (String name : columnNames) {
        builder.addColumn(name, String.class);
      }
    }
  }

  @Test
  public void testInitialization() {
    UpdatableTableModel<String, String> model = new UpdatableTableModel<>(new StringWithColonTableBuilder("a", "b", "c"), new DefaultUpdateTableColumnModel());
    assertEquals(3, model.getColumnCount());
    assertEquals("a", model.getColumnName(0));
    assertEquals("b", model.getColumnName(1));
    assertEquals("c", model.getColumnName(2));
  }

  @Test
  public void testSetData() {
    UpdatableTableModel<String, String> model = new UpdatableTableModel<>(new StringWithColonTableBuilder("a", "b", "c"), new DefaultUpdateTableColumnModel());

    model.setData(new String[] {"text1:text2:text3"});

    assertEquals(1, model.getRowCount());

    assertEquals("text1", model.getValueAt(0, 0));
    assertEquals("text2", model.getValueAt(0, 1));
    assertEquals("text3", model.getValueAt(0, 2));

    model.setData("a1:b1:c1", "a2:b2:c2");

    assertEquals(2, model.getRowCount());
    assertEquals("a1", model.getValueAt(0, 0));
    assertEquals("b1", model.getValueAt(0, 1));
    assertEquals("c1", model.getValueAt(0, 2));
    assertEquals("a2", model.getValueAt(1, 0));
    assertEquals("b2", model.getValueAt(1, 1));
    assertEquals("c2", model.getValueAt(1, 2));
  }

  @Test
  public void testSetDataCanPrepend() {
    UpdatableTableModel<String, String> model = new UpdatableTableModel<>(new StringWithColonTableBuilder("name"), new DefaultUpdateTableColumnModel());
    model.setData("b", "c");
    model.setData("a", "b", "c");

    assertEquals(3, model.getRowCount());
    assertEquals("a", model.getValueAt(0, 0));
    assertEquals("b", model.getValueAt(1, 0));
    assertEquals("c", model.getValueAt(2, 0));
  }

  @Test
  public void testSetDataWithSeriaOfSameElements() {
    UpdatableTableModel<String, String> model = new UpdatableTableModel<>(new StringWithColonTableBuilder("name"), new DefaultUpdateTableColumnModel());
    model.setData("a", "b", "c");
    assertEquals(3, model.getRowCount());
    assertEquals("a", model.getValueAt(0, 0));
    assertEquals("b", model.getValueAt(1, 0));
    assertEquals("c", model.getValueAt(2, 0));

    model.setData("d", "e", "f");
    assertEquals(3, model.getRowCount());
    assertEquals("d", model.getValueAt(0, 0));
    assertEquals("e", model.getValueAt(1, 0));
    assertEquals("f", model.getValueAt(2, 0));

    model.setData("a", "b", "c");
    assertEquals(3, model.getRowCount());
    assertEquals("a", model.getValueAt(0, 0));
    assertEquals("b", model.getValueAt(1, 0));
    assertEquals("c", model.getValueAt(2, 0));
  }

  @Test
  public void testInsertElem() {
    UpdatableTableModel<String, String> model = new UpdatableTableModel<>(new StringWithColonTableBuilder("name"), new DefaultUpdateTableColumnModel());
    model.insertElem("b");
    assertEquals(1, model.getRowCount());
    assertEquals("b", model.getValueAt(0, 0));
    model.insertElem("c");
    assertEquals(2, model.getRowCount());
    assertEquals("b", model.getValueAt(0, 0));
    assertEquals("c", model.getValueAt(1, 0));
    model.insertElem("a");
    assertEquals(3, model.getRowCount());
    assertEquals("a", model.getValueAt(0, 0));
    assertEquals("b", model.getValueAt(1, 0));
    assertEquals("c", model.getValueAt(2, 0));
  }

  @Test
  public void testUpdateElemWithChangedKey() {
    UpdatableTableModel<String, String> model = new UpdatableTableModel<>(new StringWithColonTableBuilder("name"), new DefaultUpdateTableColumnModel());
    model.setData("a", "b", "c", "d");
    model.updateElemAt("z", 1);
    assertEquals(4, model.getRowCount());
    assertEquals("a", model.getValueAt(0, 0));
    assertEquals("c", model.getValueAt(1, 0));
    assertEquals("d", model.getValueAt(2, 0));
    assertEquals("z", model.getValueAt(3, 0));

    model.setData("a", "c", "d", "z");
    assertEquals("a", model.getValueAt(0, 0));
    assertEquals("c", model.getValueAt(1, 0));
    assertEquals("d", model.getValueAt(2, 0));
    assertEquals("z", model.getValueAt(3, 0));
  }

}
