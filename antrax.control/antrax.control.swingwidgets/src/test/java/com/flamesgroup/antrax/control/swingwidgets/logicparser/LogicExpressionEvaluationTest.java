package com.flamesgroup.antrax.control.swingwidgets.logicparser;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import org.junit.Test;

public class LogicExpressionEvaluationTest {

  private String filteredString;

  protected String getFilteredString() {
    return filteredString;
  }

  protected void setFilteredString(final String filteredString) {
    this.filteredString = filteredString;
  }

  public class DelegatedStringNodeFactory implements StringNodeFactory {
    private DelegatedStringNode.EvaluationDelegate delegate;

    private DelegatedStringNode.EvaluationDelegate getDelegate() {
      if (delegate == null) {
        delegate = new DelegatedStringNode.EvaluationDelegate() {
          @Override
          public boolean evaluate(final String value) {
            return getFilteredString().contains(value);
          }
        };
      }
      return delegate;
    }

    @Override
    public StringNode createStringNode(final String str) {
      return new DelegatedStringNode(str, getDelegate());
    }
  }

  @Test
  public void testlogicExprEvaluation() throws LogicExpressionSyntaxException {

    StringNodeFactory factory = new DelegatedStringNodeFactory();
    LogicExpressionParser parser = new LogicExpressionParser();

    Node root = parser.parseExpression("a & b", LogicToken.AND, factory);

    setFilteredString("abcd");
    assertTrue(root.evaluate());

    setFilteredString("acb");
    assertTrue(root.evaluate());

    setFilteredString("cda");
    assertFalse(root.evaluate());

    root = parser.parseExpression("a !b | c", LogicToken.AND, factory);

    setFilteredString("ad");
    assertTrue(root.evaluate());

    setFilteredString("abc");
    assertTrue(root.evaluate());

    root = parser.parseExpression("a & (b | c)", LogicToken.AND, factory);

    setFilteredString("abd");
    assertTrue(root.evaluate());

    setFilteredString("abc");
    assertTrue(root.evaluate());

    setFilteredString("bd");
    assertFalse(root.evaluate());
  }

}
