/*
 * Copyright (C) 2017 Flames Group SIA
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/**
 * Provides classes and interfaces for creating value editors.
 * <p>
 * Antrax provides several editors for common types.
 * Other types requires a proper editor to be created.
 * You can create editor implementing low level
 * interface {@link com.flamesgroup.antrax.automation.editors.PropertyEditor} or extending highlevel
 * classes {@link com.flamesgroup.antrax.automation.editors.BasePropertyEditor}, {@link com.flamesgroup.antrax.automation.editors.BaseListBasedPropertyEditor}
 * </p>
 */
package com.flamesgroup.antrax.automation.editors;

