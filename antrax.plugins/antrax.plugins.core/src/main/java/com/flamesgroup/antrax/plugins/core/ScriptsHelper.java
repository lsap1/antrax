/*
 * Copyright (C) 2017 Flames Group SIA
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.flamesgroup.antrax.plugins.core;

import com.flamesgroup.antrax.automation.annotations.Script;
import com.flamesgroup.antrax.automation.annotations.ScriptParam;
import com.flamesgroup.antrax.automation.editors.PropertyEditor;
import com.flamesgroup.antrax.automation.meta.JavaFileName;
import com.flamesgroup.antrax.automation.meta.NotFoundScriptDefinition;
import com.flamesgroup.antrax.automation.meta.ScriptDefinition;
import com.flamesgroup.antrax.automation.meta.ScriptDefinitionImpl;
import com.flamesgroup.antrax.automation.meta.ScriptInstance;
import com.flamesgroup.antrax.automation.meta.ScriptParameter;
import com.flamesgroup.antrax.automation.meta.ScriptParameterDefinition;
import com.flamesgroup.antrax.automation.meta.ScriptParameterDefinitionImpl;
import com.flamesgroup.antrax.automation.meta.ScriptType;
import com.flamesgroup.antrax.automation.meta.SharedReference;
import com.flamesgroup.antrax.automation.scripts.ActionProviderScript;
import com.flamesgroup.antrax.automation.scripts.ActivityPeriodScript;
import com.flamesgroup.antrax.automation.scripts.BusinessActivityScript;
import com.flamesgroup.antrax.automation.scripts.CallFilterScript;
import com.flamesgroup.antrax.automation.scripts.FactorEvaluationScript;
import com.flamesgroup.antrax.automation.scripts.GatewaySelectorScript;
import com.flamesgroup.antrax.automation.scripts.IMEIGeneratorScript;
import com.flamesgroup.antrax.automation.scripts.IncomingCallScript;
import com.flamesgroup.antrax.automation.scripts.SmsFilterScript;
import com.flamesgroup.antrax.storage.commons.impl.ScriptCommons;
import com.flamesgroup.utils.codebase.AntraxJavaClassLoader;
import com.flamesgroup.utils.codebase.ScriptFileInfo;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.BufferedReader;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.ObjectOutputStream;
import java.lang.reflect.Field;
import java.lang.reflect.Method;
import java.lang.reflect.Modifier;
import java.net.URISyntaxException;
import java.util.Arrays;
import java.util.Date;
import java.util.Enumeration;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Objects;
import java.util.Properties;
import java.util.Set;
import java.util.jar.Attributes;
import java.util.jar.JarEntry;
import java.util.jar.JarFile;

public class ScriptsHelper {

  public static List<PropertyEditor<?>> determinePropertyEditors(final AntraxJavaClassLoader classLoader)
      throws ClassNotFoundException, IllegalAccessException, InstantiationException {
    List<PropertyEditor<?>> propertyEditors = new LinkedList<>();
    for (JavaFileName className : classLoader.getJavaFileNames()) {
      Class<?> scriptClass = Class.forName(className.getName(), true, classLoader.getClassLoader());
      if (PropertyEditor.class.isAssignableFrom(scriptClass)) {
        propertyEditors.add((PropertyEditor<?>) scriptClass.newInstance());
      }
    }
    return propertyEditors;
  }

  public static ScriptDefinition[] determineScriptDefinitions(final AntraxJavaClassLoader antraxJavaClassLoader) throws Exception {
    List<ScriptDefinition> scriptDefinitions = new LinkedList<>();
    for (JavaFileName className : antraxJavaClassLoader.getJavaFileNames()) {
      Class<?> scriptClass = Class.forName(className.getName(), true, antraxJavaClassLoader.getClassLoader());
      Script script = scriptClass.getAnnotation(Script.class);
      if (script != null) {
        scriptDefinitions.add(determineScriptDefinition(scriptClass));
      }
    }
    return scriptDefinitions.toArray(new ScriptDefinition[scriptDefinitions.size()]);
  }

  public static ScriptDefinition determineScriptDefinition(final Class<?> scriptClass) throws Exception {
    if (!scriptClass.isAnnotationPresent(Script.class)) {
      throw new IllegalArgumentException(scriptClass + " is not script. It should be annotated with @Script");
    }

    Script script = scriptClass.getAnnotation(Script.class);
    String name = script.name();
    String desc = script.doc();
    ScriptType type = getTypeOf(scriptClass);
    return new ScriptDefinitionImpl(new JavaFileName(scriptClass), name, desc, type, determineParams(scriptClass));
  }

  private static ScriptParameterDefinition[] determineParams(final Class<?> scriptType) throws BadScriptException {
    List<ScriptParameterDefinition> retval = new LinkedList<>();

    for (Method m : scriptType.getMethods()) {
      boolean array = false;
      if (!m.isAnnotationPresent(ScriptParam.class)) {
        continue;
      }
      if (!m.getName().startsWith("set")) {
        if (!m.getName().startsWith("add")) {
          continue;
        }
        array = true;
      }
      Class<?>[] types = m.getParameterTypes();
      if (types.length != 1) {
        continue;
      }
      if (!Modifier.isPublic(m.getModifiers())) {
        continue;
      }

      ScriptParam param = m.getAnnotation(ScriptParam.class);

      ScriptParameterDefinition def = new ScriptParameterDefinitionImpl(param.name(), m.getName(), param.doc(), types[0].getCanonicalName(), array, getDefaultValue(scriptType, m));
      retval.add(def);
    }

    return retval.toArray(new ScriptParameterDefinition[retval.size()]);
  }

  private static byte[] getDefaultValue(final Class<?> clazz, final Method setter) throws BadScriptException {
    String getterName = "get" + setter.getName().substring(3);
    Method getter;
    try {
      getter = clazz.getMethod(getterName);
    } catch (NoSuchMethodException ignored) {
      throw new BadScriptException("There is no getter for " + setter.getName() + " in " + clazz.getCanonicalName());
    }
    Object instance;
    try {
      instance = clazz.newInstance();
    } catch (Exception ignored) {
      throw new BadScriptException("Failed to create instance of script");
    }

    Object value;
    try {
      value = getter.invoke(instance);
    } catch (Exception ignored) {
      throw new BadScriptException("Failed to invoke getter " + getter);
    }

    if (!getter.getReturnType().equals(setter.getParameterTypes()[0])) {
      throw new BadScriptException("Getter " + getter + " returns other value than setter " + setter);
    }

    ByteArrayOutputStream output = new ByteArrayOutputStream();
    try {
      ObjectOutputStream stream = new ObjectOutputStream(output);
      stream.writeObject(value);
      stream.close();
      output.close();
      return output.toByteArray();
    } catch (Exception e) {
      throw new BadScriptException("Failed to serialize " + value, e);
    }
  }

  public static <T> T instantiate(final ScriptInstance scriptInstance, final Class<T> type, final ScriptCommons[] commonses, final ClassLoader classLoader) throws Exception {
    Objects.requireNonNull(scriptInstance, "scriptInstance mustn't be null");
    if (getClassOf(scriptInstance.getDefinition().getType()) != type) {
      throw new IllegalArgumentException("Script " + scriptInstance + " is not " + type);
    }

    Class<?> scriptClass = Class.forName(scriptInstance.getDefinition().getCode().getName(), true, classLoader);

    T script = type.cast(scriptClass.newInstance());
    for (ScriptParameter param : scriptInstance.getParameters()) {
      if (!param.hasValue()) {
        continue;
      }
      Class<?> paramClass = ScriptParameterDefinitionImpl.convert(param.getDefinition().getType(), classLoader);

      Method method = scriptClass.getMethod(param.getDefinition().getMethodName(), paramClass);
      Object paramValue = param.getValue(classLoader);

      if (paramValue instanceof SharedReference) {
        paramValue = findSharedReverence((SharedReference) paramValue, commonses).getValue(classLoader);
      }

      method.invoke(script, paramValue);
    }
    return script;
  }

  private static ScriptCommons findSharedReverence(final SharedReference value, final ScriptCommons[] references) {
    for (ScriptCommons r : references) {
      if (r.getName().equals(value.getName())) {
        return r;
      }
    }
    throw new IllegalArgumentException("There is no shured reference to match " + value.getName());
  }

  public static Class<?> getClassOf(final ScriptType type) {
    switch (type) {
      case ACTION_PROVIDER:
        return ActionProviderScript.class;
      case ACTIVITY_PERIOD:
        return ActivityPeriodScript.class;
      case BUSINESS_ACTIVITY:
        return BusinessActivityScript.class;
      case FACTOR:
        return FactorEvaluationScript.class;
      case GATEWAY_SELECTOR:
        return GatewaySelectorScript.class;
      case INCOMING_CALL_MANAGEMENT:
        return IncomingCallScript.class;
      case IMEI_GENERATOR:
        return IMEIGeneratorScript.class;
      case CALL_FILTER:
        return CallFilterScript.class;
      case SMS_FILTER:
        return SmsFilterScript.class;
      default:
        throw new UnsupportedOperationException("Type " + type + " is not supported yet");
    }
  }

  private static ScriptType getTypeOf(final Class<?> s) {
    if (ActionProviderScript.class.isAssignableFrom(s)) {
      return ScriptType.ACTION_PROVIDER;
    }
    if (ActivityPeriodScript.class.isAssignableFrom(s)) {
      return ScriptType.ACTIVITY_PERIOD;
    }
    if (BusinessActivityScript.class.isAssignableFrom(s)) {
      return ScriptType.BUSINESS_ACTIVITY;
    }
    if (FactorEvaluationScript.class.isAssignableFrom(s)) {
      return ScriptType.FACTOR;
    }
    if (GatewaySelectorScript.class.isAssignableFrom(s)) {
      return ScriptType.GATEWAY_SELECTOR;
    }
    if (IncomingCallScript.class.isAssignableFrom(s)) {
      return ScriptType.INCOMING_CALL_MANAGEMENT;
    }
    if (CallFilterScript.class.isAssignableFrom(s)) {
      return ScriptType.CALL_FILTER;
    }
    if (SmsFilterScript.class.isAssignableFrom(s)) {
      return ScriptType.SMS_FILTER;
    }
    if (IMEIGeneratorScript.class.isAssignableFrom(s)) {
      return ScriptType.IMEI_GENERATOR;
    }
    throw new IllegalArgumentException("Unknown script type: " + s.getName() + ". Script class must be on of script type: " + Arrays.toString(ScriptType.values()));
  }

  public static ScriptInstance migrateScriptToNewPluginsStore(final ScriptInstance oldScript, final AntraxPluginsStore oldPS, final AntraxPluginsStore newPS, final Set<String> scriptCommons) {
    if (oldScript == null) {
      return null;
    }

    ScriptInstance retval;
    try {
      retval = newPS.getDefinition(oldScript.getDefinition().getName()).createInstance();
    } catch (IllegalArgumentException ignored) {
      return new NotFoundScriptDefinition(oldScript.getDefinition()).createInstance();
    }

    fillUnchangedFields(retval, oldScript, newPS, oldPS, scriptCommons);
    return retval;
  }

  /**
   * Copies all values of all parameters from old instance to the new instance
   * if possible
   *
   * @param newInstance
   * @param oldInstance
   * @param newPS
   * @param oldPS
   * @param scriptCommons
   */
  public static void fillUnchangedFields(final ScriptInstance newInstance, final ScriptInstance oldInstance, final AntraxPluginsStore newPS, final AntraxPluginsStore oldPS,
      final Set<String> scriptCommons) {
    Logger logger = LoggerFactory.getLogger(ScriptsHelper.class);
    logger.debug("[{}] - filling unchanged fields for the {} from {}", ScriptsHelper.class, newInstance, oldInstance);
    clearArrays(newInstance);
    for (ScriptParameter newParam : newInstance.getParameters()) {
      ScriptParameter oldParam = oldInstance.getParameter(newParam.getDefinition().getName());
      if (oldParam != null) {
        logger.debug("[{}] - found similar params {} and {}", ScriptsHelper.class, newParam, oldParam);
        try {
          Class<?> oldType = oldPS.getClassByName(oldParam.getDefinition().getType());
          Class<?> newType = newPS.getClassByName(newParam.getDefinition().getType());

          if (isClassEquals(newType, oldType)) {
            logger.debug("[{}] - types of params is identical, so migrating values", ScriptsHelper.class);
            byte[] oldValue = oldParam.getSerializedValue();
            if (checkScriptCommons(oldValue, scriptCommons)) {
              newParam.setSerializedValue(oldValue);
            }

            if (newParam.getDefinition().isArray() && oldParam.getDefinition().isArray()) {
              logger.debug("Copying all array parameters");
              for (ScriptParameter oldArrayParam : findAllArraysExceptGiven(oldParam, oldInstance.getParameters())) {
                byte[] oldArrValue = oldArrayParam.getSerializedValue();
                if (checkScriptCommons(oldArrValue, scriptCommons)) {
                  newInstance.addParameter(oldArrayParam.getDefinition().getName(), oldArrValue);
                }
              }
            }
          } else {
            logger.debug("[{}] - their classes is not identical", ScriptsHelper.class);
          }
        } catch (Exception e) {
          logger.warn("[{}] - exception during parameters merge", ScriptsHelper.class, e);
        }
      }
    }
  }

  private static boolean checkScriptCommons(final Object oldValue, final Set<String> scriptCommons) {
    if (!(oldValue instanceof SharedReference)) {
      return true;
    }
    SharedReference ref = (SharedReference) oldValue;
    return scriptCommons.contains(ref.getName());
  }

  private static void clearArrays(final ScriptInstance newInstance) {
    Set<String> metOnce = new HashSet<>();
    for (ScriptParameter p : newInstance.getParameters()) {
      String name = p.getDefinition().getName();
      if (metOnce.contains(name)) {
        newInstance.removeParameter(p);
      } else {
        metOnce.add(name);
      }
    }
  }

  private static List<ScriptParameter> findAllArraysExceptGiven(final ScriptParameter param, final ScriptParameter[] parameters) {
    List<ScriptParameter> retval = new LinkedList<>();
    for (ScriptParameter p : parameters) {
      if (p == param) {
        continue;
      }
      if (p.getDefinition().getName().equals(param.getDefinition().getName())) {
        retval.add(p);
      }
    }
    return retval;
  }

  /**
   * Checks whether input classes are equals by fields and innerClasses.
   * <p/>
   * This method checks whether current classes satisfies several compare
   * conditions and, if does, its recursively walks throw classes hierarchies
   * and compares fields and inner classes.
   *
   * @param a class to compare
   * @param b class to compare
   * @return boolean answer if input classes are equals
   * @throws IllegalArgumentException
   */
  public static boolean isClassEquals(final Class<?> a, final Class<?> b) {
    if (a == null || b == null) {
      throw new IllegalArgumentException();
    }
    if (!isClassesSatisfyCondiditions(a, b)) {
      return false;
    }
    if (a.getSuperclass() != null && b.getSuperclass() != null) {
      if (!isClassEquals(a.getSuperclass(), b.getSuperclass())) {
        return false;
      }
    }
    int innerClassCount = a.getDeclaredClasses().length;
    for (Class<?> aClass : a.getDeclaredClasses()) {
      for (Class<?> bClass : b.getDeclaredClasses()) {
        try {
          if (isClassEquals(aClass, bClass)) {
            innerClassCount--;
          }
        } catch (SecurityException ignored) {
          return false;
        }
      }
    }
    if (innerClassCount > 0) {
      return false;
    }
    for (Field aField : a.getDeclaredFields()) {
      try {
        if (!isFieldsEquals(aField, b.getDeclaredField(aField.getName()))) {
          return false;
        }
      } catch (NoSuchFieldException ignored) {
        return false;
      }
    }
    return true;
  }

  private static boolean isClassesSatisfyCondiditions(final Class<?> a, final Class<?> b) throws SecurityException {
    return !((!a.getName().equals(b.getName())) || (a.getDeclaredClasses().length != b.getDeclaredClasses().length) || (a.getDeclaredFields().length != b.getDeclaredFields().length));
  }

  private static boolean isFieldsEquals(final Field a, final Field b) {
    boolean result = a.getName().equals(b.getName()) && a.getType().getName().equals(b.getType().getName()) && a.getModifiers() == b.getModifiers();
    if (a.getName().equals("serialVersionUID") && b.getName().equals("serialVersionUID") && result) {
      try {
        if (!(((a.getModifiers() & Modifier.STATIC) == Modifier.STATIC) && (a.getModifiers() & Modifier.FINAL) == Modifier.FINAL)) {
          return false;
        } else {
          a.setAccessible(true);
          b.setAccessible(true);
          return (a.get(null)).equals(b.get(null));
        }
      } catch (IllegalArgumentException ignored) {
        return false;
      } catch (IllegalAccessException ignored) {
        return false;
      }
    } else {
      return result;
    }
  }

  public static String getScriptsVersion(final File jarFile) throws IOException {
    try (JarFile jarFileLocal = new JarFile(jarFile);
        BufferedReader br = new BufferedReader(new InputStreamReader(jarFileLocal.getInputStream(jarFileLocal.getEntry("META-INF/MANIFEST.MF"))))) {
      String read;
      while ((read = br.readLine()) != null) {
        if (read.contains(Attributes.Name.SPECIFICATION_VERSION.toString())) {
          return read.replace(Attributes.Name.SPECIFICATION_VERSION.toString() + ": ", "").replace("-SNAPSHOT", "");
        }
      }
    }
    return null;
  }

  public static String getAntraxVersion() throws IOException, URISyntaxException {
    File file = new File(ScriptsHelper.class.getProtectionDomain().getCodeSource().getLocation().toURI());
    if (file.isDirectory()) { // run from IDE
      Properties properties = new Properties();
      try (InputStream inputStream = new FileInputStream(file.getParent() + "/maven-archiver/pom.properties")) {
        properties.load(inputStream);
        return properties.getProperty("version");
      }
    } else {
      return getScriptsVersion(file);
    }
  }

  public static ScriptFileInfo getScriptFileInfo(final File jarFile) throws IOException {
    long lastModificationTime = 0;
    long crcSum = 0;
    int fileCount = 0;
    int classCount = 0;
    JarFile jarFileLocal = new JarFile(jarFile);
    Enumeration<JarEntry> entries = jarFileLocal.entries();
    while (entries.hasMoreElements()) {
      JarEntry jarEntry = entries.nextElement();
      if (jarEntry.isDirectory()) {
        continue;
      }

      if (jarEntry.getName().endsWith(".class")) {
        crcSum += jarEntry.getCrc();
        classCount++;
      }
      if (jarEntry.getTime() > lastModificationTime) {
        lastModificationTime = jarEntry.getTime();
      }
      fileCount++;
    }
    return new ScriptFileInfo(getScriptsVersion(jarFile), crcSum, new Date(lastModificationTime), new Date(), fileCount, classCount);
  }

}
