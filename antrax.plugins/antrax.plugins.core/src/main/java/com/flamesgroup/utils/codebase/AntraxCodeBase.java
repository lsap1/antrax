/*
 * Copyright (C) 2017 Flames Group SIA
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.flamesgroup.utils.codebase;

import java.io.IOException;
import java.io.OutputStream;
import java.io.Serializable;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;
import java.util.stream.Collectors;

public class AntraxCodeBase implements Serializable {

  private static final long serialVersionUID = -8638970765289629163L;

  private static class Code implements Serializable {

    private static final long serialVersionUID = 8898065559095903970L;

    private byte[] buffer = new byte[256];
    private int index = 0;

    public void append(final byte b) {
      growToFitSize();
      buffer[index++] = b;
    }

    private void growToFitSize() {
      if (buffer.length > index + 1) {
        return;
      }
      byte[] nbuf = new byte[(buffer.length * 3) / 2];
      System.arraycopy(buffer, 0, nbuf, 0, buffer.length);
      buffer = nbuf;
    }

  }

  private final Map<String, Code> base = new HashMap<>();

  public OutputStream addClass(final String className) {
    final Code code = new Code();
    base.put(className, code);
    return new OutputStream() {

      @Override
      public void write(final int b) throws IOException {
        code.append((byte) b);
      }
    };
  }

  public Map<String, byte[]> getBase() {
    return base.entrySet().stream().collect(Collectors.toMap(Map.Entry::getKey, e -> Arrays.copyOfRange(e.getValue().buffer, 0, e.getValue().index)));
  }

}
